# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1_courses_course_id_quizzes_id_submission_users_message import Api

from openapi_client.paths import PathValues

path = PathValues.V1_COURSES_COURSE_ID_QUIZZES_ID_SUBMISSION_USERS_MESSAGE