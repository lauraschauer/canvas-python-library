# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1_users_id_merge_into_accounts_destination_account_id_users_destination_user_id import Api

from openapi_client.paths import PathValues

path = PathValues.V1_USERS_ID_MERGE_INTO_ACCOUNTS_DESTINATION_ACCOUNT_ID_USERS_DESTINATION_USER_ID