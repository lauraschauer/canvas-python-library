# coding: utf-8

"""


    Generated by: https://openapi-generator.tech
"""

from dataclasses import dataclass
import typing_extensions
import urllib3
from urllib3._collections import HTTPHeaderDict

from openapi_client import api_client, exceptions
from datetime import date, datetime  # noqa: F401
import decimal  # noqa: F401
import functools  # noqa: F401
import io  # noqa: F401
import re  # noqa: F401
import typing  # noqa: F401
import typing_extensions  # noqa: F401
import uuid  # noqa: F401

import frozendict  # noqa: F401

from openapi_client import schemas  # noqa: F401

# Path params
IdSchema = schemas.StrSchema
RequestRequiredPathParams = typing_extensions.TypedDict(
    'RequestRequiredPathParams',
    {
        'id': typing.Union[IdSchema, str, ],
    }
)
RequestOptionalPathParams = typing_extensions.TypedDict(
    'RequestOptionalPathParams',
    {
    },
    total=False
)


class RequestPathParams(RequestRequiredPathParams, RequestOptionalPathParams):
    pass


request_path_id = api_client.PathParameter(
    name="id",
    style=api_client.ParameterStyle.SIMPLE,
    schema=IdSchema,
    required=True,
)
# body param


class SchemaForRequestBodyApplicationJson(
    schemas.DictSchema
):


    class MetaOapg:
        
        class properties:
            course_account_id = schemas.Int64Schema
            course_allow_student_forum_attachments = schemas.BoolSchema
            course_allow_student_wiki_edits = schemas.BoolSchema
            course_allow_wiki_comments = schemas.BoolSchema
            course_apply_assignment_group_weights = schemas.BoolSchema
            course_blueprint = schemas.BoolSchema
            course_blueprint_restrictions = schemas.AnyTypeSchema
            course_blueprint_restrictions_by_object_type = schemas.AnyTypeSchema
            course_course_code = schemas.StrSchema
            course_course_format = schemas.StrSchema
            
            
            class course_default_view(
                schemas.EnumBase,
                schemas.StrSchema
            ):
                
                @schemas.classproperty
                def FEED(cls):
                    return cls("feed")
                
                @schemas.classproperty
                def WIKI(cls):
                    return cls("wiki")
                
                @schemas.classproperty
                def MODULES(cls):
                    return cls("modules")
                
                @schemas.classproperty
                def SYLLABUS(cls):
                    return cls("syllabus")
                
                @schemas.classproperty
                def ASSIGNMENTS(cls):
                    return cls("assignments")
            course_end_at = schemas.DateTimeSchema
            
            
            class course_event(
                schemas.EnumBase,
                schemas.StrSchema
            ):
                
                @schemas.classproperty
                def CLAIM(cls):
                    return cls("claim")
                
                @schemas.classproperty
                def OFFER(cls):
                    return cls("offer")
                
                @schemas.classproperty
                def CONCLUDE(cls):
                    return cls("conclude")
                
                @schemas.classproperty
                def DELETE(cls):
                    return cls("delete")
                
                @schemas.classproperty
                def UNDELETE(cls):
                    return cls("undelete")
            course_grading_standard_id = schemas.Int64Schema
            course_hide_final_grades = schemas.BoolSchema
            course_image_id = schemas.Int64Schema
            course_image_url = schemas.StrSchema
            course_integration_id = schemas.StrSchema
            course_is_public = schemas.BoolSchema
            course_is_public_to_auth_users = schemas.BoolSchema
            course_license = schemas.StrSchema
            course_name = schemas.StrSchema
            course_open_enrollment = schemas.BoolSchema
            course_public_description = schemas.StrSchema
            course_public_syllabus = schemas.BoolSchema
            course_public_syllabus_to_auth = schemas.BoolSchema
            course_remove_image = schemas.BoolSchema
            course_restrict_enrollments_to_course_dates = schemas.BoolSchema
            course_self_enrollment = schemas.BoolSchema
            course_sis_course_id = schemas.StrSchema
            course_start_at = schemas.DateTimeSchema
            course_storage_quota_mb = schemas.Int64Schema
            course_syllabus_body = schemas.StrSchema
            course_term_id = schemas.Int64Schema
            course_time_zone = schemas.StrSchema
            course_use_blueprint_restrictions_by_object_type = schemas.BoolSchema
            offer = schemas.BoolSchema
            __annotations__ = {
                "course[account_id]": course_account_id,
                "course[allow_student_forum_attachments]": course_allow_student_forum_attachments,
                "course[allow_student_wiki_edits]": course_allow_student_wiki_edits,
                "course[allow_wiki_comments]": course_allow_wiki_comments,
                "course[apply_assignment_group_weights]": course_apply_assignment_group_weights,
                "course[blueprint]": course_blueprint,
                "course[blueprint_restrictions]": course_blueprint_restrictions,
                "course[blueprint_restrictions_by_object_type]": course_blueprint_restrictions_by_object_type,
                "course[course_code]": course_course_code,
                "course[course_format]": course_course_format,
                "course[default_view]": course_default_view,
                "course[end_at]": course_end_at,
                "course[event]": course_event,
                "course[grading_standard_id]": course_grading_standard_id,
                "course[hide_final_grades]": course_hide_final_grades,
                "course[image_id]": course_image_id,
                "course[image_url]": course_image_url,
                "course[integration_id]": course_integration_id,
                "course[is_public]": course_is_public,
                "course[is_public_to_auth_users]": course_is_public_to_auth_users,
                "course[license]": course_license,
                "course[name]": course_name,
                "course[open_enrollment]": course_open_enrollment,
                "course[public_description]": course_public_description,
                "course[public_syllabus]": course_public_syllabus,
                "course[public_syllabus_to_auth]": course_public_syllabus_to_auth,
                "course[remove_image]": course_remove_image,
                "course[restrict_enrollments_to_course_dates]": course_restrict_enrollments_to_course_dates,
                "course[self_enrollment]": course_self_enrollment,
                "course[sis_course_id]": course_sis_course_id,
                "course[start_at]": course_start_at,
                "course[storage_quota_mb]": course_storage_quota_mb,
                "course[syllabus_body]": course_syllabus_body,
                "course[term_id]": course_term_id,
                "course[time_zone]": course_time_zone,
                "course[use_blueprint_restrictions_by_object_type]": course_use_blueprint_restrictions_by_object_type,
                "offer": offer,
            }
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[account_id]"]) -> MetaOapg.properties.course_account_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[allow_student_forum_attachments]"]) -> MetaOapg.properties.course_allow_student_forum_attachments: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[allow_student_wiki_edits]"]) -> MetaOapg.properties.course_allow_student_wiki_edits: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[allow_wiki_comments]"]) -> MetaOapg.properties.course_allow_wiki_comments: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[apply_assignment_group_weights]"]) -> MetaOapg.properties.course_apply_assignment_group_weights: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[blueprint]"]) -> MetaOapg.properties.course_blueprint: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[blueprint_restrictions]"]) -> MetaOapg.properties.course_blueprint_restrictions: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[blueprint_restrictions_by_object_type]"]) -> MetaOapg.properties.course_blueprint_restrictions_by_object_type: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[course_code]"]) -> MetaOapg.properties.course_course_code: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[course_format]"]) -> MetaOapg.properties.course_course_format: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[default_view]"]) -> MetaOapg.properties.course_default_view: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[end_at]"]) -> MetaOapg.properties.course_end_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[event]"]) -> MetaOapg.properties.course_event: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[grading_standard_id]"]) -> MetaOapg.properties.course_grading_standard_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[hide_final_grades]"]) -> MetaOapg.properties.course_hide_final_grades: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[image_id]"]) -> MetaOapg.properties.course_image_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[image_url]"]) -> MetaOapg.properties.course_image_url: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[integration_id]"]) -> MetaOapg.properties.course_integration_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[is_public]"]) -> MetaOapg.properties.course_is_public: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[is_public_to_auth_users]"]) -> MetaOapg.properties.course_is_public_to_auth_users: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[license]"]) -> MetaOapg.properties.course_license: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[name]"]) -> MetaOapg.properties.course_name: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[open_enrollment]"]) -> MetaOapg.properties.course_open_enrollment: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[public_description]"]) -> MetaOapg.properties.course_public_description: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[public_syllabus]"]) -> MetaOapg.properties.course_public_syllabus: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[public_syllabus_to_auth]"]) -> MetaOapg.properties.course_public_syllabus_to_auth: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[remove_image]"]) -> MetaOapg.properties.course_remove_image: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[restrict_enrollments_to_course_dates]"]) -> MetaOapg.properties.course_restrict_enrollments_to_course_dates: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[self_enrollment]"]) -> MetaOapg.properties.course_self_enrollment: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[sis_course_id]"]) -> MetaOapg.properties.course_sis_course_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[start_at]"]) -> MetaOapg.properties.course_start_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[storage_quota_mb]"]) -> MetaOapg.properties.course_storage_quota_mb: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[syllabus_body]"]) -> MetaOapg.properties.course_syllabus_body: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[term_id]"]) -> MetaOapg.properties.course_term_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[time_zone]"]) -> MetaOapg.properties.course_time_zone: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["course[use_blueprint_restrictions_by_object_type]"]) -> MetaOapg.properties.course_use_blueprint_restrictions_by_object_type: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["offer"]) -> MetaOapg.properties.offer: ...
    
    @typing.overload
    def __getitem__(self, name: str) -> schemas.UnsetAnyTypeSchema: ...
    
    def __getitem__(self, name: typing.Union[typing_extensions.Literal["course[account_id]", "course[allow_student_forum_attachments]", "course[allow_student_wiki_edits]", "course[allow_wiki_comments]", "course[apply_assignment_group_weights]", "course[blueprint]", "course[blueprint_restrictions]", "course[blueprint_restrictions_by_object_type]", "course[course_code]", "course[course_format]", "course[default_view]", "course[end_at]", "course[event]", "course[grading_standard_id]", "course[hide_final_grades]", "course[image_id]", "course[image_url]", "course[integration_id]", "course[is_public]", "course[is_public_to_auth_users]", "course[license]", "course[name]", "course[open_enrollment]", "course[public_description]", "course[public_syllabus]", "course[public_syllabus_to_auth]", "course[remove_image]", "course[restrict_enrollments_to_course_dates]", "course[self_enrollment]", "course[sis_course_id]", "course[start_at]", "course[storage_quota_mb]", "course[syllabus_body]", "course[term_id]", "course[time_zone]", "course[use_blueprint_restrictions_by_object_type]", "offer", ], str]):
        # dict_instance[name] accessor
        return super().__getitem__(name)
    
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[account_id]"]) -> typing.Union[MetaOapg.properties.course_account_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[allow_student_forum_attachments]"]) -> typing.Union[MetaOapg.properties.course_allow_student_forum_attachments, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[allow_student_wiki_edits]"]) -> typing.Union[MetaOapg.properties.course_allow_student_wiki_edits, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[allow_wiki_comments]"]) -> typing.Union[MetaOapg.properties.course_allow_wiki_comments, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[apply_assignment_group_weights]"]) -> typing.Union[MetaOapg.properties.course_apply_assignment_group_weights, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[blueprint]"]) -> typing.Union[MetaOapg.properties.course_blueprint, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[blueprint_restrictions]"]) -> typing.Union[MetaOapg.properties.course_blueprint_restrictions, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[blueprint_restrictions_by_object_type]"]) -> typing.Union[MetaOapg.properties.course_blueprint_restrictions_by_object_type, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[course_code]"]) -> typing.Union[MetaOapg.properties.course_course_code, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[course_format]"]) -> typing.Union[MetaOapg.properties.course_course_format, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[default_view]"]) -> typing.Union[MetaOapg.properties.course_default_view, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[end_at]"]) -> typing.Union[MetaOapg.properties.course_end_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[event]"]) -> typing.Union[MetaOapg.properties.course_event, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[grading_standard_id]"]) -> typing.Union[MetaOapg.properties.course_grading_standard_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[hide_final_grades]"]) -> typing.Union[MetaOapg.properties.course_hide_final_grades, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[image_id]"]) -> typing.Union[MetaOapg.properties.course_image_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[image_url]"]) -> typing.Union[MetaOapg.properties.course_image_url, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[integration_id]"]) -> typing.Union[MetaOapg.properties.course_integration_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[is_public]"]) -> typing.Union[MetaOapg.properties.course_is_public, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[is_public_to_auth_users]"]) -> typing.Union[MetaOapg.properties.course_is_public_to_auth_users, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[license]"]) -> typing.Union[MetaOapg.properties.course_license, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[name]"]) -> typing.Union[MetaOapg.properties.course_name, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[open_enrollment]"]) -> typing.Union[MetaOapg.properties.course_open_enrollment, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[public_description]"]) -> typing.Union[MetaOapg.properties.course_public_description, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[public_syllabus]"]) -> typing.Union[MetaOapg.properties.course_public_syllabus, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[public_syllabus_to_auth]"]) -> typing.Union[MetaOapg.properties.course_public_syllabus_to_auth, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[remove_image]"]) -> typing.Union[MetaOapg.properties.course_remove_image, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[restrict_enrollments_to_course_dates]"]) -> typing.Union[MetaOapg.properties.course_restrict_enrollments_to_course_dates, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[self_enrollment]"]) -> typing.Union[MetaOapg.properties.course_self_enrollment, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[sis_course_id]"]) -> typing.Union[MetaOapg.properties.course_sis_course_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[start_at]"]) -> typing.Union[MetaOapg.properties.course_start_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[storage_quota_mb]"]) -> typing.Union[MetaOapg.properties.course_storage_quota_mb, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[syllabus_body]"]) -> typing.Union[MetaOapg.properties.course_syllabus_body, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[term_id]"]) -> typing.Union[MetaOapg.properties.course_term_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[time_zone]"]) -> typing.Union[MetaOapg.properties.course_time_zone, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["course[use_blueprint_restrictions_by_object_type]"]) -> typing.Union[MetaOapg.properties.course_use_blueprint_restrictions_by_object_type, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["offer"]) -> typing.Union[MetaOapg.properties.offer, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: str) -> typing.Union[schemas.UnsetAnyTypeSchema, schemas.Unset]: ...
    
    def get_item_oapg(self, name: typing.Union[typing_extensions.Literal["course[account_id]", "course[allow_student_forum_attachments]", "course[allow_student_wiki_edits]", "course[allow_wiki_comments]", "course[apply_assignment_group_weights]", "course[blueprint]", "course[blueprint_restrictions]", "course[blueprint_restrictions_by_object_type]", "course[course_code]", "course[course_format]", "course[default_view]", "course[end_at]", "course[event]", "course[grading_standard_id]", "course[hide_final_grades]", "course[image_id]", "course[image_url]", "course[integration_id]", "course[is_public]", "course[is_public_to_auth_users]", "course[license]", "course[name]", "course[open_enrollment]", "course[public_description]", "course[public_syllabus]", "course[public_syllabus_to_auth]", "course[remove_image]", "course[restrict_enrollments_to_course_dates]", "course[self_enrollment]", "course[sis_course_id]", "course[start_at]", "course[storage_quota_mb]", "course[syllabus_body]", "course[term_id]", "course[time_zone]", "course[use_blueprint_restrictions_by_object_type]", "offer", ], str]):
        return super().get_item_oapg(name)
    

    def __new__(
        cls,
        *args: typing.Union[dict, frozendict.frozendict, ],
        offer: typing.Union[MetaOapg.properties.offer, bool, schemas.Unset] = schemas.unset,
        _configuration: typing.Optional[schemas.Configuration] = None,
        **kwargs: typing.Union[schemas.AnyTypeSchema, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, None, list, tuple, bytes],
    ) -> 'SchemaForRequestBodyApplicationJson':
        return super().__new__(
            cls,
            *args,
            offer=offer,
            _configuration=_configuration,
            **kwargs,
        )


request_body_any_type = api_client.RequestBody(
    content={
        'application/json': api_client.MediaType(
            schema=SchemaForRequestBodyApplicationJson),
    },
)


@dataclass
class ApiResponseFor200(api_client.ApiResponse):
    response: urllib3.HTTPResponse
    body: typing.Union[
    ]
    headers: schemas.Unset = schemas.unset


_response_for_200 = api_client.OpenApiResponse(
    response_cls=ApiResponseFor200,
)


class BaseApi(api_client.Api):
    @typing.overload
    def _update_course_oapg(
        self,
        content_type: typing_extensions.Literal["application/json"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def _update_course_oapg(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def _update_course_oapg(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def _update_course_oapg(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def _update_course_oapg(
        self,
        content_type: str = 'application/json',
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        """
        Update a course
        :param skip_deserialization: If true then api_response.response will be set but
            api_response.body and api_response.headers will not be deserialized into schema
            class instances
        """
        self._verify_typed_dict_inputs_oapg(RequestPathParams, path_params)
        used_path = path.value

        _path_params = {}
        for parameter in (
            request_path_id,
        ):
            parameter_data = path_params.get(parameter.name, schemas.unset)
            if parameter_data is schemas.unset:
                continue
            serialized_data = parameter.serialize(parameter_data)
            _path_params.update(serialized_data)

        for k, v in _path_params.items():
            used_path = used_path.replace('{%s}' % k, v)

        _headers = HTTPHeaderDict()
        # TODO add cookie handling

        _fields = None
        _body = None
        if body is not schemas.unset:
            serialized_data = request_body_any_type.serialize(body, content_type)
            _headers.add('Content-Type', content_type)
            if 'fields' in serialized_data:
                _fields = serialized_data['fields']
            elif 'body' in serialized_data:
                _body = serialized_data['body']
        response = self.api_client.call_api(
            resource_path=used_path,
            method='put'.upper(),
            headers=_headers,
            fields=_fields,
            body=_body,
            auth_settings=_auth,
            stream=stream,
            timeout=timeout,
        )

        if skip_deserialization:
            api_response = api_client.ApiResponseWithoutDeserialization(response=response)
        else:
            response_for_status = _status_code_to_response.get(str(response.status))
            if response_for_status:
                api_response = response_for_status.deserialize(response, self.api_client.configuration)
            else:
                api_response = api_client.ApiResponseWithoutDeserialization(response=response)

        if not 200 <= response.status <= 299:
            raise exceptions.ApiException(api_response=api_response)

        return api_response


class UpdateCourse(BaseApi):
    # this class is used by api classes that refer to endpoints with operationId fn names

    @typing.overload
    def update_course(
        self,
        content_type: typing_extensions.Literal["application/json"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def update_course(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def update_course(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def update_course(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def update_course(
        self,
        content_type: str = 'application/json',
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        return self._update_course_oapg(
            body=body,
            path_params=path_params,
            content_type=content_type,
            stream=stream,
            timeout=timeout,
            skip_deserialization=skip_deserialization
        )


class ApiForput(BaseApi):
    # this class is used by api classes that refer to endpoints by path and http method names

    @typing.overload
    def put(
        self,
        content_type: typing_extensions.Literal["application/json"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def put(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def put(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def put(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def put(
        self,
        content_type: str = 'application/json',
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        return self._update_course_oapg(
            body=body,
            path_params=path_params,
            content_type=content_type,
            stream=stream,
            timeout=timeout,
            skip_deserialization=skip_deserialization
        )


