# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1_polls_poll_id_poll_sessions_poll_session_id_poll_submissions_id import Api

from openapi_client.paths import PathValues

path = PathValues.V1_POLLS_POLL_ID_POLL_SESSIONS_POLL_SESSION_ID_POLL_SUBMISSIONS_ID