# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1_courses_course_id_users_user_id_last_attended import Api

from openapi_client.paths import PathValues

path = PathValues.V1_COURSES_COURSE_ID_USERS_USER_ID_LAST_ATTENDED