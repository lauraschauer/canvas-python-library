# coding: utf-8

"""


    Generated by: https://openapi-generator.tech
"""

from dataclasses import dataclass
import typing_extensions
import urllib3
from urllib3._collections import HTTPHeaderDict

from openapi_client import api_client, exceptions
from datetime import date, datetime  # noqa: F401
import decimal  # noqa: F401
import functools  # noqa: F401
import io  # noqa: F401
import re  # noqa: F401
import typing  # noqa: F401
import typing_extensions  # noqa: F401
import uuid  # noqa: F401

import frozendict  # noqa: F401

from openapi_client import schemas  # noqa: F401

# Path params
CourseIdSchema = schemas.StrSchema
RequestRequiredPathParams = typing_extensions.TypedDict(
    'RequestRequiredPathParams',
    {
        'course_id': typing.Union[CourseIdSchema, str, ],
    }
)
RequestOptionalPathParams = typing_extensions.TypedDict(
    'RequestOptionalPathParams',
    {
    },
    total=False
)


class RequestPathParams(RequestRequiredPathParams, RequestOptionalPathParams):
    pass


request_path_course_id = api_client.PathParameter(
    name="course_id",
    style=api_client.ParameterStyle.SIMPLE,
    schema=CourseIdSchema,
    required=True,
)
# body param


class SchemaForRequestBodyApplicationJson(
    schemas.DictSchema
):


    class MetaOapg:
        
        class properties:
            allow_student_discussion_editing = schemas.BoolSchema
            allow_student_discussion_topics = schemas.BoolSchema
            allow_student_forum_attachments = schemas.BoolSchema
            allow_student_organized_groups = schemas.BoolSchema
            hide_distribution_graphs = schemas.BoolSchema
            hide_final_grades = schemas.BoolSchema
            home_page_announcement_limit = schemas.Int64Schema
            lock_all_announcements = schemas.BoolSchema
            restrict_student_future_view = schemas.BoolSchema
            restrict_student_past_view = schemas.BoolSchema
            show_announcements_on_home_page = schemas.BoolSchema
            __annotations__ = {
                "allow_student_discussion_editing": allow_student_discussion_editing,
                "allow_student_discussion_topics": allow_student_discussion_topics,
                "allow_student_forum_attachments": allow_student_forum_attachments,
                "allow_student_organized_groups": allow_student_organized_groups,
                "hide_distribution_graphs": hide_distribution_graphs,
                "hide_final_grades": hide_final_grades,
                "home_page_announcement_limit": home_page_announcement_limit,
                "lock_all_announcements": lock_all_announcements,
                "restrict_student_future_view": restrict_student_future_view,
                "restrict_student_past_view": restrict_student_past_view,
                "show_announcements_on_home_page": show_announcements_on_home_page,
            }
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["allow_student_discussion_editing"]) -> MetaOapg.properties.allow_student_discussion_editing: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["allow_student_discussion_topics"]) -> MetaOapg.properties.allow_student_discussion_topics: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["allow_student_forum_attachments"]) -> MetaOapg.properties.allow_student_forum_attachments: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["allow_student_organized_groups"]) -> MetaOapg.properties.allow_student_organized_groups: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["hide_distribution_graphs"]) -> MetaOapg.properties.hide_distribution_graphs: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["hide_final_grades"]) -> MetaOapg.properties.hide_final_grades: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["home_page_announcement_limit"]) -> MetaOapg.properties.home_page_announcement_limit: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["lock_all_announcements"]) -> MetaOapg.properties.lock_all_announcements: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["restrict_student_future_view"]) -> MetaOapg.properties.restrict_student_future_view: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["restrict_student_past_view"]) -> MetaOapg.properties.restrict_student_past_view: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["show_announcements_on_home_page"]) -> MetaOapg.properties.show_announcements_on_home_page: ...
    
    @typing.overload
    def __getitem__(self, name: str) -> schemas.UnsetAnyTypeSchema: ...
    
    def __getitem__(self, name: typing.Union[typing_extensions.Literal["allow_student_discussion_editing", "allow_student_discussion_topics", "allow_student_forum_attachments", "allow_student_organized_groups", "hide_distribution_graphs", "hide_final_grades", "home_page_announcement_limit", "lock_all_announcements", "restrict_student_future_view", "restrict_student_past_view", "show_announcements_on_home_page", ], str]):
        # dict_instance[name] accessor
        return super().__getitem__(name)
    
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["allow_student_discussion_editing"]) -> typing.Union[MetaOapg.properties.allow_student_discussion_editing, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["allow_student_discussion_topics"]) -> typing.Union[MetaOapg.properties.allow_student_discussion_topics, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["allow_student_forum_attachments"]) -> typing.Union[MetaOapg.properties.allow_student_forum_attachments, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["allow_student_organized_groups"]) -> typing.Union[MetaOapg.properties.allow_student_organized_groups, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["hide_distribution_graphs"]) -> typing.Union[MetaOapg.properties.hide_distribution_graphs, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["hide_final_grades"]) -> typing.Union[MetaOapg.properties.hide_final_grades, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["home_page_announcement_limit"]) -> typing.Union[MetaOapg.properties.home_page_announcement_limit, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["lock_all_announcements"]) -> typing.Union[MetaOapg.properties.lock_all_announcements, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["restrict_student_future_view"]) -> typing.Union[MetaOapg.properties.restrict_student_future_view, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["restrict_student_past_view"]) -> typing.Union[MetaOapg.properties.restrict_student_past_view, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["show_announcements_on_home_page"]) -> typing.Union[MetaOapg.properties.show_announcements_on_home_page, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: str) -> typing.Union[schemas.UnsetAnyTypeSchema, schemas.Unset]: ...
    
    def get_item_oapg(self, name: typing.Union[typing_extensions.Literal["allow_student_discussion_editing", "allow_student_discussion_topics", "allow_student_forum_attachments", "allow_student_organized_groups", "hide_distribution_graphs", "hide_final_grades", "home_page_announcement_limit", "lock_all_announcements", "restrict_student_future_view", "restrict_student_past_view", "show_announcements_on_home_page", ], str]):
        return super().get_item_oapg(name)
    

    def __new__(
        cls,
        *args: typing.Union[dict, frozendict.frozendict, ],
        allow_student_discussion_editing: typing.Union[MetaOapg.properties.allow_student_discussion_editing, bool, schemas.Unset] = schemas.unset,
        allow_student_discussion_topics: typing.Union[MetaOapg.properties.allow_student_discussion_topics, bool, schemas.Unset] = schemas.unset,
        allow_student_forum_attachments: typing.Union[MetaOapg.properties.allow_student_forum_attachments, bool, schemas.Unset] = schemas.unset,
        allow_student_organized_groups: typing.Union[MetaOapg.properties.allow_student_organized_groups, bool, schemas.Unset] = schemas.unset,
        hide_distribution_graphs: typing.Union[MetaOapg.properties.hide_distribution_graphs, bool, schemas.Unset] = schemas.unset,
        hide_final_grades: typing.Union[MetaOapg.properties.hide_final_grades, bool, schemas.Unset] = schemas.unset,
        home_page_announcement_limit: typing.Union[MetaOapg.properties.home_page_announcement_limit, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        lock_all_announcements: typing.Union[MetaOapg.properties.lock_all_announcements, bool, schemas.Unset] = schemas.unset,
        restrict_student_future_view: typing.Union[MetaOapg.properties.restrict_student_future_view, bool, schemas.Unset] = schemas.unset,
        restrict_student_past_view: typing.Union[MetaOapg.properties.restrict_student_past_view, bool, schemas.Unset] = schemas.unset,
        show_announcements_on_home_page: typing.Union[MetaOapg.properties.show_announcements_on_home_page, bool, schemas.Unset] = schemas.unset,
        _configuration: typing.Optional[schemas.Configuration] = None,
        **kwargs: typing.Union[schemas.AnyTypeSchema, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, None, list, tuple, bytes],
    ) -> 'SchemaForRequestBodyApplicationJson':
        return super().__new__(
            cls,
            *args,
            allow_student_discussion_editing=allow_student_discussion_editing,
            allow_student_discussion_topics=allow_student_discussion_topics,
            allow_student_forum_attachments=allow_student_forum_attachments,
            allow_student_organized_groups=allow_student_organized_groups,
            hide_distribution_graphs=hide_distribution_graphs,
            hide_final_grades=hide_final_grades,
            home_page_announcement_limit=home_page_announcement_limit,
            lock_all_announcements=lock_all_announcements,
            restrict_student_future_view=restrict_student_future_view,
            restrict_student_past_view=restrict_student_past_view,
            show_announcements_on_home_page=show_announcements_on_home_page,
            _configuration=_configuration,
            **kwargs,
        )


request_body_any_type = api_client.RequestBody(
    content={
        'application/json': api_client.MediaType(
            schema=SchemaForRequestBodyApplicationJson),
    },
)


@dataclass
class ApiResponseFor200(api_client.ApiResponse):
    response: urllib3.HTTPResponse
    body: typing.Union[
    ]
    headers: schemas.Unset = schemas.unset


_response_for_200 = api_client.OpenApiResponse(
    response_cls=ApiResponseFor200,
)


class BaseApi(api_client.Api):
    @typing.overload
    def _update_course_settings_oapg(
        self,
        content_type: typing_extensions.Literal["application/json"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def _update_course_settings_oapg(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def _update_course_settings_oapg(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def _update_course_settings_oapg(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def _update_course_settings_oapg(
        self,
        content_type: str = 'application/json',
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        """
        Update course settings
        :param skip_deserialization: If true then api_response.response will be set but
            api_response.body and api_response.headers will not be deserialized into schema
            class instances
        """
        self._verify_typed_dict_inputs_oapg(RequestPathParams, path_params)
        used_path = path.value

        _path_params = {}
        for parameter in (
            request_path_course_id,
        ):
            parameter_data = path_params.get(parameter.name, schemas.unset)
            if parameter_data is schemas.unset:
                continue
            serialized_data = parameter.serialize(parameter_data)
            _path_params.update(serialized_data)

        for k, v in _path_params.items():
            used_path = used_path.replace('{%s}' % k, v)

        _headers = HTTPHeaderDict()
        # TODO add cookie handling

        _fields = None
        _body = None
        if body is not schemas.unset:
            serialized_data = request_body_any_type.serialize(body, content_type)
            _headers.add('Content-Type', content_type)
            if 'fields' in serialized_data:
                _fields = serialized_data['fields']
            elif 'body' in serialized_data:
                _body = serialized_data['body']
        response = self.api_client.call_api(
            resource_path=used_path,
            method='put'.upper(),
            headers=_headers,
            fields=_fields,
            body=_body,
            auth_settings=_auth,
            stream=stream,
            timeout=timeout,
        )

        if skip_deserialization:
            api_response = api_client.ApiResponseWithoutDeserialization(response=response)
        else:
            response_for_status = _status_code_to_response.get(str(response.status))
            if response_for_status:
                api_response = response_for_status.deserialize(response, self.api_client.configuration)
            else:
                api_response = api_client.ApiResponseWithoutDeserialization(response=response)

        if not 200 <= response.status <= 299:
            raise exceptions.ApiException(api_response=api_response)

        return api_response


class UpdateCourseSettings(BaseApi):
    # this class is used by api classes that refer to endpoints with operationId fn names

    @typing.overload
    def update_course_settings(
        self,
        content_type: typing_extensions.Literal["application/json"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def update_course_settings(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def update_course_settings(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def update_course_settings(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def update_course_settings(
        self,
        content_type: str = 'application/json',
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        return self._update_course_settings_oapg(
            body=body,
            path_params=path_params,
            content_type=content_type,
            stream=stream,
            timeout=timeout,
            skip_deserialization=skip_deserialization
        )


class ApiForput(BaseApi):
    # this class is used by api classes that refer to endpoints by path and http method names

    @typing.overload
    def put(
        self,
        content_type: typing_extensions.Literal["application/json"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def put(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def put(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def put(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def put(
        self,
        content_type: str = 'application/json',
        body: typing.Union[SchemaForRequestBodyApplicationJson, dict, frozendict.frozendict, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        return self._update_course_settings_oapg(
            body=body,
            path_params=path_params,
            content_type=content_type,
            stream=stream,
            timeout=timeout,
            skip_deserialization=skip_deserialization
        )


