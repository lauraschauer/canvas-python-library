# coding: utf-8

"""


    Generated by: https://openapi-generator.tech
"""

from dataclasses import dataclass
import typing_extensions
import urllib3
from urllib3._collections import HTTPHeaderDict

from openapi_client import api_client, exceptions
from datetime import date, datetime  # noqa: F401
import decimal  # noqa: F401
import functools  # noqa: F401
import io  # noqa: F401
import re  # noqa: F401
import typing  # noqa: F401
import typing_extensions  # noqa: F401
import uuid  # noqa: F401

import frozendict  # noqa: F401

from openapi_client import schemas  # noqa: F401

from openapi_client.model.assignment_override import AssignmentOverride
from openapi_client.model.assignment import Assignment

from . import path

# Path params
CourseIdSchema = schemas.StrSchema
RequestRequiredPathParams = typing_extensions.TypedDict(
    'RequestRequiredPathParams',
    {
        'course_id': typing.Union[CourseIdSchema, str, ],
    }
)
RequestOptionalPathParams = typing_extensions.TypedDict(
    'RequestOptionalPathParams',
    {
    },
    total=False
)


class RequestPathParams(RequestRequiredPathParams, RequestOptionalPathParams):
    pass


request_path_course_id = api_client.PathParameter(
    name="course_id",
    style=api_client.ParameterStyle.SIMPLE,
    schema=CourseIdSchema,
    required=True,
)
# body param


class SchemaForRequestBodyApplicationXWwwFormUrlencoded(
    schemas.AnyTypeSchema,
):


    class MetaOapg:
        required = {
            "assignment[name]",
        }
        
        class properties:
            
            
            class assignment_allowed_extensions(
                schemas.ListSchema
            ):
            
            
                class MetaOapg:
                    items = schemas.StrSchema
            
                def __new__(
                    cls,
                    arg: typing.Union[typing.Tuple[typing.Union[MetaOapg.items, str, ]], typing.List[typing.Union[MetaOapg.items, str, ]]],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'assignment_allowed_extensions':
                    return super().__new__(
                        cls,
                        arg,
                        _configuration=_configuration,
                    )
            
                def __getitem__(self, i: int) -> MetaOapg.items:
                    return super().__getitem__(i)
            assignment_assignment_group_id = schemas.Int64Schema
            
            
            class assignment_assignment_overrides(
                schemas.ListSchema
            ):
            
            
                class MetaOapg:
                    
                    @staticmethod
                    def items() -> typing.Type['AssignmentOverride']:
                        return AssignmentOverride
            
                def __new__(
                    cls,
                    arg: typing.Union[typing.Tuple['AssignmentOverride'], typing.List['AssignmentOverride']],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'assignment_assignment_overrides':
                    return super().__new__(
                        cls,
                        arg,
                        _configuration=_configuration,
                    )
            
                def __getitem__(self, i: int) -> 'AssignmentOverride':
                    return super().__getitem__(i)
            assignment_automatic_peer_reviews = schemas.BoolSchema
            assignment_description = schemas.StrSchema
            assignment_due_at = schemas.DateTimeSchema
            assignment_external_tool_tag_attributes = schemas.StrSchema
            assignment_grade_group_students_individually = schemas.Int64Schema
            assignment_grading_standard_id = schemas.Int64Schema
            
            
            class assignment_grading_type(
                schemas.EnumBase,
                schemas.StrSchema
            ):
            
            
                class MetaOapg:
                    enum_value_to_name = {
                        "pass_fail": "PASS_FAIL",
                        "percent": "PERCENT",
                        "letter_grade": "LETTER_GRADE",
                        "gpa_scale": "GPA_SCALE",
                        "points": "POINTS",
                    }
                
                @schemas.classproperty
                def PASS_FAIL(cls):
                    return cls("pass_fail")
                
                @schemas.classproperty
                def PERCENT(cls):
                    return cls("percent")
                
                @schemas.classproperty
                def LETTER_GRADE(cls):
                    return cls("letter_grade")
                
                @schemas.classproperty
                def GPA_SCALE(cls):
                    return cls("gpa_scale")
                
                @schemas.classproperty
                def POINTS(cls):
                    return cls("points")
            assignment_group_category_id = schemas.Int64Schema
            assignment_integration_data = schemas.StrSchema
            assignment_integration_id = schemas.StrSchema
            assignment_lock_at = schemas.DateTimeSchema
            assignment_moderated_grading = schemas.BoolSchema
            assignment_muted = schemas.BoolSchema
            assignment_name = schemas.StrSchema
            assignment_notify_of_update = schemas.BoolSchema
            assignment_omit_from_final_grade = schemas.BoolSchema
            assignment_only_visible_to_overrides = schemas.BoolSchema
            assignment_peer_reviews = schemas.BoolSchema
            assignment_points_possible = schemas.Float32Schema
            assignment_position = schemas.Int64Schema
            assignment_published = schemas.BoolSchema
            assignment_quiz_lti = schemas.BoolSchema
            
            
            class assignment_submission_types(
                schemas.ListSchema
            ):
            
            
                class MetaOapg:
                    items = schemas.StrSchema
            
                def __new__(
                    cls,
                    arg: typing.Union[typing.Tuple[typing.Union[MetaOapg.items, str, ]], typing.List[typing.Union[MetaOapg.items, str, ]]],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'assignment_submission_types':
                    return super().__new__(
                        cls,
                        arg,
                        _configuration=_configuration,
                    )
            
                def __getitem__(self, i: int) -> MetaOapg.items:
                    return super().__getitem__(i)
            assignment_turnitin_enabled = schemas.BoolSchema
            assignment_turnitin_settings = schemas.StrSchema
            assignment_unlock_at = schemas.DateTimeSchema
            assignment_vericite_enabled = schemas.BoolSchema
            __annotations__ = {
                "assignment[allowed_extensions]": assignment_allowed_extensions,
                "assignment[assignment_group_id]": assignment_assignment_group_id,
                "assignment[assignment_overrides]": assignment_assignment_overrides,
                "assignment[automatic_peer_reviews]": assignment_automatic_peer_reviews,
                "assignment[description]": assignment_description,
                "assignment[due_at]": assignment_due_at,
                "assignment[external_tool_tag_attributes]": assignment_external_tool_tag_attributes,
                "assignment[grade_group_students_individually]": assignment_grade_group_students_individually,
                "assignment[grading_standard_id]": assignment_grading_standard_id,
                "assignment[grading_type]": assignment_grading_type,
                "assignment[group_category_id]": assignment_group_category_id,
                "assignment[integration_data]": assignment_integration_data,
                "assignment[integration_id]": assignment_integration_id,
                "assignment[lock_at]": assignment_lock_at,
                "assignment[moderated_grading]": assignment_moderated_grading,
                "assignment[muted]": assignment_muted,
                "assignment[name]": assignment_name,
                "assignment[notify_of_update]": assignment_notify_of_update,
                "assignment[omit_from_final_grade]": assignment_omit_from_final_grade,
                "assignment[only_visible_to_overrides]": assignment_only_visible_to_overrides,
                "assignment[peer_reviews]": assignment_peer_reviews,
                "assignment[points_possible]": assignment_points_possible,
                "assignment[position]": assignment_position,
                "assignment[published]": assignment_published,
                "assignment[quiz_lti]": assignment_quiz_lti,
                "assignment[submission_types]": assignment_submission_types,
                "assignment[turnitin_enabled]": assignment_turnitin_enabled,
                "assignment[turnitin_settings]": assignment_turnitin_settings,
                "assignment[unlock_at]": assignment_unlock_at,
                "assignment[vericite_enabled]": assignment_vericite_enabled,
            }

    
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[allowed_extensions]"]) -> MetaOapg.properties.assignment_allowed_extensions: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[assignment_group_id]"]) -> MetaOapg.properties.assignment_assignment_group_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[assignment_overrides]"]) -> MetaOapg.properties.assignment_assignment_overrides: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[automatic_peer_reviews]"]) -> MetaOapg.properties.assignment_automatic_peer_reviews: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[description]"]) -> MetaOapg.properties.assignment_description: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[due_at]"]) -> MetaOapg.properties.assignment_due_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[external_tool_tag_attributes]"]) -> MetaOapg.properties.assignment_external_tool_tag_attributes: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[grade_group_students_individually]"]) -> MetaOapg.properties.assignment_grade_group_students_individually: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[grading_standard_id]"]) -> MetaOapg.properties.assignment_grading_standard_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[grading_type]"]) -> MetaOapg.properties.assignment_grading_type: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[group_category_id]"]) -> MetaOapg.properties.assignment_group_category_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[integration_data]"]) -> MetaOapg.properties.assignment_integration_data: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[integration_id]"]) -> MetaOapg.properties.assignment_integration_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[lock_at]"]) -> MetaOapg.properties.assignment_lock_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[moderated_grading]"]) -> MetaOapg.properties.assignment_moderated_grading: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[muted]"]) -> MetaOapg.properties.assignment_muted: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[name]"]) -> MetaOapg.properties.assignment_name: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[notify_of_update]"]) -> MetaOapg.properties.assignment_notify_of_update: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[omit_from_final_grade]"]) -> MetaOapg.properties.assignment_omit_from_final_grade: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[only_visible_to_overrides]"]) -> MetaOapg.properties.assignment_only_visible_to_overrides: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[peer_reviews]"]) -> MetaOapg.properties.assignment_peer_reviews: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[points_possible]"]) -> MetaOapg.properties.assignment_points_possible: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[position]"]) -> MetaOapg.properties.assignment_position: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[published]"]) -> MetaOapg.properties.assignment_published: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[quiz_lti]"]) -> MetaOapg.properties.assignment_quiz_lti: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[submission_types]"]) -> MetaOapg.properties.assignment_submission_types: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[turnitin_enabled]"]) -> MetaOapg.properties.assignment_turnitin_enabled: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[turnitin_settings]"]) -> MetaOapg.properties.assignment_turnitin_settings: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[unlock_at]"]) -> MetaOapg.properties.assignment_unlock_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["assignment[vericite_enabled]"]) -> MetaOapg.properties.assignment_vericite_enabled: ...
    
    @typing.overload
    def __getitem__(self, name: str) -> schemas.UnsetAnyTypeSchema: ...
    
    def __getitem__(self, name: typing.Union[typing_extensions.Literal["assignment[allowed_extensions]", "assignment[assignment_group_id]", "assignment[assignment_overrides]", "assignment[automatic_peer_reviews]", "assignment[description]", "assignment[due_at]", "assignment[external_tool_tag_attributes]", "assignment[grade_group_students_individually]", "assignment[grading_standard_id]", "assignment[grading_type]", "assignment[group_category_id]", "assignment[integration_data]", "assignment[integration_id]", "assignment[lock_at]", "assignment[moderated_grading]", "assignment[muted]", "assignment[name]", "assignment[notify_of_update]", "assignment[omit_from_final_grade]", "assignment[only_visible_to_overrides]", "assignment[peer_reviews]", "assignment[points_possible]", "assignment[position]", "assignment[published]", "assignment[quiz_lti]", "assignment[submission_types]", "assignment[turnitin_enabled]", "assignment[turnitin_settings]", "assignment[unlock_at]", "assignment[vericite_enabled]", ], str]):
        # dict_instance[name] accessor
        return super().__getitem__(name)
    
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[allowed_extensions]"]) -> typing.Union[MetaOapg.properties.assignment_allowed_extensions, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[assignment_group_id]"]) -> typing.Union[MetaOapg.properties.assignment_assignment_group_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[assignment_overrides]"]) -> typing.Union[MetaOapg.properties.assignment_assignment_overrides, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[automatic_peer_reviews]"]) -> typing.Union[MetaOapg.properties.assignment_automatic_peer_reviews, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[description]"]) -> typing.Union[MetaOapg.properties.assignment_description, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[due_at]"]) -> typing.Union[MetaOapg.properties.assignment_due_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[external_tool_tag_attributes]"]) -> typing.Union[MetaOapg.properties.assignment_external_tool_tag_attributes, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[grade_group_students_individually]"]) -> typing.Union[MetaOapg.properties.assignment_grade_group_students_individually, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[grading_standard_id]"]) -> typing.Union[MetaOapg.properties.assignment_grading_standard_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[grading_type]"]) -> typing.Union[MetaOapg.properties.assignment_grading_type, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[group_category_id]"]) -> typing.Union[MetaOapg.properties.assignment_group_category_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[integration_data]"]) -> typing.Union[MetaOapg.properties.assignment_integration_data, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[integration_id]"]) -> typing.Union[MetaOapg.properties.assignment_integration_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[lock_at]"]) -> typing.Union[MetaOapg.properties.assignment_lock_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[moderated_grading]"]) -> typing.Union[MetaOapg.properties.assignment_moderated_grading, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[muted]"]) -> typing.Union[MetaOapg.properties.assignment_muted, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[name]"]) -> MetaOapg.properties.assignment_name: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[notify_of_update]"]) -> typing.Union[MetaOapg.properties.assignment_notify_of_update, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[omit_from_final_grade]"]) -> typing.Union[MetaOapg.properties.assignment_omit_from_final_grade, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[only_visible_to_overrides]"]) -> typing.Union[MetaOapg.properties.assignment_only_visible_to_overrides, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[peer_reviews]"]) -> typing.Union[MetaOapg.properties.assignment_peer_reviews, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[points_possible]"]) -> typing.Union[MetaOapg.properties.assignment_points_possible, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[position]"]) -> typing.Union[MetaOapg.properties.assignment_position, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[published]"]) -> typing.Union[MetaOapg.properties.assignment_published, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[quiz_lti]"]) -> typing.Union[MetaOapg.properties.assignment_quiz_lti, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[submission_types]"]) -> typing.Union[MetaOapg.properties.assignment_submission_types, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[turnitin_enabled]"]) -> typing.Union[MetaOapg.properties.assignment_turnitin_enabled, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[turnitin_settings]"]) -> typing.Union[MetaOapg.properties.assignment_turnitin_settings, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[unlock_at]"]) -> typing.Union[MetaOapg.properties.assignment_unlock_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["assignment[vericite_enabled]"]) -> typing.Union[MetaOapg.properties.assignment_vericite_enabled, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: str) -> typing.Union[schemas.UnsetAnyTypeSchema, schemas.Unset]: ...
    
    def get_item_oapg(self, name: typing.Union[typing_extensions.Literal["assignment[allowed_extensions]", "assignment[assignment_group_id]", "assignment[assignment_overrides]", "assignment[automatic_peer_reviews]", "assignment[description]", "assignment[due_at]", "assignment[external_tool_tag_attributes]", "assignment[grade_group_students_individually]", "assignment[grading_standard_id]", "assignment[grading_type]", "assignment[group_category_id]", "assignment[integration_data]", "assignment[integration_id]", "assignment[lock_at]", "assignment[moderated_grading]", "assignment[muted]", "assignment[name]", "assignment[notify_of_update]", "assignment[omit_from_final_grade]", "assignment[only_visible_to_overrides]", "assignment[peer_reviews]", "assignment[points_possible]", "assignment[position]", "assignment[published]", "assignment[quiz_lti]", "assignment[submission_types]", "assignment[turnitin_enabled]", "assignment[turnitin_settings]", "assignment[unlock_at]", "assignment[vericite_enabled]", ], str]):
        return super().get_item_oapg(name)
    

    def __new__(
        cls,
        *args: typing.Union[dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, ],
        _configuration: typing.Optional[schemas.Configuration] = None,
        **kwargs: typing.Union[schemas.AnyTypeSchema, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, None, list, tuple, bytes],
    ) -> 'SchemaForRequestBodyApplicationXWwwFormUrlencoded':
        return super().__new__(
            cls,
            *args,
            _configuration=_configuration,
            **kwargs,
        )


request_body_body = api_client.RequestBody(
    content={
        'application/x-www-form-urlencoded': api_client.MediaType(
            schema=SchemaForRequestBodyApplicationXWwwFormUrlencoded),
    },
)
_auth = [
    'bearerAuth',
]
SchemaFor200ResponseBodyApplicationJson = Assignment


@dataclass
class ApiResponseFor200(api_client.ApiResponse):
    response: urllib3.HTTPResponse
    body: typing.Union[
        SchemaFor200ResponseBodyApplicationJson,
    ]
    headers: schemas.Unset = schemas.unset


_response_for_200 = api_client.OpenApiResponse(
    response_cls=ApiResponseFor200,
    content={
        'application/json': api_client.MediaType(
            schema=SchemaFor200ResponseBodyApplicationJson),
    },
)
_status_code_to_response = {
    '200': _response_for_200,
}
_all_accept_content_types = (
    'application/json',
)


class BaseApi(api_client.Api):
    @typing.overload
    def _create_assignment_oapg(
        self,
        content_type: typing_extensions.Literal["application/x-www-form-urlencoded"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def _create_assignment_oapg(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def _create_assignment_oapg(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def _create_assignment_oapg(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def _create_assignment_oapg(
        self,
        content_type: str = 'application/x-www-form-urlencoded',
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        """
        Create an assignment
        :param skip_deserialization: If true then api_response.response will be set but
            api_response.body and api_response.headers will not be deserialized into schema
            class instances
        """
        self._verify_typed_dict_inputs_oapg(RequestPathParams, path_params)
        used_path = path.value

        _path_params = {}
        for parameter in (
            request_path_course_id,
        ):
            parameter_data = path_params.get(parameter.name, schemas.unset)
            if parameter_data is schemas.unset:
                continue
            serialized_data = parameter.serialize(parameter_data)
            _path_params.update(serialized_data)

        for k, v in _path_params.items():
            used_path = used_path.replace('{%s}' % k, v)

        _headers = HTTPHeaderDict()
        # TODO add cookie handling
        if accept_content_types:
            for accept_content_type in accept_content_types:
                _headers.add('Accept', accept_content_type)

        _fields = None
        _body = None
        if body is not schemas.unset:
            serialized_data = request_body_body.serialize(body, content_type)
            _headers.add('Content-Type', content_type)
            if 'fields' in serialized_data:
                _fields = serialized_data['fields']
            elif 'body' in serialized_data:
                _body = serialized_data['body']
        response = self.api_client.call_api(
            resource_path=used_path,
            method='post'.upper(),
            headers=_headers,
            fields=_fields,
            body=_body,
            auth_settings=_auth,
            stream=stream,
            timeout=timeout,
        )

        if skip_deserialization:
            api_response = api_client.ApiResponseWithoutDeserialization(response=response)
        else:
            response_for_status = _status_code_to_response.get(str(response.status))
            if response_for_status:
                api_response = response_for_status.deserialize(response, self.api_client.configuration)
            else:
                api_response = api_client.ApiResponseWithoutDeserialization(response=response)

        if not 200 <= response.status <= 299:
            raise exceptions.ApiException(api_response=api_response)

        return api_response


class CreateAssignment(BaseApi):
    # this class is used by api classes that refer to endpoints with operationId fn names

    @typing.overload
    def create_assignment(
        self,
        content_type: typing_extensions.Literal["application/x-www-form-urlencoded"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def create_assignment(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def create_assignment(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def create_assignment(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def create_assignment(
        self,
        content_type: str = 'application/x-www-form-urlencoded',
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        return self._create_assignment_oapg(
            body=body,
            path_params=path_params,
            content_type=content_type,
            accept_content_types=accept_content_types,
            stream=stream,
            timeout=timeout,
            skip_deserialization=skip_deserialization
        )


class ApiForpost(BaseApi):
    # this class is used by api classes that refer to endpoints by path and http method names

    @typing.overload
    def post(
        self,
        content_type: typing_extensions.Literal["application/x-www-form-urlencoded"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def post(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def post(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def post(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def post(
        self,
        content_type: str = 'application/x-www-form-urlencoded',
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        return self._create_assignment_oapg(
            body=body,
            path_params=path_params,
            content_type=content_type,
            accept_content_types=accept_content_types,
            stream=stream,
            timeout=timeout,
            skip_deserialization=skip_deserialization
        )


