# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1_accounts_account_id_outcome_groups_id_outcomes_outcome_id import Api

from openapi_client.paths import PathValues

path = PathValues.V1_ACCOUNTS_ACCOUNT_ID_OUTCOME_GROUPS_ID_OUTCOMES_OUTCOME_ID