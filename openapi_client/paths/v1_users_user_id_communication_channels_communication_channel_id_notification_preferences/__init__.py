# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1_users_user_id_communication_channels_communication_channel_id_notification_preferences import Api

from openapi_client.paths import PathValues

path = PathValues.V1_USERS_USER_ID_COMMUNICATION_CHANNELS_COMMUNICATION_CHANNEL_ID_NOTIFICATION_PREFERENCES