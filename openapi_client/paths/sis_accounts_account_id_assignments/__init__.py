# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.sis_accounts_account_id_assignments import Api

from openapi_client.paths import PathValues

path = PathValues.SIS_ACCOUNTS_ACCOUNT_ID_ASSIGNMENTS