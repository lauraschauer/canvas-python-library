# coding: utf-8

"""


    Generated by: https://openapi-generator.tech
"""

from dataclasses import dataclass
import typing_extensions
import urllib3
from urllib3._collections import HTTPHeaderDict

from openapi_client import api_client, exceptions
from datetime import date, datetime  # noqa: F401
import decimal  # noqa: F401
import functools  # noqa: F401
import io  # noqa: F401
import re  # noqa: F401
import typing  # noqa: F401
import typing_extensions  # noqa: F401
import uuid  # noqa: F401

import frozendict  # noqa: F401

from openapi_client import schemas  # noqa: F401

from openapi_client.model.quiz import Quiz

from . import path

# Path params
CourseIdSchema = schemas.StrSchema
RequestRequiredPathParams = typing_extensions.TypedDict(
    'RequestRequiredPathParams',
    {
        'course_id': typing.Union[CourseIdSchema, str, ],
    }
)
RequestOptionalPathParams = typing_extensions.TypedDict(
    'RequestOptionalPathParams',
    {
    },
    total=False
)


class RequestPathParams(RequestRequiredPathParams, RequestOptionalPathParams):
    pass


request_path_course_id = api_client.PathParameter(
    name="course_id",
    style=api_client.ParameterStyle.SIMPLE,
    schema=CourseIdSchema,
    required=True,
)
# body param


class SchemaForRequestBodyApplicationXWwwFormUrlencoded(
    schemas.AnyTypeSchema,
):


    class MetaOapg:
        required = {
            "quiz[title]",
        }
        
        class properties:
            quiz_access_code = schemas.StrSchema
            quiz_allowed_attempts = schemas.Int64Schema
            quiz_assignment_group_id = schemas.Int64Schema
            quiz_cant_go_back = schemas.BoolSchema
            quiz_description = schemas.StrSchema
            quiz_due_at = schemas.DateTimeSchema
            quiz_hide_correct_answers_at = schemas.DateTimeSchema
            
            
            class quiz_hide_results(
                schemas.EnumBase,
                schemas.StrSchema
            ):
            
            
                class MetaOapg:
                    enum_value_to_name = {
                        "always": "ALWAYS",
                        "until_after_last_attempt": "UNTIL_AFTER_LAST_ATTEMPT",
                    }
                
                @schemas.classproperty
                def ALWAYS(cls):
                    return cls("always")
                
                @schemas.classproperty
                def UNTIL_AFTER_LAST_ATTEMPT(cls):
                    return cls("until_after_last_attempt")
            quiz_ip_filter = schemas.StrSchema
            quiz_lock_at = schemas.DateTimeSchema
            quiz_one_question_at_a_time = schemas.BoolSchema
            quiz_one_time_results = schemas.BoolSchema
            quiz_only_visible_to_overrides = schemas.BoolSchema
            quiz_published = schemas.BoolSchema
            
            
            class quiz_quiz_type(
                schemas.EnumBase,
                schemas.StrSchema
            ):
            
            
                class MetaOapg:
                    enum_value_to_name = {
                        "practice_quiz": "PRACTICE_QUIZ",
                        "assignment": "ASSIGNMENT",
                        "graded_survey": "GRADED_SURVEY",
                        "survey": "SURVEY",
                    }
                
                @schemas.classproperty
                def PRACTICE_QUIZ(cls):
                    return cls("practice_quiz")
                
                @schemas.classproperty
                def ASSIGNMENT(cls):
                    return cls("assignment")
                
                @schemas.classproperty
                def GRADED_SURVEY(cls):
                    return cls("graded_survey")
                
                @schemas.classproperty
                def SURVEY(cls):
                    return cls("survey")
            
            
            class quiz_scoring_policy(
                schemas.EnumBase,
                schemas.StrSchema
            ):
            
            
                class MetaOapg:
                    enum_value_to_name = {
                        "keep_highest": "HIGHEST",
                        "keep_latest": "LATEST",
                    }
                
                @schemas.classproperty
                def HIGHEST(cls):
                    return cls("keep_highest")
                
                @schemas.classproperty
                def LATEST(cls):
                    return cls("keep_latest")
            quiz_show_correct_answers = schemas.BoolSchema
            quiz_show_correct_answers_at = schemas.DateTimeSchema
            quiz_show_correct_answers_last_attempt = schemas.BoolSchema
            quiz_shuffle_answers = schemas.BoolSchema
            quiz_time_limit = schemas.Int64Schema
            quiz_title = schemas.StrSchema
            quiz_unlock_at = schemas.DateTimeSchema
            __annotations__ = {
                "quiz[access_code]": quiz_access_code,
                "quiz[allowed_attempts]": quiz_allowed_attempts,
                "quiz[assignment_group_id]": quiz_assignment_group_id,
                "quiz[cant_go_back]": quiz_cant_go_back,
                "quiz[description]": quiz_description,
                "quiz[due_at]": quiz_due_at,
                "quiz[hide_correct_answers_at]": quiz_hide_correct_answers_at,
                "quiz[hide_results]": quiz_hide_results,
                "quiz[ip_filter]": quiz_ip_filter,
                "quiz[lock_at]": quiz_lock_at,
                "quiz[one_question_at_a_time]": quiz_one_question_at_a_time,
                "quiz[one_time_results]": quiz_one_time_results,
                "quiz[only_visible_to_overrides]": quiz_only_visible_to_overrides,
                "quiz[published]": quiz_published,
                "quiz[quiz_type]": quiz_quiz_type,
                "quiz[scoring_policy]": quiz_scoring_policy,
                "quiz[show_correct_answers]": quiz_show_correct_answers,
                "quiz[show_correct_answers_at]": quiz_show_correct_answers_at,
                "quiz[show_correct_answers_last_attempt]": quiz_show_correct_answers_last_attempt,
                "quiz[shuffle_answers]": quiz_shuffle_answers,
                "quiz[time_limit]": quiz_time_limit,
                "quiz[title]": quiz_title,
                "quiz[unlock_at]": quiz_unlock_at,
            }

    
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[access_code]"]) -> MetaOapg.properties.quiz_access_code: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[allowed_attempts]"]) -> MetaOapg.properties.quiz_allowed_attempts: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[assignment_group_id]"]) -> MetaOapg.properties.quiz_assignment_group_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[cant_go_back]"]) -> MetaOapg.properties.quiz_cant_go_back: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[description]"]) -> MetaOapg.properties.quiz_description: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[due_at]"]) -> MetaOapg.properties.quiz_due_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[hide_correct_answers_at]"]) -> MetaOapg.properties.quiz_hide_correct_answers_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[hide_results]"]) -> MetaOapg.properties.quiz_hide_results: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[ip_filter]"]) -> MetaOapg.properties.quiz_ip_filter: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[lock_at]"]) -> MetaOapg.properties.quiz_lock_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[one_question_at_a_time]"]) -> MetaOapg.properties.quiz_one_question_at_a_time: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[one_time_results]"]) -> MetaOapg.properties.quiz_one_time_results: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[only_visible_to_overrides]"]) -> MetaOapg.properties.quiz_only_visible_to_overrides: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[published]"]) -> MetaOapg.properties.quiz_published: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[quiz_type]"]) -> MetaOapg.properties.quiz_quiz_type: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[scoring_policy]"]) -> MetaOapg.properties.quiz_scoring_policy: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[show_correct_answers]"]) -> MetaOapg.properties.quiz_show_correct_answers: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[show_correct_answers_at]"]) -> MetaOapg.properties.quiz_show_correct_answers_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[show_correct_answers_last_attempt]"]) -> MetaOapg.properties.quiz_show_correct_answers_last_attempt: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[shuffle_answers]"]) -> MetaOapg.properties.quiz_shuffle_answers: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[time_limit]"]) -> MetaOapg.properties.quiz_time_limit: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[title]"]) -> MetaOapg.properties.quiz_title: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz[unlock_at]"]) -> MetaOapg.properties.quiz_unlock_at: ...
    
    @typing.overload
    def __getitem__(self, name: str) -> schemas.UnsetAnyTypeSchema: ...
    
    def __getitem__(self, name: typing.Union[typing_extensions.Literal["quiz[access_code]", "quiz[allowed_attempts]", "quiz[assignment_group_id]", "quiz[cant_go_back]", "quiz[description]", "quiz[due_at]", "quiz[hide_correct_answers_at]", "quiz[hide_results]", "quiz[ip_filter]", "quiz[lock_at]", "quiz[one_question_at_a_time]", "quiz[one_time_results]", "quiz[only_visible_to_overrides]", "quiz[published]", "quiz[quiz_type]", "quiz[scoring_policy]", "quiz[show_correct_answers]", "quiz[show_correct_answers_at]", "quiz[show_correct_answers_last_attempt]", "quiz[shuffle_answers]", "quiz[time_limit]", "quiz[title]", "quiz[unlock_at]", ], str]):
        # dict_instance[name] accessor
        return super().__getitem__(name)
    
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[access_code]"]) -> typing.Union[MetaOapg.properties.quiz_access_code, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[allowed_attempts]"]) -> typing.Union[MetaOapg.properties.quiz_allowed_attempts, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[assignment_group_id]"]) -> typing.Union[MetaOapg.properties.quiz_assignment_group_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[cant_go_back]"]) -> typing.Union[MetaOapg.properties.quiz_cant_go_back, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[description]"]) -> typing.Union[MetaOapg.properties.quiz_description, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[due_at]"]) -> typing.Union[MetaOapg.properties.quiz_due_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[hide_correct_answers_at]"]) -> typing.Union[MetaOapg.properties.quiz_hide_correct_answers_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[hide_results]"]) -> typing.Union[MetaOapg.properties.quiz_hide_results, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[ip_filter]"]) -> typing.Union[MetaOapg.properties.quiz_ip_filter, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[lock_at]"]) -> typing.Union[MetaOapg.properties.quiz_lock_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[one_question_at_a_time]"]) -> typing.Union[MetaOapg.properties.quiz_one_question_at_a_time, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[one_time_results]"]) -> typing.Union[MetaOapg.properties.quiz_one_time_results, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[only_visible_to_overrides]"]) -> typing.Union[MetaOapg.properties.quiz_only_visible_to_overrides, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[published]"]) -> typing.Union[MetaOapg.properties.quiz_published, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[quiz_type]"]) -> typing.Union[MetaOapg.properties.quiz_quiz_type, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[scoring_policy]"]) -> typing.Union[MetaOapg.properties.quiz_scoring_policy, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[show_correct_answers]"]) -> typing.Union[MetaOapg.properties.quiz_show_correct_answers, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[show_correct_answers_at]"]) -> typing.Union[MetaOapg.properties.quiz_show_correct_answers_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[show_correct_answers_last_attempt]"]) -> typing.Union[MetaOapg.properties.quiz_show_correct_answers_last_attempt, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[shuffle_answers]"]) -> typing.Union[MetaOapg.properties.quiz_shuffle_answers, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[time_limit]"]) -> typing.Union[MetaOapg.properties.quiz_time_limit, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[title]"]) -> MetaOapg.properties.quiz_title: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz[unlock_at]"]) -> typing.Union[MetaOapg.properties.quiz_unlock_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: str) -> typing.Union[schemas.UnsetAnyTypeSchema, schemas.Unset]: ...
    
    def get_item_oapg(self, name: typing.Union[typing_extensions.Literal["quiz[access_code]", "quiz[allowed_attempts]", "quiz[assignment_group_id]", "quiz[cant_go_back]", "quiz[description]", "quiz[due_at]", "quiz[hide_correct_answers_at]", "quiz[hide_results]", "quiz[ip_filter]", "quiz[lock_at]", "quiz[one_question_at_a_time]", "quiz[one_time_results]", "quiz[only_visible_to_overrides]", "quiz[published]", "quiz[quiz_type]", "quiz[scoring_policy]", "quiz[show_correct_answers]", "quiz[show_correct_answers_at]", "quiz[show_correct_answers_last_attempt]", "quiz[shuffle_answers]", "quiz[time_limit]", "quiz[title]", "quiz[unlock_at]", ], str]):
        return super().get_item_oapg(name)
    

    def __new__(
        cls,
        *args: typing.Union[dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, ],
        _configuration: typing.Optional[schemas.Configuration] = None,
        **kwargs: typing.Union[schemas.AnyTypeSchema, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, None, list, tuple, bytes],
    ) -> 'SchemaForRequestBodyApplicationXWwwFormUrlencoded':
        return super().__new__(
            cls,
            *args,
            _configuration=_configuration,
            **kwargs,
        )


request_body_body = api_client.RequestBody(
    content={
        'application/x-www-form-urlencoded': api_client.MediaType(
            schema=SchemaForRequestBodyApplicationXWwwFormUrlencoded),
    },
)
_auth = [
    'bearerAuth',
]
SchemaFor200ResponseBodyApplicationJson = Quiz


@dataclass
class ApiResponseFor200(api_client.ApiResponse):
    response: urllib3.HTTPResponse
    body: typing.Union[
        SchemaFor200ResponseBodyApplicationJson,
    ]
    headers: schemas.Unset = schemas.unset


_response_for_200 = api_client.OpenApiResponse(
    response_cls=ApiResponseFor200,
    content={
        'application/json': api_client.MediaType(
            schema=SchemaFor200ResponseBodyApplicationJson),
    },
)
_status_code_to_response = {
    '200': _response_for_200,
}
_all_accept_content_types = (
    'application/json',
)


class BaseApi(api_client.Api):
    @typing.overload
    def _create_quiz_oapg(
        self,
        content_type: typing_extensions.Literal["application/x-www-form-urlencoded"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def _create_quiz_oapg(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def _create_quiz_oapg(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def _create_quiz_oapg(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def _create_quiz_oapg(
        self,
        content_type: str = 'application/x-www-form-urlencoded',
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        """
        Create a quiz
        :param skip_deserialization: If true then api_response.response will be set but
            api_response.body and api_response.headers will not be deserialized into schema
            class instances
        """
        self._verify_typed_dict_inputs_oapg(RequestPathParams, path_params)
        used_path = path.value

        _path_params = {}
        for parameter in (
            request_path_course_id,
        ):
            parameter_data = path_params.get(parameter.name, schemas.unset)
            if parameter_data is schemas.unset:
                continue
            serialized_data = parameter.serialize(parameter_data)
            _path_params.update(serialized_data)

        for k, v in _path_params.items():
            used_path = used_path.replace('{%s}' % k, v)

        _headers = HTTPHeaderDict()
        # TODO add cookie handling
        if accept_content_types:
            for accept_content_type in accept_content_types:
                _headers.add('Accept', accept_content_type)

        _fields = None
        _body = None
        if body is not schemas.unset:
            serialized_data = request_body_body.serialize(body, content_type)
            _headers.add('Content-Type', content_type)
            if 'fields' in serialized_data:
                _fields = serialized_data['fields']
            elif 'body' in serialized_data:
                _body = serialized_data['body']
        response = self.api_client.call_api(
            resource_path=used_path,
            method='post'.upper(),
            headers=_headers,
            fields=_fields,
            body=_body,
            auth_settings=_auth,
            stream=stream,
            timeout=timeout,
        )

        if skip_deserialization:
            api_response = api_client.ApiResponseWithoutDeserialization(response=response)
        else:
            response_for_status = _status_code_to_response.get(str(response.status))
            if response_for_status:
                api_response = response_for_status.deserialize(response, self.api_client.configuration)
            else:
                api_response = api_client.ApiResponseWithoutDeserialization(response=response)

        if not 200 <= response.status <= 299:
            raise exceptions.ApiException(api_response=api_response)

        return api_response


class CreateQuiz(BaseApi):
    # this class is used by api classes that refer to endpoints with operationId fn names

    @typing.overload
    def create_quiz(
        self,
        content_type: typing_extensions.Literal["application/x-www-form-urlencoded"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def create_quiz(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def create_quiz(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def create_quiz(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def create_quiz(
        self,
        content_type: str = 'application/x-www-form-urlencoded',
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        return self._create_quiz_oapg(
            body=body,
            path_params=path_params,
            content_type=content_type,
            accept_content_types=accept_content_types,
            stream=stream,
            timeout=timeout,
            skip_deserialization=skip_deserialization
        )


class ApiForpost(BaseApi):
    # this class is used by api classes that refer to endpoints by path and http method names

    @typing.overload
    def post(
        self,
        content_type: typing_extensions.Literal["application/x-www-form-urlencoded"] = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...

    @typing.overload
    def post(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: typing_extensions.Literal[False] = ...,
    ) -> typing.Union[
        ApiResponseFor200,
    ]: ...


    @typing.overload
    def post(
        self,
        skip_deserialization: typing_extensions.Literal[True],
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
    ) -> api_client.ApiResponseWithoutDeserialization: ...

    @typing.overload
    def post(
        self,
        content_type: str = ...,
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = ...,
    ) -> typing.Union[
        ApiResponseFor200,
        api_client.ApiResponseWithoutDeserialization,
    ]: ...

    def post(
        self,
        content_type: str = 'application/x-www-form-urlencoded',
        body: typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader, schemas.Unset] = schemas.unset,
        path_params: RequestPathParams = frozendict.frozendict(),
        accept_content_types: typing.Tuple[str] = _all_accept_content_types,
        stream: bool = False,
        timeout: typing.Optional[typing.Union[int, typing.Tuple]] = None,
        skip_deserialization: bool = False,
    ):
        return self._create_quiz_oapg(
            body=body,
            path_params=path_params,
            content_type=content_type,
            accept_content_types=accept_content_types,
            stream=stream,
            timeout=timeout,
            skip_deserialization=skip_deserialization
        )


