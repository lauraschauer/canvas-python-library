# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1_folders_dest_folder_id_copy_folder import Api

from openapi_client.paths import PathValues

path = PathValues.V1_FOLDERS_DEST_FOLDER_ID_COPY_FOLDER