# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1_sections_course_section_id_assignments_assignment_id_override import Api

from openapi_client.paths import PathValues

path = PathValues.V1_SECTIONS_COURSE_SECTION_ID_ASSIGNMENTS_ASSIGNMENT_ID_OVERRIDE