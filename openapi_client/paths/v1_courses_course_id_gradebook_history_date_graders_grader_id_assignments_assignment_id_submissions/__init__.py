# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1_courses_course_id_gradebook_history_date_graders_grader_id_assignments_assignment_id_submissions import Api

from openapi_client.paths import PathValues

path = PathValues.V1_COURSES_COURSE_ID_GRADEBOOK_HISTORY_DATE_GRADERS_GRADER_ID_ASSIGNMENTS_ASSIGNMENT_ID_SUBMISSIONS