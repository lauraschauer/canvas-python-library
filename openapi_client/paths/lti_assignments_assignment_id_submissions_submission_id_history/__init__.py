# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.lti_assignments_assignment_id_submissions_submission_id_history import Api

from openapi_client.paths import PathValues

path = PathValues.LTI_ASSIGNMENTS_ASSIGNMENT_ID_SUBMISSIONS_SUBMISSION_ID_HISTORY