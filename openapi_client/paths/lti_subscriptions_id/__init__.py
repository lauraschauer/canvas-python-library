# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.lti_subscriptions_id import Api

from openapi_client.paths import PathValues

path = PathValues.LTI_SUBSCRIPTIONS_ID