# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1_groups_group_id_collaborations import Api

from openapi_client.paths import PathValues

path = PathValues.V1_GROUPS_GROUP_ID_COLLABORATIONS