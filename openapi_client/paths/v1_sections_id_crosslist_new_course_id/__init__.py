# do not import all endpoints into this module because that uses a lot of memory and stack frames
# if you need the ability to import all endpoints from this module, import them with
# from openapi_client.paths.v1_sections_id_crosslist_new_course_id import Api

from openapi_client.paths import PathValues

path = PathValues.V1_SECTIONS_ID_CROSSLIST_NEW_COURSE_ID