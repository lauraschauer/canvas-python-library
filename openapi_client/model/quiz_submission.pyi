# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from datetime import date, datetime  # noqa: F401
import decimal  # noqa: F401
import functools  # noqa: F401
import io  # noqa: F401
import re  # noqa: F401
import typing  # noqa: F401
import typing_extensions  # noqa: F401
import uuid  # noqa: F401

import frozendict  # noqa: F401

from openapi_client import schemas  # noqa: F401


class QuizSubmission(
    schemas.DictSchema
):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """


    class MetaOapg:
        required = {
            "quiz_id",
            "id",
        }
        
        class properties:
            id = schemas.Int64Schema
            quiz_id = schemas.Int64Schema
            
            
            class attempt(
                schemas.Int64Base,
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                class MetaOapg:
                    format = 'int64'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'attempt':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class end_at(
                schemas.DateTimeBase,
                schemas.StrBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneStrMixin
            ):
            
            
                class MetaOapg:
                    format = 'date-time'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, str, datetime, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'end_at':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class extra_attempts(
                schemas.Int64Base,
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                class MetaOapg:
                    format = 'int64'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'extra_attempts':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class extra_time(
                schemas.Int64Base,
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                class MetaOapg:
                    format = 'int64'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'extra_time':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class finished_at(
                schemas.DateTimeBase,
                schemas.StrBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneStrMixin
            ):
            
            
                class MetaOapg:
                    format = 'date-time'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, str, datetime, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'finished_at':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class fudge_points(
                schemas.Int64Base,
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                class MetaOapg:
                    format = 'int64'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'fudge_points':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class has_seen_results(
                schemas.BoolBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneBoolMixin
            ):
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, bool, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'has_seen_results':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class kept_score(
                schemas.Int64Base,
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                class MetaOapg:
                    format = 'int64'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'kept_score':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class manually_unlocked(
                schemas.BoolBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneBoolMixin
            ):
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, bool, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'manually_unlocked':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class overdue_and_needs_submission(
                schemas.BoolBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneBoolMixin
            ):
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, bool, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'overdue_and_needs_submission':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class score(
                schemas.Int64Base,
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                class MetaOapg:
                    format = 'int64'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'score':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class score_before_regrade(
                schemas.Int64Base,
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                class MetaOapg:
                    format = 'int64'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'score_before_regrade':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class started_at(
                schemas.DateTimeBase,
                schemas.StrBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneStrMixin
            ):
            
            
                class MetaOapg:
                    format = 'date-time'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, str, datetime, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'started_at':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class submission_id(
                schemas.Int64Base,
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                class MetaOapg:
                    format = 'int64'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'submission_id':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class time_spent(
                schemas.Int64Base,
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                class MetaOapg:
                    format = 'int64'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'time_spent':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class user_id(
                schemas.Int64Base,
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                class MetaOapg:
                    format = 'int64'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'user_id':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class workflow_state(
                schemas.StrBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneStrMixin
            ):
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, str, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'workflow_state':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            __annotations__ = {
                "id": id,
                "quiz_id": quiz_id,
                "attempt": attempt,
                "end_at": end_at,
                "extra_attempts": extra_attempts,
                "extra_time": extra_time,
                "finished_at": finished_at,
                "fudge_points": fudge_points,
                "has_seen_results": has_seen_results,
                "kept_score": kept_score,
                "manually_unlocked": manually_unlocked,
                "overdue_and_needs_submission": overdue_and_needs_submission,
                "score": score,
                "score_before_regrade": score_before_regrade,
                "started_at": started_at,
                "submission_id": submission_id,
                "time_spent": time_spent,
                "user_id": user_id,
                "workflow_state": workflow_state,
            }
    
    quiz_id: MetaOapg.properties.quiz_id
    id: MetaOapg.properties.id
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["id"]) -> MetaOapg.properties.id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["quiz_id"]) -> MetaOapg.properties.quiz_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["attempt"]) -> MetaOapg.properties.attempt: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["end_at"]) -> MetaOapg.properties.end_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["extra_attempts"]) -> MetaOapg.properties.extra_attempts: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["extra_time"]) -> MetaOapg.properties.extra_time: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["finished_at"]) -> MetaOapg.properties.finished_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["fudge_points"]) -> MetaOapg.properties.fudge_points: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["has_seen_results"]) -> MetaOapg.properties.has_seen_results: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["kept_score"]) -> MetaOapg.properties.kept_score: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["manually_unlocked"]) -> MetaOapg.properties.manually_unlocked: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["overdue_and_needs_submission"]) -> MetaOapg.properties.overdue_and_needs_submission: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["score"]) -> MetaOapg.properties.score: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["score_before_regrade"]) -> MetaOapg.properties.score_before_regrade: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["started_at"]) -> MetaOapg.properties.started_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["submission_id"]) -> MetaOapg.properties.submission_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["time_spent"]) -> MetaOapg.properties.time_spent: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["user_id"]) -> MetaOapg.properties.user_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["workflow_state"]) -> MetaOapg.properties.workflow_state: ...
    
    @typing.overload
    def __getitem__(self, name: str) -> schemas.UnsetAnyTypeSchema: ...
    
    def __getitem__(self, name: typing.Union[typing_extensions.Literal["id", "quiz_id", "attempt", "end_at", "extra_attempts", "extra_time", "finished_at", "fudge_points", "has_seen_results", "kept_score", "manually_unlocked", "overdue_and_needs_submission", "score", "score_before_regrade", "started_at", "submission_id", "time_spent", "user_id", "workflow_state", ], str]):
        # dict_instance[name] accessor
        return super().__getitem__(name)
    
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["id"]) -> MetaOapg.properties.id: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["quiz_id"]) -> MetaOapg.properties.quiz_id: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["attempt"]) -> typing.Union[MetaOapg.properties.attempt, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["end_at"]) -> typing.Union[MetaOapg.properties.end_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["extra_attempts"]) -> typing.Union[MetaOapg.properties.extra_attempts, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["extra_time"]) -> typing.Union[MetaOapg.properties.extra_time, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["finished_at"]) -> typing.Union[MetaOapg.properties.finished_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["fudge_points"]) -> typing.Union[MetaOapg.properties.fudge_points, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["has_seen_results"]) -> typing.Union[MetaOapg.properties.has_seen_results, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["kept_score"]) -> typing.Union[MetaOapg.properties.kept_score, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["manually_unlocked"]) -> typing.Union[MetaOapg.properties.manually_unlocked, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["overdue_and_needs_submission"]) -> typing.Union[MetaOapg.properties.overdue_and_needs_submission, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["score"]) -> typing.Union[MetaOapg.properties.score, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["score_before_regrade"]) -> typing.Union[MetaOapg.properties.score_before_regrade, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["started_at"]) -> typing.Union[MetaOapg.properties.started_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["submission_id"]) -> typing.Union[MetaOapg.properties.submission_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["time_spent"]) -> typing.Union[MetaOapg.properties.time_spent, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["user_id"]) -> typing.Union[MetaOapg.properties.user_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["workflow_state"]) -> typing.Union[MetaOapg.properties.workflow_state, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: str) -> typing.Union[schemas.UnsetAnyTypeSchema, schemas.Unset]: ...
    
    def get_item_oapg(self, name: typing.Union[typing_extensions.Literal["id", "quiz_id", "attempt", "end_at", "extra_attempts", "extra_time", "finished_at", "fudge_points", "has_seen_results", "kept_score", "manually_unlocked", "overdue_and_needs_submission", "score", "score_before_regrade", "started_at", "submission_id", "time_spent", "user_id", "workflow_state", ], str]):
        return super().get_item_oapg(name)
    

    def __new__(
        cls,
        *args: typing.Union[dict, frozendict.frozendict, ],
        quiz_id: typing.Union[MetaOapg.properties.quiz_id, decimal.Decimal, int, ],
        id: typing.Union[MetaOapg.properties.id, decimal.Decimal, int, ],
        attempt: typing.Union[MetaOapg.properties.attempt, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        end_at: typing.Union[MetaOapg.properties.end_at, None, str, datetime, schemas.Unset] = schemas.unset,
        extra_attempts: typing.Union[MetaOapg.properties.extra_attempts, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        extra_time: typing.Union[MetaOapg.properties.extra_time, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        finished_at: typing.Union[MetaOapg.properties.finished_at, None, str, datetime, schemas.Unset] = schemas.unset,
        fudge_points: typing.Union[MetaOapg.properties.fudge_points, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        has_seen_results: typing.Union[MetaOapg.properties.has_seen_results, None, bool, schemas.Unset] = schemas.unset,
        kept_score: typing.Union[MetaOapg.properties.kept_score, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        manually_unlocked: typing.Union[MetaOapg.properties.manually_unlocked, None, bool, schemas.Unset] = schemas.unset,
        overdue_and_needs_submission: typing.Union[MetaOapg.properties.overdue_and_needs_submission, None, bool, schemas.Unset] = schemas.unset,
        score: typing.Union[MetaOapg.properties.score, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        score_before_regrade: typing.Union[MetaOapg.properties.score_before_regrade, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        started_at: typing.Union[MetaOapg.properties.started_at, None, str, datetime, schemas.Unset] = schemas.unset,
        submission_id: typing.Union[MetaOapg.properties.submission_id, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        time_spent: typing.Union[MetaOapg.properties.time_spent, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        user_id: typing.Union[MetaOapg.properties.user_id, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        workflow_state: typing.Union[MetaOapg.properties.workflow_state, None, str, schemas.Unset] = schemas.unset,
        _configuration: typing.Optional[schemas.Configuration] = None,
        **kwargs: typing.Union[schemas.AnyTypeSchema, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, None, list, tuple, bytes],
    ) -> 'QuizSubmission':
        return super().__new__(
            cls,
            *args,
            quiz_id=quiz_id,
            id=id,
            attempt=attempt,
            end_at=end_at,
            extra_attempts=extra_attempts,
            extra_time=extra_time,
            finished_at=finished_at,
            fudge_points=fudge_points,
            has_seen_results=has_seen_results,
            kept_score=kept_score,
            manually_unlocked=manually_unlocked,
            overdue_and_needs_submission=overdue_and_needs_submission,
            score=score,
            score_before_regrade=score_before_regrade,
            started_at=started_at,
            submission_id=submission_id,
            time_spent=time_spent,
            user_id=user_id,
            workflow_state=workflow_state,
            _configuration=_configuration,
            **kwargs,
        )
