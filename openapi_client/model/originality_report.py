# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from datetime import date, datetime  # noqa: F401
import decimal  # noqa: F401
import functools  # noqa: F401
import io  # noqa: F401
import re  # noqa: F401
import typing  # noqa: F401
import typing_extensions  # noqa: F401
import uuid  # noqa: F401

import frozendict  # noqa: F401

from openapi_client import schemas  # noqa: F401


class OriginalityReport(
    schemas.DictSchema
):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """


    class MetaOapg:
        
        class properties:
            
            
            class file_id(
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'file_id':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            id = schemas.IntSchema
            
            
            class originality_report_file_id(
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'originality_report_file_id':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class originality_report_url(
                schemas.StrBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneStrMixin
            ):
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, str, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'originality_report_url':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class originality_score(
                schemas.NumberBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, float, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'originality_score':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
        
            @staticmethod
            def tool_setting() -> typing.Type['ToolSetting']:
                return ToolSetting
            __annotations__ = {
                "file_id": file_id,
                "id": id,
                "originality_report_file_id": originality_report_file_id,
                "originality_report_url": originality_report_url,
                "originality_score": originality_score,
                "tool_setting": tool_setting,
            }
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["file_id"]) -> MetaOapg.properties.file_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["id"]) -> MetaOapg.properties.id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["originality_report_file_id"]) -> MetaOapg.properties.originality_report_file_id: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["originality_report_url"]) -> MetaOapg.properties.originality_report_url: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["originality_score"]) -> MetaOapg.properties.originality_score: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["tool_setting"]) -> 'ToolSetting': ...
    
    @typing.overload
    def __getitem__(self, name: str) -> schemas.UnsetAnyTypeSchema: ...
    
    def __getitem__(self, name: typing.Union[typing_extensions.Literal["file_id", "id", "originality_report_file_id", "originality_report_url", "originality_score", "tool_setting", ], str]):
        # dict_instance[name] accessor
        return super().__getitem__(name)
    
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["file_id"]) -> typing.Union[MetaOapg.properties.file_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["id"]) -> typing.Union[MetaOapg.properties.id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["originality_report_file_id"]) -> typing.Union[MetaOapg.properties.originality_report_file_id, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["originality_report_url"]) -> typing.Union[MetaOapg.properties.originality_report_url, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["originality_score"]) -> typing.Union[MetaOapg.properties.originality_score, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["tool_setting"]) -> typing.Union['ToolSetting', schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: str) -> typing.Union[schemas.UnsetAnyTypeSchema, schemas.Unset]: ...
    
    def get_item_oapg(self, name: typing.Union[typing_extensions.Literal["file_id", "id", "originality_report_file_id", "originality_report_url", "originality_score", "tool_setting", ], str]):
        return super().get_item_oapg(name)
    

    def __new__(
        cls,
        *args: typing.Union[dict, frozendict.frozendict, ],
        file_id: typing.Union[MetaOapg.properties.file_id, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        id: typing.Union[MetaOapg.properties.id, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        originality_report_file_id: typing.Union[MetaOapg.properties.originality_report_file_id, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        originality_report_url: typing.Union[MetaOapg.properties.originality_report_url, None, str, schemas.Unset] = schemas.unset,
        originality_score: typing.Union[MetaOapg.properties.originality_score, None, decimal.Decimal, int, float, schemas.Unset] = schemas.unset,
        tool_setting: typing.Union['ToolSetting', schemas.Unset] = schemas.unset,
        _configuration: typing.Optional[schemas.Configuration] = None,
        **kwargs: typing.Union[schemas.AnyTypeSchema, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, None, list, tuple, bytes],
    ) -> 'OriginalityReport':
        return super().__new__(
            cls,
            *args,
            file_id=file_id,
            id=id,
            originality_report_file_id=originality_report_file_id,
            originality_report_url=originality_report_url,
            originality_score=originality_score,
            tool_setting=tool_setting,
            _configuration=_configuration,
            **kwargs,
        )

from openapi_client.model.tool_setting import ToolSetting
