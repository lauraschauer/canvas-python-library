# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from datetime import date, datetime  # noqa: F401
import decimal  # noqa: F401
import functools  # noqa: F401
import io  # noqa: F401
import re  # noqa: F401
import typing  # noqa: F401
import typing_extensions  # noqa: F401
import uuid  # noqa: F401

import frozendict  # noqa: F401

from openapi_client import schemas  # noqa: F401


class ContentDetails(
    schemas.DictSchema
):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """


    class MetaOapg:
        
        class properties:
            
            
            class due_at(
                schemas.DateTimeBase,
                schemas.StrBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneStrMixin
            ):
            
            
                class MetaOapg:
                    format = 'date-time'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, str, datetime, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'due_at':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class lock_at(
                schemas.DateTimeBase,
                schemas.StrBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneStrMixin
            ):
            
            
                class MetaOapg:
                    format = 'date-time'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, str, datetime, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'lock_at':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class lock_explanation(
                schemas.StrBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneStrMixin
            ):
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, str, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'lock_explanation':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
        
            @staticmethod
            def lock_info() -> typing.Type['LockInfo']:
                return LockInfo
            
            
            class locked_for_user(
                schemas.BoolBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneBoolMixin
            ):
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, bool, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'locked_for_user':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class points_possible(
                schemas.IntBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneDecimalMixin
            ):
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, decimal.Decimal, int, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'points_possible':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            
            
            class unlock_at(
                schemas.DateTimeBase,
                schemas.StrBase,
                schemas.NoneBase,
                schemas.Schema,
                schemas.NoneStrMixin
            ):
            
            
                class MetaOapg:
                    format = 'date-time'
            
            
                def __new__(
                    cls,
                    *args: typing.Union[None, str, datetime, ],
                    _configuration: typing.Optional[schemas.Configuration] = None,
                ) -> 'unlock_at':
                    return super().__new__(
                        cls,
                        *args,
                        _configuration=_configuration,
                    )
            __annotations__ = {
                "due_at": due_at,
                "lock_at": lock_at,
                "lock_explanation": lock_explanation,
                "lock_info": lock_info,
                "locked_for_user": locked_for_user,
                "points_possible": points_possible,
                "unlock_at": unlock_at,
            }
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["due_at"]) -> MetaOapg.properties.due_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["lock_at"]) -> MetaOapg.properties.lock_at: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["lock_explanation"]) -> MetaOapg.properties.lock_explanation: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["lock_info"]) -> 'LockInfo': ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["locked_for_user"]) -> MetaOapg.properties.locked_for_user: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["points_possible"]) -> MetaOapg.properties.points_possible: ...
    
    @typing.overload
    def __getitem__(self, name: typing_extensions.Literal["unlock_at"]) -> MetaOapg.properties.unlock_at: ...
    
    @typing.overload
    def __getitem__(self, name: str) -> schemas.UnsetAnyTypeSchema: ...
    
    def __getitem__(self, name: typing.Union[typing_extensions.Literal["due_at", "lock_at", "lock_explanation", "lock_info", "locked_for_user", "points_possible", "unlock_at", ], str]):
        # dict_instance[name] accessor
        return super().__getitem__(name)
    
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["due_at"]) -> typing.Union[MetaOapg.properties.due_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["lock_at"]) -> typing.Union[MetaOapg.properties.lock_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["lock_explanation"]) -> typing.Union[MetaOapg.properties.lock_explanation, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["lock_info"]) -> typing.Union['LockInfo', schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["locked_for_user"]) -> typing.Union[MetaOapg.properties.locked_for_user, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["points_possible"]) -> typing.Union[MetaOapg.properties.points_possible, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: typing_extensions.Literal["unlock_at"]) -> typing.Union[MetaOapg.properties.unlock_at, schemas.Unset]: ...
    
    @typing.overload
    def get_item_oapg(self, name: str) -> typing.Union[schemas.UnsetAnyTypeSchema, schemas.Unset]: ...
    
    def get_item_oapg(self, name: typing.Union[typing_extensions.Literal["due_at", "lock_at", "lock_explanation", "lock_info", "locked_for_user", "points_possible", "unlock_at", ], str]):
        return super().get_item_oapg(name)
    

    def __new__(
        cls,
        *args: typing.Union[dict, frozendict.frozendict, ],
        due_at: typing.Union[MetaOapg.properties.due_at, None, str, datetime, schemas.Unset] = schemas.unset,
        lock_at: typing.Union[MetaOapg.properties.lock_at, None, str, datetime, schemas.Unset] = schemas.unset,
        lock_explanation: typing.Union[MetaOapg.properties.lock_explanation, None, str, schemas.Unset] = schemas.unset,
        lock_info: typing.Union['LockInfo', schemas.Unset] = schemas.unset,
        locked_for_user: typing.Union[MetaOapg.properties.locked_for_user, None, bool, schemas.Unset] = schemas.unset,
        points_possible: typing.Union[MetaOapg.properties.points_possible, None, decimal.Decimal, int, schemas.Unset] = schemas.unset,
        unlock_at: typing.Union[MetaOapg.properties.unlock_at, None, str, datetime, schemas.Unset] = schemas.unset,
        _configuration: typing.Optional[schemas.Configuration] = None,
        **kwargs: typing.Union[schemas.AnyTypeSchema, dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, None, list, tuple, bytes],
    ) -> 'ContentDetails':
        return super().__new__(
            cls,
            *args,
            due_at=due_at,
            lock_at=lock_at,
            lock_explanation=lock_explanation,
            lock_info=lock_info,
            locked_for_user=locked_for_user,
            points_possible=points_possible,
            unlock_at=unlock_at,
            _configuration=_configuration,
            **kwargs,
        )

from openapi_client.model.lock_info import LockInfo
