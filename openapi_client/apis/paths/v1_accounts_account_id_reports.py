from openapi_client.paths.v1_accounts_account_id_reports.get import ApiForget


class V1AccountsAccountIdReports(
    ApiForget,
):
    pass
