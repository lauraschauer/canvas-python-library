from openapi_client.paths.v1_users_user_id_communication_channels_id.delete import ApiFordelete


class V1UsersUserIdCommunicationChannelsId(
    ApiFordelete,
):
    pass
