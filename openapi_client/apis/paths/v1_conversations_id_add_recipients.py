from openapi_client.paths.v1_conversations_id_add_recipients.post import ApiForpost


class V1ConversationsIdAddRecipients(
    ApiForpost,
):
    pass
