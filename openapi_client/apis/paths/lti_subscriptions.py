from openapi_client.paths.lti_subscriptions.get import ApiForget
from openapi_client.paths.lti_subscriptions.post import ApiForpost


class LtiSubscriptions(
    ApiForget,
    ApiForpost,
):
    pass
