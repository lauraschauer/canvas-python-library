from openapi_client.paths.v1_courses_course_id_blueprint_templates_template_id_associated_courses.get import ApiForget


class V1CoursesCourseIdBlueprintTemplatesTemplateIdAssociatedCourses(
    ApiForget,
):
    pass
