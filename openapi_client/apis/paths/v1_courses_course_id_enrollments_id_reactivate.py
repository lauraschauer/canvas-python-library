from openapi_client.paths.v1_courses_course_id_enrollments_id_reactivate.put import ApiForput


class V1CoursesCourseIdEnrollmentsIdReactivate(
    ApiForput,
):
    pass
