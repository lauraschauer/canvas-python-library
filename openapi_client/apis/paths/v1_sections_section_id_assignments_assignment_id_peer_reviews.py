from openapi_client.paths.v1_sections_section_id_assignments_assignment_id_peer_reviews.get import ApiForget


class V1SectionsSectionIdAssignmentsAssignmentIdPeerReviews(
    ApiForget,
):
    pass
