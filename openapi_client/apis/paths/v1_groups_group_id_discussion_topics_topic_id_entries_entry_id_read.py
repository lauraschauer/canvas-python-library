from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_entries_entry_id_read.put import ApiForput
from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_entries_entry_id_read.delete import ApiFordelete


class V1GroupsGroupIdDiscussionTopicsTopicIdEntriesEntryIdRead(
    ApiForput,
    ApiFordelete,
):
    pass
