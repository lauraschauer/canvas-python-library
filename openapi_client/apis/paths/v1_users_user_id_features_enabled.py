from openapi_client.paths.v1_users_user_id_features_enabled.get import ApiForget


class V1UsersUserIdFeaturesEnabled(
    ApiForget,
):
    pass
