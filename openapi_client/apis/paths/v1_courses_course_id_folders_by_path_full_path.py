from openapi_client.paths.v1_courses_course_id_folders_by_path_full_path.get import ApiForget


class V1CoursesCourseIdFoldersByPathFullPath(
    ApiForget,
):
    pass
