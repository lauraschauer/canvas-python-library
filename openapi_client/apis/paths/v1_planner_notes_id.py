from openapi_client.paths.v1_planner_notes_id.get import ApiForget
from openapi_client.paths.v1_planner_notes_id.put import ApiForput
from openapi_client.paths.v1_planner_notes_id.delete import ApiFordelete


class V1PlannerNotesId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
