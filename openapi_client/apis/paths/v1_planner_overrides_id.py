from openapi_client.paths.v1_planner_overrides_id.get import ApiForget
from openapi_client.paths.v1_planner_overrides_id.put import ApiForput
from openapi_client.paths.v1_planner_overrides_id.delete import ApiFordelete


class V1PlannerOverridesId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
