from openapi_client.paths.v1_sections_section_id_assignments_assignment_id_submissions_user_id.get import ApiForget
from openapi_client.paths.v1_sections_section_id_assignments_assignment_id_submissions_user_id.put import ApiForput


class V1SectionsSectionIdAssignmentsAssignmentIdSubmissionsUserId(
    ApiForget,
    ApiForput,
):
    pass
