from openapi_client.paths.v1_poll_sessions_opened.get import ApiForget


class V1PollSessionsOpened(
    ApiForget,
):
    pass
