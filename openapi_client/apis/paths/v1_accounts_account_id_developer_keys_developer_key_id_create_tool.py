from openapi_client.paths.v1_accounts_account_id_developer_keys_developer_key_id_create_tool.post import ApiForpost


class V1AccountsAccountIdDeveloperKeysDeveloperKeyIdCreateTool(
    ApiForpost,
):
    pass
