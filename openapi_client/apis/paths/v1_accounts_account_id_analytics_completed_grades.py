from openapi_client.paths.v1_accounts_account_id_analytics_completed_grades.get import ApiForget


class V1AccountsAccountIdAnalyticsCompletedGrades(
    ApiForget,
):
    pass
