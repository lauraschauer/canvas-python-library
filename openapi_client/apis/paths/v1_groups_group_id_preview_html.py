from openapi_client.paths.v1_groups_group_id_preview_html.post import ApiForpost


class V1GroupsGroupIdPreviewHtml(
    ApiForpost,
):
    pass
