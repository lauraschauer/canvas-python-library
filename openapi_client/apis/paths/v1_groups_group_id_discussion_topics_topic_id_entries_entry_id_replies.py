from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_entries_entry_id_replies.get import ApiForget
from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_entries_entry_id_replies.post import ApiForpost


class V1GroupsGroupIdDiscussionTopicsTopicIdEntriesEntryIdReplies(
    ApiForget,
    ApiForpost,
):
    pass
