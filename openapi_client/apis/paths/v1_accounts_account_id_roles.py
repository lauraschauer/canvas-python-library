from openapi_client.paths.v1_accounts_account_id_roles.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_roles.post import ApiForpost


class V1AccountsAccountIdRoles(
    ApiForget,
    ApiForpost,
):
    pass
