from openapi_client.paths.v1_groups_group_id_assignments_assignment_id_override.get import ApiForget


class V1GroupsGroupIdAssignmentsAssignmentIdOverride(
    ApiForget,
):
    pass
