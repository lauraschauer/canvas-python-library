from openapi_client.paths.v1_courses_course_id_quizzes_id_validate_access_code.post import ApiForpost


class V1CoursesCourseIdQuizzesIdValidateAccessCode(
    ApiForpost,
):
    pass
