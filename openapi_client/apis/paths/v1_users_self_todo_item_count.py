from openapi_client.paths.v1_users_self_todo_item_count.get import ApiForget


class V1UsersSelfTodoItemCount(
    ApiForget,
):
    pass
