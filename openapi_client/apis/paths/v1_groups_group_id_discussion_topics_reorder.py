from openapi_client.paths.v1_groups_group_id_discussion_topics_reorder.post import ApiForpost


class V1GroupsGroupIdDiscussionTopicsReorder(
    ApiForpost,
):
    pass
