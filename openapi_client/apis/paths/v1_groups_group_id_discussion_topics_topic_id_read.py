from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_read.put import ApiForput
from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_read.delete import ApiFordelete


class V1GroupsGroupIdDiscussionTopicsTopicIdRead(
    ApiForput,
    ApiFordelete,
):
    pass
