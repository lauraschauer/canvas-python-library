from openapi_client.paths.v1_courses_course_id_assignments_overrides.get import ApiForget
from openapi_client.paths.v1_courses_course_id_assignments_overrides.put import ApiForput
from openapi_client.paths.v1_courses_course_id_assignments_overrides.post import ApiForpost


class V1CoursesCourseIdAssignmentsOverrides(
    ApiForget,
    ApiForput,
    ApiForpost,
):
    pass
