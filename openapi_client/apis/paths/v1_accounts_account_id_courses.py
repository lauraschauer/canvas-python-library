from openapi_client.paths.v1_accounts_account_id_courses.put import ApiForput
from openapi_client.paths.v1_accounts_account_id_courses.post import ApiForpost


class V1AccountsAccountIdCourses(
    ApiForput,
    ApiForpost,
):
    pass
