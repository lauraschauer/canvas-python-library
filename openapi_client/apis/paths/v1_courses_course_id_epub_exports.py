from openapi_client.paths.v1_courses_course_id_epub_exports.post import ApiForpost


class V1CoursesCourseIdEpubExports(
    ApiForpost,
):
    pass
