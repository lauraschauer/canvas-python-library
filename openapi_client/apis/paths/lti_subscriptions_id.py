from openapi_client.paths.lti_subscriptions_id.get import ApiForget
from openapi_client.paths.lti_subscriptions_id.put import ApiForput
from openapi_client.paths.lti_subscriptions_id.delete import ApiFordelete


class LtiSubscriptionsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
