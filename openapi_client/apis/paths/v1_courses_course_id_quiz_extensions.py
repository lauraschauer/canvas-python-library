from openapi_client.paths.v1_courses_course_id_quiz_extensions.post import ApiForpost


class V1CoursesCourseIdQuizExtensions(
    ApiForpost,
):
    pass
