from openapi_client.paths.v1_courses_course_id_quizzes_id_submission_users_message.post import ApiForpost


class V1CoursesCourseIdQuizzesIdSubmissionUsersMessage(
    ApiForpost,
):
    pass
