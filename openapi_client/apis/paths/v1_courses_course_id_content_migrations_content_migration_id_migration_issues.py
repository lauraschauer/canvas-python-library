from openapi_client.paths.v1_courses_course_id_content_migrations_content_migration_id_migration_issues.get import ApiForget


class V1CoursesCourseIdContentMigrationsContentMigrationIdMigrationIssues(
    ApiForget,
):
    pass
