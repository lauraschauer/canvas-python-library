from openapi_client.paths.v1_courses_course_id_assignments_gradeable_students.get import ApiForget


class V1CoursesCourseIdAssignmentsGradeableStudents(
    ApiForget,
):
    pass
