from openapi_client.paths.v1_search_all_courses.get import ApiForget


class V1SearchAllCourses(
    ApiForget,
):
    pass
