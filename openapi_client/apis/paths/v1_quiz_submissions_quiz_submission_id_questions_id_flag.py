from openapi_client.paths.v1_quiz_submissions_quiz_submission_id_questions_id_flag.put import ApiForput


class V1QuizSubmissionsQuizSubmissionIdQuestionsIdFlag(
    ApiForput,
):
    pass
