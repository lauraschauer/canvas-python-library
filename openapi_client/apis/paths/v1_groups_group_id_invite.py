from openapi_client.paths.v1_groups_group_id_invite.post import ApiForpost


class V1GroupsGroupIdInvite(
    ApiForpost,
):
    pass
