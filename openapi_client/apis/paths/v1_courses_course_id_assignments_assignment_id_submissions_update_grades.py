from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_submissions_update_grades.post import ApiForpost


class V1CoursesCourseIdAssignmentsAssignmentIdSubmissionsUpdateGrades(
    ApiForpost,
):
    pass
