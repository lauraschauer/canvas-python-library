from openapi_client.paths.v1_courses_course_id_assignments_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_assignments_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_assignments_id.delete import ApiFordelete


class V1CoursesCourseIdAssignmentsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
