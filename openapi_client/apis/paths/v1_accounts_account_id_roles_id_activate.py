from openapi_client.paths.v1_accounts_account_id_roles_id_activate.post import ApiForpost


class V1AccountsAccountIdRolesIdActivate(
    ApiForpost,
):
    pass
