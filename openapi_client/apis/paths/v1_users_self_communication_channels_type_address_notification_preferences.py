from openapi_client.paths.v1_users_self_communication_channels_type_address_notification_preferences.put import ApiForput


class V1UsersSelfCommunicationChannelsTypeAddressNotificationPreferences(
    ApiForput,
):
    pass
