from openapi_client.paths.v1_accounts_account_id_content_migrations_id.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_content_migrations_id.put import ApiForput


class V1AccountsAccountIdContentMigrationsId(
    ApiForget,
    ApiForput,
):
    pass
