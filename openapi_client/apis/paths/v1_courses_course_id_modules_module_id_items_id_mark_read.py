from openapi_client.paths.v1_courses_course_id_modules_module_id_items_id_mark_read.post import ApiForpost


class V1CoursesCourseIdModulesModuleIdItemsIdMarkRead(
    ApiForpost,
):
    pass
