from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_reports.get import ApiForget
from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_reports.post import ApiForpost


class V1CoursesCourseIdQuizzesQuizIdReports(
    ApiForget,
    ApiForpost,
):
    pass
