from openapi_client.paths.v1_courses_course_id_gradebook_history_feed.get import ApiForget


class V1CoursesCourseIdGradebookHistoryFeed(
    ApiForget,
):
    pass
