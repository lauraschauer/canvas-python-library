from openapi_client.paths.v1_courses_course_id_course_copy.post import ApiForpost


class V1CoursesCourseIdCourseCopy(
    ApiForpost,
):
    pass
