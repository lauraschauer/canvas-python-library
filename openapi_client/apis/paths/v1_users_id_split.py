from openapi_client.paths.v1_users_id_split.post import ApiForpost


class V1UsersIdSplit(
    ApiForpost,
):
    pass
