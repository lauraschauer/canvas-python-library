from openapi_client.paths.v1_groups_group_id_external_feeds.get import ApiForget
from openapi_client.paths.v1_groups_group_id_external_feeds.post import ApiForpost


class V1GroupsGroupIdExternalFeeds(
    ApiForget,
    ApiForpost,
):
    pass
