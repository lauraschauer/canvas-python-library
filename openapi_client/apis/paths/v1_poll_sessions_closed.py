from openapi_client.paths.v1_poll_sessions_closed.get import ApiForget


class V1PollSessionsClosed(
    ApiForget,
):
    pass
