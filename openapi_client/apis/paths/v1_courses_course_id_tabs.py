from openapi_client.paths.v1_courses_course_id_tabs.get import ApiForget


class V1CoursesCourseIdTabs(
    ApiForget,
):
    pass
