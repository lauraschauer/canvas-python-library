from openapi_client.paths.v1_group_categories_group_category_id.get import ApiForget
from openapi_client.paths.v1_group_categories_group_category_id.put import ApiForput
from openapi_client.paths.v1_group_categories_group_category_id.delete import ApiFordelete


class V1GroupCategoriesGroupCategoryId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
