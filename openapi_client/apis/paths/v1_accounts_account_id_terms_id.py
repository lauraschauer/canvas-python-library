from openapi_client.paths.v1_accounts_account_id_terms_id.put import ApiForput
from openapi_client.paths.v1_accounts_account_id_terms_id.delete import ApiFordelete


class V1AccountsAccountIdTermsId(
    ApiForput,
    ApiFordelete,
):
    pass
