from openapi_client.paths.v1_courses_course_id_pages_url_duplicate.post import ApiForpost


class V1CoursesCourseIdPagesUrlDuplicate(
    ApiForpost,
):
    pass
