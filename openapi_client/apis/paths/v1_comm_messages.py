from openapi_client.paths.v1_comm_messages.get import ApiForget


class V1CommMessages(
    ApiForget,
):
    pass
