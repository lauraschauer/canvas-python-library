from openapi_client.paths.v1_courses_course_id_activity_stream.get import ApiForget


class V1CoursesCourseIdActivityStream(
    ApiForget,
):
    pass
