from openapi_client.paths.v1_courses_course_id_folders_id.get import ApiForget


class V1CoursesCourseIdFoldersId(
    ApiForget,
):
    pass
