from openapi_client.paths.v1_accounts_account_id_account_notifications_id.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_account_notifications_id.put import ApiForput
from openapi_client.paths.v1_accounts_account_id_account_notifications_id.delete import ApiFordelete


class V1AccountsAccountIdAccountNotificationsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
