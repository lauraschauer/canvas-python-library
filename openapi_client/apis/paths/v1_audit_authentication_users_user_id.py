from openapi_client.paths.v1_audit_authentication_users_user_id.get import ApiForget


class V1AuditAuthenticationUsersUserId(
    ApiForget,
):
    pass
