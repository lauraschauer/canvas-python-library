from openapi_client.paths.v1_group_categories_group_category_id_users.get import ApiForget


class V1GroupCategoriesGroupCategoryIdUsers(
    ApiForget,
):
    pass
