from openapi_client.paths.v1_courses_course_id_blueprint_templates_template_id_unsynced_changes.get import ApiForget


class V1CoursesCourseIdBlueprintTemplatesTemplateIdUnsyncedChanges(
    ApiForget,
):
    pass
