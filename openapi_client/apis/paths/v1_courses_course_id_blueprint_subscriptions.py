from openapi_client.paths.v1_courses_course_id_blueprint_subscriptions.get import ApiForget


class V1CoursesCourseIdBlueprintSubscriptions(
    ApiForget,
):
    pass
