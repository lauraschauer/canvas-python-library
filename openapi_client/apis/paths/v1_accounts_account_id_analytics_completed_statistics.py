from openapi_client.paths.v1_accounts_account_id_analytics_completed_statistics.get import ApiForget


class V1AccountsAccountIdAnalyticsCompletedStatistics(
    ApiForget,
):
    pass
