from openapi_client.paths.v1_groups_group_id_content_migrations_id.get import ApiForget
from openapi_client.paths.v1_groups_group_id_content_migrations_id.put import ApiForput


class V1GroupsGroupIdContentMigrationsId(
    ApiForget,
    ApiForput,
):
    pass
