from openapi_client.paths.v1_courses_id_late_policy.get import ApiForget
from openapi_client.paths.v1_courses_id_late_policy.post import ApiForpost
from openapi_client.paths.v1_courses_id_late_policy.patch import ApiForpatch


class V1CoursesIdLatePolicy(
    ApiForget,
    ApiForpost,
    ApiForpatch,
):
    pass
