from openapi_client.paths.v1_accounts_account_id_roles_id.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_roles_id.put import ApiForput
from openapi_client.paths.v1_accounts_account_id_roles_id.delete import ApiFordelete


class V1AccountsAccountIdRolesId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
