from openapi_client.paths.v1_courses_course_id_grading_standards_grading_standard_id.get import ApiForget


class V1CoursesCourseIdGradingStandardsGradingStandardId(
    ApiForget,
):
    pass
