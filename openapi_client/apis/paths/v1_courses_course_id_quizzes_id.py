from openapi_client.paths.v1_courses_course_id_quizzes_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_quizzes_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_quizzes_id.delete import ApiFordelete


class V1CoursesCourseIdQuizzesId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
