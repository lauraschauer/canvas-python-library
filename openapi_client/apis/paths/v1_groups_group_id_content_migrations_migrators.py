from openapi_client.paths.v1_groups_group_id_content_migrations_migrators.get import ApiForget


class V1GroupsGroupIdContentMigrationsMigrators(
    ApiForget,
):
    pass
