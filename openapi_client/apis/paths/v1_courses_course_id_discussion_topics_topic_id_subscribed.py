from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_subscribed.put import ApiForput
from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_subscribed.delete import ApiFordelete


class V1CoursesCourseIdDiscussionTopicsTopicIdSubscribed(
    ApiForput,
    ApiFordelete,
):
    pass
