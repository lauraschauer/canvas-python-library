from openapi_client.paths.v1_groups_group_id_memberships.get import ApiForget
from openapi_client.paths.v1_groups_group_id_memberships.post import ApiForpost


class V1GroupsGroupIdMemberships(
    ApiForget,
    ApiForpost,
):
    pass
