from openapi_client.paths.v1_global_outcome_groups_id.get import ApiForget
from openapi_client.paths.v1_global_outcome_groups_id.put import ApiForput
from openapi_client.paths.v1_global_outcome_groups_id.delete import ApiFordelete


class V1GlobalOutcomeGroupsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
