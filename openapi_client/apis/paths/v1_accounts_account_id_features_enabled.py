from openapi_client.paths.v1_accounts_account_id_features_enabled.get import ApiForget


class V1AccountsAccountIdFeaturesEnabled(
    ApiForget,
):
    pass
