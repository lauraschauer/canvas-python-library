from openapi_client.paths.v1_accounts_account_id_outcome_groups.get import ApiForget


class V1AccountsAccountIdOutcomeGroups(
    ApiForget,
):
    pass
