from openapi_client.paths.v1_polls_poll_id_poll_sessions_id_open.get import ApiForget


class V1PollsPollIdPollSessionsIdOpen(
    ApiForget,
):
    pass
