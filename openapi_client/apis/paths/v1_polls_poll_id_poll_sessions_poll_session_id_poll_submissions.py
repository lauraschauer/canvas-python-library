from openapi_client.paths.v1_polls_poll_id_poll_sessions_poll_session_id_poll_submissions.post import ApiForpost


class V1PollsPollIdPollSessionsPollSessionIdPollSubmissions(
    ApiForpost,
):
    pass
