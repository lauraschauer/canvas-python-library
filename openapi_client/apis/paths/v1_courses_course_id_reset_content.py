from openapi_client.paths.v1_courses_course_id_reset_content.post import ApiForpost


class V1CoursesCourseIdResetContent(
    ApiForpost,
):
    pass
