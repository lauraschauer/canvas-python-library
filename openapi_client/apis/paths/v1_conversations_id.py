from openapi_client.paths.v1_conversations_id.get import ApiForget
from openapi_client.paths.v1_conversations_id.put import ApiForput
from openapi_client.paths.v1_conversations_id.delete import ApiFordelete


class V1ConversationsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
