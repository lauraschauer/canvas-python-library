from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_submissions_user_id_read.put import ApiForput
from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_submissions_user_id_read.delete import ApiFordelete


class V1CoursesCourseIdAssignmentsAssignmentIdSubmissionsUserIdRead(
    ApiForput,
    ApiFordelete,
):
    pass
