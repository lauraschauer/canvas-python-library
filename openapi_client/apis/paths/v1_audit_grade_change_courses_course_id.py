from openapi_client.paths.v1_audit_grade_change_courses_course_id.get import ApiForget


class V1AuditGradeChangeCoursesCourseId(
    ApiForget,
):
    pass
