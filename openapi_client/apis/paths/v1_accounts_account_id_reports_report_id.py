from openapi_client.paths.v1_accounts_account_id_reports_report_id.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_reports_report_id.delete import ApiFordelete


class V1AccountsAccountIdReportsReportId(
    ApiForget,
    ApiFordelete,
):
    pass
