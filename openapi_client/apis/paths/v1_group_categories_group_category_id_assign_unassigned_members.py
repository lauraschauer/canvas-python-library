from openapi_client.paths.v1_group_categories_group_category_id_assign_unassigned_members.post import ApiForpost


class V1GroupCategoriesGroupCategoryIdAssignUnassignedMembers(
    ApiForpost,
):
    pass
