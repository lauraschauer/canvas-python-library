from openapi_client.paths.v1_courses_course_id_content_migrations_content_migration_id_migration_issues_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_content_migrations_content_migration_id_migration_issues_id.put import ApiForput


class V1CoursesCourseIdContentMigrationsContentMigrationIdMigrationIssuesId(
    ApiForget,
    ApiForput,
):
    pass
