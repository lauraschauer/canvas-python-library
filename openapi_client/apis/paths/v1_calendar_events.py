from openapi_client.paths.v1_calendar_events.get import ApiForget
from openapi_client.paths.v1_calendar_events.post import ApiForpost


class V1CalendarEvents(
    ApiForget,
    ApiForpost,
):
    pass
