from openapi_client.paths.v1_courses_course_id_assignments.get import ApiForget
from openapi_client.paths.v1_courses_course_id_assignments.post import ApiForpost


class V1CoursesCourseIdAssignments(
    ApiForget,
    ApiForpost,
):
    pass
