from openapi_client.paths.v1_users_user_id_custom_data.get import ApiForget
from openapi_client.paths.v1_users_user_id_custom_data.put import ApiForput
from openapi_client.paths.v1_users_user_id_custom_data.delete import ApiFordelete


class V1UsersUserIdCustomData(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
