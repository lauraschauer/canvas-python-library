from openapi_client.paths.v1_users_user_id_content_migrations.get import ApiForget
from openapi_client.paths.v1_users_user_id_content_migrations.post import ApiForpost


class V1UsersUserIdContentMigrations(
    ApiForget,
    ApiForpost,
):
    pass
