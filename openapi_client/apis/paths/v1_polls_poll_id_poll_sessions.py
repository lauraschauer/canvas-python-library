from openapi_client.paths.v1_polls_poll_id_poll_sessions.get import ApiForget
from openapi_client.paths.v1_polls_poll_id_poll_sessions.post import ApiForpost


class V1PollsPollIdPollSessions(
    ApiForget,
    ApiForpost,
):
    pass
