from openapi_client.paths.v1_course_accounts.get import ApiForget


class V1CourseAccounts(
    ApiForget,
):
    pass
