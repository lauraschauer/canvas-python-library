from openapi_client.paths.v1_sections_section_id_assignments_assignment_id_submissions_user_id_files.post import ApiForpost


class V1SectionsSectionIdAssignmentsAssignmentIdSubmissionsUserIdFiles(
    ApiForpost,
):
    pass
