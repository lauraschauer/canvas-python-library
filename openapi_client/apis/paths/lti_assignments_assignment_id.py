from openapi_client.paths.lti_assignments_assignment_id.get import ApiForget


class LtiAssignmentsAssignmentId(
    ApiForget,
):
    pass
