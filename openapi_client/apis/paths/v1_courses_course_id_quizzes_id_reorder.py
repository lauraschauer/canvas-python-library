from openapi_client.paths.v1_courses_course_id_quizzes_id_reorder.post import ApiForpost


class V1CoursesCourseIdQuizzesIdReorder(
    ApiForpost,
):
    pass
