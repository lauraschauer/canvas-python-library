from openapi_client.paths.v1_courses_course_id_outcome_rollups.get import ApiForget


class V1CoursesCourseIdOutcomeRollups(
    ApiForget,
):
    pass
