from openapi_client.paths.v1_files_id.get import ApiForget
from openapi_client.paths.v1_files_id.put import ApiForput
from openapi_client.paths.v1_files_id.delete import ApiFordelete


class V1FilesId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
