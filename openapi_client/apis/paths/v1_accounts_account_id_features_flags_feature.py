from openapi_client.paths.v1_accounts_account_id_features_flags_feature.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_features_flags_feature.put import ApiForput
from openapi_client.paths.v1_accounts_account_id_features_flags_feature.delete import ApiFordelete


class V1AccountsAccountIdFeaturesFlagsFeature(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
