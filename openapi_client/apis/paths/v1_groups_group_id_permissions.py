from openapi_client.paths.v1_groups_group_id_permissions.get import ApiForget


class V1GroupsGroupIdPermissions(
    ApiForget,
):
    pass
