from openapi_client.paths.v1_courses_course_id_features.get import ApiForget


class V1CoursesCourseIdFeatures(
    ApiForget,
):
    pass
