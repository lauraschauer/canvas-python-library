from openapi_client.paths.v1_users_self_favorites_courses.get import ApiForget
from openapi_client.paths.v1_users_self_favorites_courses.delete import ApiFordelete


class V1UsersSelfFavoritesCourses(
    ApiForget,
    ApiFordelete,
):
    pass
