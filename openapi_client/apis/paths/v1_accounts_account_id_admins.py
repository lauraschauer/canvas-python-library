from openapi_client.paths.v1_accounts_account_id_admins.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_admins.post import ApiForpost


class V1AccountsAccountIdAdmins(
    ApiForget,
    ApiForpost,
):
    pass
