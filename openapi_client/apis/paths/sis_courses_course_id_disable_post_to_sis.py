from openapi_client.paths.sis_courses_course_id_disable_post_to_sis.put import ApiForput


class SisCoursesCourseIdDisablePostToSis(
    ApiForput,
):
    pass
