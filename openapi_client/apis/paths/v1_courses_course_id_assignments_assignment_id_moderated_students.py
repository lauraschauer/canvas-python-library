from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_moderated_students.get import ApiForget
from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_moderated_students.post import ApiForpost


class V1CoursesCourseIdAssignmentsAssignmentIdModeratedStudents(
    ApiForget,
    ApiForpost,
):
    pass
