from openapi_client.paths.v1_courses_course_id_preview_html.post import ApiForpost


class V1CoursesCourseIdPreviewHtml(
    ApiForpost,
):
    pass
