from openapi_client.paths.v1_courses_course_id_outcome_alignments.get import ApiForget


class V1CoursesCourseIdOutcomeAlignments(
    ApiForget,
):
    pass
