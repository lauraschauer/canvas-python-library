from openapi_client.paths.v1_accounts_account_id_outcome_groups_id_subgroups.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_outcome_groups_id_subgroups.post import ApiForpost


class V1AccountsAccountIdOutcomeGroupsIdSubgroups(
    ApiForget,
    ApiForpost,
):
    pass
