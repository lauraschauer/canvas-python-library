from openapi_client.paths.v1_groups_group_id_content_licenses.get import ApiForget


class V1GroupsGroupIdContentLicenses(
    ApiForget,
):
    pass
