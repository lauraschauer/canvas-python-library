from openapi_client.paths.v1_users_self_course_nicknames_course_id.get import ApiForget
from openapi_client.paths.v1_users_self_course_nicknames_course_id.put import ApiForput
from openapi_client.paths.v1_users_self_course_nicknames_course_id.delete import ApiFordelete


class V1UsersSelfCourseNicknamesCourseId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
