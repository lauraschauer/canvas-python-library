from openapi_client.paths.v1_sections_section_id_assignments_assignment_id_submissions.get import ApiForget
from openapi_client.paths.v1_sections_section_id_assignments_assignment_id_submissions.post import ApiForpost


class V1SectionsSectionIdAssignmentsAssignmentIdSubmissions(
    ApiForget,
    ApiForpost,
):
    pass
