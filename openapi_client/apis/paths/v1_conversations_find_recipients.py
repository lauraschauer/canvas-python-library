from openapi_client.paths.v1_conversations_find_recipients.get import ApiForget


class V1ConversationsFindRecipients(
    ApiForget,
):
    pass
