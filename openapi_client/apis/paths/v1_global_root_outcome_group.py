from openapi_client.paths.v1_global_root_outcome_group.get import ApiForget


class V1GlobalRootOutcomeGroup(
    ApiForget,
):
    pass
