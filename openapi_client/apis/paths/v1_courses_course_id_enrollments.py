from openapi_client.paths.v1_courses_course_id_enrollments.get import ApiForget
from openapi_client.paths.v1_courses_course_id_enrollments.post import ApiForpost


class V1CoursesCourseIdEnrollments(
    ApiForget,
    ApiForpost,
):
    pass
