from openapi_client.paths.v1_groups_group_id.get import ApiForget
from openapi_client.paths.v1_groups_group_id.put import ApiForput
from openapi_client.paths.v1_groups_group_id.delete import ApiFordelete


class V1GroupsGroupId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
