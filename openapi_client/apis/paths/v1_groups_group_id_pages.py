from openapi_client.paths.v1_groups_group_id_pages.get import ApiForget
from openapi_client.paths.v1_groups_group_id_pages.post import ApiForpost


class V1GroupsGroupIdPages(
    ApiForget,
    ApiForpost,
):
    pass
