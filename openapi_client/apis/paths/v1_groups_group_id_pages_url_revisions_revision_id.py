from openapi_client.paths.v1_groups_group_id_pages_url_revisions_revision_id.get import ApiForget
from openapi_client.paths.v1_groups_group_id_pages_url_revisions_revision_id.post import ApiForpost


class V1GroupsGroupIdPagesUrlRevisionsRevisionId(
    ApiForget,
    ApiForpost,
):
    pass
