from openapi_client.paths.v1_courses_course_id_features_flags_feature.get import ApiForget
from openapi_client.paths.v1_courses_course_id_features_flags_feature.put import ApiForput
from openapi_client.paths.v1_courses_course_id_features_flags_feature.delete import ApiFordelete


class V1CoursesCourseIdFeaturesFlagsFeature(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
