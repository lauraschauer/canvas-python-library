from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_read_all.put import ApiForput
from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_read_all.delete import ApiFordelete


class V1GroupsGroupIdDiscussionTopicsTopicIdReadAll(
    ApiForput,
    ApiFordelete,
):
    pass
