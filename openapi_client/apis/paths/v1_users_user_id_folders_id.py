from openapi_client.paths.v1_users_user_id_folders_id.get import ApiForget


class V1UsersUserIdFoldersId(
    ApiForget,
):
    pass
