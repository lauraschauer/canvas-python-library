from openapi_client.paths.v1_accounts_account_id_enrollments_id.get import ApiForget


class V1AccountsAccountIdEnrollmentsId(
    ApiForget,
):
    pass
