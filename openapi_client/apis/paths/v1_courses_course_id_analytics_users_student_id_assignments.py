from openapi_client.paths.v1_courses_course_id_analytics_users_student_id_assignments.get import ApiForget


class V1CoursesCourseIdAnalyticsUsersStudentIdAssignments(
    ApiForget,
):
    pass
