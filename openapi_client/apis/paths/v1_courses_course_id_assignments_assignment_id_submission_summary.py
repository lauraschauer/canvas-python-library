from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_submission_summary.get import ApiForget


class V1CoursesCourseIdAssignmentsAssignmentIdSubmissionSummary(
    ApiForget,
):
    pass
