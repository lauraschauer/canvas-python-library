from openapi_client.paths.v1_courses_course_id_features_enabled.get import ApiForget


class V1CoursesCourseIdFeaturesEnabled(
    ApiForget,
):
    pass
