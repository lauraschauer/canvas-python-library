from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_entries_entry_id_rating.post import ApiForpost


class V1CoursesCourseIdDiscussionTopicsTopicIdEntriesEntryIdRating(
    ApiForpost,
):
    pass
