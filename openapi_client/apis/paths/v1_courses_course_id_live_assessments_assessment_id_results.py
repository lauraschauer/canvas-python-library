from openapi_client.paths.v1_courses_course_id_live_assessments_assessment_id_results.get import ApiForget
from openapi_client.paths.v1_courses_course_id_live_assessments_assessment_id_results.post import ApiForpost


class V1CoursesCourseIdLiveAssessmentsAssessmentIdResults(
    ApiForget,
    ApiForpost,
):
    pass
