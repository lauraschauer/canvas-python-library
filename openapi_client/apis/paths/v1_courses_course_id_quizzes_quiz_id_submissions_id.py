from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_submissions_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_submissions_id.put import ApiForput


class V1CoursesCourseIdQuizzesQuizIdSubmissionsId(
    ApiForget,
    ApiForput,
):
    pass
