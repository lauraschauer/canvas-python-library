from openapi_client.paths.v1_courses_course_id_pages_url_revisions_latest.get import ApiForget


class V1CoursesCourseIdPagesUrlRevisionsLatest(
    ApiForget,
):
    pass
