from openapi_client.paths.v1_accounts_account_id_sis_imports_id_restore_states.put import ApiForput


class V1AccountsAccountIdSisImportsIdRestoreStates(
    ApiForput,
):
    pass
