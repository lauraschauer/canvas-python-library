from openapi_client.paths.v1_jwts_refresh.post import ApiForpost


class V1JwtsRefresh(
    ApiForpost,
):
    pass
