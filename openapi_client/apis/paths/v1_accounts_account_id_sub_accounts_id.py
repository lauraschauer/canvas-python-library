from openapi_client.paths.v1_accounts_account_id_sub_accounts_id.delete import ApiFordelete


class V1AccountsAccountIdSubAccountsId(
    ApiFordelete,
):
    pass
