from openapi_client.paths.v1_accounts_account_id_grading_periods.get import ApiForget


class V1AccountsAccountIdGradingPeriods(
    ApiForget,
):
    pass
