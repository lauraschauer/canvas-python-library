from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_groups_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_groups_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_groups_id.delete import ApiFordelete


class V1CoursesCourseIdQuizzesQuizIdGroupsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
