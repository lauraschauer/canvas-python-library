from openapi_client.paths.v1_accounts_account_id_analytics_current_statistics.get import ApiForget


class V1AccountsAccountIdAnalyticsCurrentStatistics(
    ApiForget,
):
    pass
