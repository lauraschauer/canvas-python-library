from openapi_client.paths.v1_courses_course_id_content_exports_id.get import ApiForget


class V1CoursesCourseIdContentExportsId(
    ApiForget,
):
    pass
