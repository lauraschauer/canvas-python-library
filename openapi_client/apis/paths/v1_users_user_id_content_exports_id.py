from openapi_client.paths.v1_users_user_id_content_exports_id.get import ApiForget


class V1UsersUserIdContentExportsId(
    ApiForget,
):
    pass
