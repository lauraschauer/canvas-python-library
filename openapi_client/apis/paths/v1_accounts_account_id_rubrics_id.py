from openapi_client.paths.v1_accounts_account_id_rubrics_id.get import ApiForget


class V1AccountsAccountIdRubricsId(
    ApiForget,
):
    pass
