from openapi_client.paths.v1_polls_poll_id_poll_sessions_id.get import ApiForget
from openapi_client.paths.v1_polls_poll_id_poll_sessions_id.put import ApiForput
from openapi_client.paths.v1_polls_poll_id_poll_sessions_id.delete import ApiFordelete


class V1PollsPollIdPollSessionsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
