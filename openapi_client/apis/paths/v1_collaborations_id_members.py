from openapi_client.paths.v1_collaborations_id_members.get import ApiForget


class V1CollaborationsIdMembers(
    ApiForget,
):
    pass
