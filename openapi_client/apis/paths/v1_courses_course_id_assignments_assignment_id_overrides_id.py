from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_overrides_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_overrides_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_overrides_id.delete import ApiFordelete


class V1CoursesCourseIdAssignmentsAssignmentIdOverridesId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
