from openapi_client.paths.v1_courses_course_id_discussion_topics.get import ApiForget
from openapi_client.paths.v1_courses_course_id_discussion_topics.post import ApiForpost


class V1CoursesCourseIdDiscussionTopics(
    ApiForget,
    ApiForpost,
):
    pass
