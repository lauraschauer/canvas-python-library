from openapi_client.paths.v1_users_self_groups.get import ApiForget


class V1UsersSelfGroups(
    ApiForget,
):
    pass
