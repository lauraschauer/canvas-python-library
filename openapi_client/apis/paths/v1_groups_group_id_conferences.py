from openapi_client.paths.v1_groups_group_id_conferences.get import ApiForget


class V1GroupsGroupIdConferences(
    ApiForget,
):
    pass
