from openapi_client.paths.v1_planner_overrides.get import ApiForget
from openapi_client.paths.v1_planner_overrides.post import ApiForpost


class V1PlannerOverrides(
    ApiForget,
    ApiForpost,
):
    pass
