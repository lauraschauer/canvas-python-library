from openapi_client.paths.v1_courses_course_id_pages_url_revisions.get import ApiForget


class V1CoursesCourseIdPagesUrlRevisions(
    ApiForget,
):
    pass
