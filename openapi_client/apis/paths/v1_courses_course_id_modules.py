from openapi_client.paths.v1_courses_course_id_modules.get import ApiForget
from openapi_client.paths.v1_courses_course_id_modules.post import ApiForpost


class V1CoursesCourseIdModules(
    ApiForget,
    ApiForpost,
):
    pass
