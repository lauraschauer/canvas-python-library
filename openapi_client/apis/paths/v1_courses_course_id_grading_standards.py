from openapi_client.paths.v1_courses_course_id_grading_standards.get import ApiForget
from openapi_client.paths.v1_courses_course_id_grading_standards.post import ApiForpost


class V1CoursesCourseIdGradingStandards(
    ApiForget,
    ApiForpost,
):
    pass
