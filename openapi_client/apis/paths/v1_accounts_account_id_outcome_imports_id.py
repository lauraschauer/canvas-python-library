from openapi_client.paths.v1_accounts_account_id_outcome_imports_id.get import ApiForget


class V1AccountsAccountIdOutcomeImportsId(
    ApiForget,
):
    pass
