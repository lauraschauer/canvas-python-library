from openapi_client.paths.v1_polls_poll_id_poll_choices_id.get import ApiForget
from openapi_client.paths.v1_polls_poll_id_poll_choices_id.put import ApiForput
from openapi_client.paths.v1_polls_poll_id_poll_choices_id.delete import ApiFordelete


class V1PollsPollIdPollChoicesId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
