from openapi_client.paths.v1_courses_course_id_root_outcome_group.get import ApiForget


class V1CoursesCourseIdRootOutcomeGroup(
    ApiForget,
):
    pass
