from openapi_client.paths.v1_groups_group_id_potential_collaborators.get import ApiForget


class V1GroupsGroupIdPotentialCollaborators(
    ApiForget,
):
    pass
