from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_reports_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_reports_id.delete import ApiFordelete


class V1CoursesCourseIdQuizzesQuizIdReportsId(
    ApiForget,
    ApiFordelete,
):
    pass
