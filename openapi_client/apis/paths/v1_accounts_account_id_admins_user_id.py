from openapi_client.paths.v1_accounts_account_id_admins_user_id.delete import ApiFordelete


class V1AccountsAccountIdAdminsUserId(
    ApiFordelete,
):
    pass
