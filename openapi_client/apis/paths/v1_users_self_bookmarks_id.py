from openapi_client.paths.v1_users_self_bookmarks_id.get import ApiForget
from openapi_client.paths.v1_users_self_bookmarks_id.put import ApiForput
from openapi_client.paths.v1_users_self_bookmarks_id.delete import ApiFordelete


class V1UsersSelfBookmarksId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
