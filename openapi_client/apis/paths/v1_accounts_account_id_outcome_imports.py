from openapi_client.paths.v1_accounts_account_id_outcome_imports.post import ApiForpost


class V1AccountsAccountIdOutcomeImports(
    ApiForpost,
):
    pass
