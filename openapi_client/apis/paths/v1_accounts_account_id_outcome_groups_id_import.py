from openapi_client.paths.v1_accounts_account_id_outcome_groups_id_import.post import ApiForpost


class V1AccountsAccountIdOutcomeGroupsIdImport(
    ApiForpost,
):
    pass
