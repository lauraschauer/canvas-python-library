from openapi_client.paths.v1_groups_group_id_pages_url.get import ApiForget
from openapi_client.paths.v1_groups_group_id_pages_url.put import ApiForput
from openapi_client.paths.v1_groups_group_id_pages_url.delete import ApiFordelete


class V1GroupsGroupIdPagesUrl(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
