from openapi_client.paths.lti_assignments_assignment_id_files_file_id_originality_report.get import ApiForget
from openapi_client.paths.lti_assignments_assignment_id_files_file_id_originality_report.put import ApiForput


class LtiAssignmentsAssignmentIdFilesFileIdOriginalityReport(
    ApiForget,
    ApiForput,
):
    pass
