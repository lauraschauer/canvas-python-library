from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_submissions_id_complete.post import ApiForpost


class V1CoursesCourseIdQuizzesQuizIdSubmissionsIdComplete(
    ApiForpost,
):
    pass
