from openapi_client.paths.v1_groups_group_id_content_exports.get import ApiForget
from openapi_client.paths.v1_groups_group_id_content_exports.post import ApiForpost


class V1GroupsGroupIdContentExports(
    ApiForget,
    ApiForpost,
):
    pass
