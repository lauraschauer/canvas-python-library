from openapi_client.paths.v1_accounts_account_id_sis_imports_id_errors.get import ApiForget


class V1AccountsAccountIdSisImportsIdErrors(
    ApiForget,
):
    pass
