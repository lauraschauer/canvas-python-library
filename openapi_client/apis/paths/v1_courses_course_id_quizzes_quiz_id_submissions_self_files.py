from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_submissions_self_files.post import ApiForpost


class V1CoursesCourseIdQuizzesQuizIdSubmissionsSelfFiles(
    ApiForpost,
):
    pass
