from openapi_client.paths.v1_groups_group_id_content_migrations_content_migration_id_migration_issues_id.get import ApiForget
from openapi_client.paths.v1_groups_group_id_content_migrations_content_migration_id_migration_issues_id.put import ApiForput


class V1GroupsGroupIdContentMigrationsContentMigrationIdMigrationIssuesId(
    ApiForget,
    ApiForput,
):
    pass
