from openapi_client.paths.v1_courses_course_id_files.get import ApiForget


class V1CoursesCourseIdFiles(
    ApiForget,
):
    pass
