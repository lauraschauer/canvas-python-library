from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_provisional_grades_bulk_select.put import ApiForput


class V1CoursesCourseIdAssignmentsAssignmentIdProvisionalGradesBulkSelect(
    ApiForput,
):
    pass
