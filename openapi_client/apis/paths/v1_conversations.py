from openapi_client.paths.v1_conversations.get import ApiForget
from openapi_client.paths.v1_conversations.put import ApiForput
from openapi_client.paths.v1_conversations.post import ApiForpost


class V1Conversations(
    ApiForget,
    ApiForput,
    ApiForpost,
):
    pass
