from openapi_client.paths.v1_courses_course_id_usage_rights.put import ApiForput
from openapi_client.paths.v1_courses_course_id_usage_rights.delete import ApiFordelete


class V1CoursesCourseIdUsageRights(
    ApiForput,
    ApiFordelete,
):
    pass
