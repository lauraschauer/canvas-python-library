from openapi_client.paths.v1_appointment_groups.get import ApiForget
from openapi_client.paths.v1_appointment_groups.post import ApiForpost


class V1AppointmentGroups(
    ApiForget,
    ApiForpost,
):
    pass
