from openapi_client.paths.v1_courses_course_id_todo.get import ApiForget


class V1CoursesCourseIdTodo(
    ApiForget,
):
    pass
