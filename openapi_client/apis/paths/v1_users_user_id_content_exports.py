from openapi_client.paths.v1_users_user_id_content_exports.get import ApiForget
from openapi_client.paths.v1_users_user_id_content_exports.post import ApiForpost


class V1UsersUserIdContentExports(
    ApiForget,
    ApiForpost,
):
    pass
