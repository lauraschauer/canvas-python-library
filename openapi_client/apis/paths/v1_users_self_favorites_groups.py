from openapi_client.paths.v1_users_self_favorites_groups.get import ApiForget
from openapi_client.paths.v1_users_self_favorites_groups.delete import ApiFordelete


class V1UsersSelfFavoritesGroups(
    ApiForget,
    ApiFordelete,
):
    pass
