from openapi_client.paths.lti_users_id.get import ApiForget


class LtiUsersId(
    ApiForget,
):
    pass
