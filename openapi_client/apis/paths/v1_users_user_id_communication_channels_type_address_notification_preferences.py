from openapi_client.paths.v1_users_user_id_communication_channels_type_address_notification_preferences.get import ApiForget


class V1UsersUserIdCommunicationChannelsTypeAddressNotificationPreferences(
    ApiForget,
):
    pass
