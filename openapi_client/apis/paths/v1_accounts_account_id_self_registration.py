from openapi_client.paths.v1_accounts_account_id_self_registration.post import ApiForpost


class V1AccountsAccountIdSelfRegistration(
    ApiForpost,
):
    pass
