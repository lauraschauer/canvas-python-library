from openapi_client.paths.v1_users_user_id_observees.get import ApiForget
from openapi_client.paths.v1_users_user_id_observees.post import ApiForpost


class V1UsersUserIdObservees(
    ApiForget,
    ApiForpost,
):
    pass
