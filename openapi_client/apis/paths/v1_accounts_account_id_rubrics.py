from openapi_client.paths.v1_accounts_account_id_rubrics.get import ApiForget


class V1AccountsAccountIdRubrics(
    ApiForget,
):
    pass
