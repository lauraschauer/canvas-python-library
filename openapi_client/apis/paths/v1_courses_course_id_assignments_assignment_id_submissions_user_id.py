from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_submissions_user_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_submissions_user_id.put import ApiForput


class V1CoursesCourseIdAssignmentsAssignmentIdSubmissionsUserId(
    ApiForget,
    ApiForput,
):
    pass
