from openapi_client.paths.v1_courses_course_id_live_assessments.get import ApiForget
from openapi_client.paths.v1_courses_course_id_live_assessments.post import ApiForpost


class V1CoursesCourseIdLiveAssessments(
    ApiForget,
    ApiForpost,
):
    pass
