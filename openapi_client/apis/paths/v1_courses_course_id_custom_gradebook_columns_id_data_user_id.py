from openapi_client.paths.v1_courses_course_id_custom_gradebook_columns_id_data_user_id.put import ApiForput


class V1CoursesCourseIdCustomGradebookColumnsIdDataUserId(
    ApiForput,
):
    pass
