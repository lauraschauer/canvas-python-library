from openapi_client.paths.v1_groups_group_id_users_user_id.get import ApiForget
from openapi_client.paths.v1_groups_group_id_users_user_id.put import ApiForput
from openapi_client.paths.v1_groups_group_id_users_user_id.delete import ApiFordelete


class V1GroupsGroupIdUsersUserId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
