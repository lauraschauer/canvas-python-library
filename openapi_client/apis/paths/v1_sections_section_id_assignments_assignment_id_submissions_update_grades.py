from openapi_client.paths.v1_sections_section_id_assignments_assignment_id_submissions_update_grades.post import ApiForpost


class V1SectionsSectionIdAssignmentsAssignmentIdSubmissionsUpdateGrades(
    ApiForpost,
):
    pass
