from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_statistics.get import ApiForget


class V1CoursesCourseIdQuizzesQuizIdStatistics(
    ApiForget,
):
    pass
