from openapi_client.paths.v1_courses_course_id_module_item_sequence.get import ApiForget


class V1CoursesCourseIdModuleItemSequence(
    ApiForget,
):
    pass
