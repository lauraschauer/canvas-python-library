from openapi_client.paths.v1_courses_course_id_course_copy_id.get import ApiForget


class V1CoursesCourseIdCourseCopyId(
    ApiForget,
):
    pass
