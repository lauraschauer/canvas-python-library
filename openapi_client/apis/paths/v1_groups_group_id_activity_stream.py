from openapi_client.paths.v1_groups_group_id_activity_stream.get import ApiForget


class V1GroupsGroupIdActivityStream(
    ApiForget,
):
    pass
