from openapi_client.paths.v1_courses_course_id_external_feeds_external_feed_id.delete import ApiFordelete


class V1CoursesCourseIdExternalFeedsExternalFeedId(
    ApiFordelete,
):
    pass
