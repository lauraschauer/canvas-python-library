from openapi_client.paths.v1_accounts_account_id_content_migrations_content_migration_id_migration_issues.get import ApiForget


class V1AccountsAccountIdContentMigrationsContentMigrationIdMigrationIssues(
    ApiForget,
):
    pass
