from openapi_client.paths.v1_courses_course_id_grading_periods.get import ApiForget


class V1CoursesCourseIdGradingPeriods(
    ApiForget,
):
    pass
