from openapi_client.paths.v1_groups_group_id_usage_rights.put import ApiForput
from openapi_client.paths.v1_groups_group_id_usage_rights.delete import ApiFordelete


class V1GroupsGroupIdUsageRights(
    ApiForput,
    ApiFordelete,
):
    pass
