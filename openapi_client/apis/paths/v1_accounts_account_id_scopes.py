from openapi_client.paths.v1_accounts_account_id_scopes.get import ApiForget


class V1AccountsAccountIdScopes(
    ApiForget,
):
    pass
