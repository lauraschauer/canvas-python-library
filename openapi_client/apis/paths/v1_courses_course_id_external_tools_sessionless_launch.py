from openapi_client.paths.v1_courses_course_id_external_tools_sessionless_launch.get import ApiForget


class V1CoursesCourseIdExternalToolsSessionlessLaunch(
    ApiForget,
):
    pass
