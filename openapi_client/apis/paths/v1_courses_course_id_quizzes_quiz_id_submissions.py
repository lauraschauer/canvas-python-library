from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_submissions.get import ApiForget
from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_submissions.post import ApiForpost


class V1CoursesCourseIdQuizzesQuizIdSubmissions(
    ApiForget,
    ApiForpost,
):
    pass
