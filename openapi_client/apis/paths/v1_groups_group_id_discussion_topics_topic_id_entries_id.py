from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_entries_id.put import ApiForput
from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_entries_id.delete import ApiFordelete


class V1GroupsGroupIdDiscussionTopicsTopicIdEntriesId(
    ApiForput,
    ApiFordelete,
):
    pass
