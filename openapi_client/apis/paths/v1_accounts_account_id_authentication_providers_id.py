from openapi_client.paths.v1_accounts_account_id_authentication_providers_id.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_authentication_providers_id.put import ApiForput
from openapi_client.paths.v1_accounts_account_id_authentication_providers_id.delete import ApiFordelete


class V1AccountsAccountIdAuthenticationProvidersId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
