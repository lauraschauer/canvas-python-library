from openapi_client.paths.v1_courses_course_id_modules_id_relock.put import ApiForput


class V1CoursesCourseIdModulesIdRelock(
    ApiForput,
):
    pass
