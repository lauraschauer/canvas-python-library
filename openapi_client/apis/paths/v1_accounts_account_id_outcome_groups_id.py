from openapi_client.paths.v1_accounts_account_id_outcome_groups_id.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_outcome_groups_id.put import ApiForput
from openapi_client.paths.v1_accounts_account_id_outcome_groups_id.delete import ApiFordelete


class V1AccountsAccountIdOutcomeGroupsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
