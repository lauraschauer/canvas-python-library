from openapi_client.paths.v1_groups_group_id_pages_url_revisions.get import ApiForget


class V1GroupsGroupIdPagesUrlRevisions(
    ApiForget,
):
    pass
