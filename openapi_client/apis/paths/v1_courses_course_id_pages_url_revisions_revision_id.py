from openapi_client.paths.v1_courses_course_id_pages_url_revisions_revision_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_pages_url_revisions_revision_id.post import ApiForpost


class V1CoursesCourseIdPagesUrlRevisionsRevisionId(
    ApiForget,
    ApiForpost,
):
    pass
