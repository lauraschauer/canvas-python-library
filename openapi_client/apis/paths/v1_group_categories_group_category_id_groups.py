from openapi_client.paths.v1_group_categories_group_category_id_groups.post import ApiForpost


class V1GroupCategoriesGroupCategoryIdGroups(
    ApiForpost,
):
    pass
