from openapi_client.paths.v1_courses_course_id_enrollments_id_reject.post import ApiForpost


class V1CoursesCourseIdEnrollmentsIdReject(
    ApiForpost,
):
    pass
