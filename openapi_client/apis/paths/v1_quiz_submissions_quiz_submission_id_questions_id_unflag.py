from openapi_client.paths.v1_quiz_submissions_quiz_submission_id_questions_id_unflag.put import ApiForput


class V1QuizSubmissionsQuizSubmissionIdQuestionsIdUnflag(
    ApiForput,
):
    pass
