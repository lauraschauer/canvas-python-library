from openapi_client.paths.v1_accounts_account_id_authentication_providers.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_authentication_providers.post import ApiForpost


class V1AccountsAccountIdAuthenticationProviders(
    ApiForget,
    ApiForpost,
):
    pass
