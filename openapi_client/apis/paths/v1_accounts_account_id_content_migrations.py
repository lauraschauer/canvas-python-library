from openapi_client.paths.v1_accounts_account_id_content_migrations.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_content_migrations.post import ApiForpost


class V1AccountsAccountIdContentMigrations(
    ApiForget,
    ApiForpost,
):
    pass
