from openapi_client.paths.v1_courses_course_id_search_users.get import ApiForget


class V1CoursesCourseIdSearchUsers(
    ApiForget,
):
    pass
