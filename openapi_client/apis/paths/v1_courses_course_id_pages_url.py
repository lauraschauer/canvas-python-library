from openapi_client.paths.v1_courses_course_id_pages_url.get import ApiForget
from openapi_client.paths.v1_courses_course_id_pages_url.put import ApiForput
from openapi_client.paths.v1_courses_course_id_pages_url.delete import ApiFordelete


class V1CoursesCourseIdPagesUrl(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
