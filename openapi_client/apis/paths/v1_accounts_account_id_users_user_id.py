from openapi_client.paths.v1_accounts_account_id_users_user_id.delete import ApiFordelete


class V1AccountsAccountIdUsersUserId(
    ApiFordelete,
):
    pass
