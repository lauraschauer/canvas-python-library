from openapi_client.paths.v1_accounts_search.get import ApiForget


class V1AccountsSearch(
    ApiForget,
):
    pass
