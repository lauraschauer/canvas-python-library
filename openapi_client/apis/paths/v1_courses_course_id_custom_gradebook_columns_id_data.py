from openapi_client.paths.v1_courses_course_id_custom_gradebook_columns_id_data.get import ApiForget


class V1CoursesCourseIdCustomGradebookColumnsIdData(
    ApiForget,
):
    pass
