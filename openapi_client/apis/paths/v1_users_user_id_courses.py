from openapi_client.paths.v1_users_user_id_courses.get import ApiForget


class V1UsersUserIdCourses(
    ApiForget,
):
    pass
