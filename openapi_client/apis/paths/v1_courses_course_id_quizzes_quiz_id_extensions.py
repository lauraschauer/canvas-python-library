from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_extensions.post import ApiForpost


class V1CoursesCourseIdQuizzesQuizIdExtensions(
    ApiForpost,
):
    pass
