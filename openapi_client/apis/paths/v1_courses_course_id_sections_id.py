from openapi_client.paths.v1_courses_course_id_sections_id.get import ApiForget


class V1CoursesCourseIdSectionsId(
    ApiForget,
):
    pass
