from openapi_client.paths.v1_courses_course_id_users_user_id_last_attended.put import ApiForput


class V1CoursesCourseIdUsersUserIdLastAttended(
    ApiForput,
):
    pass
