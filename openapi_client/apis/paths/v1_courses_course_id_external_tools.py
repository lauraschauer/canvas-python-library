from openapi_client.paths.v1_courses_course_id_external_tools.get import ApiForget
from openapi_client.paths.v1_courses_course_id_external_tools.post import ApiForpost


class V1CoursesCourseIdExternalTools(
    ApiForget,
    ApiForpost,
):
    pass
