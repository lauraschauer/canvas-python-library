from openapi_client.paths.lti_assignments_assignment_id_submissions_submission_id_history.get import ApiForget


class LtiAssignmentsAssignmentIdSubmissionsSubmissionIdHistory(
    ApiForget,
):
    pass
