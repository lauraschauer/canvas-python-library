from openapi_client.paths.v1_courses_course_id_blueprint_subscriptions_subscription_id_migrations.get import ApiForget


class V1CoursesCourseIdBlueprintSubscriptionsSubscriptionIdMigrations(
    ApiForget,
):
    pass
