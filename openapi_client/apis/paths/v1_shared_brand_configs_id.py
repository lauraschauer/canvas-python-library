from openapi_client.paths.v1_shared_brand_configs_id.delete import ApiFordelete


class V1SharedBrandConfigsId(
    ApiFordelete,
):
    pass
