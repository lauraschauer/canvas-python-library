from openapi_client.paths.v1_audit_authentication_accounts_account_id.get import ApiForget


class V1AuditAuthenticationAccountsAccountId(
    ApiForget,
):
    pass
