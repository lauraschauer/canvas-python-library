from openapi_client.paths.v1_appointment_groups_id_groups.get import ApiForget


class V1AppointmentGroupsIdGroups(
    ApiForget,
):
    pass
