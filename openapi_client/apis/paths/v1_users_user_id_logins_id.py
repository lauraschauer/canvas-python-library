from openapi_client.paths.v1_users_user_id_logins_id.delete import ApiFordelete


class V1UsersUserIdLoginsId(
    ApiFordelete,
):
    pass
