from openapi_client.paths.v1_accounts_account_id_grading_standards_grading_standard_id.get import ApiForget


class V1AccountsAccountIdGradingStandardsGradingStandardId(
    ApiForget,
):
    pass
