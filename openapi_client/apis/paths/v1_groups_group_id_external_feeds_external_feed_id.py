from openapi_client.paths.v1_groups_group_id_external_feeds_external_feed_id.delete import ApiFordelete


class V1GroupsGroupIdExternalFeedsExternalFeedId(
    ApiFordelete,
):
    pass
