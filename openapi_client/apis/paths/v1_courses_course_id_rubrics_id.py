from openapi_client.paths.v1_courses_course_id_rubrics_id.get import ApiForget


class V1CoursesCourseIdRubricsId(
    ApiForget,
):
    pass
