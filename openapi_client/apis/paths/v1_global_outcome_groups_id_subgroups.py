from openapi_client.paths.v1_global_outcome_groups_id_subgroups.get import ApiForget
from openapi_client.paths.v1_global_outcome_groups_id_subgroups.post import ApiForpost


class V1GlobalOutcomeGroupsIdSubgroups(
    ApiForget,
    ApiForpost,
):
    pass
