from openapi_client.paths.v1_groups_group_id_front_page.get import ApiForget
from openapi_client.paths.v1_groups_group_id_front_page.put import ApiForput


class V1GroupsGroupIdFrontPage(
    ApiForget,
    ApiForput,
):
    pass
