from openapi_client.paths.v1_conversations_batches.get import ApiForget


class V1ConversationsBatches(
    ApiForget,
):
    pass
