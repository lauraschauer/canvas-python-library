from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_groups.post import ApiForpost


class V1CoursesCourseIdQuizzesQuizIdGroups(
    ApiForpost,
):
    pass
