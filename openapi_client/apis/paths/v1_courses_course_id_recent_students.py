from openapi_client.paths.v1_courses_course_id_recent_students.get import ApiForget


class V1CoursesCourseIdRecentStudents(
    ApiForget,
):
    pass
