from openapi_client.paths.v1_courses_course_id_calendar_events_timetable.get import ApiForget
from openapi_client.paths.v1_courses_course_id_calendar_events_timetable.post import ApiForpost


class V1CoursesCourseIdCalendarEventsTimetable(
    ApiForget,
    ApiForpost,
):
    pass
