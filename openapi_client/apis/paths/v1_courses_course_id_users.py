from openapi_client.paths.v1_courses_course_id_users.get import ApiForget


class V1CoursesCourseIdUsers(
    ApiForget,
):
    pass
