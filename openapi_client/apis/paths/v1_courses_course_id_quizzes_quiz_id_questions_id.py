from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_questions_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_questions_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_questions_id.delete import ApiFordelete


class V1CoursesCourseIdQuizzesQuizIdQuestionsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
