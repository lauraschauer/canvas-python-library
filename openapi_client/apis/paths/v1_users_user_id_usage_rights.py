from openapi_client.paths.v1_users_user_id_usage_rights.put import ApiForput
from openapi_client.paths.v1_users_user_id_usage_rights.delete import ApiFordelete


class V1UsersUserIdUsageRights(
    ApiForput,
    ApiFordelete,
):
    pass
