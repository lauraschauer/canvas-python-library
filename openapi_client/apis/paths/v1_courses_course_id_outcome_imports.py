from openapi_client.paths.v1_courses_course_id_outcome_imports.post import ApiForpost


class V1CoursesCourseIdOutcomeImports(
    ApiForpost,
):
    pass
