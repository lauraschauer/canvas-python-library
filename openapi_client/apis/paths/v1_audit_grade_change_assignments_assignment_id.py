from openapi_client.paths.v1_audit_grade_change_assignments_assignment_id.get import ApiForget


class V1AuditGradeChangeAssignmentsAssignmentId(
    ApiForget,
):
    pass
