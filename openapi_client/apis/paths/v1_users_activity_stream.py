from openapi_client.paths.v1_users_activity_stream.get import ApiForget


class V1UsersActivityStream(
    ApiForget,
):
    pass
