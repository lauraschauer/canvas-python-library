from openapi_client.paths.v1_accounts_account_id_sis_import_errors.get import ApiForget


class V1AccountsAccountIdSisImportErrors(
    ApiForget,
):
    pass
