from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_entry_list.get import ApiForget


class V1GroupsGroupIdDiscussionTopicsTopicIdEntryList(
    ApiForget,
):
    pass
