from openapi_client.paths.v1_groups_group_id_collaborations.get import ApiForget


class V1GroupsGroupIdCollaborations(
    ApiForget,
):
    pass
