from openapi_client.paths.v1_courses_course_id_files_id.get import ApiForget


class V1CoursesCourseIdFilesId(
    ApiForget,
):
    pass
