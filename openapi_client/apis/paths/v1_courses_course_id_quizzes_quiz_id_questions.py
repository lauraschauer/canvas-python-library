from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_questions.get import ApiForget
from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_questions.post import ApiForpost


class V1CoursesCourseIdQuizzesQuizIdQuestions(
    ApiForget,
    ApiForpost,
):
    pass
