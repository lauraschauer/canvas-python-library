from openapi_client.paths.v1_courses_course_id_blueprint_templates_template_id_migrations.get import ApiForget
from openapi_client.paths.v1_courses_course_id_blueprint_templates_template_id_migrations.post import ApiForpost


class V1CoursesCourseIdBlueprintTemplatesTemplateIdMigrations(
    ApiForget,
    ApiForpost,
):
    pass
