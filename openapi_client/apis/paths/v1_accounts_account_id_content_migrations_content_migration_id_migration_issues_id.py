from openapi_client.paths.v1_accounts_account_id_content_migrations_content_migration_id_migration_issues_id.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_content_migrations_content_migration_id_migration_issues_id.put import ApiForput


class V1AccountsAccountIdContentMigrationsContentMigrationIdMigrationIssuesId(
    ApiForget,
    ApiForput,
):
    pass
