from openapi_client.paths.v1_users_user_id_content_migrations_content_migration_id_migration_issues.get import ApiForget


class V1UsersUserIdContentMigrationsContentMigrationIdMigrationIssues(
    ApiForget,
):
    pass
