from openapi_client.paths.v1_courses_course_id_analytics_users_student_id_communication.get import ApiForget


class V1CoursesCourseIdAnalyticsUsersStudentIdCommunication(
    ApiForget,
):
    pass
