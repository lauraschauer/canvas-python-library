from openapi_client.paths.v1_services_kaltura_session.post import ApiForpost


class V1ServicesKalturaSession(
    ApiForpost,
):
    pass
