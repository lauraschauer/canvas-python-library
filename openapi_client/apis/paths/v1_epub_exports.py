from openapi_client.paths.v1_epub_exports.get import ApiForget


class V1EpubExports(
    ApiForget,
):
    pass
