from openapi_client.paths.v1_courses_course_id_settings.get import ApiForget
from openapi_client.paths.v1_courses_course_id_settings.put import ApiForput


class V1CoursesCourseIdSettings(
    ApiForget,
    ApiForput,
):
    pass
