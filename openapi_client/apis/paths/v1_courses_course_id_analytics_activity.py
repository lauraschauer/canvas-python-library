from openapi_client.paths.v1_courses_course_id_analytics_activity.get import ApiForget


class V1CoursesCourseIdAnalyticsActivity(
    ApiForget,
):
    pass
