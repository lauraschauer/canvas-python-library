from openapi_client.paths.sis_accounts_account_id_assignments.get import ApiForget


class SisAccountsAccountIdAssignments(
    ApiForget,
):
    pass
