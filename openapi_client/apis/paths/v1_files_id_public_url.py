from openapi_client.paths.v1_files_id_public_url.get import ApiForget


class V1FilesIdPublicUrl(
    ApiForget,
):
    pass
