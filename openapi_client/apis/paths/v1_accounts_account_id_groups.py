from openapi_client.paths.v1_accounts_account_id_groups.get import ApiForget


class V1AccountsAccountIdGroups(
    ApiForget,
):
    pass
