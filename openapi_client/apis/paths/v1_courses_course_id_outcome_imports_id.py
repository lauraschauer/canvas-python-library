from openapi_client.paths.v1_courses_course_id_outcome_imports_id.get import ApiForget


class V1CoursesCourseIdOutcomeImportsId(
    ApiForget,
):
    pass
