from openapi_client.paths.v1_jwts.post import ApiForpost


class V1Jwts(
    ApiForpost,
):
    pass
