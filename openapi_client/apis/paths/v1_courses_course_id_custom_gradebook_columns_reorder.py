from openapi_client.paths.v1_courses_course_id_custom_gradebook_columns_reorder.post import ApiForpost


class V1CoursesCourseIdCustomGradebookColumnsReorder(
    ApiForpost,
):
    pass
