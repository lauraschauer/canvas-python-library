from openapi_client.paths.v1_brand_variables.get import ApiForget


class V1BrandVariables(
    ApiForget,
):
    pass
