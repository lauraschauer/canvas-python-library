from openapi_client.paths.v1_calendar_events_id_reservations_participant_id.post import ApiForpost


class V1CalendarEventsIdReservationsParticipantId(
    ApiForpost,
):
    pass
