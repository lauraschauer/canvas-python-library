from openapi_client.paths.v1_users_self_favorites_groups_id.post import ApiForpost
from openapi_client.paths.v1_users_self_favorites_groups_id.delete import ApiFordelete


class V1UsersSelfFavoritesGroupsId(
    ApiForpost,
    ApiFordelete,
):
    pass
