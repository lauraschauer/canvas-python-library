from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_entries_entry_id_rating.post import ApiForpost


class V1GroupsGroupIdDiscussionTopicsTopicIdEntriesEntryIdRating(
    ApiForpost,
):
    pass
