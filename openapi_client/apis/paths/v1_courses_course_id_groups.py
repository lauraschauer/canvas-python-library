from openapi_client.paths.v1_courses_course_id_groups.get import ApiForget


class V1CoursesCourseIdGroups(
    ApiForget,
):
    pass
