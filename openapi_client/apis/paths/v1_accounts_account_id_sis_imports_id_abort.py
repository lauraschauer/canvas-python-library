from openapi_client.paths.v1_accounts_account_id_sis_imports_id_abort.put import ApiForput


class V1AccountsAccountIdSisImportsIdAbort(
    ApiForput,
):
    pass
