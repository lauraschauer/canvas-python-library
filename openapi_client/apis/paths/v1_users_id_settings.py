from openapi_client.paths.v1_users_id_settings.get import ApiForget


class V1UsersIdSettings(
    ApiForget,
):
    pass
