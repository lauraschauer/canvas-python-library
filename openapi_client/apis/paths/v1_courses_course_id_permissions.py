from openapi_client.paths.v1_courses_course_id_permissions.get import ApiForget


class V1CoursesCourseIdPermissions(
    ApiForget,
):
    pass
