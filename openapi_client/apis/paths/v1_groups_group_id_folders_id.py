from openapi_client.paths.v1_groups_group_id_folders_id.get import ApiForget


class V1GroupsGroupIdFoldersId(
    ApiForget,
):
    pass
