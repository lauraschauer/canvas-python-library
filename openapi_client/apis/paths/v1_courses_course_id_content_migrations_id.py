from openapi_client.paths.v1_courses_course_id_content_migrations_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_content_migrations_id.put import ApiForput


class V1CoursesCourseIdContentMigrationsId(
    ApiForget,
    ApiForput,
):
    pass
