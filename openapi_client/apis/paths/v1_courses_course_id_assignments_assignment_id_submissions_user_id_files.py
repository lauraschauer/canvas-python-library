from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_submissions_user_id_files.post import ApiForpost


class V1CoursesCourseIdAssignmentsAssignmentIdSubmissionsUserIdFiles(
    ApiForpost,
):
    pass
