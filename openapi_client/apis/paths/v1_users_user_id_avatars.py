from openapi_client.paths.v1_users_user_id_avatars.get import ApiForget


class V1UsersUserIdAvatars(
    ApiForget,
):
    pass
