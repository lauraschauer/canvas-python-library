from openapi_client.paths.v1_planner_items.get import ApiForget


class V1PlannerItems(
    ApiForget,
):
    pass
