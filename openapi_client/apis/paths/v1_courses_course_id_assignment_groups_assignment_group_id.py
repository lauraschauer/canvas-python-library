from openapi_client.paths.v1_courses_course_id_assignment_groups_assignment_group_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_assignment_groups_assignment_group_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_assignment_groups_assignment_group_id.delete import ApiFordelete


class V1CoursesCourseIdAssignmentGroupsAssignmentGroupId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
