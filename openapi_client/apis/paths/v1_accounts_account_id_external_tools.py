from openapi_client.paths.v1_accounts_account_id_external_tools.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_external_tools.post import ApiForpost


class V1AccountsAccountIdExternalTools(
    ApiForget,
    ApiForpost,
):
    pass
