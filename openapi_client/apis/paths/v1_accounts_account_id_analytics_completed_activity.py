from openapi_client.paths.v1_accounts_account_id_analytics_completed_activity.get import ApiForget


class V1AccountsAccountIdAnalyticsCompletedActivity(
    ApiForget,
):
    pass
