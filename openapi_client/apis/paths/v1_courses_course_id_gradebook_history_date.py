from openapi_client.paths.v1_courses_course_id_gradebook_history_date.get import ApiForget


class V1CoursesCourseIdGradebookHistoryDate(
    ApiForget,
):
    pass
