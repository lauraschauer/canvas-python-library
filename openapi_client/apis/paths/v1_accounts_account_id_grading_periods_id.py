from openapi_client.paths.v1_accounts_account_id_grading_periods_id.delete import ApiFordelete


class V1AccountsAccountIdGradingPeriodsId(
    ApiFordelete,
):
    pass
