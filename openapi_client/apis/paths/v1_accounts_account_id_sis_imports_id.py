from openapi_client.paths.v1_accounts_account_id_sis_imports_id.get import ApiForget


class V1AccountsAccountIdSisImportsId(
    ApiForget,
):
    pass
