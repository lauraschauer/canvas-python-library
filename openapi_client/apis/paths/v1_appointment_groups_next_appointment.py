from openapi_client.paths.v1_appointment_groups_next_appointment.get import ApiForget


class V1AppointmentGroupsNextAppointment(
    ApiForget,
):
    pass
