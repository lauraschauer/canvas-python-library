from openapi_client.paths.v1_users_self_activity_stream_summary.get import ApiForget


class V1UsersSelfActivityStreamSummary(
    ApiForget,
):
    pass
