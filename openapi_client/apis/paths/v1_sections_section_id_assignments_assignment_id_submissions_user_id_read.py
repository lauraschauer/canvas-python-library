from openapi_client.paths.v1_sections_section_id_assignments_assignment_id_submissions_user_id_read.put import ApiForput
from openapi_client.paths.v1_sections_section_id_assignments_assignment_id_submissions_user_id_read.delete import ApiFordelete


class V1SectionsSectionIdAssignmentsAssignmentIdSubmissionsUserIdRead(
    ApiForput,
    ApiFordelete,
):
    pass
