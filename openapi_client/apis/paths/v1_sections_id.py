from openapi_client.paths.v1_sections_id.get import ApiForget
from openapi_client.paths.v1_sections_id.put import ApiForput
from openapi_client.paths.v1_sections_id.delete import ApiFordelete


class V1SectionsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
