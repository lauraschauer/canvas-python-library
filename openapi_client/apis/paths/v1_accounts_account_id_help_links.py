from openapi_client.paths.v1_accounts_account_id_help_links.get import ApiForget


class V1AccountsAccountIdHelpLinks(
    ApiForget,
):
    pass
