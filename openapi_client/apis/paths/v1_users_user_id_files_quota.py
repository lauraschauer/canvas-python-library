from openapi_client.paths.v1_users_user_id_files_quota.get import ApiForget


class V1UsersUserIdFilesQuota(
    ApiForget,
):
    pass
