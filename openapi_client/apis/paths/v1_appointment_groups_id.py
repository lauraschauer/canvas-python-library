from openapi_client.paths.v1_appointment_groups_id.get import ApiForget
from openapi_client.paths.v1_appointment_groups_id.put import ApiForput
from openapi_client.paths.v1_appointment_groups_id.delete import ApiFordelete


class V1AppointmentGroupsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
