from openapi_client.paths.v1_accounts_account_id_reports_report.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_reports_report.post import ApiForpost


class V1AccountsAccountIdReportsReport(
    ApiForget,
    ApiForpost,
):
    pass
