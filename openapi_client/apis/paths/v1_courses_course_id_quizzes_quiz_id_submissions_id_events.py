from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_submissions_id_events.get import ApiForget
from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_submissions_id_events.post import ApiForpost


class V1CoursesCourseIdQuizzesQuizIdSubmissionsIdEvents(
    ApiForget,
    ApiForpost,
):
    pass
