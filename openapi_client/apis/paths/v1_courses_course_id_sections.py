from openapi_client.paths.v1_courses_course_id_sections.get import ApiForget
from openapi_client.paths.v1_courses_course_id_sections.post import ApiForpost


class V1CoursesCourseIdSections(
    ApiForget,
    ApiForpost,
):
    pass
