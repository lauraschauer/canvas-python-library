from openapi_client.paths.v1_users_user_id_features.get import ApiForget


class V1UsersUserIdFeatures(
    ApiForget,
):
    pass
