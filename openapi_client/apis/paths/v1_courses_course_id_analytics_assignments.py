from openapi_client.paths.v1_courses_course_id_analytics_assignments.get import ApiForget


class V1CoursesCourseIdAnalyticsAssignments(
    ApiForget,
):
    pass
