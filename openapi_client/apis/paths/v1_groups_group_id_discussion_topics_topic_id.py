from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id.get import ApiForget
from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id.put import ApiForput
from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id.delete import ApiFordelete


class V1GroupsGroupIdDiscussionTopicsTopicId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
