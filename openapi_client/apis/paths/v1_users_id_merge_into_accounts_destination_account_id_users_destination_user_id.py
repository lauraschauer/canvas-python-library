from openapi_client.paths.v1_users_id_merge_into_accounts_destination_account_id_users_destination_user_id.put import ApiForput


class V1UsersIdMergeIntoAccountsDestinationAccountIdUsersDestinationUserId(
    ApiForput,
):
    pass
