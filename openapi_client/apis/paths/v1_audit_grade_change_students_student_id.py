from openapi_client.paths.v1_audit_grade_change_students_student_id.get import ApiForget


class V1AuditGradeChangeStudentsStudentId(
    ApiForget,
):
    pass
