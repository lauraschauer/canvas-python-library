from openapi_client.paths.v1_users_id_dashboard_positions.get import ApiForget
from openapi_client.paths.v1_users_id_dashboard_positions.put import ApiForput


class V1UsersIdDashboardPositions(
    ApiForget,
    ApiForput,
):
    pass
