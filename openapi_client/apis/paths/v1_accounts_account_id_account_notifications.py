from openapi_client.paths.v1_accounts_account_id_account_notifications.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_account_notifications.post import ApiForpost


class V1AccountsAccountIdAccountNotifications(
    ApiForget,
    ApiForpost,
):
    pass
