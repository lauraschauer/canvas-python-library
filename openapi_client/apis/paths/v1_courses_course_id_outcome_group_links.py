from openapi_client.paths.v1_courses_course_id_outcome_group_links.get import ApiForget


class V1CoursesCourseIdOutcomeGroupLinks(
    ApiForget,
):
    pass
