from openapi_client.paths.v1_folders_folder_id_files.post import ApiForpost


class V1FoldersFolderIdFiles(
    ApiForpost,
):
    pass
