from openapi_client.paths.v1_accounts_account_id_permissions.get import ApiForget


class V1AccountsAccountIdPermissions(
    ApiForget,
):
    pass
