from openapi_client.paths.v1_outcomes_id.get import ApiForget
from openapi_client.paths.v1_outcomes_id.put import ApiForput


class V1OutcomesId(
    ApiForget,
    ApiForput,
):
    pass
