from openapi_client.paths.v1_courses_course_id_front_page.get import ApiForget
from openapi_client.paths.v1_courses_course_id_front_page.put import ApiForput


class V1CoursesCourseIdFrontPage(
    ApiForget,
    ApiForput,
):
    pass
