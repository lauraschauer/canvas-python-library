from openapi_client.paths.v1_accounts_account_id_outcome_group_links.get import ApiForget


class V1AccountsAccountIdOutcomeGroupLinks(
    ApiForget,
):
    pass
