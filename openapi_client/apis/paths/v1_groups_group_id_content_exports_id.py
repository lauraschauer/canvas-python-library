from openapi_client.paths.v1_groups_group_id_content_exports_id.get import ApiForget


class V1GroupsGroupIdContentExportsId(
    ApiForget,
):
    pass
