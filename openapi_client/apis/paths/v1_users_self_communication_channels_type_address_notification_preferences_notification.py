from openapi_client.paths.v1_users_self_communication_channels_type_address_notification_preferences_notification.put import ApiForput


class V1UsersSelfCommunicationChannelsTypeAddressNotificationPreferencesNotification(
    ApiForput,
):
    pass
