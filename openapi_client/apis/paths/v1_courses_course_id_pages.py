from openapi_client.paths.v1_courses_course_id_pages.get import ApiForget
from openapi_client.paths.v1_courses_course_id_pages.post import ApiForpost


class V1CoursesCourseIdPages(
    ApiForget,
    ApiForpost,
):
    pass
