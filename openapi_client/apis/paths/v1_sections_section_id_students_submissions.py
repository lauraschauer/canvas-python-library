from openapi_client.paths.v1_sections_section_id_students_submissions.get import ApiForget


class V1SectionsSectionIdStudentsSubmissions(
    ApiForget,
):
    pass
