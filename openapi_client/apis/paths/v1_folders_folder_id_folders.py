from openapi_client.paths.v1_folders_folder_id_folders.post import ApiForpost


class V1FoldersFolderIdFolders(
    ApiForpost,
):
    pass
