from openapi_client.paths.v1_global_outcome_groups_id_import.post import ApiForpost


class V1GlobalOutcomeGroupsIdImport(
    ApiForpost,
):
    pass
