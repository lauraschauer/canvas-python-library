from openapi_client.paths.v1_users_self_bookmarks.get import ApiForget
from openapi_client.paths.v1_users_self_bookmarks.post import ApiForpost


class V1UsersSelfBookmarks(
    ApiForget,
    ApiForpost,
):
    pass
