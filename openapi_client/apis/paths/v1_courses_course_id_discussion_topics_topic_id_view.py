from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_view.get import ApiForget


class V1CoursesCourseIdDiscussionTopicsTopicIdView(
    ApiForget,
):
    pass
