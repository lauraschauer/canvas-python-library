from openapi_client.paths.v1_users_user_id_logins.get import ApiForget


class V1UsersUserIdLogins(
    ApiForget,
):
    pass
