from openapi_client.paths.v1_users_user_id_communication_channels.get import ApiForget
from openapi_client.paths.v1_users_user_id_communication_channels.post import ApiForpost


class V1UsersUserIdCommunicationChannels(
    ApiForget,
    ApiForpost,
):
    pass
