from openapi_client.paths.v1_groups_group_id_memberships_membership_id.get import ApiForget
from openapi_client.paths.v1_groups_group_id_memberships_membership_id.put import ApiForput
from openapi_client.paths.v1_groups_group_id_memberships_membership_id.delete import ApiFordelete


class V1GroupsGroupIdMembershipsMembershipId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
