from openapi_client.paths.v1_sections_section_id_assignments_assignment_id_submission_summary.get import ApiForget


class V1SectionsSectionIdAssignmentsAssignmentIdSubmissionSummary(
    ApiForget,
):
    pass
