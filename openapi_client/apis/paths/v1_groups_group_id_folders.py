from openapi_client.paths.v1_groups_group_id_folders.get import ApiForget
from openapi_client.paths.v1_groups_group_id_folders.post import ApiForpost


class V1GroupsGroupIdFolders(
    ApiForget,
    ApiForpost,
):
    pass
