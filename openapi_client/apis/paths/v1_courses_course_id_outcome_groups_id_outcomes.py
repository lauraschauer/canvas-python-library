from openapi_client.paths.v1_courses_course_id_outcome_groups_id_outcomes.get import ApiForget
from openapi_client.paths.v1_courses_course_id_outcome_groups_id_outcomes.post import ApiForpost


class V1CoursesCourseIdOutcomeGroupsIdOutcomes(
    ApiForget,
    ApiForpost,
):
    pass
