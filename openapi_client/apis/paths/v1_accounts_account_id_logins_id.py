from openapi_client.paths.v1_accounts_account_id_logins_id.put import ApiForput


class V1AccountsAccountIdLoginsId(
    ApiForput,
):
    pass
