from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_entry_list.get import ApiForget


class V1CoursesCourseIdDiscussionTopicsTopicIdEntryList(
    ApiForget,
):
    pass
