from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_ip_filters.get import ApiForget


class V1CoursesCourseIdQuizzesQuizIdIpFilters(
    ApiForget,
):
    pass
