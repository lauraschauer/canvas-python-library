from openapi_client.paths.v1_error_reports.post import ApiForpost


class V1ErrorReports(
    ApiForpost,
):
    pass
