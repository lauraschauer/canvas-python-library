from openapi_client.paths.v1_global_outcome_groups_id_outcomes.get import ApiForget
from openapi_client.paths.v1_global_outcome_groups_id_outcomes.post import ApiForpost


class V1GlobalOutcomeGroupsIdOutcomes(
    ApiForget,
    ApiForpost,
):
    pass
