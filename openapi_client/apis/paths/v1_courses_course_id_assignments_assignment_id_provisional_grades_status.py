from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_provisional_grades_status.get import ApiForget


class V1CoursesCourseIdAssignmentsAssignmentIdProvisionalGradesStatus(
    ApiForget,
):
    pass
