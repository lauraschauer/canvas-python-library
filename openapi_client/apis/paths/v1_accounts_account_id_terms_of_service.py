from openapi_client.paths.v1_accounts_account_id_terms_of_service.get import ApiForget


class V1AccountsAccountIdTermsOfService(
    ApiForget,
):
    pass
