from openapi_client.paths.v1_users_user_id_content_migrations_content_migration_id_migration_issues_id.get import ApiForget
from openapi_client.paths.v1_users_user_id_content_migrations_content_migration_id_migration_issues_id.put import ApiForput


class V1UsersUserIdContentMigrationsContentMigrationIdMigrationIssuesId(
    ApiForget,
    ApiForput,
):
    pass
