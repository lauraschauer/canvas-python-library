from openapi_client.paths.v1_courses_course_id_epub_exports_id.get import ApiForget


class V1CoursesCourseIdEpubExportsId(
    ApiForget,
):
    pass
