from openapi_client.paths.v1_courses_course_id_grading_periods_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_grading_periods_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_grading_periods_id.delete import ApiFordelete


class V1CoursesCourseIdGradingPeriodsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
