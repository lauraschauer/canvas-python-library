from openapi_client.paths.v1_courses_course_id_external_feeds.get import ApiForget
from openapi_client.paths.v1_courses_course_id_external_feeds.post import ApiForpost


class V1CoursesCourseIdExternalFeeds(
    ApiForget,
    ApiForpost,
):
    pass
