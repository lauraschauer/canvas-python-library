from openapi_client.paths.v1_folders_dest_folder_id_copy_folder.post import ApiForpost


class V1FoldersDestFolderIdCopyFolder(
    ApiForpost,
):
    pass
