from openapi_client.paths.v1_accounts_account_id_users.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_users.post import ApiForpost


class V1AccountsAccountIdUsers(
    ApiForget,
    ApiForpost,
):
    pass
