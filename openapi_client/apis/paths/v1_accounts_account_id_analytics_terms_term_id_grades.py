from openapi_client.paths.v1_accounts_account_id_analytics_terms_term_id_grades.get import ApiForget


class V1AccountsAccountIdAnalyticsTermsTermIdGrades(
    ApiForget,
):
    pass
