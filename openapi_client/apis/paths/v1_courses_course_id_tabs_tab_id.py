from openapi_client.paths.v1_courses_course_id_tabs_tab_id.put import ApiForput


class V1CoursesCourseIdTabsTabId(
    ApiForput,
):
    pass
