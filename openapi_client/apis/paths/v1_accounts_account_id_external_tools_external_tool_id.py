from openapi_client.paths.v1_accounts_account_id_external_tools_external_tool_id.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_external_tools_external_tool_id.put import ApiForput
from openapi_client.paths.v1_accounts_account_id_external_tools_external_tool_id.delete import ApiFordelete


class V1AccountsAccountIdExternalToolsExternalToolId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
