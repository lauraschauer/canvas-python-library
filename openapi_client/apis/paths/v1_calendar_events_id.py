from openapi_client.paths.v1_calendar_events_id.get import ApiForget
from openapi_client.paths.v1_calendar_events_id.put import ApiForput
from openapi_client.paths.v1_calendar_events_id.delete import ApiFordelete


class V1CalendarEventsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
