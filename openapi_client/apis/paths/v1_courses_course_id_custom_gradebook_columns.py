from openapi_client.paths.v1_courses_course_id_custom_gradebook_columns.get import ApiForget
from openapi_client.paths.v1_courses_course_id_custom_gradebook_columns.post import ApiForpost


class V1CoursesCourseIdCustomGradebookColumns(
    ApiForget,
    ApiForpost,
):
    pass
