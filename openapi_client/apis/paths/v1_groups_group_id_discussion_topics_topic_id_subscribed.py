from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_subscribed.put import ApiForput
from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_subscribed.delete import ApiFordelete


class V1GroupsGroupIdDiscussionTopicsTopicIdSubscribed(
    ApiForput,
    ApiFordelete,
):
    pass
