from openapi_client.paths.v1_groups_group_id_pages_url_revisions_latest.get import ApiForget


class V1GroupsGroupIdPagesUrlRevisionsLatest(
    ApiForget,
):
    pass
