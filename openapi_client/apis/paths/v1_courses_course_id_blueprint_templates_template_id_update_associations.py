from openapi_client.paths.v1_courses_course_id_blueprint_templates_template_id_update_associations.put import ApiForput


class V1CoursesCourseIdBlueprintTemplatesTemplateIdUpdateAssociations(
    ApiForput,
):
    pass
