from openapi_client.paths.v1_users_user_id_files.post import ApiForpost


class V1UsersUserIdFiles(
    ApiForpost,
):
    pass
