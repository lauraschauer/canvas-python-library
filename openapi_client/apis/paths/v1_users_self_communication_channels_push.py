from openapi_client.paths.v1_users_self_communication_channels_push.delete import ApiFordelete


class V1UsersSelfCommunicationChannelsPush(
    ApiFordelete,
):
    pass
