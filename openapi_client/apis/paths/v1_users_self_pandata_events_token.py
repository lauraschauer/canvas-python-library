from openapi_client.paths.v1_users_self_pandata_events_token.post import ApiForpost


class V1UsersSelfPandataEventsToken(
    ApiForpost,
):
    pass
