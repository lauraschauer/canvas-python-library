from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_submissions_id_time.get import ApiForget


class V1CoursesCourseIdQuizzesQuizIdSubmissionsIdTime(
    ApiForget,
):
    pass
