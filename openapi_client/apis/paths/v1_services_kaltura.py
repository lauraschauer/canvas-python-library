from openapi_client.paths.v1_services_kaltura.get import ApiForget


class V1ServicesKaltura(
    ApiForget,
):
    pass
