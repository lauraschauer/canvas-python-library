from openapi_client.paths.v1_courses_course_id_assignment_groups.get import ApiForget
from openapi_client.paths.v1_courses_course_id_assignment_groups.post import ApiForpost


class V1CoursesCourseIdAssignmentGroups(
    ApiForget,
    ApiForpost,
):
    pass
