from openapi_client.paths.v1_audit_course_courses_course_id.get import ApiForget


class V1AuditCourseCoursesCourseId(
    ApiForget,
):
    pass
