from openapi_client.paths.v1_users_self_activity_stream.get import ApiForget
from openapi_client.paths.v1_users_self_activity_stream.delete import ApiFordelete


class V1UsersSelfActivityStream(
    ApiForget,
    ApiFordelete,
):
    pass
