from openapi_client.paths.v1_accounts_account_id_outcome_proficiency.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_outcome_proficiency.post import ApiForpost


class V1AccountsAccountIdOutcomeProficiency(
    ApiForget,
    ApiForpost,
):
    pass
