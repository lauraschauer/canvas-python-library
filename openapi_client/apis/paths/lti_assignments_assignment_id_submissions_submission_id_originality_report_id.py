from openapi_client.paths.lti_assignments_assignment_id_submissions_submission_id_originality_report_id.get import ApiForget
from openapi_client.paths.lti_assignments_assignment_id_submissions_submission_id_originality_report_id.put import ApiForput


class LtiAssignmentsAssignmentIdSubmissionsSubmissionIdOriginalityReportId(
    ApiForget,
    ApiForput,
):
    pass
