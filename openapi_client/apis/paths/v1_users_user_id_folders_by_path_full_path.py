from openapi_client.paths.v1_users_user_id_folders_by_path_full_path.get import ApiForget


class V1UsersUserIdFoldersByPathFullPath(
    ApiForget,
):
    pass
