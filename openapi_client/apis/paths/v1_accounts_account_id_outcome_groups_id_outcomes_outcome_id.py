from openapi_client.paths.v1_accounts_account_id_outcome_groups_id_outcomes_outcome_id.put import ApiForput
from openapi_client.paths.v1_accounts_account_id_outcome_groups_id_outcomes_outcome_id.delete import ApiFordelete


class V1AccountsAccountIdOutcomeGroupsIdOutcomesOutcomeId(
    ApiForput,
    ApiFordelete,
):
    pass
