from openapi_client.paths.v1_audit_authentication_logins_login_id.get import ApiForget


class V1AuditAuthenticationLoginsLoginId(
    ApiForget,
):
    pass
