from openapi_client.paths.v1_users_id_colors_asset_string.get import ApiForget
from openapi_client.paths.v1_users_id_colors_asset_string.put import ApiForput


class V1UsersIdColorsAssetString(
    ApiForget,
    ApiForput,
):
    pass
