from openapi_client.paths.v1_users_id_merge_into_destination_user_id.put import ApiForput


class V1UsersIdMergeIntoDestinationUserId(
    ApiForput,
):
    pass
