from openapi_client.paths.v1_users_user_id_folders.get import ApiForget
from openapi_client.paths.v1_users_user_id_folders.post import ApiForpost


class V1UsersUserIdFolders(
    ApiForget,
    ApiForpost,
):
    pass
