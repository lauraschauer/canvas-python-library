from openapi_client.paths.v1_courses_course_id_modules_module_id_items.get import ApiForget
from openapi_client.paths.v1_courses_course_id_modules_module_id_items.post import ApiForpost


class V1CoursesCourseIdModulesModuleIdItems(
    ApiForget,
    ApiForpost,
):
    pass
