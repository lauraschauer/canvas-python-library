from openapi_client.paths.v1_accounts_account_id_grading_standards.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_grading_standards.post import ApiForpost


class V1AccountsAccountIdGradingStandards(
    ApiForget,
    ApiForpost,
):
    pass
