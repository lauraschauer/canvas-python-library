from openapi_client.paths.v1_courses_course_id_modules_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_modules_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_modules_id.delete import ApiFordelete


class V1CoursesCourseIdModulesId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
