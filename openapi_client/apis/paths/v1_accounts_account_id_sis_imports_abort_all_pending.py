from openapi_client.paths.v1_accounts_account_id_sis_imports_abort_all_pending.put import ApiForput


class V1AccountsAccountIdSisImportsAbortAllPending(
    ApiForput,
):
    pass
