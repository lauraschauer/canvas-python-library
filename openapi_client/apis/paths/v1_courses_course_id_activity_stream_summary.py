from openapi_client.paths.v1_courses_course_id_activity_stream_summary.get import ApiForget


class V1CoursesCourseIdActivityStreamSummary(
    ApiForget,
):
    pass
