from openapi_client.paths.v1_courses_course_id_calendar_events_timetable_events.post import ApiForpost


class V1CoursesCourseIdCalendarEventsTimetableEvents(
    ApiForpost,
):
    pass
