from openapi_client.paths.v1_accounts_account_id_outcome_groups_id_outcomes.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_outcome_groups_id_outcomes.post import ApiForpost


class V1AccountsAccountIdOutcomeGroupsIdOutcomes(
    ApiForget,
    ApiForpost,
):
    pass
