from openapi_client.paths.v1_courses_course_id_outcome_results.get import ApiForget


class V1CoursesCourseIdOutcomeResults(
    ApiForget,
):
    pass
