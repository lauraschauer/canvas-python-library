from openapi_client.paths.v1_accounts_account_id_content_migrations_migrators.get import ApiForget


class V1AccountsAccountIdContentMigrationsMigrators(
    ApiForget,
):
    pass
