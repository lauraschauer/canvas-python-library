from openapi_client.paths.v1_users_user_id_files_id.get import ApiForget


class V1UsersUserIdFilesId(
    ApiForget,
):
    pass
