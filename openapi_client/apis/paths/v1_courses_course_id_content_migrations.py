from openapi_client.paths.v1_courses_course_id_content_migrations.get import ApiForget
from openapi_client.paths.v1_courses_course_id_content_migrations.post import ApiForpost


class V1CoursesCourseIdContentMigrations(
    ApiForget,
    ApiForpost,
):
    pass
