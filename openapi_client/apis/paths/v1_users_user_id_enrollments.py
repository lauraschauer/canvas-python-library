from openapi_client.paths.v1_users_user_id_enrollments.get import ApiForget


class V1UsersUserIdEnrollments(
    ApiForget,
):
    pass
