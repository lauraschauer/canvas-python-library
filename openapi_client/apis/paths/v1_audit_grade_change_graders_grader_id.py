from openapi_client.paths.v1_audit_grade_change_graders_grader_id.get import ApiForget


class V1AuditGradeChangeGradersGraderId(
    ApiForget,
):
    pass
