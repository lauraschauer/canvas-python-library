from openapi_client.paths.v1_courses_course_id_outcome_groups_id_import.post import ApiForpost


class V1CoursesCourseIdOutcomeGroupsIdImport(
    ApiForpost,
):
    pass
