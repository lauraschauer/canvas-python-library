from openapi_client.paths.v1_courses_course_id_folders_by_path.get import ApiForget


class V1CoursesCourseIdFoldersByPath(
    ApiForget,
):
    pass
