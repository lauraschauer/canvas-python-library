from openapi_client.paths.v1_planner_notes.get import ApiForget
from openapi_client.paths.v1_planner_notes.post import ApiForpost


class V1PlannerNotes(
    ApiForget,
    ApiForpost,
):
    pass
