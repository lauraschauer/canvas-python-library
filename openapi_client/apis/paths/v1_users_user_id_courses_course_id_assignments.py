from openapi_client.paths.v1_users_user_id_courses_course_id_assignments.get import ApiForget


class V1UsersUserIdCoursesCourseIdAssignments(
    ApiForget,
):
    pass
