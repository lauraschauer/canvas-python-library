from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_groups_id_reorder.post import ApiForpost


class V1CoursesCourseIdQuizzesQuizIdGroupsIdReorder(
    ApiForpost,
):
    pass
