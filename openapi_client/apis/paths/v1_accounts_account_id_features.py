from openapi_client.paths.v1_accounts_account_id_features.get import ApiForget


class V1AccountsAccountIdFeatures(
    ApiForget,
):
    pass
