from openapi_client.paths.v1_users_user_id_observees_observee_id.get import ApiForget
from openapi_client.paths.v1_users_user_id_observees_observee_id.put import ApiForput
from openapi_client.paths.v1_users_user_id_observees_observee_id.delete import ApiFordelete


class V1UsersUserIdObserveesObserveeId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
