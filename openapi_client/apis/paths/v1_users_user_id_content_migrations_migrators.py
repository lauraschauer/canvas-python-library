from openapi_client.paths.v1_users_user_id_content_migrations_migrators.get import ApiForget


class V1UsersUserIdContentMigrationsMigrators(
    ApiForget,
):
    pass
