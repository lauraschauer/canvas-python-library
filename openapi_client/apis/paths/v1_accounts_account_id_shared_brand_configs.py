from openapi_client.paths.v1_accounts_account_id_shared_brand_configs.post import ApiForpost


class V1AccountsAccountIdSharedBrandConfigs(
    ApiForpost,
):
    pass
