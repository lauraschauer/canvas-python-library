from openapi_client.paths.v1_courses_course_id_group_categories.get import ApiForget
from openapi_client.paths.v1_courses_course_id_group_categories.post import ApiForpost


class V1CoursesCourseIdGroupCategories(
    ApiForget,
    ApiForpost,
):
    pass
