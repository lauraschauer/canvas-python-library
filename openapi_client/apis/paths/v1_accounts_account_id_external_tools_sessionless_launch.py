from openapi_client.paths.v1_accounts_account_id_external_tools_sessionless_launch.get import ApiForget


class V1AccountsAccountIdExternalToolsSessionlessLaunch(
    ApiForget,
):
    pass
