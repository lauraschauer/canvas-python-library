from openapi_client.paths.v1_courses_course_id_students.get import ApiForget


class V1CoursesCourseIdStudents(
    ApiForget,
):
    pass
