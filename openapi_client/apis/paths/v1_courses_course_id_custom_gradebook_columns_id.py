from openapi_client.paths.v1_courses_course_id_custom_gradebook_columns_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_custom_gradebook_columns_id.delete import ApiFordelete


class V1CoursesCourseIdCustomGradebookColumnsId(
    ApiForput,
    ApiFordelete,
):
    pass
