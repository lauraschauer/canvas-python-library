from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_overrides.get import ApiForget
from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_overrides.post import ApiForpost


class V1CoursesCourseIdAssignmentsAssignmentIdOverrides(
    ApiForget,
    ApiForpost,
):
    pass
