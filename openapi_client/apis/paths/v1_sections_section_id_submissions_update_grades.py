from openapi_client.paths.v1_sections_section_id_submissions_update_grades.post import ApiForpost


class V1SectionsSectionIdSubmissionsUpdateGrades(
    ApiForpost,
):
    pass
