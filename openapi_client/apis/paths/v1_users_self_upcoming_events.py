from openapi_client.paths.v1_users_self_upcoming_events.get import ApiForget


class V1UsersSelfUpcomingEvents(
    ApiForget,
):
    pass
