from openapi_client.paths.v1_polls_id.get import ApiForget
from openapi_client.paths.v1_polls_id.put import ApiForput
from openapi_client.paths.v1_polls_id.delete import ApiFordelete


class V1PollsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
