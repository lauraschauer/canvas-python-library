from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_entries_entry_id_replies.get import ApiForget
from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_entries_entry_id_replies.post import ApiForpost


class V1CoursesCourseIdDiscussionTopicsTopicIdEntriesEntryIdReplies(
    ApiForget,
    ApiForpost,
):
    pass
