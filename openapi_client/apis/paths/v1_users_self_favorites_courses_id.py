from openapi_client.paths.v1_users_self_favorites_courses_id.post import ApiForpost
from openapi_client.paths.v1_users_self_favorites_courses_id.delete import ApiFordelete


class V1UsersSelfFavoritesCoursesId(
    ApiForpost,
    ApiFordelete,
):
    pass
