from openapi_client.paths.v1_accounts_account_id_analytics_terms_term_id_statistics.get import ApiForget


class V1AccountsAccountIdAnalyticsTermsTermIdStatistics(
    ApiForget,
):
    pass
