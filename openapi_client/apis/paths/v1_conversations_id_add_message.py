from openapi_client.paths.v1_conversations_id_add_message.post import ApiForpost


class V1ConversationsIdAddMessage(
    ApiForpost,
):
    pass
