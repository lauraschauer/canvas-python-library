from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_entries.get import ApiForget
from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_entries.post import ApiForpost


class V1CoursesCourseIdDiscussionTopicsTopicIdEntries(
    ApiForget,
    ApiForpost,
):
    pass
