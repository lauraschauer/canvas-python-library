from openapi_client.paths.v1_sections_course_section_id_assignments_assignment_id_override.get import ApiForget


class V1SectionsCourseSectionIdAssignmentsAssignmentIdOverride(
    ApiForget,
):
    pass
