from openapi_client.paths.v1_courses_course_id_modules_module_id_items_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_modules_module_id_items_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_modules_module_id_items_id.delete import ApiFordelete


class V1CoursesCourseIdModulesModuleIdItemsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
