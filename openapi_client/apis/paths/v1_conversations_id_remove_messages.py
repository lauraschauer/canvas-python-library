from openapi_client.paths.v1_conversations_id_remove_messages.post import ApiForpost


class V1ConversationsIdRemoveMessages(
    ApiForpost,
):
    pass
