from openapi_client.paths.v1_accounts_account_id_sis_imports.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_sis_imports.post import ApiForpost


class V1AccountsAccountIdSisImports(
    ApiForget,
    ApiForpost,
):
    pass
