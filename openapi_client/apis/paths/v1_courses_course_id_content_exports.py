from openapi_client.paths.v1_courses_course_id_content_exports.get import ApiForget
from openapi_client.paths.v1_courses_course_id_content_exports.post import ApiForpost


class V1CoursesCourseIdContentExports(
    ApiForget,
    ApiForpost,
):
    pass
