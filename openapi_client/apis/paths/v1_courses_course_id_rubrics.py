from openapi_client.paths.v1_courses_course_id_rubrics.get import ApiForget


class V1CoursesCourseIdRubrics(
    ApiForget,
):
    pass
