from openapi_client.paths.v1_accounts_account_id_analytics_current_activity.get import ApiForget


class V1AccountsAccountIdAnalyticsCurrentActivity(
    ApiForget,
):
    pass
