from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_entries.get import ApiForget
from openapi_client.paths.v1_groups_group_id_discussion_topics_topic_id_entries.post import ApiForpost


class V1GroupsGroupIdDiscussionTopicsTopicIdEntries(
    ApiForget,
    ApiForpost,
):
    pass
