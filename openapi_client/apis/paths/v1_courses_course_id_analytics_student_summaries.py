from openapi_client.paths.v1_courses_course_id_analytics_student_summaries.get import ApiForget


class V1CoursesCourseIdAnalyticsStudentSummaries(
    ApiForget,
):
    pass
