from openapi_client.paths.v1_users_self_todo.get import ApiForget


class V1UsersSelfTodo(
    ApiForget,
):
    pass
