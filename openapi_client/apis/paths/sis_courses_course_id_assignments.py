from openapi_client.paths.sis_courses_course_id_assignments.get import ApiForget


class SisCoursesCourseIdAssignments(
    ApiForget,
):
    pass
