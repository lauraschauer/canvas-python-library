from openapi_client.paths.v1_appointment_groups_id_users.get import ApiForget


class V1AppointmentGroupsIdUsers(
    ApiForget,
):
    pass
