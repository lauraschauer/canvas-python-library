from openapi_client.paths.v1_accounts_account_id_analytics_terms_term_id_activity.get import ApiForget


class V1AccountsAccountIdAnalyticsTermsTermIdActivity(
    ApiForget,
):
    pass
