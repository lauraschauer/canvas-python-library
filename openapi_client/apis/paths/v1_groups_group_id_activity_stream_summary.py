from openapi_client.paths.v1_groups_group_id_activity_stream_summary.get import ApiForget


class V1GroupsGroupIdActivityStreamSummary(
    ApiForget,
):
    pass
