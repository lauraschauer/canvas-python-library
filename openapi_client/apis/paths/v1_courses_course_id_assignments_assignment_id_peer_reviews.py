from openapi_client.paths.v1_courses_course_id_assignments_assignment_id_peer_reviews.get import ApiForget


class V1CoursesCourseIdAssignmentsAssignmentIdPeerReviews(
    ApiForget,
):
    pass
