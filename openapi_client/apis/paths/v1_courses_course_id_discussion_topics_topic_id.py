from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id.delete import ApiFordelete


class V1CoursesCourseIdDiscussionTopicsTopicId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
