from openapi_client.paths.v1_accounts_account_id_sso_settings.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_sso_settings.put import ApiForput


class V1AccountsAccountIdSsoSettings(
    ApiForget,
    ApiForput,
):
    pass
