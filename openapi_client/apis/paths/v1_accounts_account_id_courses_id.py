from openapi_client.paths.v1_accounts_account_id_courses_id.get import ApiForget


class V1AccountsAccountIdCoursesId(
    ApiForget,
):
    pass
