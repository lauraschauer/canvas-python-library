from openapi_client.paths.v1_users_user_id_missing_submissions.get import ApiForget


class V1UsersUserIdMissingSubmissions(
    ApiForget,
):
    pass
