from openapi_client.paths.lti_assignments_assignment_id_submissions_submission_id_originality_report.post import ApiForpost


class LtiAssignmentsAssignmentIdSubmissionsSubmissionIdOriginalityReport(
    ApiForpost,
):
    pass
