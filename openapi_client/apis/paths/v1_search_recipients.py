from openapi_client.paths.v1_search_recipients.get import ApiForget


class V1SearchRecipients(
    ApiForget,
):
    pass
