from openapi_client.paths.v1_courses_course_id_enrollments_id_accept.post import ApiForpost


class V1CoursesCourseIdEnrollmentsIdAccept(
    ApiForpost,
):
    pass
