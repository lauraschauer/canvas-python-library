from openapi_client.paths.v1_groups.post import ApiForpost


class V1Groups(
    ApiForpost,
):
    pass
