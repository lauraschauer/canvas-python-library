from openapi_client.paths.v1_folders_dest_folder_id_copy_file.post import ApiForpost


class V1FoldersDestFolderIdCopyFile(
    ApiForpost,
):
    pass
