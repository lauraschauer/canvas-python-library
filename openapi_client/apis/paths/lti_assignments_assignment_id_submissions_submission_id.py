from openapi_client.paths.lti_assignments_assignment_id_submissions_submission_id.get import ApiForget


class LtiAssignmentsAssignmentIdSubmissionsSubmissionId(
    ApiForget,
):
    pass
