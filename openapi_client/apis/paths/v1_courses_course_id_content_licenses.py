from openapi_client.paths.v1_courses_course_id_content_licenses.get import ApiForget


class V1CoursesCourseIdContentLicenses(
    ApiForget,
):
    pass
