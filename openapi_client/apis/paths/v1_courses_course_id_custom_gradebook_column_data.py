from openapi_client.paths.v1_courses_course_id_custom_gradebook_column_data.put import ApiForput


class V1CoursesCourseIdCustomGradebookColumnData(
    ApiForput,
):
    pass
