from openapi_client.paths.v1_courses_id.get import ApiForget
from openapi_client.paths.v1_courses_id.put import ApiForput
from openapi_client.paths.v1_courses_id.delete import ApiFordelete


class V1CoursesId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
