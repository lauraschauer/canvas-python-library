from openapi_client.paths.v1_groups_group_id_files.post import ApiForpost


class V1GroupsGroupIdFiles(
    ApiForpost,
):
    pass
