from openapi_client.paths.v1_accounts_account_id_logins.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_logins.post import ApiForpost


class V1AccountsAccountIdLogins(
    ApiForget,
    ApiForpost,
):
    pass
