from openapi_client.paths.v1_courses_course_id_effective_due_dates.get import ApiForget


class V1CoursesCourseIdEffectiveDueDates(
    ApiForget,
):
    pass
