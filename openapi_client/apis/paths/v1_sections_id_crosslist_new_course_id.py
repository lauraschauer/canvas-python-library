from openapi_client.paths.v1_sections_id_crosslist_new_course_id.post import ApiForpost


class V1SectionsIdCrosslistNewCourseId(
    ApiForpost,
):
    pass
