from openapi_client.paths.v1_users_self_activity_stream_id.delete import ApiFordelete


class V1UsersSelfActivityStreamId(
    ApiFordelete,
):
    pass
