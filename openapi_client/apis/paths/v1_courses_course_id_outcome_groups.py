from openapi_client.paths.v1_courses_course_id_outcome_groups.get import ApiForget


class V1CoursesCourseIdOutcomeGroups(
    ApiForget,
):
    pass
