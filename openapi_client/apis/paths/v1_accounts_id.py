from openapi_client.paths.v1_accounts_id.get import ApiForget
from openapi_client.paths.v1_accounts_id.put import ApiForput


class V1AccountsId(
    ApiForget,
    ApiForput,
):
    pass
