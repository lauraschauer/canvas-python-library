from openapi_client.paths.v1_calendar_events_id_reservations.post import ApiForpost


class V1CalendarEventsIdReservations(
    ApiForpost,
):
    pass
