from openapi_client.paths.v1_groups_group_id_discussion_topics.get import ApiForget
from openapi_client.paths.v1_groups_group_id_discussion_topics.post import ApiForpost


class V1GroupsGroupIdDiscussionTopics(
    ApiForget,
    ApiForpost,
):
    pass
