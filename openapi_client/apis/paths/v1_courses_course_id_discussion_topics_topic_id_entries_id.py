from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_entries_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_entries_id.delete import ApiFordelete


class V1CoursesCourseIdDiscussionTopicsTopicIdEntriesId(
    ApiForput,
    ApiFordelete,
):
    pass
