from openapi_client.paths.v1_sections_id_crosslist.delete import ApiFordelete


class V1SectionsIdCrosslist(
    ApiFordelete,
):
    pass
