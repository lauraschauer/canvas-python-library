from openapi_client.paths.v1_courses_course_id_outcome_groups_id.get import ApiForget
from openapi_client.paths.v1_courses_course_id_outcome_groups_id.put import ApiForput
from openapi_client.paths.v1_courses_course_id_outcome_groups_id.delete import ApiFordelete


class V1CoursesCourseIdOutcomeGroupsId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
