from openapi_client.paths.v1_courses_course_id_content_migrations_migrators.get import ApiForget


class V1CoursesCourseIdContentMigrationsMigrators(
    ApiForget,
):
    pass
