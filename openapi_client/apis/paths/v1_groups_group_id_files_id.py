from openapi_client.paths.v1_groups_group_id_files_id.get import ApiForget


class V1GroupsGroupIdFilesId(
    ApiForget,
):
    pass
