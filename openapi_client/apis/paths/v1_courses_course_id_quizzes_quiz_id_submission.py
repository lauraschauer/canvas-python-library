from openapi_client.paths.v1_courses_course_id_quizzes_quiz_id_submission.get import ApiForget


class V1CoursesCourseIdQuizzesQuizIdSubmission(
    ApiForget,
):
    pass
