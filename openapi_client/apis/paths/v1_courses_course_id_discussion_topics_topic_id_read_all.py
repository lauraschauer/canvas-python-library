from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_read_all.put import ApiForput
from openapi_client.paths.v1_courses_course_id_discussion_topics_topic_id_read_all.delete import ApiFordelete


class V1CoursesCourseIdDiscussionTopicsTopicIdReadAll(
    ApiForput,
    ApiFordelete,
):
    pass
