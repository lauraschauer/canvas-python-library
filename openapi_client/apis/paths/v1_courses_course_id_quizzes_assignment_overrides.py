from openapi_client.paths.v1_courses_course_id_quizzes_assignment_overrides.get import ApiForget


class V1CoursesCourseIdQuizzesAssignmentOverrides(
    ApiForget,
):
    pass
