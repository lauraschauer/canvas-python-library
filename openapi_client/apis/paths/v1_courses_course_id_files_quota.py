from openapi_client.paths.v1_courses_course_id_files_quota.get import ApiForget


class V1CoursesCourseIdFilesQuota(
    ApiForget,
):
    pass
