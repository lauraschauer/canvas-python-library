from openapi_client.paths.v1_users_id.get import ApiForget
from openapi_client.paths.v1_users_id.put import ApiForput


class V1UsersId(
    ApiForget,
    ApiForput,
):
    pass
