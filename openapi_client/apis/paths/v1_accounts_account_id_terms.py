from openapi_client.paths.v1_accounts_account_id_terms.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_terms.post import ApiForpost


class V1AccountsAccountIdTerms(
    ApiForget,
    ApiForpost,
):
    pass
