from openapi_client.paths.v1_conversations_unread_count.get import ApiForget


class V1ConversationsUnreadCount(
    ApiForget,
):
    pass
