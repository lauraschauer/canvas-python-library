from openapi_client.paths.v1_accounts_account_id_sub_accounts.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_sub_accounts.post import ApiForpost


class V1AccountsAccountIdSubAccounts(
    ApiForget,
    ApiForpost,
):
    pass
