from openapi_client.paths.v1_accounts_account_id_analytics_current_grades.get import ApiForget


class V1AccountsAccountIdAnalyticsCurrentGrades(
    ApiForget,
):
    pass
