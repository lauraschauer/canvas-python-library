from openapi_client.paths.v1_courses_course_id_blueprint_templates_template_id_restrict_item.put import ApiForput


class V1CoursesCourseIdBlueprintTemplatesTemplateIdRestrictItem(
    ApiForput,
):
    pass
