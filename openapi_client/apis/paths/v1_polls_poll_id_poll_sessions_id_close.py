from openapi_client.paths.v1_polls_poll_id_poll_sessions_id_close.get import ApiForget


class V1PollsPollIdPollSessionsIdClose(
    ApiForget,
):
    pass
