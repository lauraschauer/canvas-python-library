from openapi_client.paths.v1_users_user_id_profile.get import ApiForget


class V1UsersUserIdProfile(
    ApiForget,
):
    pass
