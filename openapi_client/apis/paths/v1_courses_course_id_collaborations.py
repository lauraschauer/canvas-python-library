from openapi_client.paths.v1_courses_course_id_collaborations.get import ApiForget


class V1CoursesCourseIdCollaborations(
    ApiForget,
):
    pass
