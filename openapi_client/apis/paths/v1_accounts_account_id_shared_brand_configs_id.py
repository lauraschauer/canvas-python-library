from openapi_client.paths.v1_accounts_account_id_shared_brand_configs_id.put import ApiForput


class V1AccountsAccountIdSharedBrandConfigsId(
    ApiForput,
):
    pass
