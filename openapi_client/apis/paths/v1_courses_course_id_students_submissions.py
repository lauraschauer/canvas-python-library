from openapi_client.paths.v1_courses_course_id_students_submissions.get import ApiForget


class V1CoursesCourseIdStudentsSubmissions(
    ApiForget,
):
    pass
