from openapi_client.paths.v1_courses_course_id_modules_module_id_items_id_done.put import ApiForput


class V1CoursesCourseIdModulesModuleIdItemsIdDone(
    ApiForput,
):
    pass
