from openapi_client.paths.v1_users_self_course_nicknames.get import ApiForget
from openapi_client.paths.v1_users_self_course_nicknames.delete import ApiFordelete


class V1UsersSelfCourseNicknames(
    ApiForget,
    ApiFordelete,
):
    pass
