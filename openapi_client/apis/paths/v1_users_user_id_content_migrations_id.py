from openapi_client.paths.v1_users_user_id_content_migrations_id.get import ApiForget
from openapi_client.paths.v1_users_user_id_content_migrations_id.put import ApiForput


class V1UsersUserIdContentMigrationsId(
    ApiForget,
    ApiForput,
):
    pass
