from openapi_client.paths.v1_accounts_account_id_root_outcome_group.get import ApiForget


class V1AccountsAccountIdRootOutcomeGroup(
    ApiForget,
):
    pass
