from openapi_client.paths.v1_users_self_communication_channels_communication_channel_id_notification_preferences.put import ApiForput


class V1UsersSelfCommunicationChannelsCommunicationChannelIdNotificationPreferences(
    ApiForput,
):
    pass
