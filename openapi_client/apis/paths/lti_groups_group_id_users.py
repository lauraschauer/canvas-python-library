from openapi_client.paths.lti_groups_group_id_users.get import ApiForget


class LtiGroupsGroupIdUsers(
    ApiForget,
):
    pass
