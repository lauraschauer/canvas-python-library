from openapi_client.paths.v1_courses_course_id_discussion_topics_reorder.post import ApiForpost


class V1CoursesCourseIdDiscussionTopicsReorder(
    ApiForpost,
):
    pass
