from openapi_client.paths.v1_groups_group_id_files_quota.get import ApiForget


class V1GroupsGroupIdFilesQuota(
    ApiForget,
):
    pass
