from openapi_client.paths.v1_users_user_id_communication_channels_communication_channel_id_notification_preferences.get import ApiForget


class V1UsersUserIdCommunicationChannelsCommunicationChannelIdNotificationPreferences(
    ApiForget,
):
    pass
