from openapi_client.paths.v1_users_user_id_content_licenses.get import ApiForget


class V1UsersUserIdContentLicenses(
    ApiForget,
):
    pass
