from openapi_client.paths.v1_quiz_submissions_quiz_submission_id_questions.get import ApiForget
from openapi_client.paths.v1_quiz_submissions_quiz_submission_id_questions.post import ApiForpost


class V1QuizSubmissionsQuizSubmissionIdQuestions(
    ApiForget,
    ApiForpost,
):
    pass
