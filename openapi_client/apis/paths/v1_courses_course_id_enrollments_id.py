from openapi_client.paths.v1_courses_course_id_enrollments_id.delete import ApiFordelete


class V1CoursesCourseIdEnrollmentsId(
    ApiFordelete,
):
    pass
