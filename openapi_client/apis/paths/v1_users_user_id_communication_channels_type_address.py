from openapi_client.paths.v1_users_user_id_communication_channels_type_address.delete import ApiFordelete


class V1UsersUserIdCommunicationChannelsTypeAddress(
    ApiFordelete,
):
    pass
