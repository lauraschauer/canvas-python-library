from openapi_client.paths.v1_folders_id.get import ApiForget
from openapi_client.paths.v1_folders_id.put import ApiForput
from openapi_client.paths.v1_folders_id.delete import ApiFordelete


class V1FoldersId(
    ApiForget,
    ApiForput,
    ApiFordelete,
):
    pass
