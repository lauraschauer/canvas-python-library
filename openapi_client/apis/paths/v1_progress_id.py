from openapi_client.paths.v1_progress_id.get import ApiForget


class V1ProgressId(
    ApiForget,
):
    pass
