from openapi_client.paths.v1_courses_course_id_potential_collaborators.get import ApiForget


class V1CoursesCourseIdPotentialCollaborators(
    ApiForget,
):
    pass
