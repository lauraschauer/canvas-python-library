from openapi_client.paths.v1_conversations_mark_all_as_read.post import ApiForpost


class V1ConversationsMarkAllAsRead(
    ApiForpost,
):
    pass
