from openapi_client.paths.v1_sections_section_id_enrollments.get import ApiForget
from openapi_client.paths.v1_sections_section_id_enrollments.post import ApiForpost


class V1SectionsSectionIdEnrollments(
    ApiForget,
    ApiForpost,
):
    pass
