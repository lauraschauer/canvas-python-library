from openapi_client.paths.v1_courses_course_id_folders.get import ApiForget
from openapi_client.paths.v1_courses_course_id_folders.post import ApiForpost


class V1CoursesCourseIdFolders(
    ApiForget,
    ApiForpost,
):
    pass
