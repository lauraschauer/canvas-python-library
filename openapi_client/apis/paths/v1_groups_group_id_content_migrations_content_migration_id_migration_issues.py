from openapi_client.paths.v1_groups_group_id_content_migrations_content_migration_id_migration_issues.get import ApiForget


class V1GroupsGroupIdContentMigrationsContentMigrationIdMigrationIssues(
    ApiForget,
):
    pass
