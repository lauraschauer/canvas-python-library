from openapi_client.paths.v1_accounts_account_id_group_categories.get import ApiForget
from openapi_client.paths.v1_accounts_account_id_group_categories.post import ApiForpost


class V1AccountsAccountIdGroupCategories(
    ApiForget,
    ApiForpost,
):
    pass
