from openapi_client.paths.v1_courses_course_id_outcome_groups_id_subgroups.get import ApiForget
from openapi_client.paths.v1_courses_course_id_outcome_groups_id_subgroups.post import ApiForpost


class V1CoursesCourseIdOutcomeGroupsIdSubgroups(
    ApiForget,
    ApiForpost,
):
    pass
