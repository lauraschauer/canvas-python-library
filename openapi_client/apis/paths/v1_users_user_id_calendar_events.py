from openapi_client.paths.v1_users_user_id_calendar_events.get import ApiForget


class V1UsersUserIdCalendarEvents(
    ApiForget,
):
    pass
