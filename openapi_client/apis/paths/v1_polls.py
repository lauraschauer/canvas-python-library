from openapi_client.paths.v1_polls.get import ApiForget
from openapi_client.paths.v1_polls.post import ApiForpost


class V1Polls(
    ApiForget,
    ApiForpost,
):
    pass
