# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from openapi_client.paths.v1_courses_course_id_collaborations.get import ListCollaborationsCourses
from openapi_client.paths.v1_groups_group_id_collaborations.get import ListCollaborationsGroups
from openapi_client.paths.v1_collaborations_id_members.get import ListMembersOfCollaboration
from openapi_client.paths.v1_courses_course_id_potential_collaborators.get import ListPotentialMembersCourses
from openapi_client.paths.v1_groups_group_id_potential_collaborators.get import ListPotentialMembersGroups


class CollaborationsApi(
    ListCollaborationsCourses,
    ListCollaborationsGroups,
    ListMembersOfCollaboration,
    ListPotentialMembersCourses,
    ListPotentialMembersGroups,
):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    pass
