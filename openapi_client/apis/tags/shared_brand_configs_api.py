# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from openapi_client.paths.v1_accounts_account_id_shared_brand_configs.post import ShareBrandconfigTheme
from openapi_client.paths.v1_shared_brand_configs_id.delete import UnShareBrandconfigTheme
from openapi_client.paths.v1_accounts_account_id_shared_brand_configs_id.put import UpdateSharedTheme


class SharedBrandConfigsApi(
    ShareBrandconfigTheme,
    UnShareBrandconfigTheme,
    UpdateSharedTheme,
):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    pass
