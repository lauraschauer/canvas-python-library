# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from openapi_client.paths.v1_polls_poll_id_poll_sessions_id_close.get import CloseOpenedPollSession
from openapi_client.paths.v1_polls_poll_id_poll_sessions.post import CreateSinglePollSession
from openapi_client.paths.v1_polls_poll_id_poll_sessions_id.delete import DeletePollSession
from openapi_client.paths.v1_polls_poll_id_poll_sessions_id.get import GetResultsForSinglePollSession
from openapi_client.paths.v1_poll_sessions_closed.get import ListClosedPollSessions
from openapi_client.paths.v1_poll_sessions_opened.get import ListOpenedPollSessions
from openapi_client.paths.v1_polls_poll_id_poll_sessions.get import ListPollSessionsForPoll
from openapi_client.paths.v1_polls_poll_id_poll_sessions_id_open.get import OpenPollSession
from openapi_client.paths.v1_polls_poll_id_poll_sessions_id.put import UpdateSinglePollSession


class PollSessionsApi(
    CloseOpenedPollSession,
    CreateSinglePollSession,
    DeletePollSession,
    GetResultsForSinglePollSession,
    ListClosedPollSessions,
    ListOpenedPollSessions,
    ListPollSessionsForPoll,
    OpenPollSession,
    UpdateSinglePollSession,
):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    pass
