# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from openapi_client.paths.v1_services_kaltura.get import GetKalturaConfig
from openapi_client.paths.v1_services_kaltura_session.post import StartKalturaSession


class ServicesApi(
    GetKalturaConfig,
    StartKalturaSession,
):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    pass
