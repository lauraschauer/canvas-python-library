# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from openapi_client.paths.v1_accounts_account_id_sis_import_errors.get import GetSisImportErrorListSisImportErrors
from openapi_client.paths.v1_accounts_account_id_sis_imports_id_errors.get import GetSisImportErrorListSisImports


class SisImportErrorsApi(
    GetSisImportErrorListSisImportErrors,
    GetSisImportErrorListSisImports,
):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    pass
