# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from openapi_client.paths.v1_courses_course_id_modules.post import CreateModule
from openapi_client.paths.v1_courses_course_id_modules_module_id_items.post import CreateModuleItem
from openapi_client.paths.v1_courses_course_id_modules_id.delete import DeleteModule
from openapi_client.paths.v1_courses_course_id_modules_module_id_items_id.delete import DeleteModuleItem
from openapi_client.paths.v1_courses_course_id_module_item_sequence.get import GetModuleItemSequence
from openapi_client.paths.v1_courses_course_id_modules_module_id_items.get import ListModuleItems
from openapi_client.paths.v1_courses_course_id_modules.get import ListModules
from openapi_client.paths.v1_courses_course_id_modules_module_id_items_id_done.put import MarkModuleItemAsDoneNotDone
from openapi_client.paths.v1_courses_course_id_modules_module_id_items_id_mark_read.post import MarkModuleItemRead
from openapi_client.paths.v1_courses_course_id_modules_id_relock.put import ReLockModuleProgressions
from openapi_client.paths.v1_courses_course_id_modules_module_id_items_id_select_mastery_path.post import SelectMasteryPath
from openapi_client.paths.v1_courses_course_id_modules_id.get import ShowModule
from openapi_client.paths.v1_courses_course_id_modules_module_id_items_id.get import ShowModuleItem
from openapi_client.paths.v1_courses_course_id_modules_id.put import UpdateModule
from openapi_client.paths.v1_courses_course_id_modules_module_id_items_id.put import UpdateModuleItem


class ModulesApi(
    CreateModule,
    CreateModuleItem,
    DeleteModule,
    DeleteModuleItem,
    GetModuleItemSequence,
    ListModuleItems,
    ListModules,
    MarkModuleItemAsDoneNotDone,
    MarkModuleItemRead,
    ReLockModuleProgressions,
    SelectMasteryPath,
    ShowModule,
    ShowModuleItem,
    UpdateModule,
    UpdateModuleItem,
):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    pass
