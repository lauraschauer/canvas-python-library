# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from openapi_client.paths.v1_audit_authentication_accounts_account_id.get import QueryByAccount
from openapi_client.paths.v1_audit_authentication_logins_login_id.get import QueryByLogin
from openapi_client.paths.v1_audit_authentication_users_user_id.get import QueryByUser


class AuthenticationsLogApi(
    QueryByAccount,
    QueryByLogin,
    QueryByUser,
):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    pass
