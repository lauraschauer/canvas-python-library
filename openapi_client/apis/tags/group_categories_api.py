# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from openapi_client.paths.v1_group_categories_group_category_id_assign_unassigned_members.post import AssignUnassignedMembers
from openapi_client.paths.v1_accounts_account_id_group_categories.post import CreateGroupCategoryAccounts
from openapi_client.paths.v1_courses_course_id_group_categories.post import CreateGroupCategoryCourses
from openapi_client.paths.v1_group_categories_group_category_id.delete import DeleteGroupCategory
from openapi_client.paths.v1_group_categories_group_category_id.get import GetSingleGroupCategory
from openapi_client.paths.v1_accounts_account_id_group_categories.get import ListGroupCategoriesForContextAccounts
from openapi_client.paths.v1_courses_course_id_group_categories.get import ListGroupCategoriesForContextCourses
from openapi_client.paths.v1_group_categories_group_category_id_users.get import ListUsersInGroupCategory
from openapi_client.paths.v1_group_categories_group_category_id.put import UpdateGroupCategory


class GroupCategoriesApi(
    AssignUnassignedMembers,
    CreateGroupCategoryAccounts,
    CreateGroupCategoryCourses,
    DeleteGroupCategory,
    GetSingleGroupCategory,
    ListGroupCategoriesForContextAccounts,
    ListGroupCategoriesForContextCourses,
    ListUsersInGroupCategory,
    UpdateGroupCategory,
):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    pass
