# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from openapi_client.paths.lti_assignments_assignment_id.get import GetSingleAssignmentLti


class PlagiarismDetectionPlatformAssignmentsApi(
    GetSingleAssignmentLti,
):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    pass
