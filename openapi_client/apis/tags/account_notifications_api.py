# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

from openapi_client.paths.v1_accounts_account_id_account_notifications_id.delete import CloseNotificationForUser
from openapi_client.paths.v1_accounts_account_id_account_notifications.post import CreateGlobalNotification
from openapi_client.paths.v1_accounts_account_id_account_notifications.get import IndexOfActiveGlobalNotificationForUser
from openapi_client.paths.v1_accounts_account_id_account_notifications_id.get import ShowGlobalNotification
from openapi_client.paths.v1_accounts_account_id_account_notifications_id.put import UpdateGlobalNotification


class AccountNotificationsApi(
    CloseNotificationForUser,
    CreateGlobalNotification,
    IndexOfActiveGlobalNotificationForUser,
    ShowGlobalNotification,
    UpdateGlobalNotification,
):
    """NOTE: This class is auto generated by OpenAPI Generator
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    pass
