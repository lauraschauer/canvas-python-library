import typing_extensions

from openapi_client.apis.tags import TagValues
from openapi_client.apis.tags.account_domain_lookups_api import AccountDomainLookupsApi
from openapi_client.apis.tags.account_notifications_api import AccountNotificationsApi
from openapi_client.apis.tags.account_reports_api import AccountReportsApi
from openapi_client.apis.tags.accounts_api import AccountsApi
from openapi_client.apis.tags.admins_api import AdminsApi
from openapi_client.apis.tags.analytics_api import AnalyticsApi
from openapi_client.apis.tags.announcement_external_feeds_api import AnnouncementExternalFeedsApi
from openapi_client.apis.tags.announcements_api import AnnouncementsApi
from openapi_client.apis.tags.api_token_scopes_api import ApiTokenScopesApi
from openapi_client.apis.tags.appointment_groups_api import AppointmentGroupsApi
from openapi_client.apis.tags.assignment_groups_api import AssignmentGroupsApi
from openapi_client.apis.tags.assignments_api import AssignmentsApi
from openapi_client.apis.tags.authentication_providers_api import AuthenticationProvidersApi
from openapi_client.apis.tags.authentications_log_api import AuthenticationsLogApi
from openapi_client.apis.tags.blueprint_courses_api import BlueprintCoursesApi
from openapi_client.apis.tags.bookmarks_api import BookmarksApi
from openapi_client.apis.tags.brand_configs_api import BrandConfigsApi
from openapi_client.apis.tags.calendar_events_api import CalendarEventsApi
from openapi_client.apis.tags.collaborations_api import CollaborationsApi
from openapi_client.apis.tags.comm_messages_api import CommMessagesApi
from openapi_client.apis.tags.communication_channels_api import CommunicationChannelsApi
from openapi_client.apis.tags.conferences_api import ConferencesApi
from openapi_client.apis.tags.content_exports_api import ContentExportsApi
from openapi_client.apis.tags.content_migrations_api import ContentMigrationsApi
from openapi_client.apis.tags.conversations_api import ConversationsApi
from openapi_client.apis.tags.course_audit_log_api import CourseAuditLogApi
from openapi_client.apis.tags.course_quiz_extensions_api import CourseQuizExtensionsApi
from openapi_client.apis.tags.courses_api import CoursesApi
from openapi_client.apis.tags.custom_gradebook_columns_api import CustomGradebookColumnsApi
from openapi_client.apis.tags.discussion_topics_api import DiscussionTopicsApi
from openapi_client.apis.tags.document_previews_api import DocumentPreviewsApi
from openapi_client.apis.tags.e_pub_exports_api import EPubExportsApi
from openapi_client.apis.tags.enrollment_terms_api import EnrollmentTermsApi
from openapi_client.apis.tags.enrollments_api import EnrollmentsApi
from openapi_client.apis.tags.error_reports_api import ErrorReportsApi
from openapi_client.apis.tags.external_tools_api import ExternalToolsApi
from openapi_client.apis.tags.favorites_api import FavoritesApi
from openapi_client.apis.tags.feature_flags_api import FeatureFlagsApi
from openapi_client.apis.tags.files_api import FilesApi
from openapi_client.apis.tags.grade_change_log_api import GradeChangeLogApi
from openapi_client.apis.tags.gradebook_history_api import GradebookHistoryApi
from openapi_client.apis.tags.grading_periods_api import GradingPeriodsApi
from openapi_client.apis.tags.grading_standards_api import GradingStandardsApi
from openapi_client.apis.tags.group_categories_api import GroupCategoriesApi
from openapi_client.apis.tags.groups_api import GroupsApi
from openapi_client.apis.tags.jw_ts_api import JwTsApi
from openapi_client.apis.tags.late_policy_api import LatePolicyApi
from openapi_client.apis.tags.live_assessments_api import LiveAssessmentsApi
from openapi_client.apis.tags.logins_api import LoginsApi
from openapi_client.apis.tags.moderated_grading_api import ModeratedGradingApi
from openapi_client.apis.tags.modules_api import ModulesApi
from openapi_client.apis.tags.notification_preferences_api import NotificationPreferencesApi
from openapi_client.apis.tags.originality_reports_api import OriginalityReportsApi
from openapi_client.apis.tags.outcome_groups_api import OutcomeGroupsApi
from openapi_client.apis.tags.outcome_imports_api import OutcomeImportsApi
from openapi_client.apis.tags.outcome_results_api import OutcomeResultsApi
from openapi_client.apis.tags.outcomes_api import OutcomesApi
from openapi_client.apis.tags.pages_api import PagesApi
from openapi_client.apis.tags.peer_reviews_api import PeerReviewsApi
from openapi_client.apis.tags.plagiarism_detection_platform_assignments_api import PlagiarismDetectionPlatformAssignmentsApi
from openapi_client.apis.tags.plagiarism_detection_platform_users_api import PlagiarismDetectionPlatformUsersApi
from openapi_client.apis.tags.plagiarism_detection_submissions_api import PlagiarismDetectionSubmissionsApi
from openapi_client.apis.tags.planner_api import PlannerApi
from openapi_client.apis.tags.planner_note_api import PlannerNoteApi
from openapi_client.apis.tags.planner_override_api import PlannerOverrideApi
from openapi_client.apis.tags.poll_choices_api import PollChoicesApi
from openapi_client.apis.tags.poll_sessions_api import PollSessionsApi
from openapi_client.apis.tags.poll_submissions_api import PollSubmissionsApi
from openapi_client.apis.tags.polls_api import PollsApi
from openapi_client.apis.tags.proficiency_ratings_api import ProficiencyRatingsApi
from openapi_client.apis.tags.progress_api import ProgressApi
from openapi_client.apis.tags.quiz_assignment_overrides_api import QuizAssignmentOverridesApi
from openapi_client.apis.tags.quiz_extensions_api import QuizExtensionsApi
from openapi_client.apis.tags.quiz_ip_filters_api import QuizIpFiltersApi
from openapi_client.apis.tags.quiz_question_groups_api import QuizQuestionGroupsApi
from openapi_client.apis.tags.quiz_questions_api import QuizQuestionsApi
from openapi_client.apis.tags.quiz_reports_api import QuizReportsApi
from openapi_client.apis.tags.quiz_statistics_api import QuizStatisticsApi
from openapi_client.apis.tags.quiz_submission_events_api import QuizSubmissionEventsApi
from openapi_client.apis.tags.quiz_submission_files_api import QuizSubmissionFilesApi
from openapi_client.apis.tags.quiz_submission_questions_api import QuizSubmissionQuestionsApi
from openapi_client.apis.tags.quiz_submission_user_list_api import QuizSubmissionUserListApi
from openapi_client.apis.tags.quiz_submissions_api import QuizSubmissionsApi
from openapi_client.apis.tags.quizzes_api import QuizzesApi
from openapi_client.apis.tags.roles_api import RolesApi
from openapi_client.apis.tags.rubrics_api import RubricsApi
from openapi_client.apis.tags.search_api import SearchApi
from openapi_client.apis.tags.sections_api import SectionsApi
from openapi_client.apis.tags.services_api import ServicesApi
from openapi_client.apis.tags.shared_brand_configs_api import SharedBrandConfigsApi
from openapi_client.apis.tags.sis_import_errors_api import SisImportErrorsApi
from openapi_client.apis.tags.sis_imports_api import SisImportsApi
from openapi_client.apis.tags.sis_integration_api import SisIntegrationApi
from openapi_client.apis.tags.submission_comments_api import SubmissionCommentsApi
from openapi_client.apis.tags.submissions_api import SubmissionsApi
from openapi_client.apis.tags.tabs_api import TabsApi
from openapi_client.apis.tags.user_observees_api import UserObserveesApi
from openapi_client.apis.tags.users_api import UsersApi
from openapi_client.apis.tags.webhooks_subscriptions_api import WebhooksSubscriptionsApi

TagToApi = typing_extensions.TypedDict(
    'TagToApi',
    {
        TagValues.ACCOUNT_DOMAIN_LOOKUPS: AccountDomainLookupsApi,
        TagValues.ACCOUNT_NOTIFICATIONS: AccountNotificationsApi,
        TagValues.ACCOUNT_REPORTS: AccountReportsApi,
        TagValues.ACCOUNTS: AccountsApi,
        TagValues.ADMINS: AdminsApi,
        TagValues.ANALYTICS: AnalyticsApi,
        TagValues.ANNOUNCEMENT_EXTERNAL_FEEDS: AnnouncementExternalFeedsApi,
        TagValues.ANNOUNCEMENTS: AnnouncementsApi,
        TagValues.API_TOKEN_SCOPES: ApiTokenScopesApi,
        TagValues.APPOINTMENT_GROUPS: AppointmentGroupsApi,
        TagValues.ASSIGNMENT_GROUPS: AssignmentGroupsApi,
        TagValues.ASSIGNMENTS: AssignmentsApi,
        TagValues.AUTHENTICATION_PROVIDERS: AuthenticationProvidersApi,
        TagValues.AUTHENTICATIONS_LOG: AuthenticationsLogApi,
        TagValues.BLUEPRINT_COURSES: BlueprintCoursesApi,
        TagValues.BOOKMARKS: BookmarksApi,
        TagValues.BRAND_CONFIGS: BrandConfigsApi,
        TagValues.CALENDAR_EVENTS: CalendarEventsApi,
        TagValues.COLLABORATIONS: CollaborationsApi,
        TagValues.COMM_MESSAGES: CommMessagesApi,
        TagValues.COMMUNICATION_CHANNELS: CommunicationChannelsApi,
        TagValues.CONFERENCES: ConferencesApi,
        TagValues.CONTENT_EXPORTS: ContentExportsApi,
        TagValues.CONTENT_MIGRATIONS: ContentMigrationsApi,
        TagValues.CONVERSATIONS: ConversationsApi,
        TagValues.COURSE_AUDIT_LOG: CourseAuditLogApi,
        TagValues.COURSE_QUIZ_EXTENSIONS: CourseQuizExtensionsApi,
        TagValues.COURSES: CoursesApi,
        TagValues.CUSTOM_GRADEBOOK_COLUMNS: CustomGradebookColumnsApi,
        TagValues.DISCUSSION_TOPICS: DiscussionTopicsApi,
        TagValues.DOCUMENT_PREVIEWS: DocumentPreviewsApi,
        TagValues.E_PUB_EXPORTS: EPubExportsApi,
        TagValues.ENROLLMENT_TERMS: EnrollmentTermsApi,
        TagValues.ENROLLMENTS: EnrollmentsApi,
        TagValues.ERROR_REPORTS: ErrorReportsApi,
        TagValues.EXTERNAL_TOOLS: ExternalToolsApi,
        TagValues.FAVORITES: FavoritesApi,
        TagValues.FEATURE_FLAGS: FeatureFlagsApi,
        TagValues.FILES: FilesApi,
        TagValues.GRADE_CHANGE_LOG: GradeChangeLogApi,
        TagValues.GRADEBOOK_HISTORY: GradebookHistoryApi,
        TagValues.GRADING_PERIODS: GradingPeriodsApi,
        TagValues.GRADING_STANDARDS: GradingStandardsApi,
        TagValues.GROUP_CATEGORIES: GroupCategoriesApi,
        TagValues.GROUPS: GroupsApi,
        TagValues.JW_TS: JwTsApi,
        TagValues.LATE_POLICY: LatePolicyApi,
        TagValues.LIVE_ASSESSMENTS: LiveAssessmentsApi,
        TagValues.LOGINS: LoginsApi,
        TagValues.MODERATED_GRADING: ModeratedGradingApi,
        TagValues.MODULES: ModulesApi,
        TagValues.NOTIFICATION_PREFERENCES: NotificationPreferencesApi,
        TagValues.ORIGINALITY_REPORTS: OriginalityReportsApi,
        TagValues.OUTCOME_GROUPS: OutcomeGroupsApi,
        TagValues.OUTCOME_IMPORTS: OutcomeImportsApi,
        TagValues.OUTCOME_RESULTS: OutcomeResultsApi,
        TagValues.OUTCOMES: OutcomesApi,
        TagValues.PAGES: PagesApi,
        TagValues.PEER_REVIEWS: PeerReviewsApi,
        TagValues.PLAGIARISM_DETECTION_PLATFORM_ASSIGNMENTS: PlagiarismDetectionPlatformAssignmentsApi,
        TagValues.PLAGIARISM_DETECTION_PLATFORM_USERS: PlagiarismDetectionPlatformUsersApi,
        TagValues.PLAGIARISM_DETECTION_SUBMISSIONS: PlagiarismDetectionSubmissionsApi,
        TagValues.PLANNER: PlannerApi,
        TagValues.PLANNER_NOTE: PlannerNoteApi,
        TagValues.PLANNER_OVERRIDE: PlannerOverrideApi,
        TagValues.POLL_CHOICES: PollChoicesApi,
        TagValues.POLL_SESSIONS: PollSessionsApi,
        TagValues.POLL_SUBMISSIONS: PollSubmissionsApi,
        TagValues.POLLS: PollsApi,
        TagValues.PROFICIENCY_RATINGS: ProficiencyRatingsApi,
        TagValues.PROGRESS: ProgressApi,
        TagValues.QUIZ_ASSIGNMENT_OVERRIDES: QuizAssignmentOverridesApi,
        TagValues.QUIZ_EXTENSIONS: QuizExtensionsApi,
        TagValues.QUIZ_IP_FILTERS: QuizIpFiltersApi,
        TagValues.QUIZ_QUESTION_GROUPS: QuizQuestionGroupsApi,
        TagValues.QUIZ_QUESTIONS: QuizQuestionsApi,
        TagValues.QUIZ_REPORTS: QuizReportsApi,
        TagValues.QUIZ_STATISTICS: QuizStatisticsApi,
        TagValues.QUIZ_SUBMISSION_EVENTS: QuizSubmissionEventsApi,
        TagValues.QUIZ_SUBMISSION_FILES: QuizSubmissionFilesApi,
        TagValues.QUIZ_SUBMISSION_QUESTIONS: QuizSubmissionQuestionsApi,
        TagValues.QUIZ_SUBMISSION_USER_LIST: QuizSubmissionUserListApi,
        TagValues.QUIZ_SUBMISSIONS: QuizSubmissionsApi,
        TagValues.QUIZZES: QuizzesApi,
        TagValues.ROLES: RolesApi,
        TagValues.RUBRICS: RubricsApi,
        TagValues.SEARCH: SearchApi,
        TagValues.SECTIONS: SectionsApi,
        TagValues.SERVICES: ServicesApi,
        TagValues.SHARED_BRAND_CONFIGS: SharedBrandConfigsApi,
        TagValues.SIS_IMPORT_ERRORS: SisImportErrorsApi,
        TagValues.SIS_IMPORTS: SisImportsApi,
        TagValues.SIS_INTEGRATION: SisIntegrationApi,
        TagValues.SUBMISSION_COMMENTS: SubmissionCommentsApi,
        TagValues.SUBMISSIONS: SubmissionsApi,
        TagValues.TABS: TabsApi,
        TagValues.USER_OBSERVEES: UserObserveesApi,
        TagValues.USERS: UsersApi,
        TagValues.WEBHOOKS_SUBSCRIPTIONS: WebhooksSubscriptionsApi,
    }
)

tag_to_api = TagToApi(
    {
        TagValues.ACCOUNT_DOMAIN_LOOKUPS: AccountDomainLookupsApi,
        TagValues.ACCOUNT_NOTIFICATIONS: AccountNotificationsApi,
        TagValues.ACCOUNT_REPORTS: AccountReportsApi,
        TagValues.ACCOUNTS: AccountsApi,
        TagValues.ADMINS: AdminsApi,
        TagValues.ANALYTICS: AnalyticsApi,
        TagValues.ANNOUNCEMENT_EXTERNAL_FEEDS: AnnouncementExternalFeedsApi,
        TagValues.ANNOUNCEMENTS: AnnouncementsApi,
        TagValues.API_TOKEN_SCOPES: ApiTokenScopesApi,
        TagValues.APPOINTMENT_GROUPS: AppointmentGroupsApi,
        TagValues.ASSIGNMENT_GROUPS: AssignmentGroupsApi,
        TagValues.ASSIGNMENTS: AssignmentsApi,
        TagValues.AUTHENTICATION_PROVIDERS: AuthenticationProvidersApi,
        TagValues.AUTHENTICATIONS_LOG: AuthenticationsLogApi,
        TagValues.BLUEPRINT_COURSES: BlueprintCoursesApi,
        TagValues.BOOKMARKS: BookmarksApi,
        TagValues.BRAND_CONFIGS: BrandConfigsApi,
        TagValues.CALENDAR_EVENTS: CalendarEventsApi,
        TagValues.COLLABORATIONS: CollaborationsApi,
        TagValues.COMM_MESSAGES: CommMessagesApi,
        TagValues.COMMUNICATION_CHANNELS: CommunicationChannelsApi,
        TagValues.CONFERENCES: ConferencesApi,
        TagValues.CONTENT_EXPORTS: ContentExportsApi,
        TagValues.CONTENT_MIGRATIONS: ContentMigrationsApi,
        TagValues.CONVERSATIONS: ConversationsApi,
        TagValues.COURSE_AUDIT_LOG: CourseAuditLogApi,
        TagValues.COURSE_QUIZ_EXTENSIONS: CourseQuizExtensionsApi,
        TagValues.COURSES: CoursesApi,
        TagValues.CUSTOM_GRADEBOOK_COLUMNS: CustomGradebookColumnsApi,
        TagValues.DISCUSSION_TOPICS: DiscussionTopicsApi,
        TagValues.DOCUMENT_PREVIEWS: DocumentPreviewsApi,
        TagValues.E_PUB_EXPORTS: EPubExportsApi,
        TagValues.ENROLLMENT_TERMS: EnrollmentTermsApi,
        TagValues.ENROLLMENTS: EnrollmentsApi,
        TagValues.ERROR_REPORTS: ErrorReportsApi,
        TagValues.EXTERNAL_TOOLS: ExternalToolsApi,
        TagValues.FAVORITES: FavoritesApi,
        TagValues.FEATURE_FLAGS: FeatureFlagsApi,
        TagValues.FILES: FilesApi,
        TagValues.GRADE_CHANGE_LOG: GradeChangeLogApi,
        TagValues.GRADEBOOK_HISTORY: GradebookHistoryApi,
        TagValues.GRADING_PERIODS: GradingPeriodsApi,
        TagValues.GRADING_STANDARDS: GradingStandardsApi,
        TagValues.GROUP_CATEGORIES: GroupCategoriesApi,
        TagValues.GROUPS: GroupsApi,
        TagValues.JW_TS: JwTsApi,
        TagValues.LATE_POLICY: LatePolicyApi,
        TagValues.LIVE_ASSESSMENTS: LiveAssessmentsApi,
        TagValues.LOGINS: LoginsApi,
        TagValues.MODERATED_GRADING: ModeratedGradingApi,
        TagValues.MODULES: ModulesApi,
        TagValues.NOTIFICATION_PREFERENCES: NotificationPreferencesApi,
        TagValues.ORIGINALITY_REPORTS: OriginalityReportsApi,
        TagValues.OUTCOME_GROUPS: OutcomeGroupsApi,
        TagValues.OUTCOME_IMPORTS: OutcomeImportsApi,
        TagValues.OUTCOME_RESULTS: OutcomeResultsApi,
        TagValues.OUTCOMES: OutcomesApi,
        TagValues.PAGES: PagesApi,
        TagValues.PEER_REVIEWS: PeerReviewsApi,
        TagValues.PLAGIARISM_DETECTION_PLATFORM_ASSIGNMENTS: PlagiarismDetectionPlatformAssignmentsApi,
        TagValues.PLAGIARISM_DETECTION_PLATFORM_USERS: PlagiarismDetectionPlatformUsersApi,
        TagValues.PLAGIARISM_DETECTION_SUBMISSIONS: PlagiarismDetectionSubmissionsApi,
        TagValues.PLANNER: PlannerApi,
        TagValues.PLANNER_NOTE: PlannerNoteApi,
        TagValues.PLANNER_OVERRIDE: PlannerOverrideApi,
        TagValues.POLL_CHOICES: PollChoicesApi,
        TagValues.POLL_SESSIONS: PollSessionsApi,
        TagValues.POLL_SUBMISSIONS: PollSubmissionsApi,
        TagValues.POLLS: PollsApi,
        TagValues.PROFICIENCY_RATINGS: ProficiencyRatingsApi,
        TagValues.PROGRESS: ProgressApi,
        TagValues.QUIZ_ASSIGNMENT_OVERRIDES: QuizAssignmentOverridesApi,
        TagValues.QUIZ_EXTENSIONS: QuizExtensionsApi,
        TagValues.QUIZ_IP_FILTERS: QuizIpFiltersApi,
        TagValues.QUIZ_QUESTION_GROUPS: QuizQuestionGroupsApi,
        TagValues.QUIZ_QUESTIONS: QuizQuestionsApi,
        TagValues.QUIZ_REPORTS: QuizReportsApi,
        TagValues.QUIZ_STATISTICS: QuizStatisticsApi,
        TagValues.QUIZ_SUBMISSION_EVENTS: QuizSubmissionEventsApi,
        TagValues.QUIZ_SUBMISSION_FILES: QuizSubmissionFilesApi,
        TagValues.QUIZ_SUBMISSION_QUESTIONS: QuizSubmissionQuestionsApi,
        TagValues.QUIZ_SUBMISSION_USER_LIST: QuizSubmissionUserListApi,
        TagValues.QUIZ_SUBMISSIONS: QuizSubmissionsApi,
        TagValues.QUIZZES: QuizzesApi,
        TagValues.ROLES: RolesApi,
        TagValues.RUBRICS: RubricsApi,
        TagValues.SEARCH: SearchApi,
        TagValues.SECTIONS: SectionsApi,
        TagValues.SERVICES: ServicesApi,
        TagValues.SHARED_BRAND_CONFIGS: SharedBrandConfigsApi,
        TagValues.SIS_IMPORT_ERRORS: SisImportErrorsApi,
        TagValues.SIS_IMPORTS: SisImportsApi,
        TagValues.SIS_INTEGRATION: SisIntegrationApi,
        TagValues.SUBMISSION_COMMENTS: SubmissionCommentsApi,
        TagValues.SUBMISSIONS: SubmissionsApi,
        TagValues.TABS: TabsApi,
        TagValues.USER_OBSERVEES: UserObserveesApi,
        TagValues.USERS: UsersApi,
        TagValues.WEBHOOKS_SUBSCRIPTIONS: WebhooksSubscriptionsApi,
    }
)
