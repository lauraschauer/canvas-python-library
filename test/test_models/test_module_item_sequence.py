# coding: utf-8

"""
    Canvas API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 1.0.0
    Generated by: https://openapi-generator.tech
"""

import unittest

import openapi_client
from openapi_client.model.module_item_sequence import ModuleItemSequence
from openapi_client import configuration


class TestModuleItemSequence(unittest.TestCase):
    """ModuleItemSequence unit test stubs"""
    _configuration = configuration.Configuration()


if __name__ == '__main__':
    unittest.main()
