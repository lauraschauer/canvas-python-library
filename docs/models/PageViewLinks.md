# openapi_client.model.page_view_links.PageViewLinks

The links of a page view access in Canvas

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | The links of a page view access in Canvas | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**account** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the account context for this page view | [optional] value must be a 64 bit integer
**asset** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the asset for the request, if any | [optional] value must be a 64 bit integer
**context** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the context for the request (course id if context_type is Course, etc) | [optional] value must be a 64 bit integer
**real_user** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the actual user who made this request, if the request was made by a user who was masquerading | [optional] value must be a 64 bit integer
**user** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the user for this page view | [optional] value must be a 64 bit integer
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

