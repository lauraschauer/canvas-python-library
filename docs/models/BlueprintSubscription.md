# openapi_client.model.blueprint_subscription.BlueprintSubscription

Associates a course with a blueprint

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Associates a course with a blueprint | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[blueprint_course](#blueprint_course)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | The blueprint course subscribed to | [optional] 
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the blueprint course subscription | [optional] value must be a 64 bit integer
**template_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the blueprint template the associated course is subscribed to | [optional] value must be a 64 bit integer
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# blueprint_course

The blueprint course subscribed to

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | The blueprint course subscribed to | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

