# openapi_client.model.assignment_override.AssignmentOverride

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**all_day** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the overridden all day flag (present if due_at is overridden) | [optional] 
**all_day_date** | None, str, datetime,  | NoneClass, str,  | the overridden all day date (present if due_at is overridden) | [optional] value must conform to RFC-3339 date-time
**assignment_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the ID of the assignment the override applies to | [optional] 
**course_section_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the ID of the overrides&#x27;s target section (present if the override targets a section) | [optional] 
**due_at** | None, str, datetime,  | NoneClass, str,  | the overridden due at (present if due_at is overridden) | [optional] value must conform to RFC-3339 date-time
**group_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the ID of the override&#x27;s target group (present if the override targets a group and the assignment is a group assignment) | [optional] 
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the ID of the assignment override | [optional] 
**lock_at** | None, str, datetime,  | NoneClass, str,  | the overridden lock at, if any (present if lock_at is overridden) | [optional] value must conform to RFC-3339 date-time
**[student_ids](#student_ids)** | list, tuple, None,  | tuple, NoneClass,  | the IDs of the override&#x27;s target students (present if the override targets an ad-hoc set of students) | [optional] 
**title** | None, str,  | NoneClass, str,  | the title of the override | [optional] 
**unlock_at** | None, str, datetime,  | NoneClass, str,  | the overridden unlock at (present if unlock_at is overridden) | [optional] value must conform to RFC-3339 date-time
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# student_ids

the IDs of the override's target students (present if the override targets an ad-hoc set of students)

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | the IDs of the override&#x27;s target students (present if the override targets an ad-hoc set of students) | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

