# openapi_client.model.module_item_sequence.ModuleItemSequence

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[items](#items)** | list, tuple, None,  | tuple, NoneClass,  | an array containing one ModuleItemSequenceNode for each appearence of the asset in the module sequence (up to 10 total) | [optional] 
**[modules](#modules)** | list, tuple, None,  | tuple, NoneClass,  | an array containing each Module referenced above | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# items

an array containing one ModuleItemSequenceNode for each appearence of the asset in the module sequence (up to 10 total)

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | an array containing one ModuleItemSequenceNode for each appearence of the asset in the module sequence (up to 10 total) | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**ModuleItemSequenceNode**](ModuleItemSequenceNode.md) | [**ModuleItemSequenceNode**](ModuleItemSequenceNode.md) | [**ModuleItemSequenceNode**](ModuleItemSequenceNode.md) |  | 

# modules

an array containing each Module referenced above

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | an array containing each Module referenced above | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**Module**](Module.md) | [**Module**](Module.md) | [**Module**](Module.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

