# openapi_client.model.peer_review.PeerReview

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**assessor** | None, str,  | NoneClass, str,  | The User object for the assessor if the user include parameter is provided (see user API) (optional) | [optional] 
**assessor_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The assessors user id | [optional] 
**asset_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id for the asset associated with this Peer Review | [optional] 
**asset_type** | None, str,  | NoneClass, str,  | The type of the asset | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The id of the Peer Review | [optional] 
**submission_comments** | None, str,  | NoneClass, str,  | The submission comments associated with this Peer Review if the submission_comment include parameter is provided (see submissions API) (optional) | [optional] 
**user** | None, str,  | NoneClass, str,  | the User object for the owner of the asset if the user include parameter is provided (see user API) (optional) | [optional] 
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The user id for the owner of the asset | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The state of the Peer Review, either &#x27;assigned&#x27; or &#x27;completed&#x27; | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

