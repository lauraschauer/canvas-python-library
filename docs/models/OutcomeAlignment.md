# openapi_client.model.outcome_alignment.OutcomeAlignment

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**assignment_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the id of the aligned assignment. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the id of the aligned learning outcome. | [optional] 
**submission_types** | None, str,  | NoneClass, str,  | a string representing the different submission types of an aligned assignment. | [optional] 
**title** | None, str,  | NoneClass, str,  | the title of the aligned assignment. | [optional] 
**url** | None, str,  | NoneClass, str,  | the URL for the aligned assignment. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

