# openapi_client.model.conversation_participant.ConversationParticipant

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**avatar_url** | None, str,  | NoneClass, str,  | If requested, this field will be included and contain a url to retrieve the user&#x27;s avatar. | [optional] 
**full_name** | None, str,  | NoneClass, str,  | The full name of the user. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The user ID for the participant. | [optional] value must be a 64 bit integer
**name** | None, str,  | NoneClass, str,  | A short name the user has selected, for use in conversations or other less formal places through the site. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

