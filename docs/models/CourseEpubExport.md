# openapi_client.model.course_epub_export.CourseEpubExport

Combination of a Course & EpubExport.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Combination of a Course &amp; EpubExport. | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**epub_export** | [**EpubExport**](EpubExport.md) | [**EpubExport**](EpubExport.md) |  | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the unique identifier for the course | [optional] 
**name** | None, str,  | NoneClass, str,  | the name for the course | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

