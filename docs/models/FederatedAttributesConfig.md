# openapi_client.model.federated_attributes_config.FederatedAttributesConfig

A mapping of Canvas attribute names to attribute names that a provider may send, in order to update the value of these attributes when a user logs in. The values can be a FederatedAttributeConfig, or a raw string corresponding to the \"attribute\" property of a FederatedAttributeConfig. In responses, full FederatedAttributeConfig objects are returned if JIT provisioning is enabled, otherwise just the attribute names are returned.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | A mapping of Canvas attribute names to attribute names that a provider may send, in order to update the value of these attributes when a user logs in. The values can be a FederatedAttributeConfig, or a raw string corresponding to the \&quot;attribute\&quot; property of a FederatedAttributeConfig. In responses, full FederatedAttributeConfig objects are returned if JIT provisioning is enabled, otherwise just the attribute names are returned. | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**admin_roles** | None, str,  | NoneClass, str,  | A comma separated list of role names to grant to the user. Note that these only apply at the root account level, and not sub-accounts. If the attribute is not marked for provisioning only, the user will also be removed from any other roles they currently hold that are not still specified by the IdP. | [optional] 
**display_name** | None, str,  | NoneClass, str,  | The full display name of the user | [optional] 
**email** | None, str,  | NoneClass, str,  | The user&#x27;s e-mail address | [optional] 
**given_name** | None, str,  | NoneClass, str,  | The first, or given, name of the user | [optional] 
**integration_id** | None, str,  | NoneClass, str,  | The secondary unique identifier for SIS purposes | [optional] 
**locale** | None, str,  | NoneClass, str,  | The user&#x27;s preferred locale/language | [optional] 
**name** | None, str,  | NoneClass, str,  | The full name of the user | [optional] 
**sis_user_id** | None, str,  | NoneClass, str,  | The unique SIS identifier | [optional] 
**sortable_name** | None, str,  | NoneClass, str,  | The full name of the user for sorting purposes | [optional] 
**surname** | None, str,  | NoneClass, str,  | The surname, or last name, of the user | [optional] 
**timezone** | None, str,  | NoneClass, str,  | The user&#x27;s preferred time zone | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

