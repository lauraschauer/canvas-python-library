# openapi_client.model.rubric_criteria.RubricCriteria

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**criterion_use_range** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**description** | None, str,  | NoneClass, str,  |  | [optional] 
**id** | str,  | str,  | The id of rubric criteria. | [optional] 
**learning_outcome_id** | None, str,  | NoneClass, str,  | (Optional) The id of the learning outcome this criteria uses, if any. | [optional] 
**long_description** | None, str,  | NoneClass, str,  |  | [optional] 
**points** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**[ratings](#ratings)** | list, tuple, None,  | tuple, NoneClass,  |  | [optional] 
**vendor_guid** | None, str,  | NoneClass, str,  | (Optional) The 3rd party vendor&#x27;s GUID for the outcome this criteria references, if any. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# ratings

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**RubricRating**](RubricRating.md) | [**RubricRating**](RubricRating.md) | [**RubricRating**](RubricRating.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

