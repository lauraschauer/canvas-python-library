# openapi_client.model.quiz_assignment_override_set_container.QuizAssignmentOverrideSetContainer

Container for set of assignment-overridden dates for a quiz.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Container for set of assignment-overridden dates for a quiz. | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[quiz_assignment_overrides](#quiz_assignment_overrides)** | list, tuple, None,  | tuple, NoneClass,  | The QuizAssignmentOverrideSet | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# quiz_assignment_overrides

The QuizAssignmentOverrideSet

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | The QuizAssignmentOverrideSet | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**QuizAssignmentOverrideSet**](QuizAssignmentOverrideSet.md) | [**QuizAssignmentOverrideSet**](QuizAssignmentOverrideSet.md) | [**QuizAssignmentOverrideSet**](QuizAssignmentOverrideSet.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

