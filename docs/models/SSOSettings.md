# openapi_client.model.sso_settings.SSOSettings

Settings that are applicable across an account's authentication configuration, even if there are multiple individual providers

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Settings that are applicable across an account&#x27;s authentication configuration, even if there are multiple individual providers | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**auth_discovery_url** | None, str,  | NoneClass, str,  | If a discovery url is set, canvas will forward all users to that URL when they need to be authenticated. That page will need to then help the user figure out where they need to go to log in. If no discovery url is configured, the first configuration will be used to attempt to authenticate the user. | [optional] 
**change_password_url** | None, str,  | NoneClass, str,  | The url to redirect users to for password resets. Leave blank for default Canvas behavior | [optional] 
**login_handle_name** | None, str,  | NoneClass, str,  | The label used for unique login identifiers. | [optional] 
**unknown_user_url** | None, str,  | NoneClass, str,  | If an unknown user url is set, Canvas will forward to that url when a service authenticates a user, but that user does not exist in Canvas. The default behavior is to present an error. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

