# openapi_client.model.calendar_event.CalendarEvent

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**all_context_codes** | None, str,  | NoneClass, str,  | a comma-separated list of all calendar contexts this event is part of | [optional] 
**all_day** | None, bool,  | NoneClass, BoolClass,  | Boolean indicating whether this is an all-day event (midnight to midnight) | [optional] 
**all_day_date** | None, str, datetime,  | NoneClass, str,  | The date of this event | [optional] value must conform to RFC-3339 date-time
**appointment_group_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Various Appointment-Group-related fields.These fields are only pertinent to time slots (appointments) and reservations of those time slots. See the Appointment Groups API. The id of the appointment group | [optional] 
**appointment_group_url** | None, str,  | NoneClass, str,  | The API URL of the appointment group | [optional] 
**available_slots** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | If the event is a time slot and it has a participant limit, an integer indicating how many slots are available | [optional] 
**[child_events](#child_events)** | list, tuple, None,  | tuple, NoneClass,  | Included by default, but may be excluded (see include[] option). If this is a time slot (see the Appointment Groups API) this will be a list of any reservations. If this is a course-level event, this will be a list of section-level events (if any) | [optional] 
**child_events_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The number of child_events. See child_events (and parent_event_id) | [optional] 
**context_code** | None, str,  | NoneClass, str,  | the context code of the calendar this event belongs to (course, user or group) | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | When the calendar event was created | [optional] value must conform to RFC-3339 date-time
**description** | None, str,  | NoneClass, str,  | The HTML description of the event | [optional] 
**effective_context_code** | None, str,  | NoneClass, str,  | if specified, it indicates which calendar this event should be displayed on. for example, a section-level event would have the course&#x27;s context code here, while the section&#x27;s context code would be returned above) | [optional] 
**end_at** | None, str, datetime,  | NoneClass, str,  | The end timestamp of the event | [optional] value must conform to RFC-3339 date-time
**group** | None, str,  | NoneClass, str,  | If the event is a group-level reservation, this will contain the group participant JSON (refer to the Groups API). | [optional] 
**hidden** | None, bool,  | NoneClass, BoolClass,  | Whether this event should be displayed on the calendar. Only true for course-level events with section-level child events. | [optional] 
**html_url** | None, str,  | NoneClass, str,  | URL for a user to view this event | [optional] 
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the calendar event | [optional] 
**location_address** | None, str,  | NoneClass, str,  | The address where the event is taking place | [optional] 
**location_name** | None, str,  | NoneClass, str,  | The location name of the event | [optional] 
**own_reservation** | None, bool,  | NoneClass, BoolClass,  | If the event is a reservation, this a boolean indicating whether it is the current user&#x27;s reservation, or someone else&#x27;s | [optional] 
**parent_event_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Normally null. If this is a reservation (see the Appointment Groups API), the id will indicate the time slot it is for. If this is a section-level event, this will be the course-level parent event. | [optional] 
**participant_type** | None, str,  | NoneClass, str,  | The type of participant to sign up for a slot: &#x27;User&#x27; or &#x27;Group&#x27; | [optional] 
**participants_per_appointment** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | If the event is a time slot, this is the participant limit | [optional] 
**reserve_url** | None, str,  | NoneClass, str,  | If the event is a time slot, the API URL for reserving it | [optional] 
**reserved** | None, bool,  | NoneClass, BoolClass,  | If the event is a time slot, a boolean indicating whether the user has already made a reservation for it | [optional] 
**start_at** | None, str, datetime,  | NoneClass, str,  | The start timestamp of the event | [optional] value must conform to RFC-3339 date-time
**title** | None, str,  | NoneClass, str,  | The title of the calendar event | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | When the calendar event was last updated | [optional] value must conform to RFC-3339 date-time
**url** | str,  | str,  | URL for this calendar event (to update, delete, etc.) | [optional] 
**user** | None, str,  | NoneClass, str,  | If the event is a user-level reservation, this will contain the user participant JSON (refer to the Users API). | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | Current state of the event (&#x27;active&#x27;, &#x27;locked&#x27; or &#x27;deleted&#x27;) &#x27;locked&#x27; indicates that start_at/end_at cannot be changed (though the event could be deleted). Normally only reservations or time slots with reservations are locked (see the Appointment Groups API) | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# child_events

Included by default, but may be excluded (see include[] option). If this is a time slot (see the Appointment Groups API) this will be a list of any reservations. If this is a course-level event, this will be a list of section-level events (if any)

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Included by default, but may be excluded (see include[] option). If this is a time slot (see the Appointment Groups API) this will be a list of any reservations. If this is a course-level event, this will be a list of section-level events (if any) | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

