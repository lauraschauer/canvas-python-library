# openapi_client.model.usage_rights.UsageRights

Describes the copyright and license information for a File

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Describes the copyright and license information for a File | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[file_ids](#file_ids)** | list, tuple, None,  | tuple, NoneClass,  | List of ids of files that were updated | [optional] 
**legal_copyright** | None, str,  | NoneClass, str,  | Copyright line for the file | [optional] 
**license** | None, str,  | NoneClass, str,  | License identifier for the file. | [optional] 
**license_name** | None, str,  | NoneClass, str,  | Readable license name | [optional] 
**message** | None, str,  | NoneClass, str,  | Explanation of the action performed | [optional] 
**use_justification** | None, str,  | NoneClass, str,  | Justification for using the file in a Canvas course. Valid values are &#x27;own_copyright&#x27;, &#x27;public_domain&#x27;, &#x27;used_by_permission&#x27;, &#x27;fair_use&#x27;, &#x27;creative_commons&#x27; | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# file_ids

List of ids of files that were updated

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | List of ids of files that were updated | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

