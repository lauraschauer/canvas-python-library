# openapi_client.model.discussion_topic.DiscussionTopic

A discussion topic

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | A discussion topic | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**allow_rating** | None, bool,  | NoneClass, BoolClass,  | Whether or not users can rate entries in this topic. | [optional] 
**assignment_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The unique identifier of the assignment if the topic is for grading, otherwise null. | [optional] 
**[attachments](#attachments)** | list, tuple, None,  | tuple, NoneClass,  | Array of file attachments. | [optional] 
**delayed_post_at** | None, str, datetime,  | NoneClass, str,  | The datetime to publish the topic (if not right away). | [optional] value must conform to RFC-3339 date-time
**discussion_subentry_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The count of entries in the topic. | [optional] 
**discussion_type** | None, str,  | NoneClass, str,  | The type of discussion. Values are &#x27;side_comment&#x27;, for discussions that only allow one level of nested comments, and &#x27;threaded&#x27; for fully threaded discussions. | [optional] 
**group_category_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The unique identifier of the group category if the topic is a group discussion, otherwise null. | [optional] 
**[group_topic_children](#group_topic_children)** | list, tuple, None,  | tuple, NoneClass,  | An array of group discussions the user is a part of. Fields include: id, group_id | [optional] 
**html_url** | None, str,  | NoneClass, str,  | The URL to the discussion topic in canvas. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of this topic. | [optional] 
**last_reply_at** | None, str, datetime,  | NoneClass, str,  | The datetime for when the last reply was in the topic. | [optional] value must conform to RFC-3339 date-time
**lock_at** | None, str, datetime,  | NoneClass, str,  | The datetime to lock the topic (if ever). | [optional] value must conform to RFC-3339 date-time
**lock_explanation** | None, str,  | NoneClass, str,  | (Optional) An explanation of why this is locked for the user. Present when locked_for_user is true. | [optional] 
**lock_info** | [**LockInfo**](LockInfo.md) | [**LockInfo**](LockInfo.md) |  | [optional] 
**locked** | None, bool,  | NoneClass, BoolClass,  | Whether or not the discussion is &#x27;closed for comments&#x27;. | [optional] 
**locked_for_user** | None, bool,  | NoneClass, BoolClass,  | Whether or not this is locked for the user. | [optional] 
**message** | None, str,  | NoneClass, str,  | The HTML content of the message body. | [optional] 
**only_graders_can_rate** | None, bool,  | NoneClass, BoolClass,  | Whether or not grade permissions are required to rate entries. | [optional] 
**[permissions](#permissions)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | The current user&#x27;s permissions on this topic. | [optional] 
**pinned** | None, bool,  | NoneClass, BoolClass,  | Whether or not the discussion has been &#x27;pinned&#x27; by an instructor | [optional] 
**podcast_url** | None, str,  | NoneClass, str,  | If the topic is a podcast topic this is the feed url for the current user. | [optional] 
**posted_at** | None, str, datetime,  | NoneClass, str,  | The datetime the topic was posted. If it is null it hasn&#x27;t been posted yet. (see delayed_post_at) | [optional] value must conform to RFC-3339 date-time
**published** | None, bool,  | NoneClass, BoolClass,  | Whether this discussion topic is published (true) or draft state (false) | [optional] 
**read_state** | None, str,  | NoneClass, str,  | The read_state of the topic for the current user, &#x27;read&#x27; or &#x27;unread&#x27;. | [optional] 
**require_initial_post** | None, bool,  | NoneClass, BoolClass,  | If true then a user may not respond to other replies until that user has made an initial reply. Defaults to false. | [optional] 
**root_topic_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | If the topic is for grading and a group assignment this will point to the original topic in the course. | [optional] 
**sort_by_rating** | None, bool,  | NoneClass, BoolClass,  | Whether or not entries should be sorted by rating. | [optional] 
**subscribed** | None, bool,  | NoneClass, BoolClass,  | Whether or not the current user is subscribed to this topic. | [optional] 
**subscription_hold** | None, str,  | NoneClass, str,  | (Optional) Why the user cannot subscribe to this topic. Only one reason will be returned even if multiple apply. Can be one of: &#x27;initial_post_required&#x27;: The user must post a reply first; &#x27;not_in_group_set&#x27;: The user is not in the group set for this graded group discussion; &#x27;not_in_group&#x27;: The user is not in this topic&#x27;s group; &#x27;topic_is_announcement&#x27;: This topic is an announcement | [optional] 
**title** | None, str,  | NoneClass, str,  | The topic title. | [optional] 
**[topic_children](#topic_children)** | list, tuple, None,  | tuple, NoneClass,  | DEPRECATED An array of topic_ids for the group discussions the user is a part of. | [optional] 
**unread_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The count of unread entries of this topic for the current user. | [optional] 
**user_can_see_posts** | None, bool,  | NoneClass, BoolClass,  | Whether or not posts in this topic are visible to the user. | [optional] 
**user_name** | None, str,  | NoneClass, str,  | The username of the topic creator. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# attachments

Array of file attachments.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Array of file attachments. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**FileAttachment**](FileAttachment.md) | [**FileAttachment**](FileAttachment.md) | [**FileAttachment**](FileAttachment.md) |  | 

# group_topic_children

An array of group discussions the user is a part of. Fields include: id, group_id

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | An array of group discussions the user is a part of. Fields include: id, group_id | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[items](#items) | dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

# items

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

# permissions

The current user's permissions on this topic.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | The current user&#x27;s permissions on this topic. | 

# topic_children

DEPRECATED An array of topic_ids for the group discussions the user is a part of.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | DEPRECATED An array of topic_ids for the group discussions the user is a part of. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

