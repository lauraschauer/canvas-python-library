# openapi_client.model.conversation.Conversation

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[audience](#audience)** | list, tuple, None,  | tuple, NoneClass,  | Array of user ids who are involved in the conversation, ordered by participation level, then alphabetical. Excludes current user, unless this is a monologue. | [optional] 
**[audience_contexts](#audience_contexts)** | list, tuple, None,  | tuple, NoneClass,  | Most relevant shared contexts (courses and groups) between current user and other participants. If there is only one participant, it will also include that user&#x27;s enrollment(s)/ membership type(s) in each course/group. | [optional] 
**avatar_url** | None, str,  | NoneClass, str,  | URL to appropriate icon for this conversation (custom, individual or group avatar, depending on audience). | [optional] 
**context_name** | None, str,  | NoneClass, str,  | Name of the course or group in which the conversation is occurring. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the unique identifier for the conversation. | [optional] value must be a 64 bit integer
**last_message** | None, str,  | NoneClass, str,  | A &lt;&#x3D;100 character preview from the most recent message. | [optional] 
**message_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the number of messages in the conversation. | [optional] 
**[participants](#participants)** | list, tuple, None,  | tuple, NoneClass,  | Array of users participating in the conversation. Includes current user. | [optional] 
**private** | None, bool,  | NoneClass, BoolClass,  | whether the conversation is private. | [optional] 
**[properties](#properties)** | list, tuple, None,  | tuple, NoneClass,  | Additional conversation flags (last_author, attachments, media_objects). Each listed property means the flag is set to true (i.e. the current user is the most recent author, there are attachments, or there are media objects) | [optional] 
**starred** | None, bool,  | NoneClass, BoolClass,  | whether the conversation is starred. | [optional] 
**start_at** | None, str, datetime,  | NoneClass, str,  | the date and time at which the last message was sent. | [optional] value must conform to RFC-3339 date-time
**subject** | None, str,  | NoneClass, str,  | the subject of the conversation. | [optional] 
**subscribed** | None, bool,  | NoneClass, BoolClass,  | whether the current user is subscribed to the conversation. | [optional] 
**visible** | None, bool,  | NoneClass, BoolClass,  | indicates whether the conversation is visible under the current scope and filter. This attribute is always true in the index API response, and is primarily useful in create/update responses so that you can know if the record should be displayed in the UI. The default scope is assumed, unless a scope or filter is passed to the create/update API call. | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The current state of the conversation (read, unread or archived). | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# audience

Array of user ids who are involved in the conversation, ordered by participation level, then alphabetical. Excludes current user, unless this is a monologue.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Array of user ids who are involved in the conversation, ordered by participation level, then alphabetical. Excludes current user, unless this is a monologue. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

# audience_contexts

Most relevant shared contexts (courses and groups) between current user and other participants. If there is only one participant, it will also include that user's enrollment(s)/ membership type(s) in each course/group.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Most relevant shared contexts (courses and groups) between current user and other participants. If there is only one participant, it will also include that user&#x27;s enrollment(s)/ membership type(s) in each course/group. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# participants

Array of users participating in the conversation. Includes current user.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Array of users participating in the conversation. Includes current user. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**ConversationParticipant**](ConversationParticipant.md) | [**ConversationParticipant**](ConversationParticipant.md) | [**ConversationParticipant**](ConversationParticipant.md) |  | 

# properties

Additional conversation flags (last_author, attachments, media_objects). Each listed property means the flag is set to true (i.e. the current user is the most recent author, there are attachments, or there are media objects)

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Additional conversation flags (last_author, attachments, media_objects). Each listed property means the flag is set to true (i.e. the current user is the most recent author, there are attachments, or there are media objects) | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

