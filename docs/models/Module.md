# openapi_client.model.module.Module

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**completed_at** | None, str, datetime,  | NoneClass, str,  | the date the calling user completed the module (Optional; present only if the caller is a student or if the optional parameter &#x27;student_id&#x27; is included) | [optional] value must conform to RFC-3339 date-time
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the unique identifier for the module | [optional] 
**[items](#items)** | list, tuple, None,  | tuple, NoneClass,  | The contents of this module, as an array of Module Items. (Present only if requested via include[]&#x3D;items AND the module is not deemed too large by Canvas.) | [optional] 
**items_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The number of items in the module | [optional] 
**items_url** | None, str,  | NoneClass, str,  | The API URL to retrive this module&#x27;s items | [optional] 
**name** | None, str,  | NoneClass, str,  | the name of this module | [optional] 
**position** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the position of this module in the course (1-based) | [optional] 
**[prerequisite_module_ids](#prerequisite_module_ids)** | list, tuple, None,  | tuple, NoneClass,  | IDs of Modules that must be completed before this one is unlocked | [optional] 
**publish_final_grade** | None, bool,  | NoneClass, BoolClass,  | if the student&#x27;s final grade for the course should be published to the SIS upon completion of this module | [optional] 
**published** | None, bool,  | NoneClass, BoolClass,  | (Optional) Whether this module is published. This field is present only if the caller has permission to view unpublished modules. | [optional] 
**require_sequential_progress** | None, bool,  | NoneClass, BoolClass,  | Whether module items must be unlocked in order | [optional] 
**state** | None, str,  | NoneClass, str,  | The state of this Module for the calling user one of &#x27;locked&#x27;, &#x27;unlocked&#x27;, &#x27;started&#x27;, &#x27;completed&#x27; (Optional; present only if the caller is a student or if the optional parameter &#x27;student_id&#x27; is included) | [optional] 
**unlock_at** | None, str, datetime,  | NoneClass, str,  | (Optional) the date this module will unlock | [optional] value must conform to RFC-3339 date-time
**workflow_state** | None, str,  | NoneClass, str,  | the state of the module: &#x27;active&#x27;, &#x27;deleted&#x27; | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# items

The contents of this module, as an array of Module Items. (Present only if requested via include[]=items AND the module is not deemed too large by Canvas.)

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | The contents of this module, as an array of Module Items. (Present only if requested via include[]&#x3D;items AND the module is not deemed too large by Canvas.) | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**ModuleItem**](ModuleItem.md) | [**ModuleItem**](ModuleItem.md) | [**ModuleItem**](ModuleItem.md) |  | 

# prerequisite_module_ids

IDs of Modules that must be completed before this one is unlocked

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | IDs of Modules that must be completed before this one is unlocked | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

