# openapi_client.model.submission.Submission

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**anonymous_id** | None, str,  | NoneClass, str,  | A unique short ID identifying this submission without reference to the owning user. Only included if the caller has administrator access for the current account. | [optional] 
**assignment** | None, str,  | NoneClass, str,  | The submission&#x27;s assignment (see the assignments API) (optional) | [optional] 
**assignment_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The submission&#x27;s assignment id | [optional] 
**assignment_visible** | None, bool,  | NoneClass, BoolClass,  | Whether the assignment is visible to the user who submitted the assignment. Submissions where &#x60;assignment_visible&#x60; is false no longer count towards the student&#x27;s grade and the assignment can no longer be accessed by the student. &#x60;assignment_visible&#x60; becomes false for submissions that do not have a grade and whose assignment is no longer assigned to the student&#x27;s section. | [optional] 
**attempt** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | This is the submission attempt number. | [optional] 
**body** | None, str,  | NoneClass, str,  | The content of the submission, if it was submitted directly in a text field. | [optional] 
**course** | None, str,  | NoneClass, str,  | The submission&#x27;s course (see the course API) (optional) | [optional] 
**excused** | None, bool,  | NoneClass, BoolClass,  | Whether the assignment is excused.  Excused assignments have no impact on a user&#x27;s grade. | [optional] 
**extra_attempts** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Extra submission attempts allowed fro the given user and assingment. | [optional] 
**grade** | None, str,  | NoneClass, str,  | The grade for the submission, translated into the assignment grading scheme (so a letter grade, for example). | [optional] 
**grade_matches_current_submission** | None, bool,  | NoneClass, BoolClass,  | A boolean flag which is false if the student has re-submitted since the submission was last graded. | [optional] 
**graded_at** | None, str, datetime,  | NoneClass, str,  |  | [optional] value must conform to RFC-3339 date-time
**grader_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the user who graded the submission. This will be null for submissions that haven&#x27;t been graded yet. It will be a positive number if a real user has graded the submission and a negative number if the submission was graded by a process (e.g. Quiz autograder and autograding LTI tools).  Specifically autograded quizzes set grader_id to the negative of the quiz id.  Submissions autograded by LTI tools set grader_id to the negative of the tool id. | [optional] 
**html_url** | None, str,  | NoneClass, str,  | URL to the submission. This will require the user to log in. | [optional] 
**late** | None, bool,  | NoneClass, BoolClass,  | Whether the submission was made after the applicable due date | [optional] 
**late_policy_status** | None, str,  | NoneClass, str,  | The status of the submission in relation to the late policy. Can be late, missing, none, or null. | [optional] 
**missing** | None, bool,  | NoneClass, BoolClass,  | Whether the assignment is missing. | [optional] 
**points_deducted** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | The amount of points automatically deducted from the score by the missing/late policy for a late or missing assignment. | [optional] 
**posted_at** | None, str,  | NoneClass, str,  | The date this submission was posted to the student, or nil if it has not been posted | [optional] 
**preview_url** | None, str,  | NoneClass, str,  | URL to the submission preview. This will require the user to log in. | [optional] 
**read_status** | None, str,  | NoneClass, str,  | The read status of this submission for the given user (optional). Including read_status will mark submission(s) as read. | [optional] 
**redo_request** | None, bool,  | NoneClass, BoolClass,  | This indicates whether the submission has been reassigned by the instructor. | [optional] 
**score** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | The raw score | [optional] 
**seconds_late** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | The amount of time, in seconds, that an submission is late by. | [optional] 
**[submission_comments](#submission_comments)** | list, tuple, None,  | tuple, NoneClass,  | Associated comments for a submission (optional) | [optional] 
**submission_type** | None, str,  | NoneClass, str,  | The types of submission ex: (&#x27;online_text_entry&#x27;|&#x27;online_url&#x27;|&#x27;online_upload&#x27;|&#x27;media_recording&#x27;) | [optional] 
**submitted_at** | None, str, datetime,  | NoneClass, str,  | The timestamp when the assignment was submitted | [optional] value must conform to RFC-3339 date-time
**url** | None, str,  | NoneClass, str,  | The URL of the submission (for &#x27;online_url&#x27; submissions). | [optional] 
**user** | None, str,  | NoneClass, str,  | The submissions user (see user API) (optional) | [optional] 
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the user who created the submission | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The current state of the submission | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# submission_comments

Associated comments for a submission (optional)

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Associated comments for a submission (optional) | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**SubmissionComment**](SubmissionComment.md) | [**SubmissionComment**](SubmissionComment.md) | [**SubmissionComment**](SubmissionComment.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

