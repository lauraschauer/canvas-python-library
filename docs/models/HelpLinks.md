# openapi_client.model.help_links.HelpLinks

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[custom_help_links](#custom_help_links)** | list, tuple, None,  | tuple, NoneClass,  | Help links defined by the account. Could include default help links. | [optional] 
**[default_help_links](#default_help_links)** | list, tuple, None,  | tuple, NoneClass,  | Default help links provided when account has not set help links of their own. | [optional] 
**help_link_icon** | None, str,  | NoneClass, str,  | Help link button icon | [optional] 
**help_link_name** | None, str,  | NoneClass, str,  | Help link button title | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# custom_help_links

Help links defined by the account. Could include default help links.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Help links defined by the account. Could include default help links. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**HelpLink**](HelpLink.md) | [**HelpLink**](HelpLink.md) | [**HelpLink**](HelpLink.md) |  | 

# default_help_links

Default help links provided when account has not set help links of their own.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Default help links provided when account has not set help links of their own. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**HelpLink**](HelpLink.md) | [**HelpLink**](HelpLink.md) | [**HelpLink**](HelpLink.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

