# openapi_client.model.submission_version.SubmissionVersion

A SubmissionVersion object contains all the fields that a Submission object does, plus additional fields prefixed with current_* new_* and previous_* described below.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | A SubmissionVersion object contains all the fields that a Submission object does, plus additional fields prefixed with current_* new_* and previous_* described below. | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**assignment_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the id of the assignment this submissions is for | [optional] 
**assignment_name** | None, str,  | NoneClass, str,  | the name of the assignment this submission is for | [optional] 
**body** | None, str,  | NoneClass, str,  | the body text of the submission | [optional] 
**current_grade** | None, str,  | NoneClass, str,  | the most up to date grade for the current version of this submission | [optional] 
**current_graded_at** | None, str, datetime,  | NoneClass, str,  | the latest time stamp for the grading of this submission | [optional] value must conform to RFC-3339 date-time
**current_grader** | None, str,  | NoneClass, str,  | the name of the most recent grader for this submission | [optional] 
**grade_matches_current_submission** | None, bool,  | NoneClass, BoolClass,  | boolean indicating whether the grade is equal to the current submission grade | [optional] 
**graded_at** | None, str, datetime,  | NoneClass, str,  | time stamp for the grading of this version of the submission | [optional] value must conform to RFC-3339 date-time
**grader** | None, str,  | NoneClass, str,  | the name of the user who graded this version of the submission | [optional] 
**grader_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the user id of the user who graded this version of the submission | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the id of the submission of which this is a version | [optional] 
**new_grade** | None, str,  | NoneClass, str,  | the updated grade provided in this version of the submission | [optional] 
**new_graded_at** | None, str, datetime,  | NoneClass, str,  | the timestamp for the grading of this version of the submission (alias for graded_at) | [optional] value must conform to RFC-3339 date-time
**new_grader** | None, str,  | NoneClass, str,  | alias for &#x27;grader&#x27; | [optional] 
**previous_grade** | None, str,  | NoneClass, str,  | the grade for the submission version immediately preceding this one | [optional] 
**previous_graded_at** | None, str, datetime,  | NoneClass, str,  | the timestamp for the grading of the submission version immediately preceding this one | [optional] value must conform to RFC-3339 date-time
**previous_grader** | None, str,  | NoneClass, str,  | the name of the grader who graded the version of this submission immediately preceding this one | [optional] 
**score** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the score for this version of the submission | [optional] 
**submission_type** | None, str,  | NoneClass, str,  | the type of submission | [optional] 
**url** | None, str,  | NoneClass, str,  | the url of the submission, if there is one | [optional] 
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the user ID of the student who created this submission | [optional] 
**user_name** | None, str,  | NoneClass, str,  | the name of the student who created this submission | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | the state of the submission at this version | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

