# openapi_client.model.rubric_assessment.RubricAssessment

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**artifact_attempt** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the current number of attempts made on the object of the assessment | [optional] 
**artifact_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the id of the object of the assessment | [optional] 
**artifact_type** | None, str,  | NoneClass, str,  | the object of the assessment | [optional] 
**assessment_type** | None, str,  | NoneClass, str,  | the type of assessment. values will be either &#x27;grading&#x27;, &#x27;peer_review&#x27;, or &#x27;provisional_grade&#x27; | [optional] 
**assessor_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | user id of the person who made the assessment | [optional] 
**[comments](#comments)** | list, tuple, None,  | tuple, NoneClass,  | (Optional) If &#x27;comments_only&#x27; is included in the &#x27;style&#x27; parameter, returned assessments will include only the comments portion of their data hash. If the user does not request a style, this key will be absent. | [optional] 
**[data](#data)** | list, tuple, None,  | tuple, NoneClass,  | (Optional) If &#x27;full&#x27; is included in the &#x27;style&#x27; parameter, returned assessments will have their full details contained in their data hash. If the user does not request a style, this key will be absent. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the ID of the rubric | [optional] 
**rubric_association_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**rubric_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the rubric the assessment belongs to | [optional] 
**score** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# comments

(Optional) If 'comments_only' is included in the 'style' parameter, returned assessments will include only the comments portion of their data hash. If the user does not request a style, this key will be absent.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | (Optional) If &#x27;comments_only&#x27; is included in the &#x27;style&#x27; parameter, returned assessments will include only the comments portion of their data hash. If the user does not request a style, this key will be absent. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# data

(Optional) If 'full' is included in the 'style' parameter, returned assessments will have their full details contained in their data hash. If the user does not request a style, this key will be absent.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | (Optional) If &#x27;full&#x27; is included in the &#x27;style&#x27; parameter, returned assessments will have their full details contained in their data hash. If the user does not request a style, this key will be absent. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[items](#items) | dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

# items

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

