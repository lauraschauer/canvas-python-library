# openapi_client.model.outcome_link.OutcomeLink

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**assessed** | None, bool,  | NoneClass, BoolClass,  | whether this outcome has been used to assess a student in the context of this outcome link.  In other words, this will be set to true if the context is a course, and a student has been assessed with this outcome in that course. | [optional] 
**can_unlink** | None, bool,  | NoneClass, BoolClass,  | whether this outcome link is manageable and is not the last link to an aligned outcome | [optional] 
**context_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the context owning the outcome link. will match the context owning the outcome group containing the outcome link; included for convenience. may be null for links in global outcome groups. | [optional] 
**context_type** | None, str,  | NoneClass, str,  |  | [optional] 
**outcome** | [**Outcome**](Outcome.md) | [**Outcome**](Outcome.md) |  | [optional] 
**outcome_group** | [**OutcomeGroup**](OutcomeGroup.md) | [**OutcomeGroup**](OutcomeGroup.md) |  | [optional] 
**url** | None, str,  | NoneClass, str,  | the URL for fetching/updating the outcome link. should be treated as opaque | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

