# openapi_client.model.outcome_import.OutcomeImport

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**created_at** | None, str, datetime,  | NoneClass, str,  | The date the outcome import was created. | [optional] value must conform to RFC-3339 date-time
**data** | [**OutcomeImportData**](OutcomeImportData.md) | [**OutcomeImportData**](OutcomeImportData.md) |  | [optional] 
**ended_at** | None, str, datetime,  | NoneClass, str,  | The date the outcome import finished. Returns null if not finished. | [optional] value must conform to RFC-3339 date-time
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The unique identifier for the outcome import. | [optional] 
**[processing_errors](#processing_errors)** | list, tuple, None,  | tuple, NoneClass,  | An array of row number / error message pairs. Returns the first 25 errors. | [optional] 
**progress** | None, str,  | NoneClass, str,  | The progress of the outcome import. | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | The date the outcome import was last updated. | [optional] value must conform to RFC-3339 date-time
**user** | [**User**](User.md) | [**User**](User.md) |  | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The current state of the outcome import.  - &#x27;created&#x27;: The outcome import has been created.  - &#x27;importing&#x27;: The outcome import is currently processing.  - &#x27;succeeded&#x27;: The outcome import has completed successfully.  - &#x27;failed&#x27;: The outcome import failed. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# processing_errors

An array of row number / error message pairs. Returns the first 25 errors.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | An array of row number / error message pairs. Returns the first 25 errors. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[items](#items) | list, tuple,  | tuple,  |  | 

# items

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[items](#items) | dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

# items

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

