# openapi_client.model.external_feed.ExternalFeed

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**created_at** | None, str, datetime,  | NoneClass, str,  | When this external feed was added to Canvas | [optional] value must conform to RFC-3339 date-time
**display_name** | None, str,  | NoneClass, str,  | The title of the feed, pulled from the feed itself. If the feed hasn&#x27;t yet been pulled, a temporary name will be synthesized based on the URL | [optional] 
**header_match** | None, str,  | NoneClass, str,  | If not null, only feed entries whose title contains this string will trigger new posts in Canvas | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the feed | [optional] 
**url** | None, str,  | NoneClass, str,  | The HTTP/HTTPS URL to the feed | [optional] 
**verbosity** | None, str,  | NoneClass, str,  | The verbosity setting determines how much of the feed&#x27;s content is imported into Canvas as part of the posting. &#x27;link_only&#x27; means that only the title and a link to the item. &#x27;truncate&#x27; means that a summary of the first portion of the item body will be used. &#x27;full&#x27; means that the full item body will be used. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

