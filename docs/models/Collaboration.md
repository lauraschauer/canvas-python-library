# openapi_client.model.collaboration.Collaboration

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**collaboration_type** | None, str,  | NoneClass, str,  | A name for the type of collaboration | [optional] 
**context_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The canvas id of the course or group to which the collaboration belongs | [optional] 
**context_type** | None, str,  | NoneClass, str,  | The canvas type of the course or group to which the collaboration belongs | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | The timestamp when the collaboration was created | [optional] value must conform to RFC-3339 date-time
**description** | None, str,  | NoneClass, str,  |  | [optional] 
**document_id** | None, str,  | NoneClass, str,  | The collaboration document identifier for the collaboration provider | [optional] 
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The unique identifier for the collaboration | [optional] 
**title** | None, str,  | NoneClass, str,  |  | [optional] 
**type** | str,  | str,  | Another representation of the collaboration type | [optional] 
**update_url** | None, str,  | NoneClass, str,  | The LTI launch url to edit the collaboration | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | The timestamp when the collaboration was last modified | [optional] value must conform to RFC-3339 date-time
**url** | str,  | str,  | The LTI launch url to view collaboration. | [optional] 
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The canvas id of the user who created the collaboration | [optional] 
**user_name** | None, str,  | NoneClass, str,  | The name of the user who owns the collaboration | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

