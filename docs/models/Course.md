# openapi_client.model.course.Course

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**access_restricted_by_date** | None, bool,  | NoneClass, BoolClass,  | optional: this will be true if this user is currently prevented from viewing the course because of date restriction settings | [optional] 
**account_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the account associated with the course | [optional] 
**allow_student_assignment_edits** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**allow_student_forum_attachments** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**allow_wiki_comments** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**apply_assignment_group_weights** | None, bool,  | NoneClass, BoolClass,  | weight final grade based on assignment group percentages | [optional] 
**blueprint** | None, bool,  | NoneClass, BoolClass,  | optional: whether the course is set as a Blueprint Course (blueprint fields require the Blueprint Courses feature) | [optional] 
**[blueprint_restrictions](#blueprint_restrictions)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | optional: Set of restrictions applied to all locked course objects | [optional] 
**[blueprint_restrictions_by_object_type](#blueprint_restrictions_by_object_type)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | optional: Sets of restrictions differentiated by object type applied to locked course objects | [optional] 
**calendar** | [**CalendarLink**](CalendarLink.md) | [**CalendarLink**](CalendarLink.md) |  | [optional] 
**course_code** | None, str,  | NoneClass, str,  | the course code | [optional] 
**course_color** | None, str,  | NoneClass, str,  | Not specified in Canvas API spec, I&#x27;m just guessing the type here | [optional] 
**course_format** | None, str,  | NoneClass, str,  |  | [optional] 
**course_progress** | [**CourseProgress**](CourseProgress.md) | [**CourseProgress**](CourseProgress.md) |  | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | the date the course was created. | [optional] value must conform to RFC-3339 date-time
**default_view** | None, str,  | NoneClass, str,  | the type of page that users will see when they first visit the course - &#x27;feed&#x27;: Recent Activity Dashboard - &#x27;wiki&#x27;: Wiki Front Page - &#x27;modules&#x27;: Course Modules/Sections Page - &#x27;assignments&#x27;: Course Assignments List - &#x27;syllabus&#x27;: Course Syllabus Page other types may be added in the future | [optional] 
**end_at** | None, str, datetime,  | NoneClass, str,  | the end date for the course, if applicable | [optional] value must conform to RFC-3339 date-time
**enrollment_term_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the enrollment term associated with the course | [optional] 
**[enrollments](#enrollments)** | list, tuple, None,  | tuple, NoneClass,  | A list of enrollments linking the current user to the course. for student enrollments, grading information may be included if include[]&#x3D;total_scores | [optional] 
**friendly_name** | None, str,  | NoneClass, str,  | Not specified in Canvas API spec, I&#x27;m just guessing the type here | [optional] 
**grade_passback_setting** | None, str,  | NoneClass, str,  | The grade_passback_setting on this course | [optional] 
**[grading_periods](#grading_periods)** | list, tuple, None,  | tuple, NoneClass,  | A list of grading periods associated with the course | [optional] 
**grading_standard_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the grading standard associated with the course | [optional] 
**hide_final_grades** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**homeroom_course** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the unique identifier for the course | [optional] 
**integration_id** | None, str,  | NoneClass, str,  | the integration identifier for the course, if defined. This field is only included if the user has permission to view SIS information. | [optional] 
**is_public** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**is_public_to_auth_users** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**license** | None, str,  | NoneClass, str,  |  | [optional] 
**locale** | None, str,  | NoneClass, str,  | the course-set locale, if applicable | [optional] 
**name** | None, str,  | NoneClass, str,  | the full name of the course | [optional] 
**needs_grading_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | optional: the number of submissions needing grading returned only if the current user has grading rights and include[]&#x3D;needs_grading_count | [optional] 
**open_enrollment** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**original_name** | None, str,  | NoneClass, str,  |  | [optional] 
**[permissions](#permissions)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | optional: the permissions the user has for the course. returned only for a single course and include[]&#x3D;permissions | [optional] 
**public_description** | None, str,  | NoneClass, str,  | optional: the public description of the course | [optional] 
**public_syllabus** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**public_syllabus_to_auth** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**restrict_enrollments_to_course_dates** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**root_account_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the root account associated with the course | [optional] 
**self_enrollment** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**sis_course_id** | None, str,  | NoneClass, str,  | the SIS identifier for the course, if defined. This field is only included if the user has permission to view SIS information. | [optional] 
**sis_import_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the unique identifier for the SIS import. This field is only included if the user has permission to manage SIS information. | [optional] 
**start_at** | None, str, datetime,  | NoneClass, str,  | the start date for the course, if applicable | [optional] value must conform to RFC-3339 date-time
**storage_quota_mb** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**storage_quota_used_mb** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  |  | [optional] 
**syllabus_body** | None, str,  | NoneClass, str,  | optional: user-generated HTML for the course syllabus | [optional] 
**template** | None, bool,  | NoneClass, BoolClass,  | optional: whether the course is set as a template (requires the course templates feature) | [optional] 
**term** | [**Term**](Term.md) | [**Term**](Term.md) |  | [optional] 
**time_zone** | None, str,  | NoneClass, str,  | The course&#x27;s IANA time zone name. | [optional] 
**total_students** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | optional: the total number of active and invited students in the course | [optional] 
**uuid** | None, str,  | NoneClass, str,  | the UUID of the course | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | the current state of the course one of &#x27;unpublished&#x27;, &#x27;available&#x27;, &#x27;completed&#x27;, or &#x27;deleted&#x27; | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# blueprint_restrictions

optional: Set of restrictions applied to all locked course objects

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | optional: Set of restrictions applied to all locked course objects | 

# blueprint_restrictions_by_object_type

optional: Sets of restrictions differentiated by object type applied to locked course objects

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | optional: Sets of restrictions differentiated by object type applied to locked course objects | 

# enrollments

A list of enrollments linking the current user to the course. for student enrollments, grading information may be included if include[]=total_scores

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | A list of enrollments linking the current user to the course. for student enrollments, grading information may be included if include[]&#x3D;total_scores | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**Enrollment**](Enrollment.md) | [**Enrollment**](Enrollment.md) | [**Enrollment**](Enrollment.md) |  | 

# grading_periods

A list of grading periods associated with the course

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | A list of grading periods associated with the course | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**GradingPeriod**](GradingPeriod.md) | [**GradingPeriod**](GradingPeriod.md) | [**GradingPeriod**](GradingPeriod.md) |  | 

# permissions

optional: the permissions the user has for the course. returned only for a single course and include[]=permissions

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | optional: the permissions the user has for the course. returned only for a single course and include[]&#x3D;permissions | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

