# openapi_client.model.assignment.Assignment

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[all_dates](#all_dates)** | list, tuple, None,  | tuple, NoneClass,  | (Optional) all dates associated with the assignment, if applicable | [optional] 
**[allowed_extensions](#allowed_extensions)** | list, tuple, None,  | tuple, NoneClass,  | Allowed file extensions, which take effect if submission_types includes &#x27;online_upload&#x27;. | [optional] 
**anonymous_submissions** | None, bool,  | NoneClass, BoolClass,  | (Optional) whether anonymous submissions are accepted (applies only to quiz assignments) | [optional] 
**assignment_group_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the ID of the assignment&#x27;s group | [optional] 
**[assignment_visibility](#assignment_visibility)** | list, tuple, None,  | tuple, NoneClass,  | (Optional) If &#x27;assignment_visibility&#x27; is included in the &#x27;include&#x27; parameter, includes an array of student IDs who can see this assignment. | [optional] 
**automatic_peer_reviews** | None, bool,  | NoneClass, BoolClass,  | Boolean indicating peer reviews are assigned automatically. If false, the teacher is expected to manually assign peer reviews. | [optional] 
**course_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the ID of the course the assignment belongs to | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | The time at which this assignment was originally created | [optional] value must conform to RFC-3339 date-time
**description** | None, str,  | NoneClass, str,  | the assignment description, in an HTML fragment | [optional] 
**discussion_topic** | [**DiscussionTopic**](DiscussionTopic.md) | [**DiscussionTopic**](DiscussionTopic.md) |  | [optional] 
**due_at** | None, str, datetime,  | NoneClass, str,  | the due date for the assignment. returns null if not present. NOTE: If this assignment has assignment overrides, this field will be the due date as it applies to the user requesting information from the API. | [optional] value must conform to RFC-3339 date-time
**due_date_required** | None, bool,  | NoneClass, BoolClass,  | Boolean flag indicating whether the assignment requires a due date based on the account level setting | [optional] 
**external_tool_tag_attributes** | [**ExternalToolTagAttributes**](ExternalToolTagAttributes.md) | [**ExternalToolTagAttributes**](ExternalToolTagAttributes.md) |  | [optional] 
**freeze_on_copy** | None, bool,  | NoneClass, BoolClass,  | (Optional) Boolean indicating if assignment will be frozen when it is copied. NOTE: This field will only be present if the AssignmentFreezer plugin is available for your account. | [optional] 
**frozen** | None, bool,  | NoneClass, BoolClass,  | (Optional) Boolean indicating if assignment is frozen for the calling user. NOTE: This field will only be present if the AssignmentFreezer plugin is available for your account. | [optional] 
**[frozen_attributes](#frozen_attributes)** | list, tuple, None,  | tuple, NoneClass,  | (Optional) Array of frozen attributes for the assignment. Only account administrators currently have permission to change an attribute in this list. Will be empty if no attributes are frozen for this assignment. Possible frozen attributes are: title, description, lock_at, points_possible, grading_type, submission_types, assignment_group_id, allowed_extensions, group_category_id, notify_of_update, peer_reviews NOTE: This field will only be present if the AssignmentFreezer plugin is available for your account. | [optional] 
**grade_group_students_individually** | None, bool,  | NoneClass, BoolClass,  | If this is a group assignment, boolean flag indicating whether or not students will be graded individually. | [optional] 
**grading_standard_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the grading standard being applied to this assignment. Valid if grading_type is &#x27;letter_grade&#x27; or &#x27;gpa_scale&#x27;. | [optional] 
**grading_type** | None, str,  | NoneClass, str,  | The type of grading the assignment receives; one of &#x27;pass_fail&#x27;, &#x27;percent&#x27;, &#x27;letter_grade&#x27;, &#x27;gpa_scale&#x27;, &#x27;points&#x27; | [optional] 
**group_category_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the assignments group set, if this is a group assignment. For group discussions, set group_category_id on the discussion topic, not the linked assignment. | [optional] 
**has_overrides** | None, bool,  | NoneClass, BoolClass,  | whether this assignment has overrides | [optional] 
**has_submitted_submissions** | None, bool,  | NoneClass, BoolClass,  | If true, the assignment has been submitted to by at least one student | [optional] 
**html_url** | None, str,  | NoneClass, str,  | the URL to the assignment&#x27;s web page | [optional] 
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the ID of the assignment | [optional] 
**[integration_data](#integration_data)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | (optional, Third Party integration data for assignment) | [optional] 
**integration_id** | None, str,  | NoneClass, str,  | (optional, Third Party unique identifier for Assignment) | [optional] 
**intra_group_peer_reviews** | None, bool,  | NoneClass, BoolClass,  | Boolean representing whether or not members from within the same group on a group assignment can be assigned to peer review their own group&#x27;s work | [optional] 
**lock_at** | None, str, datetime,  | NoneClass, str,  | the lock date (assignment is locked after this date). returns null if not present. NOTE: If this assignment has assignment overrides, this field will be the lock date as it applies to the user requesting information from the API. | [optional] value must conform to RFC-3339 date-time
**lock_explanation** | None, str,  | NoneClass, str,  | (Optional) An explanation of why this is locked for the user. Present when locked_for_user is true. | [optional] 
**lock_info** | [**LockInfo**](LockInfo.md) | [**LockInfo**](LockInfo.md) |  | [optional] 
**locked_for_user** | None, bool,  | NoneClass, BoolClass,  | Whether or not this is locked for the user. | [optional] 
**max_name_length** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | An integer indicating the maximum length an assignment&#x27;s name may be | [optional] 
**moderated_grading** | None, bool,  | NoneClass, BoolClass,  | Boolean indicating if the assignment is moderated. | [optional] 
**muted** | None, bool,  | NoneClass, BoolClass,  | whether the assignment is muted | [optional] 
**name** | None, str,  | NoneClass, str,  | the name of the assignment | [optional] 
**needs_grading_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | if the requesting user has grading rights, the number of submissions that need grading. | [optional] 
**[needs_grading_count_by_section](#needs_grading_count_by_section)** | list, tuple, None,  | tuple, NoneClass,  | if the requesting user has grading rights and the &#x27;needs_grading_count_by_section&#x27; flag is specified, the number of submissions that need grading split out by section. NOTE: This key is NOT present unless you pass the &#x27;needs_grading_count_by_section&#x27; argument as true.  ANOTHER NOTE: it&#x27;s possible to be enrolled in multiple sections, and if a student is setup that way they will show an assignment that needs grading in multiple sections (effectively the count will be duplicated between sections) | [optional] 
**omit_from_final_grade** | None, bool,  | NoneClass, BoolClass,  | (Optional) If true, the assignment will be omitted from the student&#x27;s final grade | [optional] 
**only_visible_to_overrides** | None, bool,  | NoneClass, BoolClass,  | Whether the assignment is only visible to overrides. | [optional] 
**[overrides](#overrides)** | list, tuple, None,  | tuple, NoneClass,  | (Optional) If &#x27;overrides&#x27; is included in the &#x27;include&#x27; parameter, includes an array of assignment override objects. | [optional] 
**peer_review_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Integer representing the amount of reviews each user is assigned. NOTE: This key is NOT present unless you have automatic_peer_reviews set to true. | [optional] 
**peer_reviews** | None, bool,  | NoneClass, BoolClass,  | Boolean indicating if peer reviews are required for this assignment | [optional] 
**peer_reviews_assign_at** | None, str, datetime,  | NoneClass, str,  | String representing a date the reviews are due by. Must be a date that occurs after the default due date. If blank, or date is not after the assignment&#x27;s due date, the assignment&#x27;s due date will be used. NOTE: This key is NOT present unless you have automatic_peer_reviews set to true. | [optional] value must conform to RFC-3339 date-time
**points_possible** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the maximum points possible for the assignment | [optional] 
**position** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the sorting order of the assignment in the group | [optional] 
**post_to_sis** | None, bool,  | NoneClass, BoolClass,  | (optional, present if Sync Grades to SIS feature is enabled) | [optional] 
**published** | None, bool,  | NoneClass, BoolClass,  | Whether the assignment is published | [optional] 
**quiz_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | (Optional) id of the associated quiz (applies only when submission_types is [&#x27;online_quiz&#x27;]) | [optional] 
**[rubric](#rubric)** | list, tuple, None,  | tuple, NoneClass,  | (Optional) A list of scoring criteria and ratings for each rubric criterion. Included if there is an associated rubric. | [optional] 
**rubric_settings** | None, str,  | NoneClass, str,  | (Optional) An object describing the basic attributes of the rubric, including the point total. Included if there is an associated rubric. | [optional] 
**submission** | [**Submission**](Submission.md) | [**Submission**](Submission.md) |  | [optional] 
**[submission_types](#submission_types)** | list, tuple, None,  | tuple, NoneClass,  | the types of submissions allowed for this assignment list containing one or more of the following: &#x27;discussion_topic&#x27;, &#x27;online_quiz&#x27;, &#x27;on_paper&#x27;, &#x27;none&#x27;, &#x27;external_tool&#x27;, &#x27;online_text_entry&#x27;, &#x27;online_url&#x27;, &#x27;online_upload&#x27; &#x27;media_recording&#x27; | [optional] 
**submissions_download_url** | None, str,  | NoneClass, str,  | the URL to download all submissions as a zip | [optional] 
**turnitin_enabled** | None, bool,  | NoneClass, BoolClass,  | Boolean flag indicating whether or not Turnitin has been enabled for the assignment. NOTE: This flag will not appear unless your account has the Turnitin plugin available | [optional] 
**turnitin_settings** | [**TurnitinSettings**](TurnitinSettings.md) | [**TurnitinSettings**](TurnitinSettings.md) |  | [optional] 
**unlock_at** | None, str, datetime,  | NoneClass, str,  | the unlock date (assignment is unlocked after this date) returns null if not present NOTE: If this assignment has assignment overrides, this field will be the unlock date as it applies to the user requesting information from the API. | [optional] value must conform to RFC-3339 date-time
**unpublishable** | None, bool,  | NoneClass, BoolClass,  | Whether the assignment&#x27;s &#x27;published&#x27; state can be changed to false. Will be false if there are student submissions for the assignment. | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | The time at which this assignment was last modified in any way | [optional] value must conform to RFC-3339 date-time
**use_rubric_for_grading** | None, bool,  | NoneClass, BoolClass,  | (Optional) If true, the rubric is directly tied to grading the assignment. Otherwise, it is only advisory. Included if there is an associated rubric. | [optional] 
**vericite_enabled** | None, bool,  | NoneClass, BoolClass,  | Boolean flag indicating whether or not VeriCite has been enabled for the assignment. NOTE: This flag will not appear unless your account has the VeriCite plugin available | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# all_dates

(Optional) all dates associated with the assignment, if applicable

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | (Optional) all dates associated with the assignment, if applicable | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AssignmentDate**](AssignmentDate.md) | [**AssignmentDate**](AssignmentDate.md) | [**AssignmentDate**](AssignmentDate.md) |  | 

# allowed_extensions

Allowed file extensions, which take effect if submission_types includes 'online_upload'.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Allowed file extensions, which take effect if submission_types includes &#x27;online_upload&#x27;. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# assignment_visibility

(Optional) If 'assignment_visibility' is included in the 'include' parameter, includes an array of student IDs who can see this assignment.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | (Optional) If &#x27;assignment_visibility&#x27; is included in the &#x27;include&#x27; parameter, includes an array of student IDs who can see this assignment. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

# frozen_attributes

(Optional) Array of frozen attributes for the assignment. Only account administrators currently have permission to change an attribute in this list. Will be empty if no attributes are frozen for this assignment. Possible frozen attributes are: title, description, lock_at, points_possible, grading_type, submission_types, assignment_group_id, allowed_extensions, group_category_id, notify_of_update, peer_reviews NOTE: This field will only be present if the AssignmentFreezer plugin is available for your account.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | (Optional) Array of frozen attributes for the assignment. Only account administrators currently have permission to change an attribute in this list. Will be empty if no attributes are frozen for this assignment. Possible frozen attributes are: title, description, lock_at, points_possible, grading_type, submission_types, assignment_group_id, allowed_extensions, group_category_id, notify_of_update, peer_reviews NOTE: This field will only be present if the AssignmentFreezer plugin is available for your account. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# integration_data

(optional, Third Party integration data for assignment)

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | (optional, Third Party integration data for assignment) | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**any_string_name** | str,  | str,  | any string name can be used but the value must be the correct type | [optional] 

# needs_grading_count_by_section

if the requesting user has grading rights and the 'needs_grading_count_by_section' flag is specified, the number of submissions that need grading split out by section. NOTE: This key is NOT present unless you pass the 'needs_grading_count_by_section' argument as true.  ANOTHER NOTE: it's possible to be enrolled in multiple sections, and if a student is setup that way they will show an assignment that needs grading in multiple sections (effectively the count will be duplicated between sections)

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | if the requesting user has grading rights and the &#x27;needs_grading_count_by_section&#x27; flag is specified, the number of submissions that need grading split out by section. NOTE: This key is NOT present unless you pass the &#x27;needs_grading_count_by_section&#x27; argument as true.  ANOTHER NOTE: it&#x27;s possible to be enrolled in multiple sections, and if a student is setup that way they will show an assignment that needs grading in multiple sections (effectively the count will be duplicated between sections) | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**NeedsGradingCount**](NeedsGradingCount.md) | [**NeedsGradingCount**](NeedsGradingCount.md) | [**NeedsGradingCount**](NeedsGradingCount.md) |  | 

# overrides

(Optional) If 'overrides' is included in the 'include' parameter, includes an array of assignment override objects.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | (Optional) If &#x27;overrides&#x27; is included in the &#x27;include&#x27; parameter, includes an array of assignment override objects. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AssignmentOverride**](AssignmentOverride.md) | [**AssignmentOverride**](AssignmentOverride.md) | [**AssignmentOverride**](AssignmentOverride.md) |  | 

# rubric

(Optional) A list of scoring criteria and ratings for each rubric criterion. Included if there is an associated rubric.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | (Optional) A list of scoring criteria and ratings for each rubric criterion. Included if there is an associated rubric. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**RubricCriteria**](RubricCriteria.md) | [**RubricCriteria**](RubricCriteria.md) | [**RubricCriteria**](RubricCriteria.md) |  | 

# submission_types

the types of submissions allowed for this assignment list containing one or more of the following: 'discussion_topic', 'online_quiz', 'on_paper', 'none', 'external_tool', 'online_text_entry', 'online_url', 'online_upload' 'media_recording'

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | the types of submissions allowed for this assignment list containing one or more of the following: &#x27;discussion_topic&#x27;, &#x27;online_quiz&#x27;, &#x27;on_paper&#x27;, &#x27;none&#x27;, &#x27;external_tool&#x27;, &#x27;online_text_entry&#x27;, &#x27;online_url&#x27;, &#x27;online_upload&#x27; &#x27;media_recording&#x27; | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

