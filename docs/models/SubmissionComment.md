# openapi_client.model.submission_comment.SubmissionComment

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**author** | None, str,  | NoneClass, str,  | Abbreviated user object UserDisplay (see users API). | [optional] 
**author_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**author_name** | None, str,  | NoneClass, str,  |  | [optional] 
**comment** | None, str,  | NoneClass, str,  |  | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  |  | [optional] value must conform to RFC-3339 date-time
**edited_at** | None, str, datetime,  | NoneClass, str,  |  | [optional] value must conform to RFC-3339 date-time
**id** | decimal.Decimal, int,  | decimal.Decimal,  |  | [optional] 
**media_comment** | [**MediaComment**](MediaComment.md) | [**MediaComment**](MediaComment.md) |  | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

