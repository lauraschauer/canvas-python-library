# openapi_client.model.report.Report

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**attachment** | [**File**](File.md) | [**File**](File.md) |  | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | The date and time the report was created. | [optional] value must conform to RFC-3339 date-time
**current_line** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | This is the current line count being written to the report. It updates every 1000 records. | [optional] 
**ended_at** | None, str, datetime,  | NoneClass, str,  | The date and time the report finished processing. | [optional] value must conform to RFC-3339 date-time
**file_url** | None, str,  | NoneClass, str,  | The url to the report download. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The unique identifier for the report. | [optional] 
**parameters** | [**ReportParameters**](ReportParameters.md) | [**ReportParameters**](ReportParameters.md) |  | [optional] 
**progress** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The progress of the report | [optional] 
**report** | None, str,  | NoneClass, str,  | The type of report. | [optional] 
**started_at** | None, str, datetime,  | NoneClass, str,  | The date and time the report started processing. | [optional] value must conform to RFC-3339 date-time
**status** | None, str,  | NoneClass, str,  | The status of the report | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

