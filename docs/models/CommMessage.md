# openapi_client.model.comm_message.CommMessage

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**body** | None, str,  | NoneClass, str,  | The plain text body of the message | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | The date and time this message was created | [optional] value must conform to RFC-3339 date-time
**from** | None, str,  | NoneClass, str,  | The address that was put in the &#x27;from&#x27; field of the message | [optional] 
**from_name** | None, str,  | NoneClass, str,  | The display name for the from address | [optional] 
**html_body** | None, str,  | NoneClass, str,  | The HTML body of the message. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the CommMessage. | [optional] 
**reply_to** | None, str,  | NoneClass, str,  | The reply_to header of the message | [optional] 
**sent_at** | None, str, datetime,  | NoneClass, str,  | The date and time this message was sent | [optional] value must conform to RFC-3339 date-time
**subject** | None, str,  | NoneClass, str,  | The message subject | [optional] 
**to** | None, str,  | NoneClass, str,  | The address the message was sent to: | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The workflow state of the message. One of &#x27;created&#x27;, &#x27;staged&#x27;, &#x27;sending&#x27;, &#x27;sent&#x27;, &#x27;bounced&#x27;, &#x27;dashboard&#x27;, &#x27;cancelled&#x27;, or &#x27;closed&#x27; | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

