# openapi_client.model.page.Page

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**body** | None, str,  | NoneClass, str,  | the page content, in HTML (present when requesting a single page; omitted when listing pages) | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | the creation date for the page | [optional] value must conform to RFC-3339 date-time
**editing_roles** | None, str,  | NoneClass, str,  | roles allowed to edit the page; comma-separated list comprising a combination of &#x27;teachers&#x27;, &#x27;students&#x27;, &#x27;members&#x27;, and/or &#x27;public&#x27; if not supplied, course defaults are used | [optional] 
**front_page** | None, bool,  | NoneClass, BoolClass,  | whether this page is the front page for the wiki | [optional] 
**hide_from_students** | None, bool,  | NoneClass, BoolClass,  | (DEPRECATED) whether this page is hidden from students (note: this is always reflected as the inverse of the published value) | [optional] 
**last_edited_by** | [**User**](User.md) | [**User**](User.md) |  | [optional] 
**lock_explanation** | None, str,  | NoneClass, str,  | (Optional) An explanation of why this is locked for the user. Present when locked_for_user is true. | [optional] 
**lock_info** | [**LockInfo**](LockInfo.md) | [**LockInfo**](LockInfo.md) |  | [optional] 
**locked_for_user** | None, bool,  | NoneClass, BoolClass,  | Whether or not this is locked for the user. | [optional] 
**published** | None, bool,  | NoneClass, BoolClass,  | whether the page is published (true) or draft state (false). | [optional] 
**title** | None, str,  | NoneClass, str,  | the title of the page | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | the date the page was last updated | [optional] value must conform to RFC-3339 date-time
**url** | None, str,  | NoneClass, str,  | the unique locator for the page | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

