# openapi_client.model.grade_change_event_links.GradeChangeEventLinks

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**assignment** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | ID of the assignment associated with the event | [optional] 
**course** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | ID of the course associated with the event. will match the context_id in the associated assignment if the context type for the assignment is a course | [optional] 
**grader** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | ID of the grader associated with the event. will match the grader_id in the associated submission. | [optional] 
**page_view** | None, str,  | NoneClass, str,  | ID of the page view during the event if it exists. | [optional] 
**student** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | ID of the student associated with the event. will match the user_id in the associated submission. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

