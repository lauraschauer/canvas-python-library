# openapi_client.model.change_record.ChangeRecord

Describes a learning object change propagated to associated courses from a blueprint course

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Describes a learning object change propagated to associated courses from a blueprint course | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**asset_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the learning object that was changed in the blueprint course. | [optional] value must be a 64 bit integer
**asset_name** | None, str,  | NoneClass, str,  | The name of the learning object that was changed in the blueprint course. | [optional] 
**asset_type** | None, str,  | NoneClass, str,  | The type of the learning object that was changed in the blueprint course.  One of &#x27;assignment&#x27;, &#x27;attachment&#x27;, &#x27;discussion_topic&#x27;, &#x27;external_tool&#x27;, &#x27;quiz&#x27;, or &#x27;wiki_page&#x27;. | [optional] 
**change_type** | None, str,  | NoneClass, str,  | The type of change; one of &#x27;created&#x27;, &#x27;updated&#x27;, &#x27;deleted&#x27; | [optional] 
**[exceptions](#exceptions)** | list, tuple, None,  | tuple, NoneClass,  | A list of ExceptionRecords for linked courses that did not receive this update. | [optional] 
**html_url** | None, str,  | NoneClass, str,  | The URL of the changed object | [optional] 
**locked** | None, bool,  | NoneClass, BoolClass,  | Whether the object is locked in the blueprint | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# exceptions

A list of ExceptionRecords for linked courses that did not receive this update.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | A list of ExceptionRecords for linked courses that did not receive this update. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[items](#items) | dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

# items

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

