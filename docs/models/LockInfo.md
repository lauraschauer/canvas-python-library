# openapi_client.model.lock_info.LockInfo

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**asset_string** | None, str,  | NoneClass, str,  | Asset string for the object causing the lock | [optional] 
**context_module** | None, str,  | NoneClass, str,  | (Optional) Context module causing the lock. | [optional] 
**lock_at** | None, str, datetime,  | NoneClass, str,  | (Optional) Time at which this was/will be locked. Must be after the due date. | [optional] value must conform to RFC-3339 date-time
**manually_locked** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**unlock_at** | None, str, datetime,  | NoneClass, str,  | (Optional) Time at which this was/will be unlocked. Must be before the due date. | [optional] value must conform to RFC-3339 date-time
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

