# openapi_client.model.blueprint_template.BlueprintTemplate

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**associated_course_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Number of associated courses for the template | [optional] 
**course_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the Course the template belongs to. | [optional] value must be a 64 bit integer
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the template. | [optional] value must be a 64 bit integer
**last_export_completed_at** | None, str, datetime,  | NoneClass, str,  | Time when the last export was completed | [optional] value must conform to RFC-3339 date-time
**latest_migration** | [**BlueprintMigration**](BlueprintMigration.md) | [**BlueprintMigration**](BlueprintMigration.md) |  | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

