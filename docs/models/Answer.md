# openapi_client.model.answer.Answer

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**answer_text** | str,  | str,  | The text of the answer. | 
**answer_weight** | decimal.Decimal, int,  | decimal.Decimal,  | An integer to determine correctness of the answer. Incorrect answers should be 0, correct answers should be non-negative. | value must be a 64 bit integer
**answer_comments** | None, str,  | NoneClass, str,  | Specific contextual comments for a particular answer. | [optional] 
**answer_match_left** | None, str,  | NoneClass, str,  | Used in matching questions.  The static value of the answer that will be displayed on the left for students to match for. | [optional] 
**answer_match_right** | None, str,  | NoneClass, str,  | Used in matching questions. The correct match for the value given in answer_match_left.  Will be displayed in a dropdown with the other answer_match_right values.. | [optional] 
**approximate** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | Used in numerical questions of type &#x27;precision_answer&#x27;.  The value the answer should equal. | [optional] 
**blank_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Used in fill in multiple blank and multiple dropdowns questions. | [optional] value must be a 64 bit integer
**end** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Used in numerical questions of type &#x27;range_answer&#x27;. The end of the allowed range (inclusive). | [optional] value must be a 64 bit integer
**exact** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Used in numerical questions of type &#x27;exact_answer&#x27;.  The value the answer should equal. | [optional] value must be a 64 bit integer
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The unique identifier for the answer.  Do not supply if this answer is part of a new question | [optional] value must be a 64 bit integer
**margin** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Used in numerical questions of type &#x27;exact_answer&#x27;. The margin of error allowed for the student&#x27;s answer. | [optional] value must be a 64 bit integer
**matching_answer_incorrect_matches** | None, str,  | NoneClass, str,  | Used in matching questions. A list of distractors, delimited by new lines ( ) that will be seeded with all the answer_match_right values. | [optional] 
**numerical_answer_type** | None, str,  | NoneClass, str,  | Used in numerical questions.  Values can be &#x27;exact_answer&#x27;, &#x27;range_answer&#x27;, or &#x27;precision_answer&#x27;. | [optional] 
**precision** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Used in numerical questions of type &#x27;precision_answer&#x27;. The numerical precision that will be used when comparing the student&#x27;s answer. | [optional] value must be a 64 bit integer
**start** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Used in numerical questions of type &#x27;range_answer&#x27;. The start of the allowed range (inclusive). | [optional] value must be a 64 bit integer
**text_after_answers** | None, str,  | NoneClass, str,  | Used in missing word questions.  The text to follow the missing word | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

