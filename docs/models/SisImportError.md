# openapi_client.model.sis_import_error.SisImportError

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**file** | None, str,  | NoneClass, str,  | The file where the error message occurred. | [optional] 
**message** | None, str,  | NoneClass, str,  | The error message that from the record. | [optional] 
**row** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The line number where the error occurred. Some Importers do not yet support this. This is a 1 based index starting with the header row. | [optional] 
**sis_import_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The unique identifier for the SIS import. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

