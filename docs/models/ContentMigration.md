# openapi_client.model.content_migration.ContentMigration

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**attachment** | None, str,  | NoneClass, str,  | attachment api object for the uploaded file may not be present for all migrations | [optional] 
**finished_at** | None, str, datetime,  | NoneClass, str,  | timestamp | [optional] value must conform to RFC-3339 date-time
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the unique identifier for the migration | [optional] 
**migration_issues_url** | None, str,  | NoneClass, str,  | API url to the content migration&#x27;s issues | [optional] 
**migration_type** | None, str,  | NoneClass, str,  | the type of content migration | [optional] 
**migration_type_title** | None, str,  | NoneClass, str,  | the name of the content migration type | [optional] 
**pre_attachment** | None, str,  | NoneClass, str,  | file uploading data, see {file:file_uploads.html File Upload Documentation} for file upload workflow This works a little differently in that all the file data is in the pre_attachment hash if there is no upload_url then there was an attachment pre-processing error, the error message will be in the message key This data will only be here after a create or update call | [optional] 
**progress_url** | None, str,  | NoneClass, str,  | The api endpoint for polling the current progress | [optional] 
**started_at** | None, str, datetime,  | NoneClass, str,  | timestamp | [optional] value must conform to RFC-3339 date-time
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The user who started the migration | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | Current state of the content migration: pre_processing, pre_processed, running, waiting_for_select, completed, failed | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

