# openapi_client.model.quiz_question.QuizQuestion

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**quiz_id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the Quiz the question belongs to. | value must be a 64 bit integer
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the quiz question. | value must be a 64 bit integer
**[answers](#answers)** | list, tuple, None,  | tuple, NoneClass,  | An array of available answers to display to the student. | [optional] 
**correct_comments** | None, str,  | NoneClass, str,  | The comments to display if the student answers the question correctly. | [optional] 
**incorrect_comments** | None, str,  | NoneClass, str,  | The comments to display if the student answers incorrectly. | [optional] 
**neutral_comments** | None, str,  | NoneClass, str,  | The comments to display regardless of how the student answered. | [optional] 
**points_possible** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The maximum amount of points possible received for getting this question correct. | [optional] value must be a 64 bit integer
**position** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The order in which the question will be retrieved and displayed. | [optional] value must be a 64 bit integer
**question_name** | None, str,  | NoneClass, str,  | The name of the question. | [optional] 
**question_text** | None, str,  | NoneClass, str,  | The text of the question. | [optional] 
**question_type** | None, str,  | NoneClass, str,  | The type of the question. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# answers

An array of available answers to display to the student.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | An array of available answers to display to the student. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**Answer**](Answer.md) | [**Answer**](Answer.md) | [**Answer**](Answer.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

