# openapi_client.model.provisional_grade.ProvisionalGrade

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**final** | None, bool,  | NoneClass, BoolClass,  | Whether this is the &#x27;final&#x27; provisional grade created by the moderator | [optional] 
**grade** | None, str,  | NoneClass, str,  | The grade | [optional] 
**grade_matches_current_submission** | None, bool,  | NoneClass, BoolClass,  | Whether the grade was applied to the most current submission (false if the student resubmitted after grading) | [optional] 
**graded_at** | None, str, datetime,  | NoneClass, str,  | When the grade was given | [optional] value must conform to RFC-3339 date-time
**provisional_grade_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The identifier for the provisional grade | [optional] 
**score** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The numeric score | [optional] 
**speedgrader_url** | None, str,  | NoneClass, str,  | A link to view this provisional grade in SpeedGrader | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

