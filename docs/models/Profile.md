# openapi_client.model.profile.Profile

Profile details for a Canvas user.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Profile details for a Canvas user. | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**avatar_url** | None, str,  | NoneClass, str,  | The avatar_url can change over time, so we recommend not caching it for more than a few hours | [optional] 
**bio** | None, str,  | NoneClass, str,  |  | [optional] 
**calendar** | [**CalendarLink**](CalendarLink.md) | [**CalendarLink**](CalendarLink.md) |  | [optional] 
**effective_locale** | None, str,  | NoneClass, str,  | This field is not in the Canvas API specification | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the user. | [optional] 
**integration_id** | None, str,  | NoneClass, str,  | The integration_id associated with the user. | [optional] 
**locale** | None, str,  | NoneClass, str,  | The users locale. | [optional] 
**login_id** | None, str,  | NoneClass, str,  | sample_user@example.com | [optional] 
**lti_user_id** | None, str,  | NoneClass, str,  |  | [optional] 
**name** | None, str,  | NoneClass, str,  | Sample User | [optional] 
**primary_email** | None, str,  | NoneClass, str,  | sample_user@example.com | [optional] 
**pronouns** | None, str,  | NoneClass, str,  | The pronouns set on the profile (as of 12/02/2023 this is not in the API specification, but is returned with a profile call) | [optional] 
**short_name** | None, str,  | NoneClass, str,  | Sample User | [optional] 
**sis_user_id** | None, str,  | NoneClass, str,  | sis1 | [optional] 
**sortable_name** | None, str,  | NoneClass, str,  | user, sample | [optional] 
**time_zone** | None, str,  | NoneClass, str,  | Optional: This field is only returned in certain API calls, and will return the IANA time zone name of the user&#x27;s preferred timezone. | [optional] 
**title** | None, str,  | NoneClass, str,  |  | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

