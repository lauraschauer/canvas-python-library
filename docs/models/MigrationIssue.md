# openapi_client.model.migration_issue.MigrationIssue

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**content_migration_url** | None, str,  | NoneClass, str,  | API url to the content migration | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | timestamp | [optional] value must conform to RFC-3339 date-time
**description** | None, str,  | NoneClass, str,  | Description of the issue for the end-user | [optional] 
**error_message** | None, str,  | NoneClass, str,  | Site administrator error message (If the requesting user has permissions) | [optional] 
**error_report_html_url** | None, str,  | NoneClass, str,  | Link to a Canvas error report if present (If the requesting user has permissions) | [optional] 
**fix_issue_html_url** | None, str,  | NoneClass, str,  | HTML Url to the Canvas page to investigate the issue | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the unique identifier for the issue | [optional] 
**issue_type** | None, str,  | NoneClass, str,  | Severity of the issue: todo, warning, error | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | timestamp | [optional] value must conform to RFC-3339 date-time
**workflow_state** | None, str,  | NoneClass, str,  | Current state of the issue: active, resolved | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

