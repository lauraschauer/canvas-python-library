# openapi_client.model.notification_preference.NotificationPreference

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**category** | None, str,  | NoneClass, str,  | The category of that notification | [optional] 
**frequency** | None, str,  | NoneClass, str,  | How often to send notifications to this communication channel for the given notification. Possible values are &#x27;immediately&#x27;, &#x27;daily&#x27;, &#x27;weekly&#x27;, and &#x27;never&#x27; | [optional] 
**href** | None, str,  | NoneClass, str,  |  | [optional] 
**notification** | None, str,  | NoneClass, str,  | The notification this preference belongs to | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

