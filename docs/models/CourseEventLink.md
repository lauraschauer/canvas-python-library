# openapi_client.model.course_event_link.CourseEventLink

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**copied_from** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | ID of the course that this course was copied from. This is only included if the event_type is copied_from. | [optional] 
**copied_to** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | ID of the course that this course was copied to. This is only included if the event_type is copied_to. | [optional] 
**course** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | ID of the course for the event. | [optional] 
**page_view** | None, str,  | NoneClass, str,  | ID of the page view during the event if it exists. | [optional] 
**sis_batch** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | ID of the SIS batch that triggered the event. | [optional] 
**user** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | ID of the user for the event (who made the change). | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

