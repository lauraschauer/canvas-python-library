# openapi_client.model.grading_period.GradingPeriod

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**close_date** | None, str, datetime,  | NoneClass, str,  | Grades can only be changed before the close date of the grading period. | [optional] value must conform to RFC-3339 date-time
**end_date** | None, str, datetime,  | NoneClass, str,  | The end date of the grading period. | [optional] value must conform to RFC-3339 date-time
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The unique identifier for the grading period. | [optional] 
**is_closed** | None, bool,  | NoneClass, BoolClass,  | If true, the grading period&#x27;s close_date has passed. | [optional] 
**start_date** | None, str, datetime,  | NoneClass, str,  | The start date of the grading period. | [optional] value must conform to RFC-3339 date-time
**title** | None, str,  | NoneClass, str,  | The title for the grading period. | [optional] 
**weight** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | A weight value that contributes to the overall weight of a grading period set which is used to calculate how much assignments in this period contribute to the total grade. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

