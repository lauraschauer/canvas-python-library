# openapi_client.model.page_revision.PageRevision

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**body** | None, str,  | NoneClass, str,  | the historic page contents | [optional] 
**edited_by** | [**User**](User.md) | [**User**](User.md) |  | [optional] 
**latest** | None, bool,  | NoneClass, BoolClass,  | whether this is the latest revision or not | [optional] 
**revision_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | an identifier for this revision of the page | [optional] 
**title** | None, str,  | NoneClass, str,  | the historic page title | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | the time when this revision was saved | [optional] value must conform to RFC-3339 date-time
**url** | None, str,  | NoneClass, str,  | the following fields are not included in the index action and may be omitted from the show action via summary&#x3D;1 the historic url of the page | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

