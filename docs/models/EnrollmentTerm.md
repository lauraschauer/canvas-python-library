# openapi_client.model.enrollment_term.EnrollmentTerm

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**end_at** | None, str, datetime,  | NoneClass, str,  | The datetime of the end of the term. | [optional] value must conform to RFC-3339 date-time
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The unique identifier for the enrollment term. | [optional] 
**name** | None, str,  | NoneClass, str,  | The name of the term. | [optional] 
**[overrides](#overrides)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | Term date overrides for specific enrollment types | [optional] 
**sis_import_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the unique identifier for the SIS import. This field is only included if the user has permission to manage SIS information. | [optional] 
**sis_term_id** | None, str,  | NoneClass, str,  | The SIS id of the term. Only included if the user has permission to view SIS information. | [optional] 
**start_at** | None, str, datetime,  | NoneClass, str,  | The datetime of the start of the term. | [optional] value must conform to RFC-3339 date-time
**workflow_state** | None, str,  | NoneClass, str,  | The state of the term. Can be &#x27;active&#x27; or &#x27;deleted&#x27;. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# overrides

Term date overrides for specific enrollment types

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | Term date overrides for specific enrollment types | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

