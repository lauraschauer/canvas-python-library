# openapi_client.model.group_membership.GroupMembership

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**group_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the group object to which the membership belongs | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The id of the membership object | [optional] 
**just_created** | None, bool,  | NoneClass, BoolClass,  | optional: whether or not the record was just created on a create call (POST), i.e. was the user just added to the group, or was the user already a member | [optional] 
**moderator** | None, bool,  | NoneClass, BoolClass,  | Whether or not the user is a moderator of the group (the must also be an active member of the group to moderate) | [optional] 
**sis_import_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the SIS import if created through SIS. Only included if the user has permission to manage SIS information. | [optional] 
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the user object to which the membership belongs | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The current state of the membership. Current possible values are &#x27;accepted&#x27;, &#x27;invited&#x27;, and &#x27;requested&#x27; | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

