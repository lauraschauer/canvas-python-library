# openapi_client.model.authentication_provider.AuthenticationProvider

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**auth_base** | None, str,  | NoneClass, str,  | Valid for LDAP and CAS providers. | [optional] 
**auth_filter** | None, str,  | NoneClass, str,  | Valid for LDAP providers. | [optional] 
**auth_host** | None, str,  | NoneClass, str,  | Valid for LDAP providers. | [optional] 
**auth_over_tls** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Valid for LDAP providers. | [optional] 
**auth_port** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Valid for LDAP providers. | [optional] 
**auth_type** | None, str,  | NoneClass, str,  | Valid for all providers. | [optional] 
**auth_username** | None, str,  | NoneClass, str,  | Valid for LDAP providers. | [optional] 
**certificate_fingerprint** | None, str,  | NoneClass, str,  | Valid for SAML providers. | [optional] 
**federated_attributes** | [**FederatedAttributesConfig**](FederatedAttributesConfig.md) | [**FederatedAttributesConfig**](FederatedAttributesConfig.md) |  | [optional] 
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Valid for all providers. | [optional] 
**identifier_format** | None, str,  | NoneClass, str,  | Valid for SAML providers. | [optional] 
**idp_entity_id** | None, str,  | NoneClass, str,  | Valid for SAML providers. | [optional] 
**jit_provisioning** | None, bool,  | NoneClass, BoolClass,  | Just In Time provisioning. Valid for all providers except Canvas (which has the similar in concept self_registration setting). | [optional] 
**log_in_url** | None, str,  | NoneClass, str,  | Valid for SAML and CAS providers. | [optional] 
**log_out_url** | None, str,  | NoneClass, str,  | Valid for SAML providers. | [optional] 
**login_attribute** | None, str,  | NoneClass, str,  | Valid for SAML providers. | [optional] 
**position** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Valid for all providers. | [optional] 
**requested_authn_context** | None, str,  | NoneClass, str,  | Valid for SAML providers. | [optional] 
**sig_alg** | None, str,  | NoneClass, str,  | Valid for SAML providers. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

