# openapi_client.model.account.Account

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**default_group_storage_quota_mb** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The storage quota for a group in the account in megabytes, if not otherwise specified | [optional] 
**default_storage_quota_mb** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The storage quota for the account in megabytes, if not otherwise specified | [optional] 
**default_time_zone** | None, str,  | NoneClass, str,  | The default time zone of the account. Allowed time zones are {http://www.iana.org/time-zones IANA time zones} or friendlier {http://api.rubyonrails.org/classes/ActiveSupport/TimeZone.html Ruby on Rails time zones}. | [optional] 
**default_user_storage_quota_mb** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The storage quota for a user in the account in megabytes, if not otherwise specified | [optional] 
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the ID of the Account object | [optional] 
**integration_id** | None, str,  | NoneClass, str,  | The account&#x27;s identifier in the Student Information System. Only included if the user has permission to view SIS information. | [optional] 
**lti_guid** | None, str,  | NoneClass, str,  | The account&#x27;s identifier that is sent as context_id in LTI launches. | [optional] 
**name** | None, str,  | NoneClass, str,  | The display name of the account | [optional] 
**parent_account_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The account&#x27;s parent ID, or null if this is the root account | [optional] 
**root_account_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the root account, or null if this is the root account | [optional] 
**sis_account_id** | None, str,  | NoneClass, str,  | The account&#x27;s identifier in the Student Information System. Only included if the user has permission to view SIS information. | [optional] 
**sis_import_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the SIS import if created through SIS. Only included if the user has permission to manage SIS information. | [optional] 
**uuid** | None, str,  | NoneClass, str,  | The UUID of the account | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The state of the account. Can be &#x27;active&#x27; or &#x27;deleted&#x27;. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

