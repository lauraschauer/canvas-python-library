# openapi_client.model.grade.Grade

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**current_grade** | None, str,  | NoneClass, str,  | The user&#x27;s current grade in the class. Only included if user has permissions to view this grade. | [optional] 
**current_score** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | The user&#x27;s current score in the class. Only included if user has permissions to view this score. | [optional] 
**final_grade** | None, str,  | NoneClass, str,  | The user&#x27;s final grade for the class. Only included if user has permissions to view this grade. | [optional] 
**final_score** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | The user&#x27;s final score for the class. Only included if user has permissions to view this score. | [optional] 
**html_url** | None, str,  | NoneClass, str,  | The URL to the Canvas web UI page for the user&#x27;s grades, if this is a student enrollment. | [optional] 
**unposted_current_grade** | None, str,  | NoneClass, str,  | The user&#x27;s current grade in the class including muted/unposted assignments. Only included if user has permissions to view this grade, typically teachers, TAs, and admins. | [optional] 
**unposted_current_score** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | The user&#x27;s current score in the class including muted/unposted assignments. Only included if user has permissions to view this score, typically teachers, TAs, and admins.. | [optional] 
**unposted_final_grade** | None, str,  | NoneClass, str,  | The user&#x27;s final grade for the class including muted/unposted assignments. Only included if user has permissions to view this grade, typically teachers, TAs, and admins.. | [optional] 
**unposted_final_score** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | The user&#x27;s final score for the class including muted/unposted assignments. Only included if user has permissions to view this score, typically teachers, TAs, and admins.. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

