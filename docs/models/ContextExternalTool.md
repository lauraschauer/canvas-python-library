# openapi_client.model.context_external_tool.ContextExternalTool

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**consumer_key** | None, str,  | NoneClass, str,  | The consumer key used by the tool (The associated shared secret is not returned) | [optional] 
**domain** | None, str,  | NoneClass, str,  | The domain to match links against | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The unique identifier for the tool | [optional] 
**name** | None, str,  | NoneClass, str,  | The name of the tool | [optional] 
**url** | None, str,  | NoneClass, str,  | The url to match links against | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

