# openapi_client.model.feature.Feature

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**applies_to** | None, str,  | NoneClass, str,  | The type of object the feature applies to (RootAccount, Account, Course, or User):  * RootAccount features may only be controlled by flags on root accounts.  * Account features may be controlled by flags on accounts and their parent accounts.  * Course features may be controlled by flags on courses and their parent accounts.  * User features may be controlled by flags on users and site admin only. | [optional] 
**autoexpand** | None, bool,  | NoneClass, BoolClass,  | Whether the details of the feature are autoexpanded on page load vs. the user clicking to expand. | [optional] 
**beta** | None, bool,  | NoneClass, BoolClass,  | Whether the feature is a beta feature. If true, the feature may not be fully polished and may be subject to change in the future. | [optional] 
**development** | None, bool,  | NoneClass, BoolClass,  | Whether the feature is in active development. Features in this state are only visible in test and beta instances and are not yet available for production use. | [optional] 
**display_name** | None, str,  | NoneClass, str,  | The user-visible name of the feature | [optional] 
**enable_at** | None, str, datetime,  | NoneClass, str,  | The date this feature will be globally enabled, or null if this is not planned. (This information is subject to change.) | [optional] value must conform to RFC-3339 date-time
**feature_flag** | [**FeatureFlag**](FeatureFlag.md) | [**FeatureFlag**](FeatureFlag.md) |  | [optional] 
**name** | None, str,  | NoneClass, str,  | The symbolic name of the feature, used in FeatureFlags | [optional] 
**release_notes_url** | None, str,  | NoneClass, str,  | A URL to the release notes describing the feature | [optional] 
**root_opt_in** | None, bool,  | NoneClass, BoolClass,  | If true, a feature that is &#x27;allowed&#x27; globally will be &#x27;off&#x27; by default in root accounts. Otherwise, root accounts inherit the global &#x27;allowed&#x27; setting, which allows sub-accounts and courses to turn features on with no root account action. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

