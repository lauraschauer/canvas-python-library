# openapi_client.model.completion_requirement.CompletionRequirement

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**completed** | None, bool,  | NoneClass, BoolClass,  | whether the calling user has met this requirement (Optional; present only if the caller is a student or if the optional parameter &#x27;student_id&#x27; is included) | [optional] 
**min_score** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | minimum score required to complete (only present when type &#x3D;&#x3D; &#x27;min_score&#x27;) | [optional] 
**type** | None, str,  | NoneClass, str,  | one of &#x27;must_view&#x27;, &#x27;must_submit&#x27;, &#x27;must_contribute&#x27;, &#x27;min_score&#x27; | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

