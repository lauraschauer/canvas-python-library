# openapi_client.model.module_item_sequence_node.ModuleItemSequenceNode

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**current** | [**ModuleItem**](ModuleItem.md) | [**ModuleItem**](ModuleItem.md) |  | [optional] 
**[mastery_path](#mastery_path)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | The conditional release rule for the module item, if applicable | [optional] 
**next** | [**ModuleItem**](ModuleItem.md) | [**ModuleItem**](ModuleItem.md) |  | [optional] 
**prev** | [**ModuleItem**](ModuleItem.md) | [**ModuleItem**](ModuleItem.md) |  | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# mastery_path

The conditional release rule for the module item, if applicable

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | The conditional release rule for the module item, if applicable | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

