# openapi_client.model.grading_rules.GradingRules

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**drop_highest** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Number of highest scores to be dropped for each user. | [optional] 
**drop_lowest** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Number of lowest scores to be dropped for each user. | [optional] 
**[never_drop](#never_drop)** | list, tuple, None,  | tuple, NoneClass,  | Assignment IDs that should never be dropped. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# never_drop

Assignment IDs that should never be dropped.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Assignment IDs that should never be dropped. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

