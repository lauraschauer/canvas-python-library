# openapi_client.model.blueprint_migration.BlueprintMigration

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**comment** | None, str,  | NoneClass, str,  | User-specified comment describing changes made in this operation | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | Time when the migration was queued | [optional] value must conform to RFC-3339 date-time
**exports_started_at** | None, str, datetime,  | NoneClass, str,  | Time when the exports begun | [optional] value must conform to RFC-3339 date-time
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the migration. | [optional] value must be a 64 bit integer
**imports_completed_at** | None, str, datetime,  | NoneClass, str,  | Time when the imports were completed | [optional] value must conform to RFC-3339 date-time
**imports_queued_at** | None, str, datetime,  | NoneClass, str,  | Time when the exports were completed and imports were queued | [optional] value must conform to RFC-3339 date-time
**subscription_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the associated course&#x27;s blueprint subscription. Only present when querying a course associated with a blueprint. | [optional] value must be a 64 bit integer
**template_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the template the migration belongs to. Only present when querying a blueprint course. | [optional] value must be a 64 bit integer
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the user who queued the migration. | [optional] value must be a 64 bit integer
**workflow_state** | None, str,  | NoneClass, str,  | Current state of the content migration: queued, exporting, imports_queued, completed, exports_failed, imports_failed | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

