# openapi_client.model.feature_flag.FeatureFlag

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**context_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the object to which this flag applies (This field is not present if this FeatureFlag represents the global Canvas default) | [optional] 
**context_type** | None, str,  | NoneClass, str,  | The type of object to which this flag applies (Account, Course, or User). (This field is not present if this FeatureFlag represents the global Canvas default) | [optional] 
**feature** | None, str,  | NoneClass, str,  | The feature this flag controls | [optional] 
**locked** | None, bool,  | NoneClass, BoolClass,  | If set, this feature flag cannot be changed in the caller&#x27;s context because the flag is set &#x27;off&#x27; or &#x27;on&#x27; in a higher context | [optional] 
**state** | None, str,  | NoneClass, str,  | The policy for the feature at this context.  can be &#x27;off&#x27;, &#x27;allowed&#x27;, or &#x27;on&#x27;. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

