# openapi_client.model.folder.Folder

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**context_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**context_type** | None, str,  | NoneClass, str,  |  | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  |  | [optional] value must conform to RFC-3339 date-time
**files_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**files_url** | None, str,  | NoneClass, str,  |  | [optional] 
**folders_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**folders_url** | None, str,  | NoneClass, str,  |  | [optional] 
**for_submissions** | None, bool,  | NoneClass, BoolClass,  | If true, indicates this is a read-only folder containing files submitted to assignments | [optional] 
**full_name** | None, str,  | NoneClass, str,  |  | [optional] 
**hidden** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**hidden_for_user** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  |  | [optional] 
**lock_at** | None, str, datetime,  | NoneClass, str,  |  | [optional] value must conform to RFC-3339 date-time
**locked** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**locked_for_user** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**name** | None, str,  | NoneClass, str,  |  | [optional] 
**parent_folder_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**position** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**unlock_at** | None, str, datetime,  | NoneClass, str,  |  | [optional] value must conform to RFC-3339 date-time
**updated_at** | None, str, datetime,  | NoneClass, str,  |  | [optional] value must conform to RFC-3339 date-time
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

