# openapi_client.model.quiz_submission.QuizSubmission

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**quiz_id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the Quiz the quiz submission belongs to. | value must be a 64 bit integer
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the quiz submission. | value must be a 64 bit integer
**attempt** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | For quizzes that allow multiple attempts, this field specifies the quiz submission attempt number. | [optional] value must be a 64 bit integer
**end_at** | None, str, datetime,  | NoneClass, str,  | The time at which the quiz submission will be overdue, and be flagged as a late submission. | [optional] value must conform to RFC-3339 date-time
**extra_attempts** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Number of times the student was allowed to re-take the quiz over the multiple-attempt limit. | [optional] value must be a 64 bit integer
**extra_time** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Amount of extra time allowed for the quiz submission, in minutes. | [optional] value must be a 64 bit integer
**finished_at** | None, str, datetime,  | NoneClass, str,  | The time at which the student submitted the quiz submission. | [optional] value must conform to RFC-3339 date-time
**fudge_points** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Number of points the quiz submission&#x27;s score was fudged by. | [optional] value must be a 64 bit integer
**has_seen_results** | None, bool,  | NoneClass, BoolClass,  | Whether the student has viewed their results to the quiz. | [optional] 
**kept_score** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | For quizzes that allow multiple attempts, this is the score that will be used, which might be the score of the latest, or the highest, quiz submission. | [optional] value must be a 64 bit integer
**manually_unlocked** | None, bool,  | NoneClass, BoolClass,  | The student can take the quiz even if it&#x27;s locked for everyone else | [optional] 
**overdue_and_needs_submission** | None, bool,  | NoneClass, BoolClass,  | Indicates whether the quiz submission is overdue and needs submission | [optional] 
**score** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The score of the quiz submission, if graded. | [optional] value must be a 64 bit integer
**score_before_regrade** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The original score of the quiz submission prior to any re-grading. | [optional] value must be a 64 bit integer
**started_at** | None, str, datetime,  | NoneClass, str,  | The time at which the student started the quiz submission. | [optional] value must conform to RFC-3339 date-time
**submission_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the Submission the quiz submission represents. | [optional] value must be a 64 bit integer
**time_spent** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Amount of time spent, in seconds. | [optional] value must be a 64 bit integer
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the Student that made the quiz submission. | [optional] value must be a 64 bit integer
**workflow_state** | None, str,  | NoneClass, str,  | The current state of the quiz submission. Possible values: [&#x27;untaken&#x27;|&#x27;pending_review&#x27;|&#x27;complete&#x27;|&#x27;settings_only&#x27;|&#x27;preview&#x27;]. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

