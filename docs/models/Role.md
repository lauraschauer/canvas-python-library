# openapi_client.model.role.Role

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[account](#account)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | JSON representation of the account the role is in. | [optional] 
**base_role_type** | None, str,  | NoneClass, str,  | The role type that is being used as a base for this role. For account-level roles, this is &#x27;AccountMembership&#x27;. For course-level roles, it is an enrollment type. | [optional] 
**label** | None, str,  | NoneClass, str,  | The label of the role. | [optional] 
**[permissions](#permissions)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | A dictionary of permissions keyed by name (see permissions input parameter in the &#x27;Create a role&#x27; API). | [optional] 
**role** | None, str,  | NoneClass, str,  | The label of the role. (Deprecated alias for &#x27;label&#x27;) | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The state of the role: &#x27;active&#x27;, &#x27;inactive&#x27;, or &#x27;built_in&#x27; | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# account

JSON representation of the account the role is in.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | JSON representation of the account the role is in. | 

# permissions

A dictionary of permissions keyed by name (see permissions input parameter in the 'Create a role' API).

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | A dictionary of permissions keyed by name (see permissions input parameter in the &#x27;Create a role&#x27; API). | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

