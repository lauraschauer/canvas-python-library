# openapi_client.model.module_item.ModuleItem

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**completion_requirement** | [**CompletionRequirement**](CompletionRequirement.md) | [**CompletionRequirement**](CompletionRequirement.md) |  | [optional] 
**content_details** | [**ContentDetails**](ContentDetails.md) | [**ContentDetails**](ContentDetails.md) |  | [optional] 
**content_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the id of the object referred to applies to &#x27;File&#x27;, &#x27;Discussion&#x27;, &#x27;Assignment&#x27;, &#x27;Quiz&#x27;, &#x27;ExternalTool&#x27; types | [optional] 
**external_url** | None, str,  | NoneClass, str,  | (only for &#x27;ExternalUrl&#x27; and &#x27;ExternalTool&#x27; types) external url that the item points to | [optional] 
**html_url** | None, str,  | NoneClass, str,  | link to the item in Canvas | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the unique identifier for the module item | [optional] 
**indent** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | 0-based indent level; module items may be indented to show a hierarchy | [optional] 
**module_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the id of the Module this item appears in | [optional] 
**new_tab** | None, bool,  | NoneClass, BoolClass,  | (only for &#x27;ExternalTool&#x27; type) whether the external tool opens in a new tab | [optional] 
**page_url** | None, str,  | NoneClass, str,  | (only for &#x27;Page&#x27; type) unique locator for the linked wiki page | [optional] 
**position** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the position of this item in the module (1-based) | [optional] 
**published** | None, bool,  | NoneClass, BoolClass,  | (Optional) Whether this module item is published. This field is present only if the caller has permission to view unpublished items. | [optional] 
**title** | None, str,  | NoneClass, str,  | the title of this item | [optional] 
**type** | None, str,  | NoneClass, str,  | the type of object referred to one of &#x27;File&#x27;, &#x27;Page&#x27;, &#x27;Discussion&#x27;, &#x27;Assignment&#x27;, &#x27;Quiz&#x27;, &#x27;SubHeader&#x27;, &#x27;ExternalUrl&#x27;, &#x27;ExternalTool&#x27; | [optional] 
**url** | None, str,  | NoneClass, str,  | (Optional) link to the Canvas API object, if applicable | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

