# openapi_client.model.assignment_group.AssignmentGroup

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[assignments](#assignments)** | list, tuple, None,  | tuple, NoneClass,  | the assignments in this Assignment Group (see the Assignment API for a detailed list of fields) | [optional] 
**group_weight** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the weight of the Assignment Group | [optional] 
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the id of the Assignment Group | [optional] 
**[integration_data](#integration_data)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | the integration data of the Assignment Group | [optional] 
**name** | None, str,  | NoneClass, str,  | the name of the Assignment Group | [optional] 
**position** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the position of the Assignment Group | [optional] 
**rules** | [**GradingRules**](GradingRules.md) | [**GradingRules**](GradingRules.md) |  | [optional] 
**sis_source_id** | None, str,  | NoneClass, str,  | the sis source id of the Assignment Group | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# assignments

the assignments in this Assignment Group (see the Assignment API for a detailed list of fields)

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | the assignments in this Assignment Group (see the Assignment API for a detailed list of fields) | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

# integration_data

the integration data of the Assignment Group

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | the integration data of the Assignment Group | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

