# openapi_client.model.account_notification.AccountNotification

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**end_at** | None, str, datetime,  | NoneClass, str,  | When to expire the notification. | [optional] value must conform to RFC-3339 date-time
**icon** | None, str,  | NoneClass, str,  | The icon to display with the message.  Defaults to warning. | [optional] 
**message** | None, str,  | NoneClass, str,  | The message to be sent in the notification. | [optional] 
**[role_ids](#role_ids)** | list, tuple, None,  | tuple, NoneClass,  | The roles to send the notification to.  If roles is not passed it defaults to all roles | [optional] 
**[roles](#roles)** | list, tuple, None,  | tuple, NoneClass,  | (Deprecated) The roles to send the notification to.  If roles is not passed it defaults to all roles | [optional] 
**start_at** | None, str, datetime,  | NoneClass, str,  | When to send out the notification. | [optional] value must conform to RFC-3339 date-time
**subject** | None, str,  | NoneClass, str,  | The subject of the notifications | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# role_ids

The roles to send the notification to.  If roles is not passed it defaults to all roles

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | The roles to send the notification to.  If roles is not passed it defaults to all roles | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

# roles

(Deprecated) The roles to send the notification to.  If roles is not passed it defaults to all roles

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | (Deprecated) The roles to send the notification to.  If roles is not passed it defaults to all roles | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

