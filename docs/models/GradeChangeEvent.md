# openapi_client.model.grade_change_event.GradeChangeEvent

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**created_at** | None, str, datetime,  | NoneClass, str,  | timestamp of the event | [optional] value must conform to RFC-3339 date-time
**event_type** | None, str,  | NoneClass, str,  | GradeChange event type | [optional] 
**excused_after** | None, bool,  | NoneClass, BoolClass,  | Boolean indicating whether the submission was excused after the change. | [optional] 
**excused_before** | None, bool,  | NoneClass, BoolClass,  | Boolean indicating whether the submission was excused before the change. | [optional] 
**grade_after** | None, str,  | NoneClass, str,  | The grade after the change. | [optional] 
**grade_before** | None, str,  | NoneClass, str,  | The grade before the change. | [optional] 
**graded_anonymously** | None, bool,  | NoneClass, BoolClass,  | Boolean indicating whether the student name was visible when the grade was given. Could be null if the grade change record was created before this feature existed. | [optional] 
**id** | str,  | str,  | ID of the event. | [optional] 
**links** | [**GradeChangeEventLinks**](GradeChangeEventLinks.md) | [**GradeChangeEventLinks**](GradeChangeEventLinks.md) |  | [optional] 
**request_id** | None, str,  | NoneClass, str,  | The unique request id of the request during the grade change. | [optional] 
**version_number** | None, str,  | NoneClass, str,  | Version Number of the grade change submission. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

