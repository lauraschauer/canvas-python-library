# openapi_client.model.quiz.Quiz

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**access_code** | None, str,  | NoneClass, str,  | access code to restrict quiz access | [optional] 
**[all_dates](#all_dates)** | list, tuple, None,  | tuple, NoneClass,  | list of due dates for the quiz | [optional] 
**allowed_attempts** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | how many times a student can take the quiz -1 &#x3D; unlimited attempts | [optional] 
**anonymous_submissions** | None, bool,  | NoneClass, BoolClass,  | Whether survey submissions will be kept anonymous (only applicable to &#x27;graded_survey&#x27;, &#x27;survey&#x27; quiz types) | [optional] 
**assignment_group_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the ID of the quiz&#x27;s assignment group: | [optional] 
**cant_go_back** | None, bool,  | NoneClass, BoolClass,  | lock questions after answering? only valid if one_question_at_a_time&#x3D;true | [optional] 
**description** | None, str,  | NoneClass, str,  | the description of the quiz | [optional] 
**due_at** | None, str, datetime,  | NoneClass, str,  | when the quiz is due | [optional] value must conform to RFC-3339 date-time
**hide_correct_answers_at** | None, str, datetime,  | NoneClass, str,  | prevent the students from seeing correct answers after the specified date has passed. only valid if show_correct_answers&#x3D;true | [optional] value must conform to RFC-3339 date-time
**hide_results** | None, str,  | NoneClass, str,  | let students see their quiz responses? possible values: null, &#x27;always&#x27;, &#x27;until_after_last_attempt&#x27; | [optional] 
**html_url** | None, str,  | NoneClass, str,  | the HTTP/HTTPS URL to the quiz | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the ID of the quiz | [optional] 
**ip_filter** | None, str,  | NoneClass, str,  | IP address or range that quiz access is limited to | [optional] 
**lock_at** | None, str, datetime,  | NoneClass, str,  | when to lock the quiz | [optional] value must conform to RFC-3339 date-time
**lock_explanation** | None, str,  | NoneClass, str,  | (Optional) An explanation of why this is locked for the user. Present when locked_for_user is true. | [optional] 
**lock_info** | [**LockInfo**](LockInfo.md) | [**LockInfo**](LockInfo.md) |  | [optional] 
**locked_for_user** | None, bool,  | NoneClass, BoolClass,  | Whether or not this is locked for the user. | [optional] 
**mobile_url** | None, str,  | NoneClass, str,  | a url suitable for loading the quiz in a mobile webview.  it will persiste the headless session and, for quizzes in public courses, will force the user to login | [optional] 
**one_question_at_a_time** | None, bool,  | NoneClass, BoolClass,  | show one question at a time? | [optional] 
**one_time_results** | None, bool,  | NoneClass, BoolClass,  | prevent the students from seeing their results more than once (right after they submit the quiz) | [optional] 
**permissions** | [**QuizPermissions**](QuizPermissions.md) | [**QuizPermissions**](QuizPermissions.md) |  | [optional] 
**points_possible** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The total point value given to the quiz | [optional] 
**preview_url** | None, str,  | NoneClass, str,  | A url that can be visited in the browser with a POST request to preview a quiz as the teacher. Only present when the user may grade | [optional] 
**published** | None, bool,  | NoneClass, BoolClass,  | whether the quiz has a published or unpublished draft state. | [optional] 
**question_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the number of questions in the quiz | [optional] 
**[question_types](#question_types)** | list, tuple, None,  | tuple, NoneClass,  | List of question types in the quiz | [optional] 
**quiz_extensions_url** | None, str,  | NoneClass, str,  | Link to endpoint to send extensions for this quiz. | [optional] 
**quiz_type** | None, str,  | NoneClass, str,  | type of quiz possible values: &#x27;practice_quiz&#x27;, &#x27;assignment&#x27;, &#x27;graded_survey&#x27;, &#x27;survey&#x27; | [optional] 
**scoring_policy** | None, str,  | NoneClass, str,  | which quiz score to keep (only if allowed_attempts !&#x3D; 1) possible values: &#x27;keep_highest&#x27;, &#x27;keep_latest&#x27; | [optional] 
**show_correct_answers** | None, bool,  | NoneClass, BoolClass,  | show which answers were correct when results are shown? only valid if hide_results&#x3D;null | [optional] 
**show_correct_answers_at** | None, str, datetime,  | NoneClass, str,  | when should the correct answers be visible by students? only valid if show_correct_answers&#x3D;true | [optional] value must conform to RFC-3339 date-time
**show_correct_answers_last_attempt** | None, bool,  | NoneClass, BoolClass,  | restrict the show_correct_answers option above to apply only to the last submitted attempt of a quiz that allows multiple attempts. only valid if show_correct_answers&#x3D;true and allowed_attempts &gt; 1 | [optional] 
**shuffle_answers** | None, bool,  | NoneClass, BoolClass,  | shuffle answers for students? | [optional] 
**speedgrader_url** | None, str,  | NoneClass, str,  | Link to Speed Grader for this quiz. Will not be present if quiz is unpublished | [optional] 
**time_limit** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | quiz time limit in minutes | [optional] 
**title** | None, str,  | NoneClass, str,  | the title of the quiz | [optional] 
**unlock_at** | None, str, datetime,  | NoneClass, str,  | when to unlock the quiz | [optional] value must conform to RFC-3339 date-time
**unpublishable** | None, bool,  | NoneClass, BoolClass,  | Whether the assignment&#x27;s &#x27;published&#x27; state can be changed to false. Will be false if there are student submissions for the quiz. | [optional] 
**version_number** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | Current version number of the quiz | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# all_dates

list of due dates for the quiz

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | list of due dates for the quiz | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AssignmentDate**](AssignmentDate.md) | [**AssignmentDate**](AssignmentDate.md) | [**AssignmentDate**](AssignmentDate.md) |  | 

# question_types

List of question types in the quiz

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | List of question types in the quiz | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

