# openapi_client.model.quiz_submission_question.QuizSubmissionQuestion

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the QuizQuestion this answer is for. | value must be a 64 bit integer
**answer** | None, str,  | NoneClass, str,  | The provided answer (if any) for this question. The format of this parameter depends on the type of the question, see the Appendix for more information. | [optional] 
**[answers](#answers)** | list, tuple, None,  | tuple, NoneClass,  | The possible answers for this question when those possible answers are necessary.  The presence of this parameter is dependent on permissions. | [optional] 
**flagged** | None, bool,  | NoneClass, BoolClass,  | Whether this question is flagged. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# answers

The possible answers for this question when those possible answers are necessary.  The presence of this parameter is dependent on permissions.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | The possible answers for this question when those possible answers are necessary.  The presence of this parameter is dependent on permissions. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

