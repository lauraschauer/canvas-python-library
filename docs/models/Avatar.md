# openapi_client.model.avatar.Avatar

Possible avatar for a user.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Possible avatar for a user. | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**display_name** | str,  | str,  | A textual description of the avatar record. | 
**type** | str,  | str,  | [&#x27;gravatar&#x27;|&#x27;attachment&#x27;|&#x27;no_pic&#x27;] The type of avatar record, for categorization purposes. | 
**url** | str,  | str,  | The url of the avatar | 
**token** | str,  | str,  | A unique representation of the avatar record which can be used to set the avatar with the user update endpoint. Note: this is an internal representation and is subject to change without notice. It should be consumed with this api endpoint and used in the user update endpoint, and should not be constructed by the client. | 
**content-type** | None, str,  | NoneClass, str,  | [&#x27;attachment&#x27; type only] the content-type of the attachment. | [optional] 
**filename** | None, str,  | NoneClass, str,  | [&#x27;attachment&#x27; type only] the filename of the attachment | [optional] 
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | [&#x27;attachment&#x27; type only] the internal id of the attachment | [optional] 
**size** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | [&#x27;attachment&#x27; type only] the size of the attachment | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

