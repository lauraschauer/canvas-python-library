# openapi_client.model.terms_of_service.TermsOfService

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**account_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the root account that owns the Terms of Service | [optional] 
**content** | None, str,  | NoneClass, str,  | Content of the Terms of Service | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | Terms Of Service id | [optional] 
**passive** | None, bool,  | NoneClass, BoolClass,  | Boolean dictating if the user must accept Terms of Service | [optional] 
**terms_type** | None, str,  | NoneClass, str,  | The given type for the Terms of Service | [optional] must be one of ["default", "custom", "no_terms", ] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

