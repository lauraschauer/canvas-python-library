# openapi_client.model.quiz_permissions.QuizPermissions

Permissions the user has for the quiz

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Permissions the user has for the quiz | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**create** | None, bool,  | NoneClass, BoolClass,  | whether the user may create a new quiz | [optional] 
**manage** | None, bool,  | NoneClass, BoolClass,  | whether the user may edit, update, or delete the quiz | [optional] 
**read** | None, bool,  | NoneClass, BoolClass,  | whether the user can view the quiz | [optional] 
**read_statistics** | None, bool,  | NoneClass, BoolClass,  | whether the user may view quiz statistics for this quiz | [optional] 
**review_grades** | None, bool,  | NoneClass, BoolClass,  | whether the user may review grades for all quiz submissions for this quiz | [optional] 
**submit** | None, bool,  | NoneClass, BoolClass,  | whether the user may submit a submission for the quiz | [optional] 
**update** | None, bool,  | NoneClass, BoolClass,  | whether the user may update the quiz | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

