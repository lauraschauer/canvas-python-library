# openapi_client.model.tab.Tab

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**hidden** | None, bool,  | NoneClass, BoolClass,  | only included if true | [optional] 
**html_url** | None, str,  | NoneClass, str,  |  | [optional] 
**id** | str,  | str,  |  | [optional] 
**label** | None, str,  | NoneClass, str,  |  | [optional] 
**position** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | 1 based | [optional] 
**type** | None, str,  | NoneClass, str,  |  | [optional] 
**visibility** | None, str,  | NoneClass, str,  | possible values are: public, members, admins, and none | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

