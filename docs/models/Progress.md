# openapi_client.model.progress.Progress

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**completion** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | percent completed | [optional] 
**context_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the context owning the job. | [optional] 
**context_type** | None, str,  | NoneClass, str,  |  | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | the time the job was created | [optional] value must conform to RFC-3339 date-time
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the ID of the Progress object | [optional] 
**message** | None, str,  | NoneClass, str,  | optional details about the job | [optional] 
**[results](#results)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | optional results of the job. omitted when job is still pending | [optional] 
**tag** | None, str,  | NoneClass, str,  | the type of operation | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | the time the job was last updated | [optional] value must conform to RFC-3339 date-time
**url** | None, str,  | NoneClass, str,  | url where a progress update can be retrieved | [optional] 
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the id of the user who started the job | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | the state of the job one of &#x27;queued&#x27;, &#x27;running&#x27;, &#x27;completed&#x27;, &#x27;failed&#x27; | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# results

optional results of the job. omitted when job is still pending

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | optional results of the job. omitted when job is still pending | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

