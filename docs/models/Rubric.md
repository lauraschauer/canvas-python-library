# openapi_client.model.rubric.Rubric

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[assessments](#assessments)** | list, tuple, None,  | tuple, NoneClass,  | If an assessment type is included in the &#x27;include&#x27; parameter, includes an array of rubric assessment objects for a given rubric, based on the assessment type requested. If the user does not request an assessment type this key will be absent. | [optional] 
**context_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the context owning the rubric | [optional] 
**context_type** | None, str,  | NoneClass, str,  |  | [optional] 
**free_form_criterion_comments** | None, bool,  | NoneClass, BoolClass,  | whether or not free-form comments are used | [optional] 
**hide_score_total** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the ID of the rubric | [optional] 
**points_possible** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**read_only** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**reusable** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**title** | None, str,  | NoneClass, str,  | title of the rubric | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# assessments

If an assessment type is included in the 'include' parameter, includes an array of rubric assessment objects for a given rubric, based on the assessment type requested. If the user does not request an assessment type this key will be absent.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | If an assessment type is included in the &#x27;include&#x27; parameter, includes an array of rubric assessment objects for a given rubric, based on the assessment type requested. If the user does not request an assessment type this key will be absent. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**RubricAssessment**](RubricAssessment.md) | [**RubricAssessment**](RubricAssessment.md) | [**RubricAssessment**](RubricAssessment.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

