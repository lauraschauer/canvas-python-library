# openapi_client.model.user.User

A Canvas user, e.g. a student, teacher, administrator, observer, etc.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | A Canvas user, e.g. a student, teacher, administrator, observer, etc. | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the user. | value must be a 64 bit integer
**avatar_state** | None, str,  | NoneClass, str,  | Optional If avatars are enabled and caller is admin, this field can be requested and will contain the current state of the user&#x27;s avatar. | [optional] 
**avatar_url** | None, str,  | NoneClass, str,  | If avatars are enabled, this field will be included and contain a url to retrieve the user&#x27;s avatar. | [optional] 
**bio** | None, str,  | NoneClass, str,  | Optional: The user&#x27;s bio. | [optional] 
**email** | None, str,  | NoneClass, str,  | Optional: This field can be requested with certain API calls, and will return the users primary email address. | [optional] 
**[enrollments](#enrollments)** | list, tuple, None,  | tuple, NoneClass,  | Optional: This field can be requested with certain API calls, and will return a list of the users active enrollments. See the List enrollments API for more details about the format of these records. | [optional] 
**first_name** | None, str,  | NoneClass, str,  | The first name of the user. | [optional] 
**integration_id** | None, str,  | NoneClass, str,  | The integration_id associated with the user.  This field is only included if the user came from a SIS import and has permissions to view SIS information. | [optional] 
**last_login** | None, str, datetime,  | NoneClass, str,  | Optional: This field is only returned in certain API calls, and will return a timestamp representing the last time the user logged in to canvas. | [optional] value must conform to RFC-3339 date-time
**last_name** | None, str,  | NoneClass, str,  | The last name of the user. | [optional] 
**locale** | None, str,  | NoneClass, str,  | Optional: This field can be requested with certain API calls, and will return the users locale in RFC 5646 format. | [optional] 
**login_id** | None, str,  | NoneClass, str,  | The unique login id for the user.  This is what the user uses to log in to Canvas. | [optional] 
**name** | None, str,  | NoneClass, str,  | The name of the user. | [optional] 
**short_name** | None, str,  | NoneClass, str,  | A short name the user has selected, for use in conversations or other less formal places through the site. | [optional] 
**sis_import_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the SIS import.  This field is only included if the user came from a SIS import and has permissions to manage SIS information. | [optional] value must be a 64 bit integer
**sis_user_id** | None, str,  | NoneClass, str,  | The SIS ID associated with the user.  This field is only included if the user came from a SIS import and has permissions to view SIS information. | [optional] 
**sortable_name** | None, str,  | NoneClass, str,  | The name of the user that is should be used for sorting groups of users, such as in the gradebook. | [optional] 
**time_zone** | None, str,  | NoneClass, str,  | Optional: This field is only returned in certain API calls, and will return the IANA time zone name of the user&#x27;s preferred timezone. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# enrollments

Optional: This field can be requested with certain API calls, and will return a list of the users active enrollments. See the List enrollments API for more details about the format of these records.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Optional: This field can be requested with certain API calls, and will return a list of the users active enrollments. See the List enrollments API for more details about the format of these records. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**Enrollment**](Enrollment.md) | [**Enrollment**](Enrollment.md) | [**Enrollment**](Enrollment.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

