# openapi_client.model.sis_import.SisImport

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**add_sis_stickiness** | None, bool,  | NoneClass, BoolClass,  | Whether stickiness was added to the batch changes. | [optional] 
**batch_mode** | None, bool,  | NoneClass, BoolClass,  | Whether the import was run in batch mode. | [optional] 
**batch_mode_term_id** | None, str,  | NoneClass, str,  | The term the batch was limited to. | [optional] 
**clear_sis_stickiness** | None, bool,  | NoneClass, BoolClass,  | Whether stickiness was cleared. | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | The date the SIS import was created. | [optional] value must conform to RFC-3339 date-time
**[csv_attachments](#csv_attachments)** | list, tuple, None,  | tuple, NoneClass,  | An array of CSV files for processing | [optional] 
**data** | [**SisImportData**](SisImportData.md) | [**SisImportData**](SisImportData.md) |  | [optional] 
**diffed_against_import_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the SIS Import that this import was diffed against | [optional] 
**diffing_data_set_identifier** | None, str,  | NoneClass, str,  | The identifier of the data set that this SIS batch diffs against | [optional] 
**ended_at** | None, str, datetime,  | NoneClass, str,  | The date the SIS import finished. Returns null if not finished. | [optional] value must conform to RFC-3339 date-time
**errors_attachment** | [**File**](File.md) | [**File**](File.md) |  | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The unique identifier for the SIS import. | [optional] 
**multi_term_batch_mode** | None, bool,  | NoneClass, BoolClass,  | Enables batch mode against all terms in term file. Requires change_threshold to be set. | [optional] 
**override_sis_stickiness** | None, bool,  | NoneClass, BoolClass,  | Whether UI changes were overridden. | [optional] 
**[processing_errors](#processing_errors)** | list, tuple, None,  | tuple, NoneClass,  | An array of CSV_file/error_message pairs. | [optional] 
**[processing_warnings](#processing_warnings)** | list, tuple, None,  | tuple, NoneClass,  | Only imports that are complete will get this data. An array of CSV_file/warning_message pairs. | [optional] 
**progress** | None, str,  | NoneClass, str,  | The progress of the SIS import. The progress will reset when using batch_mode and have a different progress for the cleanup stage | [optional] 
**skip_deletes** | None, bool,  | NoneClass, BoolClass,  | When set the import will skip any deletes. | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | The date the SIS import was last updated. | [optional] value must conform to RFC-3339 date-time
**user** | [**User**](User.md) | [**User**](User.md) |  | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The current state of the SIS import.  - &#x27;created&#x27;: The SIS import has been created.  - &#x27;importing&#x27;: The SIS import is currently processing.  - &#x27;cleanup_batch&#x27;: The SIS import is currently cleaning up courses, sections, and enrollments not included in the batch for batch_mode imports.  - &#x27;imported&#x27;: The SIS import has completed successfully.  - &#x27;imported_with_messages&#x27;: The SIS import completed with errors or warnings.  - &#x27;aborted&#x27;: The SIS import was aborted.  - &#x27;failed_with_messages&#x27;: The SIS import failed with errors.  - &#x27;failed&#x27;: The SIS import failed. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# csv_attachments

An array of CSV files for processing

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | An array of CSV files for processing | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[items](#items) | list, tuple,  | tuple,  |  | 

# items

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**File**](File.md) | [**File**](File.md) | [**File**](File.md) |  | 

# processing_errors

An array of CSV_file/error_message pairs.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | An array of CSV_file/error_message pairs. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[items](#items) | list, tuple,  | tuple,  |  | 

# items

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# processing_warnings

Only imports that are complete will get this data. An array of CSV_file/warning_message pairs.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Only imports that are complete will get this data. An array of CSV_file/warning_message pairs. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[items](#items) | list, tuple,  | tuple,  |  | 

# items

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

