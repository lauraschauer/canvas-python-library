# openapi_client.model.conference.Conference

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**conference_key** | None, str,  | NoneClass, str,  | The 3rd party&#x27;s ID for the conference | [optional] 
**conference_type** | None, str,  | NoneClass, str,  | The type of conference | [optional] 
**description** | None, str,  | NoneClass, str,  | The description for the conference | [optional] 
**duration** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The expected duration the conference is supposed to last | [optional] 
**ended_at** | None, str, datetime,  | NoneClass, str,  | The date that the conference ended at, null if it hasn&#x27;t ended | [optional] value must conform to RFC-3339 date-time
**has_advanced_settings** | None, bool,  | NoneClass, BoolClass,  | True if the conference type has advanced settings. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The id of the conference | [optional] 
**join_url** | None, str,  | NoneClass, str,  | URL to join the conference, may be null if the conference type doesn&#x27;t set it | [optional] 
**long_running** | None, bool,  | NoneClass, BoolClass,  | If true the conference is long running and has no expected end time | [optional] 
**[recordings](#recordings)** | list, tuple, None,  | tuple, NoneClass,  | A List of recordings for the conference | [optional] 
**started_at** | None, str, datetime,  | NoneClass, str,  | The date the conference started at, null if it hasn&#x27;t started | [optional] value must conform to RFC-3339 date-time
**title** | None, str,  | NoneClass, str,  | The title of the conference | [optional] 
**url** | None, str,  | NoneClass, str,  | URL for the conference, may be null if the conference type doesn&#x27;t set it | [optional] 
**[user_settings](#user_settings)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | A collection of settings specific to the conference type | [optional] 
**[users](#users)** | list, tuple, None,  | tuple, NoneClass,  | Array of user ids that are participants in the conference | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# recordings

A List of recordings for the conference

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | A List of recordings for the conference | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**ConferenceRecording**](ConferenceRecording.md) | [**ConferenceRecording**](ConferenceRecording.md) | [**ConferenceRecording**](ConferenceRecording.md) |  | 

# user_settings

A collection of settings specific to the conference type

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | A collection of settings specific to the conference type | 

# users

Array of user ids that are participants in the conference

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Array of user ids that are participants in the conference | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

