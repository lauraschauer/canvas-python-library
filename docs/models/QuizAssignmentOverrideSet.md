# openapi_client.model.quiz_assignment_override_set.QuizAssignmentOverrideSet

Set of assignment-overridden dates for a quiz.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Set of assignment-overridden dates for a quiz. | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**all_dates** | [**QuizAssignmentOverride**](QuizAssignmentOverride.md) | [**QuizAssignmentOverride**](QuizAssignmentOverride.md) |  | [optional] 
**due_dates** | [**QuizAssignmentOverride**](QuizAssignmentOverride.md) | [**QuizAssignmentOverride**](QuizAssignmentOverride.md) |  | [optional] 
**quiz_id** | None, str,  | NoneClass, str,  | ID of the quiz those dates are for. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

