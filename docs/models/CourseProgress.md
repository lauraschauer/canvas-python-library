# openapi_client.model.course_progress.CourseProgress

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**completed_at** | None, str, datetime,  | NoneClass, str,  | date the course was completed. null if the course has not been completed by this user | [optional] value must conform to RFC-3339 date-time
**next_requirement_url** | None, str,  | NoneClass, str,  | url to next module item that has an unmet requirement. null if the user has completed the course or the current module does not require sequential progress | [optional] 
**requirement_completed_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | total number of requirements the user has completed from all modules | [optional] 
**requirement_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | total number of requirements from all modules | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

