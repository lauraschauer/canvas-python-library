# openapi_client.model.planner_override.PlannerOverride

User-controlled setting for whether an item should be displayed on the planner or not

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | User-controlled setting for whether an item should be displayed on the planner or not | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**assignment_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the plannable&#x27;s associated assignment, if it has one | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | The datetime of when the planner override was created | [optional] value must conform to RFC-3339 date-time
**deleted_at** | None, str, datetime,  | NoneClass, str,  | The datetime of when the planner override was deleted, if applicable | [optional] value must conform to RFC-3339 date-time
**dismissed** | None, bool,  | NoneClass, BoolClass,  | Controls whether or not the associated plannable item shows up in the opportunities list | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the planner override | [optional] 
**marked_complete** | None, bool,  | NoneClass, BoolClass,  | Controls whether or not the associated plannable item is marked complete on the planner | [optional] 
**plannable_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the associated object for the planner override | [optional] 
**plannable_type** | None, str,  | NoneClass, str,  | The type of the associated object for the planner override | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | The datetime of when the planner override was updated | [optional] value must conform to RFC-3339 date-time
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the associated user for the planner override | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The current published state of the item, synced with the associated object | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

