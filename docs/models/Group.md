# openapi_client.model.group.Group

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**avatar_url** | None, str,  | NoneClass, str,  | The url of the group&#x27;s avatar | [optional] 
**context_type** | None, str,  | NoneClass, str,  | The course or account that the group belongs to. The pattern here is that whatever the context_type is, there will be an _id field named after that type. So if instead context_type was &#x27;account&#x27;, the course_id field would be replaced by an account_id field. | [optional] 
**course_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**description** | None, str,  | NoneClass, str,  | A description of the group. This is plain text. | [optional] 
**followed_by_user** | None, bool,  | NoneClass, BoolClass,  | Whether or not the current user is following this group. | [optional] 
**group_category_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the group&#x27;s category. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the group. | [optional] 
**is_public** | None, bool,  | NoneClass, BoolClass,  | Whether or not the group is public.  Currently only community groups can be made public.  Also, once a group has been set to public, it cannot be changed back to private. | [optional] 
**join_level** | None, str,  | NoneClass, str,  | How people are allowed to join the group.  For all groups except for community groups, the user must share the group&#x27;s parent course or account.  For student organized or community groups, where a user can be a member of as many or few as they want, the applicable levels are &#x27;parent_context_auto_join&#x27;, &#x27;parent_context_request&#x27;, and &#x27;invitation_only&#x27;.  For class groups, where students are divided up and should only be part of one group of the category, this value will always be &#x27;invitation_only&#x27;, and is not relevant. * If &#x27;parent_context_auto_join&#x27;, anyone can join and will be automatically accepted. * If &#x27;parent_context_request&#x27;, anyone  can request to join, which must be approved by a group moderator. * If &#x27;invitation_only&#x27;, only those how have received an invitation my join the group, by accepting that invitation. | [optional] 
**members_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The number of members currently in the group | [optional] 
**name** | None, str,  | NoneClass, str,  | The display name of the group. | [optional] 
**[permissions](#permissions)** | dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | optional: the permissions the user has for the group. returned only for a single group and include[]&#x3D;permissions | [optional] 
**role** | None, str,  | NoneClass, str,  | Certain types of groups have special role designations. Currently, these include: &#x27;communities&#x27;, &#x27;student_organized&#x27;, and &#x27;imported&#x27;. Regular course/account groups have a role of null. | [optional] 
**sis_group_id** | None, str,  | NoneClass, str,  | The SIS ID of the group. Only included if the user has permission to view SIS information. | [optional] 
**sis_import_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the SIS import if created through SIS. Only included if the user has permission to manage SIS information. | [optional] 
**storage_quota_mb** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the storage quota for the group, in megabytes | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# permissions

optional: the permissions the user has for the group. returned only for a single group and include[]=permissions

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, None,  | frozendict.frozendict, NoneClass,  | optional: the permissions the user has for the group. returned only for a single group and include[]&#x3D;permissions | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

