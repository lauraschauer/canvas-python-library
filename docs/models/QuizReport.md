# openapi_client.model.quiz_report.QuizReport

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**anonymous** | None, bool,  | NoneClass, BoolClass,  | boolean indicating whether the report is for an anonymous survey. if true, no student names will be included in the csv | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | when the report was created | [optional] value must conform to RFC-3339 date-time
**file** | [**File**](File.md) | [**File**](File.md) |  | [optional] 
**generatable** | None, bool,  | NoneClass, BoolClass,  | boolean indicating whether the report can be generated, which is true unless the quiz is a survey one | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the ID of the quiz report | [optional] 
**includes_all_versions** | None, bool,  | NoneClass, BoolClass,  | boolean indicating whether the report represents all submissions or only the most recent ones for each student | [optional] 
**progress** | [**Progress**](Progress.md) | [**Progress**](Progress.md) |  | [optional] 
**progress_url** | None, str,  | NoneClass, str,  | if the report has not yet finished generating, a URL where information about its progress can be retrieved. refer to the Progress API for more information (Note: not available in JSON-API format) | [optional] 
**quiz_id** | decimal.Decimal, int,  | decimal.Decimal,  | the ID of the quiz | [optional] 
**readable_type** | None, str,  | NoneClass, str,  | a human-readable (and localized) version of the report_type | [optional] 
**report_type** | None, str,  | NoneClass, str,  | which type of report this is possible values: &#x27;student_analysis&#x27;, &#x27;item_analysis&#x27; | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | when the report was last updated | [optional] value must conform to RFC-3339 date-time
**url** | None, str,  | NoneClass, str,  | the API endpoint for this report | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

