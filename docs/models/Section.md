# openapi_client.model.section.Section

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**course_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The unique Canvas identifier for the course in which the section belongs | [optional] 
**end_at** | None, str, datetime,  | NoneClass, str,  | the end date for the section, if applicable | [optional] value must conform to RFC-3339 date-time
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The unique identifier for the section. | [optional] 
**integration_id** | None, str,  | NoneClass, str,  | Optional: The integration ID of the section. This field is only included if the user has permission to view SIS information. | [optional] 
**name** | None, str,  | NoneClass, str,  | The name of the section. | [optional] 
**nonxlist_course_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The unique identifier of the original course of a cross-listed section | [optional] 
**restrict_enrollments_to_section_dates** | None, bool,  | NoneClass, BoolClass,  | Restrict user enrollments to the start and end dates of the section | [optional] 
**sis_course_id** | None, str,  | NoneClass, str,  | The unique SIS identifier for the course in which the section belongs. This field is only included if the user has permission to view SIS information. | [optional] 
**sis_import_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The unique identifier for the SIS import if created through SIS. This field is only included if the user has permission to manage SIS information. | [optional] 
**sis_section_id** | None, str,  | NoneClass, str,  | The sis id of the section. This field is only included if the user has permission to view SIS information. | [optional] 
**start_at** | None, str, datetime,  | NoneClass, str,  | the start date for the section, if applicable | [optional] value must conform to RFC-3339 date-time
**total_students** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | optional: the total number of active and invited students in the section | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

