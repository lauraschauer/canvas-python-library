# openapi_client.model.report_parameters.ReportParameters

The parameters returned will vary for each report.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | The parameters returned will vary for each report. | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**accounts** | None, bool,  | NoneClass, BoolClass,  | If true, account data will be included. If false, account data will be omitted. | [optional] 
**course_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the course to report on | [optional] 
**courses** | None, bool,  | NoneClass, BoolClass,  | If true, course data will be included. If false, course data will be omitted. | [optional] 
**end_at** | None, str, datetime,  | NoneClass, str,  | The end date for submissions. Max time range is 2 weeks. | [optional] value must conform to RFC-3339 date-time
**[enrollment_state](#enrollment_state)** | list, tuple, None,  | tuple, NoneClass,  | Include enrollment state. Defaults to &#x27;all&#x27; Options: [&#x27;active&#x27;| &#x27;invited&#x27;| &#x27;creation_pending&#x27;| &#x27;deleted&#x27;| &#x27;rejected&#x27;| &#x27;completed&#x27;| &#x27;inactive&#x27;| &#x27;all&#x27;] | [optional] 
**enrollment_term_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The canvas id of the term to get grades from | [optional] 
**enrollments** | None, bool,  | NoneClass, BoolClass,  | If true, enrollment data will be included. If false, enrollment data will be omitted. | [optional] 
**groups** | None, bool,  | NoneClass, BoolClass,  | If true, group data will be included. If false, group data will be omitted. | [optional] 
**include_deleted** | None, bool,  | NoneClass, BoolClass,  | If true, deleted objects will be included. If false, deleted objects will be omitted. | [optional] 
**include_enrollment_state** | None, bool,  | NoneClass, BoolClass,  | If true, enrollment state will be included. If false, enrollment state will be omitted. Defaults to false. | [optional] 
**order** | None, str,  | NoneClass, str,  | The sort order for the csv, Options: &#x27;users&#x27;, &#x27;courses&#x27;, &#x27;outcomes&#x27;. | [optional] 
**sections** | None, bool,  | NoneClass, BoolClass,  | If true, section data will be included. If false, section data will be omitted. | [optional] 
**sis_accounts_csv** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**sis_terms_csv** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**start_at** | None, str, datetime,  | NoneClass, str,  | The beginning date for submissions. Max time range is 2 weeks. | [optional] value must conform to RFC-3339 date-time
**terms** | None, bool,  | NoneClass, BoolClass,  | If true, term data will be included. If false, term data will be omitted. | [optional] 
**users** | None, bool,  | NoneClass, BoolClass,  | If true, user data will be included. If false, user data will be omitted. | [optional] 
**xlist** | None, bool,  | NoneClass, BoolClass,  | If true, data for crosslisted courses will be included. If false, data for crosslisted courses will be omitted. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# enrollment_state

Include enrollment state. Defaults to 'all' Options: ['active'| 'invited'| 'creation_pending'| 'deleted'| 'rejected'| 'completed'| 'inactive'| 'all']

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | Include enrollment state. Defaults to &#x27;all&#x27; Options: [&#x27;active&#x27;| &#x27;invited&#x27;| &#x27;creation_pending&#x27;| &#x27;deleted&#x27;| &#x27;rejected&#x27;| &#x27;completed&#x27;| &#x27;inactive&#x27;| &#x27;all&#x27;] | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

