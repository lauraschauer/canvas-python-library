# openapi_client.model.sis_import_counts.SisImportCounts

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**abstract_courses** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**accounts** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**batch_courses_deleted** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the number of courses that were removed because they were not included in the batch for batch_mode imports. Only included if courses were deleted | [optional] 
**batch_enrollments_deleted** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the number of enrollments that were removed because they were not included in the batch for batch_mode imports. Only included if enrollments were deleted | [optional] 
**batch_sections_deleted** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the number of sections that were removed because they were not included in the batch for batch_mode imports. Only included if sections were deleted | [optional] 
**courses** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**enrollments** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**error_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**grade_publishing_results** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**group_memberships** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**groups** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**sections** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**terms** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**users** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**warning_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**xlists** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

