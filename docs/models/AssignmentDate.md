# openapi_client.model.assignment_date.AssignmentDate

Object representing a due date for an assignment or quiz. If the due date came from an assignment override, it will have an 'id' field.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Object representing a due date for an assignment or quiz. If the due date came from an assignment override, it will have an &#x27;id&#x27; field. | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**base** | None, bool,  | NoneClass, BoolClass,  | (Optional, present if &#x27;id&#x27; is missing) whether this date represents the assignment&#x27;s or quiz&#x27;s default due date | [optional] 
**due_at** | None, str, datetime,  | NoneClass, str,  | The due date for the assignment. Must be between the unlock date and the lock date if there are lock dates | [optional] value must conform to RFC-3339 date-time
**id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | (Optional, missing if &#x27;base&#x27; is present) id of the assignment override this date represents | [optional] 
**lock_at** | None, str, datetime,  | NoneClass, str,  | The lock date for the assignment. Must be after the due date if there is a due date. | [optional] value must conform to RFC-3339 date-time
**title** | None, str,  | NoneClass, str,  |  | [optional] 
**unlock_at** | None, str, datetime,  | NoneClass, str,  | The unlock date for the assignment. Must be before the due date if there is a due date. | [optional] value must conform to RFC-3339 date-time
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

