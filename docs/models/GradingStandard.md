# openapi_client.model.grading_standard.GradingStandard

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**context_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the id for the context either the Account or Course id | [optional] 
**context_type** | None, str,  | NoneClass, str,  | the context this standard is associated with, either &#x27;Account&#x27; or &#x27;Course&#x27; | [optional] 
**[grading_scheme](#grading_scheme)** | list, tuple, None,  | tuple, NoneClass,  | A list of GradingSchemeEntry that make up the Grading Standard as an array of values with the scheme name and value | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the id of the grading standard | [optional] 
**title** | None, str,  | NoneClass, str,  | the title of the grading standard | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# grading_scheme

A list of GradingSchemeEntry that make up the Grading Standard as an array of values with the scheme name and value

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | A list of GradingSchemeEntry that make up the Grading Standard as an array of values with the scheme name and value | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**GradingSchemeEntry**](GradingSchemeEntry.md) | [**GradingSchemeEntry**](GradingSchemeEntry.md) | [**GradingSchemeEntry**](GradingSchemeEntry.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

