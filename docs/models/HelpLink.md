# openapi_client.model.help_link.HelpLink

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[available_to](#available_to)** | list, tuple, None,  | tuple, NoneClass,  | The roles that have access to this help link | [optional] 
**id** | str,  | str,  | The ID of the help link | [optional] 
**subtext** | None, str,  | NoneClass, str,  | The description of the help link | [optional] 
**text** | None, str,  | NoneClass, str,  | The name of the help link | [optional] 
**type** | None, str,  | NoneClass, str,  | The type of the help link | [optional] must be one of ["default", "custom", ] 
**url** | None, str,  | NoneClass, str,  | The URL of the help link | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# available_to

The roles that have access to this help link

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | The roles that have access to this help link | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

