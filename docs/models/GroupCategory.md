# openapi_client.model.group_category.GroupCategory

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**account_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**auto_leader** | None, str,  | NoneClass, str,  | Gives instructors the ability to automatically have group leaders assigned.  Values include &#x27;random&#x27;, &#x27;first&#x27;, and null; &#x27;random&#x27; picks a student from the group at random as the leader, &#x27;first&#x27; sets the first student to be assigned to the group as the leader | [optional] 
**context_type** | None, str,  | NoneClass, str,  | The course or account that the category group belongs to. The pattern here is that whatever the context_type is, there will be an _id field named after that type. So if instead context_type was &#x27;Course&#x27;, the course_id field would be replaced by an course_id field. | [optional] 
**group_limit** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | If self-signup is enabled, group_limit can be set to cap the number of users in each group. If null, there is no limit. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the group category. | [optional] 
**name** | None, str,  | NoneClass, str,  | The display name of the group category. | [optional] 
**progress** | [**Progress**](Progress.md) | [**Progress**](Progress.md) |  | [optional] 
**role** | None, str,  | NoneClass, str,  | Certain types of group categories have special role designations. Currently, these include: &#x27;communities&#x27;, &#x27;student_organized&#x27;, and &#x27;imported&#x27;. Regular course/account group categories have a role of null. | [optional] 
**self_signup** | None, str,  | NoneClass, str,  | If the group category allows users to join a group themselves, thought they may only be a member of one group per group category at a time. Values include &#x27;restricted&#x27;, &#x27;enabled&#x27;, and null &#x27;enabled&#x27; allows students to assign themselves to a group &#x27;restricted&#x27; restricts them to only joining a group in their section null disallows students from joining groups | [optional] 
**sis_group_category_id** | None, str,  | NoneClass, str,  | The SIS identifier for the group category. This field is only included if the user has permission to manage or view SIS information. | [optional] 
**sis_import_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The unique identifier for the SIS import. This field is only included if the user has permission to manage SIS information. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

