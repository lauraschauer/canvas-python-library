# openapi_client.model.originality_report.OriginalityReport

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**file_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the file receiving the originality score | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The id of the OriginalityReport | [optional] 
**originality_report_file_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the file within Canvas containing the originality report document (if provided) | [optional] 
**originality_report_url** | None, str,  | NoneClass, str,  | A non-LTI launch URL where the originality score of the file may be found. | [optional] 
**originality_score** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | A number between 0 and 100 representing the originality score | [optional] 
**tool_setting** | [**ToolSetting**](ToolSetting.md) | [**ToolSetting**](ToolSetting.md) |  | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

