# openapi_client.model.scope.Scope

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**action** | None, str,  | NoneClass, str,  | The controller action the scope is associated to | [optional] 
**controller** | None, str,  | NoneClass, str,  | The controller the scope is associated to | [optional] 
**resource** | None, str,  | NoneClass, str,  | The resource the scope is associated with | [optional] 
**resource_name** | None, str,  | NoneClass, str,  | The localized resource name | [optional] 
**scope** | None, str,  | NoneClass, str,  | The identifier for the scope | [optional] 
**verb** | None, str,  | NoneClass, str,  | The HTTP verb for the scope | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

