# openapi_client.model.custom_column.CustomColumn

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**hidden** | None, bool,  | NoneClass, BoolClass,  | won&#x27;t be displayed if hidden is true | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the custom gradebook column | [optional] 
**position** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | column order | [optional] 
**read_only** | None, bool,  | NoneClass, BoolClass,  | won&#x27;t be editable in the gradebook UI | [optional] 
**teacher_notes** | None, bool,  | NoneClass, BoolClass,  | When true, this column&#x27;s visibility will be toggled in the Gradebook when a user selects to show or hide notes | [optional] 
**title** | None, str,  | NoneClass, str,  | header text | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

