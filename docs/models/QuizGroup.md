# openapi_client.model.quiz_group.QuizGroup

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**quiz_id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the Quiz the question group belongs to. | value must be a 64 bit integer
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the question group. | value must be a 64 bit integer
**assessment_question_bank_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the Assessment question bank to pull questions from. | [optional] value must be a 64 bit integer
**name** | None, str,  | NoneClass, str,  | The name of the question group. | [optional] 
**pick_count** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The number of questions to pick from the group to display to the student. | [optional] value must be a 64 bit integer
**position** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The order in which the question group will be retrieved and displayed. | [optional] value must be a 64 bit integer
**question_points** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The amount of points allotted to each question in the group. | [optional] value must be a 64 bit integer
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

