# openapi_client.model.communication_channel.CommunicationChannel

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**address** | None, str,  | NoneClass, str,  | The address, or path, of the communication channel. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the communication channel. | [optional] 
**position** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The position of this communication channel relative to the user&#x27;s other channels when they are ordered. | [optional] 
**type** | None, str,  | NoneClass, str,  | The type of communcation channel being described. Possible values are: &#x27;email&#x27;, &#x27;push&#x27;, &#x27;sms&#x27;, or &#x27;twitter&#x27;. This field determines the type of value seen in &#x27;address&#x27;. | [optional] 
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the user that owns this communication channel. | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The current state of the communication channel. Possible values are: &#x27;unconfirmed&#x27; or &#x27;active&#x27;. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

