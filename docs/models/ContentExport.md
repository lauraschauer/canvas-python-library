# openapi_client.model.content_export.ContentExport

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**attachment** | [**File**](File.md) | [**File**](File.md) |  | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | the date and time this export was requested | [optional] value must conform to RFC-3339 date-time
**export_type** | None, str,  | NoneClass, str,  | the type of content migration: &#x27;common_cartridge&#x27; or &#x27;qti&#x27; | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the unique identifier for the export | [optional] 
**progress_url** | None, str,  | NoneClass, str,  | The api endpoint for polling the current progress | [optional] 
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The ID of the user who started the export | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | Current state of the content migration: created exporting exported failed | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

