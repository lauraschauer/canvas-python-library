# openapi_client.model.page_view.PageView

The record of a user page view access in Canvas

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | The record of a user page view access in Canvas | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**id** | str, uuid.UUID,  | str,  | A UUID representing the page view.  This is also the unique request id | value must be a uuid
**action** | None, str,  | NoneClass, str,  | The rails action that handled the request | [optional] 
**app_name** | None, str,  | NoneClass, str,  | If the request is from an API request, the app that generated the access token | [optional] 
**asset_type** | None, str,  | NoneClass, str,  | The type of asset in the context for the request, if any | [optional] 
**context_type** | None, str,  | NoneClass, str,  | The type of context for the request | [optional] 
**contributed** | None, bool,  | NoneClass, BoolClass,  | This field is deprecated, and will always be false | [optional] 
**controller** | None, str,  | NoneClass, str,  | The rails controller that handled the request | [optional] 
**created_at** | None, str,  | NoneClass, str,  | When the request was made | [optional] 
**http_method** | None, str,  | NoneClass, str,  | The HTTP method such as GET or POST | [optional] 
**interaction_seconds** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | An approximation of how long the user spent on the page, in seconds | [optional] 
**links** | [**PageViewLinks**](PageViewLinks.md) | [**PageViewLinks**](PageViewLinks.md) |  | [optional] 
**participated** | None, bool,  | NoneClass, BoolClass,  | True if the request counted as participating, such as submitting homework | [optional] 
**remote_ip** | None, str,  | NoneClass, str,  | The origin IP address of the request | [optional] 
**render_time** | None, decimal.Decimal, int, float,  | NoneClass, decimal.Decimal,  | How long the response took to render, in seconds | [optional] 
**url** | None, str,  | NoneClass, str,  | The URL requested | [optional] 
**user_agent** | None, str,  | NoneClass, str,  | The user-agent of the browser or program that made the request | [optional] 
**user_request** | None, bool,  | NoneClass, BoolClass,  | A flag indicating whether the request was user-initiated, or automatic (such as an AJAX call) | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

