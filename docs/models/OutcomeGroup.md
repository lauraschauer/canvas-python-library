# openapi_client.model.outcome_group.OutcomeGroup

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**can_edit** | None, bool,  | NoneClass, BoolClass,  | whether the current user can update the outcome group | [optional] 
**context_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the context owning the outcome group. may be null for global outcome groups. omitted in the abbreviated form. | [optional] 
**context_type** | None, str,  | NoneClass, str,  |  | [optional] 
**description** | None, str,  | NoneClass, str,  | description of the outcome group. omitted in the abbreviated form. | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the ID of the outcome group | [optional] 
**import_url** | None, str,  | NoneClass, str,  | the URL for importing another group into this outcome group. should be treated as opaque. omitted in the abbreviated form. | [optional] 
**outcomes_url** | None, str,  | NoneClass, str,  | the URL for listing/creating outcome links under the outcome group. should be treated as opaque | [optional] 
**parent_outcome_group** | [**OutcomeGroup**](OutcomeGroup.md) | [**OutcomeGroup**](OutcomeGroup.md) |  | [optional] 
**subgroups_url** | None, str,  | NoneClass, str,  | the URL for listing/creating subgroups under the outcome group. should be treated as opaque | [optional] 
**title** | None, str,  | NoneClass, str,  | title of the outcome group | [optional] 
**url** | None, str,  | NoneClass, str,  | the URL for fetching/updating the outcome group. should be treated as opaque | [optional] 
**vendor_guid** | None, str,  | NoneClass, str,  | A custom GUID for the learning standard. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

