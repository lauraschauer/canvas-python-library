# openapi_client.model.planner_note.PlannerNote

A planner note

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | A planner note | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**course_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The course that the note is in relation too, if applicable | [optional] 
**description** | None, str,  | NoneClass, str,  | The description of the planner note | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the planner note | [optional] 
**linked_object_html_url** | None, str,  | NoneClass, str,  | the Canvas web URL of the linked learning object | [optional] 
**linked_object_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the id of the linked learning object | [optional] 
**linked_object_type** | None, str,  | NoneClass, str,  | the type of the linked learning object | [optional] 
**linked_object_url** | None, str,  | NoneClass, str,  | the API URL of the linked learning object | [optional] 
**title** | None, str,  | NoneClass, str,  | The title for a planner note | [optional] 
**todo_date** | None, str, datetime,  | NoneClass, str,  | The datetime of when the planner note should show up on their planner | [optional] value must conform to RFC-3339 date-time
**user_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | The id of the associated user creating the planner note | [optional] 
**workflow_state** | None, str,  | NoneClass, str,  | The current published state of the planner note | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

