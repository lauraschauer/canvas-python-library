# openapi_client.model.course_event.CourseEvent

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**created_at** | None, str, datetime,  | NoneClass, str,  | timestamp of the event | [optional] value must conform to RFC-3339 date-time
**event_data** | None, str,  | NoneClass, str,  | Course event data depending on the event type.  This will return an object containing the relevant event data.  An updated event type will return an UpdatedEventData object. | [optional] 
**event_source** | None, str,  | NoneClass, str,  | Course event source depending on the event type.  This will return a string containing the source of the event. | [optional] 
**event_type** | None, str,  | NoneClass, str,  | Course event type The event type defines the type and schema of the event_data object. | [optional] 
**id** | str,  | str,  | ID of the event. | [optional] 
**links** | [**CourseEventLink**](CourseEventLink.md) | [**CourseEventLink**](CourseEventLink.md) |  | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

