# openapi_client.model.quiz_assignment_override.QuizAssignmentOverride

Set of assignment-overridden dates for a quiz.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  | Set of assignment-overridden dates for a quiz. | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**base** | None, bool,  | NoneClass, BoolClass,  | If this property is present, it means that dates in this structure are not based on an assignment override, but are instead for all students. | [optional] 
**due_at** | None, str, datetime,  | NoneClass, str,  | The date after which any quiz submission is considered late. | [optional] value must conform to RFC-3339 date-time
**id** | decimal.Decimal, int,  | decimal.Decimal,  | ID of the assignment override, unless this is the base construct, in which case the &#x27;id&#x27; field is omitted. | [optional] 
**lock_at** | None, str, datetime,  | NoneClass, str,  | When the quiz will stop being available for taking. A value of null means it can always be taken. | [optional] value must conform to RFC-3339 date-time
**title** | None, str,  | NoneClass, str,  | Title of the section this assignment override is for, if any. | [optional] 
**unlock_at** | None, str, datetime,  | NoneClass, str,  | Date when the quiz becomes available for taking. | [optional] value must conform to RFC-3339 date-time
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

