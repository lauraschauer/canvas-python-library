# openapi_client.model.shared_brand_config.SharedBrandConfig

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**account_id** | None, str,  | NoneClass, str,  | The id of the account it should be shared within. | [optional] 
**brand_config_md5** | None, str,  | NoneClass, str,  | The md5 (since BrandConfigs are identified by MD5 and not numeric id) of the BrandConfig to share. | [optional] 
**created_at** | None, str, datetime,  | NoneClass, str,  | When this was created | [optional] value must conform to RFC-3339 date-time
**id** | decimal.Decimal, int,  | decimal.Decimal,  | The shared_brand_config identifier. | [optional] 
**name** | None, str,  | NoneClass, str,  | The name to share this theme as | [optional] 
**updated_at** | None, str, datetime,  | NoneClass, str,  | When this was last updated | [optional] value must conform to RFC-3339 date-time
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

