# openapi_client.model.turnitin_settings.TurnitinSettings

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**exclude_biblio** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**exclude_quoted** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**exclude_small_matches_type** | None, str,  | NoneClass, str,  |  | [optional] 
**exclude_small_matches_value** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  |  | [optional] 
**internet_check** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**journal_check** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**originality_report_visibility** | None, str,  | NoneClass, str,  |  | [optional] 
**s_paper_check** | None, bool,  | NoneClass, BoolClass,  |  | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

