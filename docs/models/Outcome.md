# openapi_client.model.outcome.Outcome

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**assessed** | None, bool,  | NoneClass, BoolClass,  | whether this outcome has been used to assess a student | [optional] 
**calculation_int** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | this defines the variable value used by the calculation_method. included only if calculation_method uses it | [optional] 
**calculation_method** | None, str,  | NoneClass, str,  | the method used to calculate a students score | [optional] 
**can_edit** | None, bool,  | NoneClass, BoolClass,  | whether the current user can update the outcome | [optional] 
**can_unlink** | None, bool,  | NoneClass, BoolClass,  | whether the outcome can be unlinked | [optional] 
**context_id** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | the context owning the outcome. may be null for global outcomes | [optional] 
**context_type** | None, str,  | NoneClass, str,  |  | [optional] 
**description** | None, str,  | NoneClass, str,  | description of the outcome. omitted in the abbreviated form. | [optional] 
**display_name** | None, str,  | NoneClass, str,  | Optional friendly name for reporting | [optional] 
**has_updateable_rubrics** | None, bool,  | NoneClass, BoolClass,  | whether updates to this outcome will propagate to unassessed rubrics that have imported it | [optional] 
**id** | decimal.Decimal, int,  | decimal.Decimal,  | the ID of the outcome | [optional] 
**mastery_points** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | points necessary to demonstrate mastery outcomes. included only if the outcome embeds a rubric criterion. omitted in the abbreviated form. | [optional] 
**points_possible** | None, decimal.Decimal, int,  | NoneClass, decimal.Decimal,  | maximum points possible. included only if the outcome embeds a rubric criterion. omitted in the abbreviated form. | [optional] 
**[ratings](#ratings)** | list, tuple, None,  | tuple, NoneClass,  | possible ratings for this outcome. included only if the outcome embeds a rubric criterion. omitted in the abbreviated form. | [optional] 
**title** | None, str,  | NoneClass, str,  | title of the outcome | [optional] 
**url** | None, str,  | NoneClass, str,  | the URL for fetching/updating the outcome. should be treated as opaque | [optional] 
**vendor_guid** | None, str,  | NoneClass, str,  | A custom GUID for the learning standard. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# ratings

possible ratings for this outcome. included only if the outcome embeds a rubric criterion. omitted in the abbreviated form.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple, None,  | tuple, NoneClass,  | possible ratings for this outcome. included only if the outcome embeds a rubric criterion. omitted in the abbreviated form. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**RubricRating**](RubricRating.md) | [**RubricRating**](RubricRating.md) | [**RubricRating**](RubricRating.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

