<a name="__pageTop"></a>
# openapi_client.apis.tags.quiz_submission_user_list_api.QuizSubmissionUserListApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**send_message_to_unsubmitted_or_submitted_users_for_quiz**](#send_message_to_unsubmitted_or_submitted_users_for_quiz) | **post** /v1/courses/{course_id}/quizzes/{id}/submission_users/message | Send a message to unsubmitted or submitted users for the quiz

# **send_message_to_unsubmitted_or_submitted_users_for_quiz**
<a name="send_message_to_unsubmitted_or_submitted_users_for_quiz"></a>
> send_message_to_unsubmitted_or_submitted_users_for_quiz(course_idid)

Send a message to unsubmitted or submitted users for the quiz

{   \"body\": {     \"type\": \"string\",     \"description\": \"message body of the conversation to be created\",     \"example\": \"Please take the quiz.\"   },   \"recipients\": {     \"type\": \"string\",     \"description\": \"Who to send the message to. May be either 'submitted' or 'unsubmitted'\",     \"example\": \"submitted\"   },   \"subject\": {     \"type\": \"string\",     \"description\": \"Subject of the new Conversation created\",     \"example\": \"ATTN: Quiz 101 Students\"   } }

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import quiz_submission_user_list_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = quiz_submission_user_list_api.QuizSubmissionUserListApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'id': "id_example",
    }
    try:
        # Send a message to unsubmitted or submitted users for the quiz
        api_response = api_instance.send_message_to_unsubmitted_or_submitted_users_for_quiz(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling QuizSubmissionUserListApi->send_message_to_unsubmitted_or_submitted_users_for_quiz: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'id': "id_example",
    }
    body = None
    try:
        # Send a message to unsubmitted or submitted users for the quiz
        api_response = api_instance.send_message_to_unsubmitted_or_submitted_users_for_quiz(
            path_params=path_params,
            body=body,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling QuizSubmissionUserListApi->send_message_to_unsubmitted_or_submitted_users_for_quiz: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**conversations** | dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO | - Body and recipients to send the message to. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
id | IdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#send_message_to_unsubmitted_or_submitted_users_for_quiz.ApiResponseFor200) | No response was specified

#### send_message_to_unsubmitted_or_submitted_users_for_quiz.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

