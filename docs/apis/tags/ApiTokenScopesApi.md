<a name="__pageTop"></a>
# openapi_client.apis.tags.api_token_scopes_api.ApiTokenScopesApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**list_scopes**](#list_scopes) | **get** /v1/accounts/{account_id}/scopes | List scopes

# **list_scopes**
<a name="list_scopes"></a>
> [Scope] list_scopes(account_id)

List scopes

A list of scopes that can be applied to developer keys and access tokens.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import api_token_scopes_api
from openapi_client.model.scope import Scope
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = api_token_scopes_api.ApiTokenScopesApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    query_params = {
    }
    try:
        # List scopes
        api_response = api_instance.list_scopes(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ApiTokenScopesApi->list_scopes: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': "account_id_example",
    }
    query_params = {
        'group_by': "resource_name",
    }
    try:
        # List scopes
        api_response = api_instance.list_scopes(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ApiTokenScopesApi->list_scopes: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
group_by | GroupBySchema | | optional


# GroupBySchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["resource_name", ] 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_scopes.ApiResponseFor200) | No response was specified

#### list_scopes.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**Scope**]({{complexTypePrefix}}Scope.md) | [**Scope**]({{complexTypePrefix}}Scope.md) | [**Scope**]({{complexTypePrefix}}Scope.md) |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

