<a name="__pageTop"></a>
# openapi_client.apis.tags.submissions_api.SubmissionsApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_single_submission_courses**](#get_single_submission_courses) | **get** /v1/courses/{course_id}/assignments/{assignment_id}/submissions/{user_id} | Get a single submission
[**get_single_submission_sections**](#get_single_submission_sections) | **get** /v1/sections/{section_id}/assignments/{assignment_id}/submissions/{user_id} | Get a single submission
[**grade_or_comment_on_multiple_submissions_courses_assignments**](#grade_or_comment_on_multiple_submissions_courses_assignments) | **post** /v1/courses/{course_id}/assignments/{assignment_id}/submissions/update_grades | Grade or comment on multiple submissions
[**grade_or_comment_on_multiple_submissions_courses_submissions**](#grade_or_comment_on_multiple_submissions_courses_submissions) | **post** /v1/courses/{course_id}/submissions/update_grades | Grade or comment on multiple submissions
[**grade_or_comment_on_multiple_submissions_sections_assignments**](#grade_or_comment_on_multiple_submissions_sections_assignments) | **post** /v1/sections/{section_id}/assignments/{assignment_id}/submissions/update_grades | Grade or comment on multiple submissions
[**grade_or_comment_on_multiple_submissions_sections_submissions**](#grade_or_comment_on_multiple_submissions_sections_submissions) | **post** /v1/sections/{section_id}/submissions/update_grades | Grade or comment on multiple submissions
[**grade_or_comment_on_submission_courses**](#grade_or_comment_on_submission_courses) | **put** /v1/courses/{course_id}/assignments/{assignment_id}/submissions/{user_id} | Grade or comment on a submission
[**grade_or_comment_on_submission_sections**](#grade_or_comment_on_submission_sections) | **put** /v1/sections/{section_id}/assignments/{assignment_id}/submissions/{user_id} | Grade or comment on a submission
[**list_assignment_submissions_courses**](#list_assignment_submissions_courses) | **get** /v1/courses/{course_id}/assignments/{assignment_id}/submissions | List assignment submissions
[**list_assignment_submissions_sections**](#list_assignment_submissions_sections) | **get** /v1/sections/{section_id}/assignments/{assignment_id}/submissions | List assignment submissions
[**list_gradeable_students**](#list_gradeable_students) | **get** /v1/courses/{course_id}/assignments/{assignment_id}/gradeable_students | List gradeable students
[**list_multiple_assignments_gradeable_students**](#list_multiple_assignments_gradeable_students) | **get** /v1/courses/{course_id}/assignments/gradeable_students | List multiple assignments gradeable students
[**list_submissions_for_multiple_assignments_courses**](#list_submissions_for_multiple_assignments_courses) | **get** /v1/courses/{course_id}/students/submissions | List submissions for multiple assignments
[**list_submissions_for_multiple_assignments_sections**](#list_submissions_for_multiple_assignments_sections) | **get** /v1/sections/{section_id}/students/submissions | List submissions for multiple assignments
[**mark_submission_as_read_courses**](#mark_submission_as_read_courses) | **put** /v1/courses/{course_id}/assignments/{assignment_id}/submissions/{user_id}/read | Mark submission as read
[**mark_submission_as_read_sections**](#mark_submission_as_read_sections) | **put** /v1/sections/{section_id}/assignments/{assignment_id}/submissions/{user_id}/read | Mark submission as read
[**mark_submission_as_unread_courses**](#mark_submission_as_unread_courses) | **delete** /v1/courses/{course_id}/assignments/{assignment_id}/submissions/{user_id}/read | Mark submission as unread
[**mark_submission_as_unread_sections**](#mark_submission_as_unread_sections) | **delete** /v1/sections/{section_id}/assignments/{assignment_id}/submissions/{user_id}/read | Mark submission as unread
[**submission_summary_courses**](#submission_summary_courses) | **get** /v1/courses/{course_id}/assignments/{assignment_id}/submission_summary | Submission Summary
[**submission_summary_sections**](#submission_summary_sections) | **get** /v1/sections/{section_id}/assignments/{assignment_id}/submission_summary | Submission Summary
[**submit_assignment_courses**](#submit_assignment_courses) | **post** /v1/courses/{course_id}/assignments/{assignment_id}/submissions | Submit an assignment
[**submit_assignment_sections**](#submit_assignment_sections) | **post** /v1/sections/{section_id}/assignments/{assignment_id}/submissions | Submit an assignment
[**upload_file_courses**](#upload_file_courses) | **post** /v1/courses/{course_id}/assignments/{assignment_id}/submissions/{user_id}/files | Upload a file
[**upload_file_sections**](#upload_file_sections) | **post** /v1/sections/{section_id}/assignments/{assignment_id}/submissions/{user_id}/files | Upload a file

# **get_single_submission_courses**
<a name="get_single_submission_courses"></a>
> get_single_submission_courses(course_idassignment_iduser_id)

Get a single submission

Get a single submission, based on user id.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    query_params = {
    }
    try:
        # Get a single submission
        api_response = api_instance.get_single_submission_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->get_single_submission_courses: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    query_params = {
        'include': [
        "submission_history"
    ],
    }
    try:
        # Get a single submission
        api_response = api_instance.get_single_submission_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->get_single_submission_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
include | IncludeSchema | | optional


# IncludeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 
user_id | UserIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_single_submission_courses.ApiResponseFor200) | No response was specified

#### get_single_submission_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **get_single_submission_sections**
<a name="get_single_submission_sections"></a>
> get_single_submission_sections(section_idassignment_iduser_id)

Get a single submission

Get a single submission, based on user id.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    query_params = {
    }
    try:
        # Get a single submission
        api_response = api_instance.get_single_submission_sections(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->get_single_submission_sections: %s\n" % e)

    # example passing only optional values
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    query_params = {
        'include': [
        "submission_history"
    ],
    }
    try:
        # Get a single submission
        api_response = api_instance.get_single_submission_sections(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->get_single_submission_sections: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
include | IncludeSchema | | optional


# IncludeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
section_id | SectionIdSchema | | 
assignment_id | AssignmentIdSchema | | 
user_id | UserIdSchema | | 

# SectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_single_submission_sections.ApiResponseFor200) | No response was specified

#### get_single_submission_sections.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **grade_or_comment_on_multiple_submissions_courses_assignments**
<a name="grade_or_comment_on_multiple_submissions_courses_assignments"></a>
> Progress grade_or_comment_on_multiple_submissions_courses_assignments(course_idassignment_id)

Grade or comment on multiple submissions

Update the grading and comments on multiple student's assignment submissions in an asynchronous job.  The user must have permission to manage grades in the appropriate context (course or section).

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from openapi_client.model.progress import Progress
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    try:
        # Grade or comment on multiple submissions
        api_response = api_instance.grade_or_comment_on_multiple_submissions_courses_assignments(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_multiple_submissions_courses_assignments: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    body = None
    try:
        # Grade or comment on multiple submissions
        api_response = api_instance.grade_or_comment_on_multiple_submissions_courses_assignments(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_multiple_submissions_courses_assignments: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**grade_data[student_id][assignment_id]** | decimal.Decimal, int,  | decimal.Decimal,  | Specifies which assignment to grade.  This argument is not necessary when using the assignment-specific endpoints. | [optional] value must be a 64 bit integer
**grade_data[student_id][excuse]** | bool,  | BoolClass,  | See documentation for the excuse argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**[grade_data[student_id][file_ids]](#grade_data[student_id][file_ids])** | list, tuple,  | tuple,  | See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][group_comment]** | bool,  | BoolClass,  | no description | [optional] 
**grade_data[student_id][media_comment_id]** | str,  | str,  | no description | [optional] 
**grade_data[student_id][media_comment_type]** | str,  | str,  | no description | [optional] must be one of ["audio", "video", ] 
**grade_data[student_id][posted_grade]** | str,  | str,  | See documentation for the posted_grade argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][rubric_assessment]** | dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO | See documentation for the rubric_assessment argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][text_comment]** | str,  | str,  | no description | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# grade_data[student_id][file_ids]

See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#grade_or_comment_on_multiple_submissions_courses_assignments.ApiResponseFor200) | No response was specified

#### grade_or_comment_on_multiple_submissions_courses_assignments.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**Progress**](../../models/Progress.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **grade_or_comment_on_multiple_submissions_courses_submissions**
<a name="grade_or_comment_on_multiple_submissions_courses_submissions"></a>
> Progress grade_or_comment_on_multiple_submissions_courses_submissions(course_id)

Grade or comment on multiple submissions

Update the grading and comments on multiple student's assignment submissions in an asynchronous job.  The user must have permission to manage grades in the appropriate context (course or section).

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from openapi_client.model.progress import Progress
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    try:
        # Grade or comment on multiple submissions
        api_response = api_instance.grade_or_comment_on_multiple_submissions_courses_submissions(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_multiple_submissions_courses_submissions: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
    }
    body = None
    try:
        # Grade or comment on multiple submissions
        api_response = api_instance.grade_or_comment_on_multiple_submissions_courses_submissions(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_multiple_submissions_courses_submissions: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**grade_data[student_id][assignment_id]** | decimal.Decimal, int,  | decimal.Decimal,  | Specifies which assignment to grade.  This argument is not necessary when using the assignment-specific endpoints. | [optional] value must be a 64 bit integer
**grade_data[student_id][excuse]** | bool,  | BoolClass,  | See documentation for the excuse argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**[grade_data[student_id][file_ids]](#grade_data[student_id][file_ids])** | list, tuple,  | tuple,  | See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][group_comment]** | bool,  | BoolClass,  | no description | [optional] 
**grade_data[student_id][media_comment_id]** | str,  | str,  | no description | [optional] 
**grade_data[student_id][media_comment_type]** | str,  | str,  | no description | [optional] must be one of ["audio", "video", ] 
**grade_data[student_id][posted_grade]** | str,  | str,  | See documentation for the posted_grade argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][rubric_assessment]** | dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO | See documentation for the rubric_assessment argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][text_comment]** | str,  | str,  | no description | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# grade_data[student_id][file_ids]

See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#grade_or_comment_on_multiple_submissions_courses_submissions.ApiResponseFor200) | No response was specified

#### grade_or_comment_on_multiple_submissions_courses_submissions.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**Progress**](../../models/Progress.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **grade_or_comment_on_multiple_submissions_sections_assignments**
<a name="grade_or_comment_on_multiple_submissions_sections_assignments"></a>
> Progress grade_or_comment_on_multiple_submissions_sections_assignments(section_idassignment_id)

Grade or comment on multiple submissions

Update the grading and comments on multiple student's assignment submissions in an asynchronous job.  The user must have permission to manage grades in the appropriate context (course or section).

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from openapi_client.model.progress import Progress
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
    }
    try:
        # Grade or comment on multiple submissions
        api_response = api_instance.grade_or_comment_on_multiple_submissions_sections_assignments(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_multiple_submissions_sections_assignments: %s\n" % e)

    # example passing only optional values
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
    }
    body = None
    try:
        # Grade or comment on multiple submissions
        api_response = api_instance.grade_or_comment_on_multiple_submissions_sections_assignments(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_multiple_submissions_sections_assignments: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**grade_data[student_id][assignment_id]** | decimal.Decimal, int,  | decimal.Decimal,  | Specifies which assignment to grade.  This argument is not necessary when using the assignment-specific endpoints. | [optional] value must be a 64 bit integer
**grade_data[student_id][excuse]** | bool,  | BoolClass,  | See documentation for the excuse argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**[grade_data[student_id][file_ids]](#grade_data[student_id][file_ids])** | list, tuple,  | tuple,  | See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][group_comment]** | bool,  | BoolClass,  | no description | [optional] 
**grade_data[student_id][media_comment_id]** | str,  | str,  | no description | [optional] 
**grade_data[student_id][media_comment_type]** | str,  | str,  | no description | [optional] must be one of ["audio", "video", ] 
**grade_data[student_id][posted_grade]** | str,  | str,  | See documentation for the posted_grade argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][rubric_assessment]** | dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO | See documentation for the rubric_assessment argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][text_comment]** | str,  | str,  | no description | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# grade_data[student_id][file_ids]

See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
section_id | SectionIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# SectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#grade_or_comment_on_multiple_submissions_sections_assignments.ApiResponseFor200) | No response was specified

#### grade_or_comment_on_multiple_submissions_sections_assignments.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**Progress**](../../models/Progress.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **grade_or_comment_on_multiple_submissions_sections_submissions**
<a name="grade_or_comment_on_multiple_submissions_sections_submissions"></a>
> Progress grade_or_comment_on_multiple_submissions_sections_submissions(section_id)

Grade or comment on multiple submissions

Update the grading and comments on multiple student's assignment submissions in an asynchronous job.  The user must have permission to manage grades in the appropriate context (course or section).

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from openapi_client.model.progress import Progress
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'section_id': "section_id_example",
    }
    try:
        # Grade or comment on multiple submissions
        api_response = api_instance.grade_or_comment_on_multiple_submissions_sections_submissions(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_multiple_submissions_sections_submissions: %s\n" % e)

    # example passing only optional values
    path_params = {
        'section_id': "section_id_example",
    }
    body = None
    try:
        # Grade or comment on multiple submissions
        api_response = api_instance.grade_or_comment_on_multiple_submissions_sections_submissions(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_multiple_submissions_sections_submissions: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**grade_data[student_id][assignment_id]** | decimal.Decimal, int,  | decimal.Decimal,  | Specifies which assignment to grade.  This argument is not necessary when using the assignment-specific endpoints. | [optional] value must be a 64 bit integer
**grade_data[student_id][excuse]** | bool,  | BoolClass,  | See documentation for the excuse argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**[grade_data[student_id][file_ids]](#grade_data[student_id][file_ids])** | list, tuple,  | tuple,  | See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][group_comment]** | bool,  | BoolClass,  | no description | [optional] 
**grade_data[student_id][media_comment_id]** | str,  | str,  | no description | [optional] 
**grade_data[student_id][media_comment_type]** | str,  | str,  | no description | [optional] must be one of ["audio", "video", ] 
**grade_data[student_id][posted_grade]** | str,  | str,  | See documentation for the posted_grade argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][rubric_assessment]** | dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO | See documentation for the rubric_assessment argument in the {api:SubmissionsApiController#update Submissions Update} documentation | [optional] 
**grade_data[student_id][text_comment]** | str,  | str,  | no description | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# grade_data[student_id][file_ids]

See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | See documentation for the comment[] arguments in the {api:SubmissionsApiController#update Submissions Update} documentation | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
section_id | SectionIdSchema | | 

# SectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#grade_or_comment_on_multiple_submissions_sections_submissions.ApiResponseFor200) | No response was specified

#### grade_or_comment_on_multiple_submissions_sections_submissions.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**Progress**](../../models/Progress.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **grade_or_comment_on_submission_courses**
<a name="grade_or_comment_on_submission_courses"></a>
> Submission grade_or_comment_on_submission_courses(course_idassignment_iduser_id)

Grade or comment on a submission

Comment on and/or update the grading for a student's assignment submission. If any submission or rubric_assessment arguments are provided, the user must have permission to manage grades in the appropriate context (course or section).

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from openapi_client.model.submission import Submission
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    try:
        # Grade or comment on a submission
        api_response = api_instance.grade_or_comment_on_submission_courses(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_submission_courses: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    body = dict(
        comment_file_ids=[
            1
        ],
        comment_group_comment=True,
        comment_media_comment_id="comment_media_comment_id_example",
        comment_media_comment_type="audio",
        comment_text_comment="comment_text_comment_example",
        include_visibility="include_visibility_example",
        rubric_assessment=None,
        submission_excuse=True,
        submission_late_policy_status="submission_late_policy_status_example",
        submission_posted_grade="submission_posted_grade_example",
        submission_seconds_late_override=1,
    )
    try:
        # Grade or comment on a submission
        api_response = api_instance.grade_or_comment_on_submission_courses(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_submission_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[comment[file_ids]](#comment[file_ids])** | list, tuple,  | tuple,  | Attach files to this comment that were previously uploaded using the Submission Comment API&#x27;s files action | [optional] 
**comment[group_comment]** | bool,  | BoolClass,  | Whether or not this comment should be sent to the entire group (defaults to false). Ignored if this is not a group assignment or if no text_comment is provided. | [optional] 
**comment[media_comment_id]** | str,  | str,  | Add an audio/video comment to the submission. Media comments can be added via this API, however, note that there is not yet an API to generate or list existing media comments, so this functionality is currently of limited use. | [optional] 
**comment[media_comment_type]** | str,  | str,  | The type of media comment being added. | [optional] must be one of ["audio", "video", ] 
**comment[text_comment]** | str,  | str,  | Add a textual comment to the submission. | [optional] 
**include[visibility]** | str,  | str,  | Whether this assignment is visible to the owner of the submission | [optional] 
**rubric_assessment** | dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO | Assign a rubric assessment to this assignment submission. The sub-parameters here depend on the rubric for the assignment. The general format is, for each row in the rubric:  The points awarded for this row.   rubric_assessment[criterion_id][points]  Comments to add for this row.   rubric_assessment[criterion_id][comments]  For example, if the assignment rubric is (in JSON format):   !!!javascript   [     {       &#x27;id&#x27;: &#x27;crit1&#x27;,       &#x27;points&#x27;: 10,       &#x27;description&#x27;: &#x27;Criterion 1&#x27;,       &#x27;ratings&#x27;:       [         { &#x27;description&#x27;: &#x27;Good&#x27;, &#x27;points&#x27;: 10 },         { &#x27;description&#x27;: &#x27;Poor&#x27;, &#x27;points&#x27;: 3 }       ]     },     {       &#x27;id&#x27;: &#x27;crit2&#x27;,       &#x27;points&#x27;: 5,       &#x27;description&#x27;: &#x27;Criterion 2&#x27;,       &#x27;ratings&#x27;:       [         { &#x27;description&#x27;: &#x27;Complete&#x27;, &#x27;points&#x27;: 5 },         { &#x27;description&#x27;: &#x27;Incomplete&#x27;, &#x27;points&#x27;: 0 }       ]     }   ]  Then a possible set of values for rubric_assessment would be:     rubric_assessment[crit1][points]&#x3D;3&amp;rubric_assessment[crit2][points]&#x3D;5&amp;rubric_assessment[crit2][comments]&#x3D;Well%20Done. | [optional] 
**submission[excuse]** | bool,  | BoolClass,  | Sets the \&quot;excused\&quot; status of an assignment. | [optional] 
**submission[late_policy_status]** | str,  | str,  | Sets the late policy status to either \&quot;late\&quot;, \&quot;missing\&quot;, \&quot;none\&quot;, or null. | [optional] 
**submission[posted_grade]** | str,  | str,  | Assign a score to the submission, updating both the \&quot;score\&quot; and \&quot;grade\&quot; fields on the submission record. This parameter can be passed in a few different formats:  points:: A floating point or integral value, such as \&quot;13.5\&quot;. The grade   will be interpreted directly as the score of the assignment.   Values above assignment.points_possible are allowed, for awarding   extra credit. percentage:: A floating point value appended with a percent sign, such as    \&quot;40%\&quot;. The grade will be interpreted as a percentage score on the    assignment, where 100% &#x3D;&#x3D; assignment.points_possible. Values above 100%    are allowed, for awarding extra credit. letter grade:: A letter grade, following the assignment&#x27;s defined letter    grading scheme. For example, \&quot;A-\&quot;. The resulting score will be the high    end of the defined range for the letter grade. For instance, if \&quot;B\&quot; is    defined as 86% to 84%, a letter grade of \&quot;B\&quot; will be worth 86%. The    letter grade will be rejected if the assignment does not have a defined    letter grading scheme. For more fine-grained control of scores, pass in    points or percentage rather than the letter grade. \&quot;pass/complete/fail/incomplete\&quot;:: A string value of \&quot;pass\&quot; or \&quot;complete\&quot;    will give a score of 100%. \&quot;fail\&quot; or \&quot;incomplete\&quot; will give a score of    0.  Note that assignments with grading_type of \&quot;pass_fail\&quot; can only be assigned a score of 0 or assignment.points_possible, nothing inbetween. If a posted_grade in the \&quot;points\&quot; or \&quot;percentage\&quot; format is sent, the grade will only be accepted if the grade equals one of those two values. | [optional] 
**submission[seconds_late_override]** | decimal.Decimal, int,  | decimal.Decimal,  | Sets the seconds late if late policy status is \&quot;late\&quot; | [optional] value must be a 64 bit integer
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# comment[file_ids]

Attach files to this comment that were previously uploaded using the Submission Comment API's files action

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | Attach files to this comment that were previously uploaded using the Submission Comment API&#x27;s files action | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 
user_id | UserIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#grade_or_comment_on_submission_courses.ApiResponseFor200) | No response was specified

#### grade_or_comment_on_submission_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**Submission**](../../models/Submission.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **grade_or_comment_on_submission_sections**
<a name="grade_or_comment_on_submission_sections"></a>
> Submission grade_or_comment_on_submission_sections(section_idassignment_iduser_id)

Grade or comment on a submission

Comment on and/or update the grading for a student's assignment submission. If any submission or rubric_assessment arguments are provided, the user must have permission to manage grades in the appropriate context (course or section).

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from openapi_client.model.submission import Submission
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    try:
        # Grade or comment on a submission
        api_response = api_instance.grade_or_comment_on_submission_sections(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_submission_sections: %s\n" % e)

    # example passing only optional values
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    body = dict(
        comment_file_ids=[
            1
        ],
        comment_group_comment=True,
        comment_media_comment_id="comment_media_comment_id_example",
        comment_media_comment_type="audio",
        comment_text_comment="comment_text_comment_example",
        include_visibility="include_visibility_example",
        rubric_assessment=None,
        submission_excuse=True,
        submission_late_policy_status="submission_late_policy_status_example",
        submission_posted_grade="submission_posted_grade_example",
        submission_seconds_late_override=1,
    )
    try:
        # Grade or comment on a submission
        api_response = api_instance.grade_or_comment_on_submission_sections(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->grade_or_comment_on_submission_sections: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[comment[file_ids]](#comment[file_ids])** | list, tuple,  | tuple,  | Attach files to this comment that were previously uploaded using the Submission Comment API&#x27;s files action | [optional] 
**comment[group_comment]** | bool,  | BoolClass,  | Whether or not this comment should be sent to the entire group (defaults to false). Ignored if this is not a group assignment or if no text_comment is provided. | [optional] 
**comment[media_comment_id]** | str,  | str,  | Add an audio/video comment to the submission. Media comments can be added via this API, however, note that there is not yet an API to generate or list existing media comments, so this functionality is currently of limited use. | [optional] 
**comment[media_comment_type]** | str,  | str,  | The type of media comment being added. | [optional] must be one of ["audio", "video", ] 
**comment[text_comment]** | str,  | str,  | Add a textual comment to the submission. | [optional] 
**include[visibility]** | str,  | str,  | Whether this assignment is visible to the owner of the submission | [optional] 
**rubric_assessment** | dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO | Assign a rubric assessment to this assignment submission. The sub-parameters here depend on the rubric for the assignment. The general format is, for each row in the rubric:  The points awarded for this row.   rubric_assessment[criterion_id][points]  Comments to add for this row.   rubric_assessment[criterion_id][comments]  For example, if the assignment rubric is (in JSON format):   !!!javascript   [     {       &#x27;id&#x27;: &#x27;crit1&#x27;,       &#x27;points&#x27;: 10,       &#x27;description&#x27;: &#x27;Criterion 1&#x27;,       &#x27;ratings&#x27;:       [         { &#x27;description&#x27;: &#x27;Good&#x27;, &#x27;points&#x27;: 10 },         { &#x27;description&#x27;: &#x27;Poor&#x27;, &#x27;points&#x27;: 3 }       ]     },     {       &#x27;id&#x27;: &#x27;crit2&#x27;,       &#x27;points&#x27;: 5,       &#x27;description&#x27;: &#x27;Criterion 2&#x27;,       &#x27;ratings&#x27;:       [         { &#x27;description&#x27;: &#x27;Complete&#x27;, &#x27;points&#x27;: 5 },         { &#x27;description&#x27;: &#x27;Incomplete&#x27;, &#x27;points&#x27;: 0 }       ]     }   ]  Then a possible set of values for rubric_assessment would be:     rubric_assessment[crit1][points]&#x3D;3&amp;rubric_assessment[crit2][points]&#x3D;5&amp;rubric_assessment[crit2][comments]&#x3D;Well%20Done. | [optional] 
**submission[excuse]** | bool,  | BoolClass,  | Sets the \&quot;excused\&quot; status of an assignment. | [optional] 
**submission[late_policy_status]** | str,  | str,  | Sets the late policy status to either \&quot;late\&quot;, \&quot;missing\&quot;, \&quot;none\&quot;, or null. | [optional] 
**submission[posted_grade]** | str,  | str,  | Assign a score to the submission, updating both the \&quot;score\&quot; and \&quot;grade\&quot; fields on the submission record. This parameter can be passed in a few different formats:  points:: A floating point or integral value, such as \&quot;13.5\&quot;. The grade   will be interpreted directly as the score of the assignment.   Values above assignment.points_possible are allowed, for awarding   extra credit. percentage:: A floating point value appended with a percent sign, such as    \&quot;40%\&quot;. The grade will be interpreted as a percentage score on the    assignment, where 100% &#x3D;&#x3D; assignment.points_possible. Values above 100%    are allowed, for awarding extra credit. letter grade:: A letter grade, following the assignment&#x27;s defined letter    grading scheme. For example, \&quot;A-\&quot;. The resulting score will be the high    end of the defined range for the letter grade. For instance, if \&quot;B\&quot; is    defined as 86% to 84%, a letter grade of \&quot;B\&quot; will be worth 86%. The    letter grade will be rejected if the assignment does not have a defined    letter grading scheme. For more fine-grained control of scores, pass in    points or percentage rather than the letter grade. \&quot;pass/complete/fail/incomplete\&quot;:: A string value of \&quot;pass\&quot; or \&quot;complete\&quot;    will give a score of 100%. \&quot;fail\&quot; or \&quot;incomplete\&quot; will give a score of    0.  Note that assignments with grading_type of \&quot;pass_fail\&quot; can only be assigned a score of 0 or assignment.points_possible, nothing inbetween. If a posted_grade in the \&quot;points\&quot; or \&quot;percentage\&quot; format is sent, the grade will only be accepted if the grade equals one of those two values. | [optional] 
**submission[seconds_late_override]** | decimal.Decimal, int,  | decimal.Decimal,  | Sets the seconds late if late policy status is \&quot;late\&quot; | [optional] value must be a 64 bit integer
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# comment[file_ids]

Attach files to this comment that were previously uploaded using the Submission Comment API's files action

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | Attach files to this comment that were previously uploaded using the Submission Comment API&#x27;s files action | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
section_id | SectionIdSchema | | 
assignment_id | AssignmentIdSchema | | 
user_id | UserIdSchema | | 

# SectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#grade_or_comment_on_submission_sections.ApiResponseFor200) | No response was specified

#### grade_or_comment_on_submission_sections.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**Submission**](../../models/Submission.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_assignment_submissions_courses**
<a name="list_assignment_submissions_courses"></a>
> [Submission] list_assignment_submissions_courses(course_idassignment_id)

List assignment submissions

A paginated list of all existing submissions for an assignment.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from openapi_client.model.submission import Submission
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    query_params = {
    }
    try:
        # List assignment submissions
        api_response = api_instance.list_assignment_submissions_courses(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->list_assignment_submissions_courses: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    query_params = {
        'include': [
        "submission_history"
    ],
        'grouped': True,
    }
    try:
        # List assignment submissions
        api_response = api_instance.list_assignment_submissions_courses(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->list_assignment_submissions_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
include | IncludeSchema | | optional
grouped | GroupedSchema | | optional


# IncludeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# GroupedSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_assignment_submissions_courses.ApiResponseFor200) | No response was specified

#### list_assignment_submissions_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**Submission**]({{complexTypePrefix}}Submission.md) | [**Submission**]({{complexTypePrefix}}Submission.md) | [**Submission**]({{complexTypePrefix}}Submission.md) |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_assignment_submissions_sections**
<a name="list_assignment_submissions_sections"></a>
> [Submission] list_assignment_submissions_sections(section_idassignment_id)

List assignment submissions

A paginated list of all existing submissions for an assignment.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from openapi_client.model.submission import Submission
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
    }
    query_params = {
    }
    try:
        # List assignment submissions
        api_response = api_instance.list_assignment_submissions_sections(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->list_assignment_submissions_sections: %s\n" % e)

    # example passing only optional values
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
    }
    query_params = {
        'include': [
        "submission_history"
    ],
        'grouped': True,
    }
    try:
        # List assignment submissions
        api_response = api_instance.list_assignment_submissions_sections(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->list_assignment_submissions_sections: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
include | IncludeSchema | | optional
grouped | GroupedSchema | | optional


# IncludeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# GroupedSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
section_id | SectionIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# SectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_assignment_submissions_sections.ApiResponseFor200) | No response was specified

#### list_assignment_submissions_sections.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**Submission**]({{complexTypePrefix}}Submission.md) | [**Submission**]({{complexTypePrefix}}Submission.md) | [**Submission**]({{complexTypePrefix}}Submission.md) |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_gradeable_students**
<a name="list_gradeable_students"></a>
> list_gradeable_students(course_idassignment_id)

List gradeable students

A paginated list of students eligible to submit the assignment. The caller must have permission to view grades.  Section-limited instructors will only see students in their own sections.  returns [UserDisplay]

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    try:
        # List gradeable students
        api_response = api_instance.list_gradeable_students(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->list_gradeable_students: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_gradeable_students.ApiResponseFor200) | No response was specified

#### list_gradeable_students.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_multiple_assignments_gradeable_students**
<a name="list_multiple_assignments_gradeable_students"></a>
> list_multiple_assignments_gradeable_students(course_id)

List multiple assignments gradeable students

A paginated list of students eligible to submit a list of assignments. The caller must have permission to view grades for the requested course.  Section-limited instructors will only see students in their own sections.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
    }
    try:
        # List multiple assignments gradeable students
        api_response = api_instance.list_multiple_assignments_gradeable_students(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->list_multiple_assignments_gradeable_students: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
        'assignment_ids': [
        "assignment_ids_example"
    ],
    }
    try:
        # List multiple assignments gradeable students
        api_response = api_instance.list_multiple_assignments_gradeable_students(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->list_multiple_assignments_gradeable_students: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
assignment_ids | AssignmentIdsSchema | | optional


# AssignmentIdsSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_multiple_assignments_gradeable_students.ApiResponseFor200) | No response was specified

#### list_multiple_assignments_gradeable_students.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_submissions_for_multiple_assignments_courses**
<a name="list_submissions_for_multiple_assignments_courses"></a>
> list_submissions_for_multiple_assignments_courses(course_id)

List submissions for multiple assignments

A paginated list of all existing submissions for a given set of students and assignments.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
    }
    try:
        # List submissions for multiple assignments
        api_response = api_instance.list_submissions_for_multiple_assignments_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->list_submissions_for_multiple_assignments_courses: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
        'student_ids': [
        "student_ids_example"
    ],
        'assignment_ids': [
        "assignment_ids_example"
    ],
        'grouped': True,
        'post_to_sis': True,
        'submitted_since': "1970-01-01T00:00:00.00Z",
        'graded_since': "1970-01-01T00:00:00.00Z",
        'grading_period_id': 1,
        'workflow_state': "submitted",
        'enrollment_state': "active",
        'state_based_on_date': True,
        'order': "id",
        'order_direction': "ascending",
        'include': [
        "submission_history"
    ],
    }
    try:
        # List submissions for multiple assignments
        api_response = api_instance.list_submissions_for_multiple_assignments_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->list_submissions_for_multiple_assignments_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
student_ids | StudentIdsSchema | | optional
assignment_ids | AssignmentIdsSchema | | optional
grouped | GroupedSchema | | optional
post_to_sis | PostToSisSchema | | optional
submitted_since | SubmittedSinceSchema | | optional
graded_since | GradedSinceSchema | | optional
grading_period_id | GradingPeriodIdSchema | | optional
workflow_state | WorkflowStateSchema | | optional
enrollment_state | EnrollmentStateSchema | | optional
state_based_on_date | StateBasedOnDateSchema | | optional
order | OrderSchema | | optional
order_direction | OrderDirectionSchema | | optional
include | IncludeSchema | | optional


# StudentIdsSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# AssignmentIdsSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# GroupedSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# PostToSisSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# SubmittedSinceSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str, datetime,  | str,  |  | value must conform to RFC-3339 date-time

# GradedSinceSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str, datetime,  | str,  |  | value must conform to RFC-3339 date-time

# GradingPeriodIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
decimal.Decimal, int,  | decimal.Decimal,  |  | value must be a 64 bit integer

# WorkflowStateSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["submitted", "unsubmitted", "graded", "pending_review", ] 

# EnrollmentStateSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["active", "concluded", ] 

# StateBasedOnDateSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# OrderSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["id", "graded_at", ] 

# OrderDirectionSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["ascending", "descending", ] 

# IncludeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_submissions_for_multiple_assignments_courses.ApiResponseFor200) | No response was specified

#### list_submissions_for_multiple_assignments_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_submissions_for_multiple_assignments_sections**
<a name="list_submissions_for_multiple_assignments_sections"></a>
> list_submissions_for_multiple_assignments_sections(section_id)

List submissions for multiple assignments

A paginated list of all existing submissions for a given set of students and assignments.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'section_id': "section_id_example",
    }
    query_params = {
    }
    try:
        # List submissions for multiple assignments
        api_response = api_instance.list_submissions_for_multiple_assignments_sections(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->list_submissions_for_multiple_assignments_sections: %s\n" % e)

    # example passing only optional values
    path_params = {
        'section_id': "section_id_example",
    }
    query_params = {
        'student_ids': [
        "student_ids_example"
    ],
        'assignment_ids': [
        "assignment_ids_example"
    ],
        'grouped': True,
        'post_to_sis': True,
        'submitted_since': "1970-01-01T00:00:00.00Z",
        'graded_since': "1970-01-01T00:00:00.00Z",
        'grading_period_id': 1,
        'workflow_state': "submitted",
        'enrollment_state': "active",
        'state_based_on_date': True,
        'order': "id",
        'order_direction': "ascending",
        'include': [
        "submission_history"
    ],
    }
    try:
        # List submissions for multiple assignments
        api_response = api_instance.list_submissions_for_multiple_assignments_sections(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->list_submissions_for_multiple_assignments_sections: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
student_ids | StudentIdsSchema | | optional
assignment_ids | AssignmentIdsSchema | | optional
grouped | GroupedSchema | | optional
post_to_sis | PostToSisSchema | | optional
submitted_since | SubmittedSinceSchema | | optional
graded_since | GradedSinceSchema | | optional
grading_period_id | GradingPeriodIdSchema | | optional
workflow_state | WorkflowStateSchema | | optional
enrollment_state | EnrollmentStateSchema | | optional
state_based_on_date | StateBasedOnDateSchema | | optional
order | OrderSchema | | optional
order_direction | OrderDirectionSchema | | optional
include | IncludeSchema | | optional


# StudentIdsSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# AssignmentIdsSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# GroupedSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# PostToSisSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# SubmittedSinceSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str, datetime,  | str,  |  | value must conform to RFC-3339 date-time

# GradedSinceSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str, datetime,  | str,  |  | value must conform to RFC-3339 date-time

# GradingPeriodIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
decimal.Decimal, int,  | decimal.Decimal,  |  | value must be a 64 bit integer

# WorkflowStateSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["submitted", "unsubmitted", "graded", "pending_review", ] 

# EnrollmentStateSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["active", "concluded", ] 

# StateBasedOnDateSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# OrderSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["id", "graded_at", ] 

# OrderDirectionSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["ascending", "descending", ] 

# IncludeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
section_id | SectionIdSchema | | 

# SectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_submissions_for_multiple_assignments_sections.ApiResponseFor200) | No response was specified

#### list_submissions_for_multiple_assignments_sections.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **mark_submission_as_read_courses**
<a name="mark_submission_as_read_courses"></a>
> mark_submission_as_read_courses(course_idassignment_iduser_id)

Mark submission as read

No request fields are necessary.  On success, the response will be 204 No Content with an empty body.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    try:
        # Mark submission as read
        api_response = api_instance.mark_submission_as_read_courses(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->mark_submission_as_read_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 
user_id | UserIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#mark_submission_as_read_courses.ApiResponseFor200) | No response was specified

#### mark_submission_as_read_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **mark_submission_as_read_sections**
<a name="mark_submission_as_read_sections"></a>
> mark_submission_as_read_sections(section_idassignment_iduser_id)

Mark submission as read

No request fields are necessary.  On success, the response will be 204 No Content with an empty body.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    try:
        # Mark submission as read
        api_response = api_instance.mark_submission_as_read_sections(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->mark_submission_as_read_sections: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
section_id | SectionIdSchema | | 
assignment_id | AssignmentIdSchema | | 
user_id | UserIdSchema | | 

# SectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#mark_submission_as_read_sections.ApiResponseFor200) | No response was specified

#### mark_submission_as_read_sections.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **mark_submission_as_unread_courses**
<a name="mark_submission_as_unread_courses"></a>
> mark_submission_as_unread_courses(course_idassignment_iduser_id)

Mark submission as unread

No request fields are necessary.  On success, the response will be 204 No Content with an empty body.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    try:
        # Mark submission as unread
        api_response = api_instance.mark_submission_as_unread_courses(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->mark_submission_as_unread_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 
user_id | UserIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#mark_submission_as_unread_courses.ApiResponseFor200) | No response was specified

#### mark_submission_as_unread_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **mark_submission_as_unread_sections**
<a name="mark_submission_as_unread_sections"></a>
> mark_submission_as_unread_sections(section_idassignment_iduser_id)

Mark submission as unread

No request fields are necessary.  On success, the response will be 204 No Content with an empty body.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    try:
        # Mark submission as unread
        api_response = api_instance.mark_submission_as_unread_sections(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->mark_submission_as_unread_sections: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
section_id | SectionIdSchema | | 
assignment_id | AssignmentIdSchema | | 
user_id | UserIdSchema | | 

# SectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#mark_submission_as_unread_sections.ApiResponseFor200) | No response was specified

#### mark_submission_as_unread_sections.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **submission_summary_courses**
<a name="submission_summary_courses"></a>
> submission_summary_courses(course_idassignment_id)

Submission Summary

Returns the number of submissions for the given assignment based on gradeable students that fall into three categories: graded, ungraded, not submitted.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    query_params = {
    }
    try:
        # Submission Summary
        api_response = api_instance.submission_summary_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->submission_summary_courses: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    query_params = {
        'grouped': True,
    }
    try:
        # Submission Summary
        api_response = api_instance.submission_summary_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->submission_summary_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
grouped | GroupedSchema | | optional


# GroupedSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#submission_summary_courses.ApiResponseFor200) | No response was specified

#### submission_summary_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **submission_summary_sections**
<a name="submission_summary_sections"></a>
> submission_summary_sections(section_idassignment_id)

Submission Summary

Returns the number of submissions for the given assignment based on gradeable students that fall into three categories: graded, ungraded, not submitted.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
    }
    query_params = {
    }
    try:
        # Submission Summary
        api_response = api_instance.submission_summary_sections(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->submission_summary_sections: %s\n" % e)

    # example passing only optional values
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
    }
    query_params = {
        'grouped': True,
    }
    try:
        # Submission Summary
        api_response = api_instance.submission_summary_sections(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->submission_summary_sections: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
grouped | GroupedSchema | | optional


# GroupedSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
section_id | SectionIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# SectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#submission_summary_sections.ApiResponseFor200) | No response was specified

#### submission_summary_sections.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **submit_assignment_courses**
<a name="submit_assignment_courses"></a>
> submit_assignment_courses(course_idassignment_id)

Submit an assignment

Make a submission for an assignment. You must be enrolled as a student in the course/section to do this.  All online turn-in submission types are supported in this API. However, there are a few things that are not yet supported:  * Files can be submitted based on a file ID of a user or group file. However, there is no API yet for listing the user and group files, or uploading new files via the API. A file upload API is coming soon. * Media comments can be submitted, however, there is no API yet for creating a media comment to submit. * Integration with Google Docs is not yet supported.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    try:
        # Submit an assignment
        api_response = api_instance.submit_assignment_courses(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->submit_assignment_courses: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    body = None
    try:
        # Submit an assignment
        api_response = api_instance.submit_assignment_courses(
            path_params=path_params,
            body=body,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->submit_assignment_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**submission[submission_type]** | str,  | str,  | The type of submission being made. The assignment submission_types must include this submission type as an allowed option, or the submission will be rejected with a 400 error.  The submission_type given determines which of the following parameters is used. For instance, to submit a URL, submission [submission_type] must be set to \&quot;online_url\&quot;, otherwise the submission [url] parameter will be ignored. | must be one of ["online_text_entry", "online_url", "online_upload", "media_recording", "basic_lti_launch", ] 
**comment[text_comment]** | str,  | str,  | Include a textual comment with the submission. | [optional] 
**submission[body]** | str,  | str,  | Submit the assignment as an HTML document snippet. Note this HTML snippet will be sanitized using the same ruleset as a submission made from the Canvas web UI. The sanitized HTML will be returned in the response as the submission body. Requires a submission_type of \&quot;online_text_entry\&quot;. | [optional] 
**[submission[file_ids]](#submission[file_ids])** | list, tuple,  | tuple,  | Submit the assignment as a set of one or more previously uploaded files residing in the submitting user&#x27;s files section (or the group&#x27;s files section, for group assignments).  To upload a new file to submit, see the submissions {api:SubmissionsApiController#create_file Upload a file API}.  Requires a submission_type of \&quot;online_upload\&quot;. | [optional] 
**submission[media_comment_id]** | str,  | str,  | The media comment id to submit. Media comment ids can be submitted via this API, however, note that there is not yet an API to generate or list existing media comments, so this functionality is currently of limited use.  Requires a submission_type of \&quot;media_recording\&quot;. | [optional] 
**submission[media_comment_type]** | str,  | str,  | The type of media comment being submitted. | [optional] must be one of ["audio", "video", ] 
**submission[url]** | str,  | str,  | Submit the assignment as a URL. The URL scheme must be \&quot;http\&quot; or \&quot;https\&quot;, no \&quot;ftp\&quot; or other URL schemes are allowed. If no scheme is given (e.g. \&quot;www.example.com\&quot;) then \&quot;http\&quot; will be assumed. Requires a submission_type of \&quot;online_url\&quot; or \&quot;basic_lti_launch\&quot;. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# submission[file_ids]

Submit the assignment as a set of one or more previously uploaded files residing in the submitting user's files section (or the group's files section, for group assignments).  To upload a new file to submit, see the submissions {api:SubmissionsApiController#create_file Upload a file API}.  Requires a submission_type of \"online_upload\".

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | Submit the assignment as a set of one or more previously uploaded files residing in the submitting user&#x27;s files section (or the group&#x27;s files section, for group assignments).  To upload a new file to submit, see the submissions {api:SubmissionsApiController#create_file Upload a file API}.  Requires a submission_type of \&quot;online_upload\&quot;. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#submit_assignment_courses.ApiResponseFor200) | No response was specified

#### submit_assignment_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **submit_assignment_sections**
<a name="submit_assignment_sections"></a>
> submit_assignment_sections(section_idassignment_id)

Submit an assignment

Make a submission for an assignment. You must be enrolled as a student in the course/section to do this.  All online turn-in submission types are supported in this API. However, there are a few things that are not yet supported:  * Files can be submitted based on a file ID of a user or group file. However, there is no API yet for listing the user and group files, or uploading new files via the API. A file upload API is coming soon. * Media comments can be submitted, however, there is no API yet for creating a media comment to submit. * Integration with Google Docs is not yet supported.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
    }
    try:
        # Submit an assignment
        api_response = api_instance.submit_assignment_sections(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->submit_assignment_sections: %s\n" % e)

    # example passing only optional values
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
    }
    body = None
    try:
        # Submit an assignment
        api_response = api_instance.submit_assignment_sections(
            path_params=path_params,
            body=body,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->submit_assignment_sections: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**submission[submission_type]** | str,  | str,  | The type of submission being made. The assignment submission_types must include this submission type as an allowed option, or the submission will be rejected with a 400 error.  The submission_type given determines which of the following parameters is used. For instance, to submit a URL, submission [submission_type] must be set to \&quot;online_url\&quot;, otherwise the submission [url] parameter will be ignored. | must be one of ["online_text_entry", "online_url", "online_upload", "media_recording", "basic_lti_launch", ] 
**comment[text_comment]** | str,  | str,  | Include a textual comment with the submission. | [optional] 
**submission[body]** | str,  | str,  | Submit the assignment as an HTML document snippet. Note this HTML snippet will be sanitized using the same ruleset as a submission made from the Canvas web UI. The sanitized HTML will be returned in the response as the submission body. Requires a submission_type of \&quot;online_text_entry\&quot;. | [optional] 
**[submission[file_ids]](#submission[file_ids])** | list, tuple,  | tuple,  | Submit the assignment as a set of one or more previously uploaded files residing in the submitting user&#x27;s files section (or the group&#x27;s files section, for group assignments).  To upload a new file to submit, see the submissions {api:SubmissionsApiController#create_file Upload a file API}.  Requires a submission_type of \&quot;online_upload\&quot;. | [optional] 
**submission[media_comment_id]** | str,  | str,  | The media comment id to submit. Media comment ids can be submitted via this API, however, note that there is not yet an API to generate or list existing media comments, so this functionality is currently of limited use.  Requires a submission_type of \&quot;media_recording\&quot;. | [optional] 
**submission[media_comment_type]** | str,  | str,  | The type of media comment being submitted. | [optional] must be one of ["audio", "video", ] 
**submission[url]** | str,  | str,  | Submit the assignment as a URL. The URL scheme must be \&quot;http\&quot; or \&quot;https\&quot;, no \&quot;ftp\&quot; or other URL schemes are allowed. If no scheme is given (e.g. \&quot;www.example.com\&quot;) then \&quot;http\&quot; will be assumed. Requires a submission_type of \&quot;online_url\&quot; or \&quot;basic_lti_launch\&quot;. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# submission[file_ids]

Submit the assignment as a set of one or more previously uploaded files residing in the submitting user's files section (or the group's files section, for group assignments).  To upload a new file to submit, see the submissions {api:SubmissionsApiController#create_file Upload a file API}.  Requires a submission_type of \"online_upload\".

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | Submit the assignment as a set of one or more previously uploaded files residing in the submitting user&#x27;s files section (or the group&#x27;s files section, for group assignments).  To upload a new file to submit, see the submissions {api:SubmissionsApiController#create_file Upload a file API}.  Requires a submission_type of \&quot;online_upload\&quot;. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
section_id | SectionIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# SectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#submit_assignment_sections.ApiResponseFor200) | No response was specified

#### submit_assignment_sections.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **upload_file_courses**
<a name="upload_file_courses"></a>
> upload_file_courses(course_idassignment_iduser_id)

Upload a file

Upload a file to a submission.  This API endpoint is the first step in uploading a file to a submission as a student. See the {file:file_uploads.html File Upload Documentation} for details on the file upload workflow.  The final step of the file upload workflow will return the attachment data, including the new file id. The caller can then POST to submit the +online_upload+ assignment with these file ids.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    try:
        # Upload a file
        api_response = api_instance.upload_file_courses(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->upload_file_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 
user_id | UserIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#upload_file_courses.ApiResponseFor200) | No response was specified

#### upload_file_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **upload_file_sections**
<a name="upload_file_sections"></a>
> upload_file_sections(section_idassignment_iduser_id)

Upload a file

Upload a file to a submission.  This API endpoint is the first step in uploading a file to a submission as a student. See the {file:file_uploads.html File Upload Documentation} for details on the file upload workflow.  The final step of the file upload workflow will return the attachment data, including the new file id. The caller can then POST to submit the +online_upload+ assignment with these file ids.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import submissions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = submissions_api.SubmissionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'section_id': "section_id_example",
        'assignment_id': "assignment_id_example",
        'user_id': "user_id_example",
    }
    try:
        # Upload a file
        api_response = api_instance.upload_file_sections(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SubmissionsApi->upload_file_sections: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
section_id | SectionIdSchema | | 
assignment_id | AssignmentIdSchema | | 
user_id | UserIdSchema | | 

# SectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#upload_file_sections.ApiResponseFor200) | No response was specified

#### upload_file_sections.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

