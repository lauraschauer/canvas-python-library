<a name="__pageTop"></a>
# openapi_client.apis.tags.plagiarism_detection_platform_assignments_api.PlagiarismDetectionPlatformAssignmentsApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_single_assignment_lti**](#get_single_assignment_lti) | **get** /lti/assignments/{assignment_id} | Get a single assignment (lti)

# **get_single_assignment_lti**
<a name="get_single_assignment_lti"></a>
> LtiAssignment get_single_assignment_lti(assignment_id)

Get a single assignment (lti)

Get a single Canvas assignment by Canvas id or LTI id. Tool providers may only access assignments that are associated with their tool.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import plagiarism_detection_platform_assignments_api
from openapi_client.model.lti_assignment import LtiAssignment
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = plagiarism_detection_platform_assignments_api.PlagiarismDetectionPlatformAssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'assignment_id': "assignment_id_example",
    }
    query_params = {
    }
    try:
        # Get a single assignment (lti)
        api_response = api_instance.get_single_assignment_lti(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling PlagiarismDetectionPlatformAssignmentsApi->get_single_assignment_lti: %s\n" % e)

    # example passing only optional values
    path_params = {
        'assignment_id': "assignment_id_example",
    }
    query_params = {
        'user_id': "user_id_example",
    }
    try:
        # Get a single assignment (lti)
        api_response = api_instance.get_single_assignment_lti(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling PlagiarismDetectionPlatformAssignmentsApi->get_single_assignment_lti: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
user_id | UserIdSchema | | optional


# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
assignment_id | AssignmentIdSchema | | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_single_assignment_lti.ApiResponseFor200) | No response was specified

#### get_single_assignment_lti.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**LtiAssignment**](../../models/LtiAssignment.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

