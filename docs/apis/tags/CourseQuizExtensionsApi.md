<a name="__pageTop"></a>
# openapi_client.apis.tags.course_quiz_extensions_api.CourseQuizExtensionsApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**set_extensions_for_student_quiz_submissions**](#set_extensions_for_student_quiz_submissions) | **post** /v1/courses/{course_id}/quiz_extensions | Set extensions for student quiz submissions

# **set_extensions_for_student_quiz_submissions**
<a name="set_extensions_for_student_quiz_submissions"></a>
> set_extensions_for_student_quiz_submissions(course_id)

Set extensions for student quiz submissions

<b>Responses</b>  * <b>200 OK</b> if the request was successful * <b>403 Forbidden</b> if you are not allowed to extend quizzes for this course

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import course_quiz_extensions_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = course_quiz_extensions_api.CourseQuizExtensionsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    try:
        # Set extensions for student quiz submissions
        api_response = api_instance.set_extensions_for_student_quiz_submissions(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling CourseQuizExtensionsApi->set_extensions_for_student_quiz_submissions: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
    }
    body = None
    try:
        # Set extensions for student quiz submissions
        api_response = api_instance.set_extensions_for_student_quiz_submissions(
            path_params=path_params,
            body=body,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling CourseQuizExtensionsApi->set_extensions_for_student_quiz_submissions: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**user_id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the user we want to add quiz extensions for. | value must be a 64 bit integer
**extend_from_end_at** | decimal.Decimal, int,  | decimal.Decimal,  | The number of minutes to extend the quiz beyond the quiz&#x27;s current ending time. This is mutually exclusive to extend_from_now. This is limited to 1440 minutes (24 hours) | [optional] value must be a 64 bit integer
**extend_from_now** | decimal.Decimal, int,  | decimal.Decimal,  | The number of minutes to extend the quiz from the current time. This is mutually exclusive to extend_from_end_at. This is limited to 1440 minutes (24 hours) | [optional] value must be a 64 bit integer
**extra_attempts** | decimal.Decimal, int,  | decimal.Decimal,  | Number of times the student is allowed to re-take the quiz over the multiple-attempt limit. This is limited to 1000 attempts or less. | [optional] value must be a 64 bit integer
**extra_time** | decimal.Decimal, int,  | decimal.Decimal,  | The number of extra minutes to allow for all attempts. This will add to the existing time limit on the submission. This is limited to 10080 minutes (1 week) | [optional] value must be a 64 bit integer
**manually_unlocked** | bool,  | BoolClass,  | Allow the student to take the quiz even if it&#x27;s locked for everyone else. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#set_extensions_for_student_quiz_submissions.ApiResponseFor200) | No response was specified

#### set_extensions_for_student_quiz_submissions.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

