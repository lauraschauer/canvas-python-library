<a name="__pageTop"></a>
# openapi_client.apis.tags.assignments_api.AssignmentsApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**batch_create_overrides_in_course**](#batch_create_overrides_in_course) | **post** /v1/courses/{course_id}/assignments/overrides | Batch create overrides in a course
[**batch_retrieve_overrides_in_course**](#batch_retrieve_overrides_in_course) | **get** /v1/courses/{course_id}/assignments/overrides | Batch retrieve overrides in a course
[**batch_update_overrides_in_course**](#batch_update_overrides_in_course) | **put** /v1/courses/{course_id}/assignments/overrides | Batch update overrides in a course
[**create_assignment**](#create_assignment) | **post** /v1/courses/{course_id}/assignments | Create an assignment
[**create_assignment_override**](#create_assignment_override) | **post** /v1/courses/{course_id}/assignments/{assignment_id}/overrides | Create an assignment override
[**delete_assignment**](#delete_assignment) | **delete** /v1/courses/{course_id}/assignments/{id} | Delete an assignment
[**delete_assignment_override**](#delete_assignment_override) | **delete** /v1/courses/{course_id}/assignments/{assignment_id}/overrides/{id} | Delete an assignment override
[**edit_assignment**](#edit_assignment) | **put** /v1/courses/{course_id}/assignments/{id} | Edit an assignment
[**get_single_assignment**](#get_single_assignment) | **get** /v1/courses/{course_id}/assignments/{id} | Get a single assignment
[**get_single_assignment_override**](#get_single_assignment_override) | **get** /v1/courses/{course_id}/assignments/{assignment_id}/overrides/{id} | Get a single assignment override
[**list_assignment_overrides**](#list_assignment_overrides) | **get** /v1/courses/{course_id}/assignments/{assignment_id}/overrides | List assignment overrides
[**list_assignments**](#list_assignments) | **get** /v1/courses/{course_id}/assignments | List assignments
[**list_assignments_for_user**](#list_assignments_for_user) | **get** /v1/users/{user_id}/courses/{course_id}/assignments | List assignments for user
[**redirect_to_assignment_override_for_group**](#redirect_to_assignment_override_for_group) | **get** /v1/groups/{group_id}/assignments/{assignment_id}/override | Redirect to the assignment override for a group
[**redirect_to_assignment_override_for_section**](#redirect_to_assignment_override_for_section) | **get** /v1/sections/{course_section_id}/assignments/{assignment_id}/override | Redirect to the assignment override for a section
[**update_assignment_override**](#update_assignment_override) | **put** /v1/courses/{course_id}/assignments/{assignment_id}/overrides/{id} | Update an assignment override

# **batch_create_overrides_in_course**
<a name="batch_create_overrides_in_course"></a>
> [AssignmentOverride] batch_create_overrides_in_course(course_id)

Batch create overrides in a course

Creates the specified overrides for each assignment.  Handles creation in a transaction, so all records are created or none are.  One of student_ids, group_id, or course_section_id must be present. At most one should be present; if multiple are present only the most specific (student_ids first, then group_id, then course_section_id) is used and any others are ignored.  Errors are reported in an errors attribute, an array of errors corresponding to inputs.  Global errors will be reported as a single element errors array

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment_override import AssignmentOverride
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    try:
        # Batch create overrides in a course
        api_response = api_instance.batch_create_overrides_in_course(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->batch_create_overrides_in_course: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
    }
    body = None
    try:
        # Batch create overrides in a course
        api_response = api_instance.batch_create_overrides_in_course(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->batch_create_overrides_in_course: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[assignment_overrides](#assignment_overrides)** | list, tuple,  | tuple,  | Attributes for the new assignment overrides. See {api:AssignmentOverridesController#create Create an assignment override} for available attributes | 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# assignment_overrides

Attributes for the new assignment overrides. See {api:AssignmentOverridesController#create Create an assignment override} for available attributes

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | Attributes for the new assignment overrides. See {api:AssignmentOverridesController#create Create an assignment override} for available attributes | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#batch_create_overrides_in_course.ApiResponseFor200) | No response was specified

#### batch_create_overrides_in_course.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **batch_retrieve_overrides_in_course**
<a name="batch_retrieve_overrides_in_course"></a>
> [AssignmentOverride] batch_retrieve_overrides_in_course(course_idassignment_overrides_idassignment_overrides_assignment_id)

Batch retrieve overrides in a course

Returns a list of specified overrides in this course, providing they target sections/groups/students visible to the current user. Returns null elements in the list for requests that were not found.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment_override import AssignmentOverride
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
        'assignment_overrides[id]': [
        "assignment_overrides[id]_example"
    ],
        'assignment_overrides[assignment_id]': [
        "assignment_overrides[assignment_id]_example"
    ],
    }
    try:
        # Batch retrieve overrides in a course
        api_response = api_instance.batch_retrieve_overrides_in_course(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->batch_retrieve_overrides_in_course: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
assignment_overrides[id] | AssignmentOverridesIdSchema | | 
assignment_overrides[assignment_id] | AssignmentOverridesAssignmentIdSchema | | 


# AssignmentOverridesIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# AssignmentOverridesAssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#batch_retrieve_overrides_in_course.ApiResponseFor200) | No response was specified

#### batch_retrieve_overrides_in_course.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **batch_update_overrides_in_course**
<a name="batch_update_overrides_in_course"></a>
> [AssignmentOverride] batch_update_overrides_in_course(course_idany_type)

Batch update overrides in a course

Updates a list of specified overrides for each assignment.  Handles overrides in a transaction, so either all updates are applied or none. See {api:AssignmentOverridesController#update Update an assignment override} for available attributes.  All current overridden values must be supplied if they are to be retained; e.g. if due_at was overridden, but this PUT omits a value for due_at, due_at will no longer be overridden. If the override is adhoc and student_ids is not supplied, the target override set is unchanged. Target override sets cannot be changed for group or section overrides.  Errors are reported in an errors attribute, an array of errors corresponding to inputs.  Global errors will be reported as a single element errors array

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment_override import AssignmentOverride
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    body = dict(
        assignment_overrides=[
            AssignmentOverride(
                all_day=1,
                all_day_date="1970-01-01T00:00:00.00Z",
                assignment_id=123,
                course_section_id=1,
                due_at="1970-01-01T00:00:00.00Z",
                group_id=2,
                id=4,
                lock_at="1970-01-01T00:00:00.00Z",
                student_ids=[1,2,3],
                title="an assignment override",
                unlock_at="1970-01-01T00:00:00.00Z",
            )
        ],
    )
    try:
        # Batch update overrides in a course
        api_response = api_instance.batch_update_overrides_in_course(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->batch_update_overrides_in_course: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson] | required |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[assignment_overrides](#assignment_overrides)** | list, tuple,  | tuple,  |  | 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# assignment_overrides

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#batch_update_overrides_in_course.ApiResponseFor200) | No response was specified

#### batch_update_overrides_in_course.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **create_assignment**
<a name="create_assignment"></a>
> Assignment create_assignment(course_id)

Create an assignment

Create a new assignment for this course. The assignment is created in the active state.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment_override import AssignmentOverride
from openapi_client.model.assignment import Assignment
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    try:
        # Create an assignment
        api_response = api_instance.create_assignment(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->create_assignment: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
    }
    body = None
    try:
        # Create an assignment
        api_response = api_instance.create_assignment(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->create_assignment: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**assignment[name]** | str,  | str,  | The assignment name. | 
**[assignment[allowed_extensions]](#assignment[allowed_extensions])** | list, tuple,  | tuple,  |  | [optional] 
**assignment[assignment_group_id]** | decimal.Decimal, int,  | decimal.Decimal,  | The assignment group id to put the assignment in. Defaults to the top assignment group in the course. | [optional] value must be a 64 bit integer
**[assignment[assignment_overrides]](#assignment[assignment_overrides])** | list, tuple,  | tuple,  | List of overrides for the assignment. | [optional] 
**assignment[automatic_peer_reviews]** | bool,  | BoolClass,  | Whether peer reviews will be assigned automatically by Canvas or if teachers must manually assign peer reviews. Does not apply if peer reviews are not enabled. | [optional] 
**assignment[description]** | str,  | str,  | The assignment&#x27;s description, supports HTML. | [optional] 
**assignment[due_at]** | str, datetime,  | str,  | The day/time the assignment is due. Must be between the lock dates if there are lock dates. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. | [optional] value must conform to RFC-3339 date-time
**assignment[external_tool_tag_attributes]** | str,  | str,  | Hash of external tool parameters if submission_types is [\&quot;external_tool\&quot;]. See Assignment object definition for format. | [optional] 
**assignment[grade_group_students_individually]** | decimal.Decimal, int,  | decimal.Decimal,  | If this is a group assignment, teachers have the options to grade students individually. If false, Canvas will apply the assignment&#x27;s score to each member of the group. If true, the teacher can manually assign scores to each member of the group. | [optional] value must be a 64 bit integer
**assignment[grading_standard_id]** | decimal.Decimal, int,  | decimal.Decimal,  | The grading standard id to set for the course.  If no value is provided for this argument the current grading_standard will be un-set from this course. This will update the grading_type for the course to &#x27;letter_grade&#x27; unless it is already &#x27;gpa_scale&#x27;. | [optional] value must be a 64 bit integer
**assignment[grading_type]** | str,  | str,  | The strategy used for grading the assignment. The assignment defaults to \&quot;points\&quot; if this field is omitted. | [optional] must be one of ["pass_fail", "percent", "letter_grade", "gpa_scale", "points", ] 
**assignment[group_category_id]** | decimal.Decimal, int,  | decimal.Decimal,  | If present, the assignment will become a group assignment assigned to the group. | [optional] value must be a 64 bit integer
**assignment[integration_data]** | str,  | str,  | Data used for SIS integrations. Requires admin-level token with the \&quot;Manage SIS\&quot; permission. JSON string required. | [optional] 
**assignment[integration_id]** | str,  | str,  | Unique ID from third party integrations | [optional] 
**assignment[lock_at]** | str, datetime,  | str,  | The day/time the assignment is locked after. Must be after the due date if there is a due date. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. | [optional] value must conform to RFC-3339 date-time
**assignment[moderated_grading]** | bool,  | BoolClass,  | Whether this assignment is moderated. | [optional] 
**assignment[muted]** | bool,  | BoolClass,  | Whether this assignment is muted. A muted assignment does not send change notifications and hides grades from students. Defaults to false. | [optional] 
**assignment[notify_of_update]** | bool,  | BoolClass,  | If true, Canvas will send a notification to students in the class notifying them that the content has changed. | [optional] 
**assignment[omit_from_final_grade]** | bool,  | BoolClass,  | Whether this assignment is counted towards a student&#x27;s final grade. | [optional] 
**assignment[only_visible_to_overrides]** | bool,  | BoolClass,  | Whether this assignment is only visible to overrides (Only useful if &#x27;differentiated assignments&#x27; account setting is on) | [optional] 
**assignment[peer_reviews]** | bool,  | BoolClass,  | If submission_types does not include external_tool,discussion_topic, online_quiz, or on_paper, determines whether or not peer reviews will be turned on for the assignment. | [optional] 
**assignment[points_possible]** | decimal.Decimal, int, float,  | decimal.Decimal,  | The maximum points possible on the assignment. | [optional] value must be a 32 bit float
**assignment[position]** | decimal.Decimal, int,  | decimal.Decimal,  | The position of this assignment in the group when displaying assignment lists. | [optional] value must be a 64 bit integer
**assignment[published]** | bool,  | BoolClass,  | Whether this assignment is published. (Only useful if &#x27;draft state&#x27; account setting is on) Unpublished assignments are not visible to students. | [optional] 
**assignment[quiz_lti]** | bool,  | BoolClass,  | Whether this assignment should use the Quizzes 2 LTI tool. Sets the submission type to &#x27;external_tool&#x27; and configures the external tool attributes to use the Quizzes 2 LTI tool configured for this course. Has no effect if no Quizzes 2 LTI tool is configured. | [optional] 
**[assignment[submission_types]](#assignment[submission_types])** | list, tuple,  | tuple,  |  | [optional] 
**assignment[turnitin_enabled]** | bool,  | BoolClass,  | Only applies when the Turnitin plugin is enabled for a course and the submission_types array includes \&quot;online_upload\&quot;. Toggles Turnitin submissions for the assignment. Will be ignored if Turnitin is not available for the course. | [optional] 
**assignment[turnitin_settings]** | str,  | str,  | Settings to send along to turnitin. See Assignment object definition for format. | [optional] 
**assignment[unlock_at]** | str, datetime,  | str,  | The day/time the assignment is unlocked. Must be before the due date if there is a due date. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. | [optional] value must conform to RFC-3339 date-time
**assignment[vericite_enabled]** | bool,  | BoolClass,  | Only applies when the VeriCite plugin is enabled for a course and the submission_types array includes \&quot;online_upload\&quot;. Toggles VeriCite submissions for the assignment. Will be ignored if VeriCite is not available for the course. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# assignment[allowed_extensions]

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# assignment[assignment_overrides]

List of overrides for the assignment.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | List of overrides for the assignment. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) |  | 

# assignment[submission_types]

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#create_assignment.ApiResponseFor200) | No response was specified

#### create_assignment.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**Assignment**](../../models/Assignment.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **create_assignment_override**
<a name="create_assignment_override"></a>
> AssignmentOverride create_assignment_override(course_idassignment_id)

Create an assignment override

One of student_ids, group_id, or course_section_id must be present. At most one should be present; if multiple are present only the most specific (student_ids first, then group_id, then course_section_id) is used and any others are ignored.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment_override import AssignmentOverride
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    try:
        # Create an assignment override
        api_response = api_instance.create_assignment_override(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->create_assignment_override: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    body = None
    try:
        # Create an assignment override
        api_response = api_instance.create_assignment_override(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->create_assignment_override: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**assignment_override[course_section_id]** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the override&#x27;s target section. If present, must identify an active section of the assignment&#x27;s course not already targetted by a different override. | [optional] value must be a 64 bit integer
**assignment_override[due_at]** | str, datetime,  | str,  | The day/time the overridden assignment is due. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. If absent, this override will not affect due date. May be present but null to indicate the override removes any previous due date. | [optional] value must conform to RFC-3339 date-time
**assignment_override[group_id]** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the override&#x27;s target group. If present, the following conditions must be met for the override to be successful:  1. the assignment MUST be a group assignment (a group_category_id is assigned to it) 2. the ID must identify an active group in the group set the assignment is in 3. the ID must not be targetted by a different override  See {Appendix: Group assignments} for more info. | [optional] value must be a 64 bit integer
**assignment_override[lock_at]** | str, datetime,  | str,  | The day/time the overridden assignment becomes locked. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. If absent, this override will not affect the lock date. May be present but null to indicate the override removes any previous lock date. | [optional] value must conform to RFC-3339 date-time
**[assignment_override[student_ids]](#assignment_override[student_ids])** | list, tuple,  | tuple,  | The IDs of the override&#x27;s target students. If present, the IDs must each identify a user with an active student enrollment in the course that is not already targetted by a different adhoc override. | [optional] 
**assignment_override[title]** | str,  | str,  | The title of the adhoc assignment override. Required if student_ids is present, ignored otherwise (the title is set to the name of the targetted group or section instead). | [optional] 
**assignment_override[unlock_at]** | str, datetime,  | str,  | The day/time the overridden assignment becomes unlocked. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. If absent, this override will not affect the unlock date. May be present but null to indicate the override removes any previous unlock date. | [optional] value must conform to RFC-3339 date-time
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# assignment_override[student_ids]

The IDs of the override's target students. If present, the IDs must each identify a user with an active student enrollment in the course that is not already targetted by a different adhoc override.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | The IDs of the override&#x27;s target students. If present, the IDs must each identify a user with an active student enrollment in the course that is not already targetted by a different adhoc override. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#create_assignment_override.ApiResponseFor200) | No response was specified

#### create_assignment_override.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**AssignmentOverride**](../../models/AssignmentOverride.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **delete_assignment**
<a name="delete_assignment"></a>
> Assignment delete_assignment(course_idid)

Delete an assignment

Delete the given assignment.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment import Assignment
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'id': "id_example",
    }
    try:
        # Delete an assignment
        api_response = api_instance.delete_assignment(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->delete_assignment: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
id | IdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#delete_assignment.ApiResponseFor200) | No response was specified

#### delete_assignment.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**Assignment**](../../models/Assignment.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **delete_assignment_override**
<a name="delete_assignment_override"></a>
> AssignmentOverride delete_assignment_override(course_idassignment_idid)

Delete an assignment override

Deletes an override and returns its former details.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment_override import AssignmentOverride
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
        'id': "id_example",
    }
    try:
        # Delete an assignment override
        api_response = api_instance.delete_assignment_override(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->delete_assignment_override: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 
id | IdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#delete_assignment_override.ApiResponseFor200) | No response was specified

#### delete_assignment_override.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**AssignmentOverride**](../../models/AssignmentOverride.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **edit_assignment**
<a name="edit_assignment"></a>
> Assignment edit_assignment(course_idid)

Edit an assignment

Modify an existing assignment.  If the assignment [assignment_overrides] key is absent, any existing overrides are kept as is. If the assignment [assignment_overrides] key is present, existing overrides are updated or deleted (and new ones created, as necessary) to match the provided list.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment_override import AssignmentOverride
from openapi_client.model.assignment import Assignment
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'id': "id_example",
    }
    try:
        # Edit an assignment
        api_response = api_instance.edit_assignment(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->edit_assignment: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'id': "id_example",
    }
    body = dict(
        assignment_allowed_extensions=[
            "assignment_allowed_extensions_example"
        ],
        assignment_assignment_group_id=1,
        assignment_assignment_overrides=[
            AssignmentOverride(
                all_day=1,
                all_day_date="1970-01-01T00:00:00.00Z",
                assignment_id=123,
                course_section_id=1,
                due_at="1970-01-01T00:00:00.00Z",
                group_id=2,
                id=4,
                lock_at="1970-01-01T00:00:00.00Z",
                student_ids=[1,2,3],
                title="an assignment override",
                unlock_at="1970-01-01T00:00:00.00Z",
            )
        ],
        assignment_automatic_peer_reviews=True,
        assignment_description="assignment_description_example",
        assignment_due_at="1970-01-01T00:00:00.00Z",
        assignment_external_tool_tag_attributes="assignment_external_tool_tag_attributes_example",
        assignment_grade_group_students_individually=1,
        assignment_grading_standard_id=1,
        assignment_grading_type="pass_fail",
        assignment_group_category_id=1,
        assignment_integration_data="assignment_integration_data_example",
        assignment_integration_id="assignment_integration_id_example",
        assignment_lock_at="1970-01-01T00:00:00.00Z",
        assignment_moderated_grading=True,
        assignment_muted=True,
        assignment_name="assignment_name_example",
        assignment_notify_of_update=True,
        assignment_omit_from_final_grade=True,
        assignment_only_visible_to_overrides=True,
        assignment_peer_reviews=True,
        assignment_points_possible=3.14,
        assignment_position=1,
        assignment_published=True,
        assignment_submission_types=[
            "online_quiz"
        ],
        assignment_turnitin_enabled=True,
        assignment_turnitin_settings="assignment_turnitin_settings_example",
        assignment_unlock_at="1970-01-01T00:00:00.00Z",
        assignment_vericite_enabled=True,
    )
    try:
        # Edit an assignment
        api_response = api_instance.edit_assignment(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->edit_assignment: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**[assignment[allowed_extensions]](#assignment[allowed_extensions])** | list, tuple,  | tuple,  | Allowed extensions if submission_types includes \&quot;online_upload\&quot;  Example:   allowed_extensions: [\&quot;docx\&quot;,\&quot;ppt\&quot;] | [optional] 
**assignment[assignment_group_id]** | decimal.Decimal, int,  | decimal.Decimal,  | The assignment group id to put the assignment in. Defaults to the top assignment group in the course. | [optional] value must be a 64 bit integer
**[assignment[assignment_overrides]](#assignment[assignment_overrides])** | list, tuple,  | tuple,  | List of overrides for the assignment. | [optional] 
**assignment[automatic_peer_reviews]** | bool,  | BoolClass,  | Whether peer reviews will be assigned automatically by Canvas or if teachers must manually assign peer reviews. Does not apply if peer reviews are not enabled. | [optional] 
**assignment[description]** | str,  | str,  | The assignment&#x27;s description, supports HTML. | [optional] 
**assignment[due_at]** | str, datetime,  | str,  | The day/time the assignment is due. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. | [optional] value must conform to RFC-3339 date-time
**assignment[external_tool_tag_attributes]** | str,  | str,  | Hash of external tool parameters if submission_types is [\&quot;external_tool\&quot;]. See Assignment object definition for format. | [optional] 
**assignment[grade_group_students_individually]** | decimal.Decimal, int,  | decimal.Decimal,  | If this is a group assignment, teachers have the options to grade students individually. If false, Canvas will apply the assignment&#x27;s score to each member of the group. If true, the teacher can manually assign scores to each member of the group. | [optional] value must be a 64 bit integer
**assignment[grading_standard_id]** | decimal.Decimal, int,  | decimal.Decimal,  | The grading standard id to set for the course.  If no value is provided for this argument the current grading_standard will be un-set from this course. This will update the grading_type for the course to &#x27;letter_grade&#x27; unless it is already &#x27;gpa_scale&#x27;. | [optional] value must be a 64 bit integer
**assignment[grading_type]** | str,  | str,  | The strategy used for grading the assignment. The assignment defaults to \&quot;points\&quot; if this field is omitted. | [optional] must be one of ["pass_fail", "percent", "letter_grade", "gpa_scale", "points", ] 
**assignment[group_category_id]** | decimal.Decimal, int,  | decimal.Decimal,  | If present, the assignment will become a group assignment assigned to the group. | [optional] value must be a 64 bit integer
**assignment[integration_data]** | str,  | str,  | Data used for SIS integrations. Requires admin-level token with the \&quot;Manage SIS\&quot; permission. JSON string required. | [optional] 
**assignment[integration_id]** | str,  | str,  | Unique ID from third party integrations | [optional] 
**assignment[lock_at]** | str, datetime,  | str,  | The day/time the assignment is locked after. Must be after the due date if there is a due date. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. | [optional] value must conform to RFC-3339 date-time
**assignment[moderated_grading]** | bool,  | BoolClass,  | Whether this assignment is moderated. | [optional] 
**assignment[muted]** | bool,  | BoolClass,  | Whether this assignment is muted. A muted assignment does not send change notifications and hides grades from students. Defaults to false. | [optional] 
**assignment[name]** | str,  | str,  | The assignment name. | [optional] 
**assignment[notify_of_update]** | bool,  | BoolClass,  | If true, Canvas will send a notification to students in the class notifying them that the content has changed. | [optional] 
**assignment[omit_from_final_grade]** | bool,  | BoolClass,  | Whether this assignment is counted towards a student&#x27;s final grade. | [optional] 
**assignment[only_visible_to_overrides]** | bool,  | BoolClass,  | Whether this assignment is only visible to overrides (Only useful if &#x27;differentiated assignments&#x27; account setting is on) | [optional] 
**assignment[peer_reviews]** | bool,  | BoolClass,  | If submission_types does not include external_tool,discussion_topic, online_quiz, or on_paper, determines whether or not peer reviews will be turned on for the assignment. | [optional] 
**assignment[points_possible]** | decimal.Decimal, int, float,  | decimal.Decimal,  | The maximum points possible on the assignment. | [optional] value must be a 32 bit float
**assignment[position]** | decimal.Decimal, int,  | decimal.Decimal,  | The position of this assignment in the group when displaying assignment lists. | [optional] value must be a 64 bit integer
**assignment[published]** | bool,  | BoolClass,  | Whether this assignment is published. (Only useful if &#x27;draft state&#x27; account setting is on) Unpublished assignments are not visible to students. | [optional] 
**[assignment[submission_types]](#assignment[submission_types])** | list, tuple,  | tuple,  | List of supported submission types for the assignment. Unless the assignment is allowing online submissions, the array should only have one element.  If not allowing online submissions, your options are:   \&quot;online_quiz\&quot;   \&quot;none\&quot;   \&quot;on_paper\&quot;   \&quot;discussion_topic\&quot;   \&quot;external_tool\&quot;  If you are allowing online submissions, you can have one or many allowed submission types:    \&quot;online_upload\&quot;   \&quot;online_text_entry\&quot;   \&quot;online_url\&quot;   \&quot;media_recording\&quot; (Only valid when the Kaltura plugin is enabled) | [optional] 
**assignment[turnitin_enabled]** | bool,  | BoolClass,  | Only applies when the Turnitin plugin is enabled for a course and the submission_types array includes \&quot;online_upload\&quot;. Toggles Turnitin submissions for the assignment. Will be ignored if Turnitin is not available for the course. | [optional] 
**assignment[turnitin_settings]** | str,  | str,  | Settings to send along to turnitin. See Assignment object definition for format. | [optional] 
**assignment[unlock_at]** | str, datetime,  | str,  | The day/time the assignment is unlocked. Must be before the due date if there is a due date. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. | [optional] value must conform to RFC-3339 date-time
**assignment[vericite_enabled]** | bool,  | BoolClass,  | Only applies when the VeriCite plugin is enabled for a course and the submission_types array includes \&quot;online_upload\&quot;. Toggles VeriCite submissions for the assignment. Will be ignored if VeriCite is not available for the course. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# assignment[allowed_extensions]

Allowed extensions if submission_types includes \"online_upload\"  Example:   allowed_extensions: [\"docx\",\"ppt\"]

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | Allowed extensions if submission_types includes \&quot;online_upload\&quot;  Example:   allowed_extensions: [\&quot;docx\&quot;,\&quot;ppt\&quot;] | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# assignment[assignment_overrides]

List of overrides for the assignment.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | List of overrides for the assignment. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) |  | 

# assignment[submission_types]

List of supported submission types for the assignment. Unless the assignment is allowing online submissions, the array should only have one element.  If not allowing online submissions, your options are:   \"online_quiz\"   \"none\"   \"on_paper\"   \"discussion_topic\"   \"external_tool\"  If you are allowing online submissions, you can have one or many allowed submission types:    \"online_upload\"   \"online_text_entry\"   \"online_url\"   \"media_recording\" (Only valid when the Kaltura plugin is enabled)

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | List of supported submission types for the assignment. Unless the assignment is allowing online submissions, the array should only have one element.  If not allowing online submissions, your options are:   \&quot;online_quiz\&quot;   \&quot;none\&quot;   \&quot;on_paper\&quot;   \&quot;discussion_topic\&quot;   \&quot;external_tool\&quot;  If you are allowing online submissions, you can have one or many allowed submission types:    \&quot;online_upload\&quot;   \&quot;online_text_entry\&quot;   \&quot;online_url\&quot;   \&quot;media_recording\&quot; (Only valid when the Kaltura plugin is enabled) | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
id | IdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#edit_assignment.ApiResponseFor200) | No response was specified

#### edit_assignment.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**Assignment**](../../models/Assignment.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **get_single_assignment**
<a name="get_single_assignment"></a>
> Assignment get_single_assignment(course_idid)

Get a single assignment

Returns the assignment with the given id.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment import Assignment
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'id': "id_example",
    }
    query_params = {
    }
    try:
        # Get a single assignment
        api_response = api_instance.get_single_assignment(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->get_single_assignment: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'id': "id_example",
    }
    query_params = {
        'include': [
        "submission"
    ],
        'override_assignment_dates': True,
        'needs_grading_count_by_section': True,
        'all_dates': True,
    }
    try:
        # Get a single assignment
        api_response = api_instance.get_single_assignment(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->get_single_assignment: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
include | IncludeSchema | | optional
override_assignment_dates | OverrideAssignmentDatesSchema | | optional
needs_grading_count_by_section | NeedsGradingCountBySectionSchema | | optional
all_dates | AllDatesSchema | | optional


# IncludeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# OverrideAssignmentDatesSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# NeedsGradingCountBySectionSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# AllDatesSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
id | IdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_single_assignment.ApiResponseFor200) | No response was specified

#### get_single_assignment.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**Assignment**](../../models/Assignment.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **get_single_assignment_override**
<a name="get_single_assignment_override"></a>
> AssignmentOverride get_single_assignment_override(course_idassignment_idid)

Get a single assignment override

Returns details of the the override with the given id.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment_override import AssignmentOverride
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
        'id': "id_example",
    }
    try:
        # Get a single assignment override
        api_response = api_instance.get_single_assignment_override(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->get_single_assignment_override: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 
id | IdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_single_assignment_override.ApiResponseFor200) | No response was specified

#### get_single_assignment_override.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**AssignmentOverride**](../../models/AssignmentOverride.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_assignment_overrides**
<a name="list_assignment_overrides"></a>
> [AssignmentOverride] list_assignment_overrides(course_idassignment_id)

List assignment overrides

Returns the paginated list of overrides for this assignment that target sections/groups/students visible to the current user.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment_override import AssignmentOverride
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
    }
    try:
        # List assignment overrides
        api_response = api_instance.list_assignment_overrides(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->list_assignment_overrides: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_assignment_overrides.ApiResponseFor200) | No response was specified

#### list_assignment_overrides.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) | [**AssignmentOverride**]({{complexTypePrefix}}AssignmentOverride.md) |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_assignments**
<a name="list_assignments"></a>
> [Assignment] list_assignments(course_id)

List assignments

Returns the paginated list of assignments for the current context.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment import Assignment
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
    }
    try:
        # List assignments
        api_response = api_instance.list_assignments(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->list_assignments: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
        'include': [
        "submission"
    ],
        'search_term': "search_term_example",
        'override_assignment_dates': True,
        'needs_grading_count_by_section': True,
        'bucket': "past",
        'assignment_ids': [
        "assignment_ids_example"
    ],
        'order_by': "position",
    }
    try:
        # List assignments
        api_response = api_instance.list_assignments(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->list_assignments: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
include | IncludeSchema | | optional
search_term | SearchTermSchema | | optional
override_assignment_dates | OverrideAssignmentDatesSchema | | optional
needs_grading_count_by_section | NeedsGradingCountBySectionSchema | | optional
bucket | BucketSchema | | optional
assignment_ids | AssignmentIdsSchema | | optional
order_by | OrderBySchema | | optional


# IncludeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# SearchTermSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# OverrideAssignmentDatesSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# NeedsGradingCountBySectionSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# BucketSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["past", "overdue", "undated", "ungraded", "unsubmitted", "upcoming", "future", ] 

# AssignmentIdsSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# OrderBySchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["position", "name", ] 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_assignments.ApiResponseFor200) | No response was specified

#### list_assignments.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**Assignment**]({{complexTypePrefix}}Assignment.md) | [**Assignment**]({{complexTypePrefix}}Assignment.md) | [**Assignment**]({{complexTypePrefix}}Assignment.md) |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_assignments_for_user**
<a name="list_assignments_for_user"></a>
> list_assignments_for_user(user_idcourse_id)

List assignments for user

Returns the paginated list of assignments for the specified user if the current user has rights to view. See {api:AssignmentsApiController#index List assignments} for valid arguments.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'user_id': "user_id_example",
        'course_id': "course_id_example",
    }
    try:
        # List assignments for user
        api_response = api_instance.list_assignments_for_user(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->list_assignments_for_user: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
user_id | UserIdSchema | | 
course_id | CourseIdSchema | | 

# UserIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_assignments_for_user.ApiResponseFor200) | No response was specified

#### list_assignments_for_user.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **redirect_to_assignment_override_for_group**
<a name="redirect_to_assignment_override_for_group"></a>
> redirect_to_assignment_override_for_group(group_idassignment_id)

Redirect to the assignment override for a group

Responds with a redirect to the override for the given group, if any (404 otherwise).

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'group_id': "group_id_example",
        'assignment_id': "assignment_id_example",
    }
    try:
        # Redirect to the assignment override for a group
        api_response = api_instance.redirect_to_assignment_override_for_group(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->redirect_to_assignment_override_for_group: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
group_id | GroupIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# GroupIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#redirect_to_assignment_override_for_group.ApiResponseFor200) | No response was specified

#### redirect_to_assignment_override_for_group.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **redirect_to_assignment_override_for_section**
<a name="redirect_to_assignment_override_for_section"></a>
> redirect_to_assignment_override_for_section(course_section_idassignment_id)

Redirect to the assignment override for a section

Responds with a redirect to the override for the given section, if any (404 otherwise).

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_section_id': "course_section_id_example",
        'assignment_id': "assignment_id_example",
    }
    try:
        # Redirect to the assignment override for a section
        api_response = api_instance.redirect_to_assignment_override_for_section(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->redirect_to_assignment_override_for_section: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_section_id | CourseSectionIdSchema | | 
assignment_id | AssignmentIdSchema | | 

# CourseSectionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#redirect_to_assignment_override_for_section.ApiResponseFor200) | No response was specified

#### redirect_to_assignment_override_for_section.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **update_assignment_override**
<a name="update_assignment_override"></a>
> AssignmentOverride update_assignment_override(course_idassignment_idid)

Update an assignment override

All current overridden values must be supplied if they are to be retained; e.g. if due_at was overridden, but this PUT omits a value for due_at, due_at will no longer be overridden. If the override is adhoc and student_ids is not supplied, the target override set is unchanged. Target override sets cannot be changed for group or section overrides.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import assignments_api
from openapi_client.model.assignment_override import AssignmentOverride
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = assignments_api.AssignmentsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
        'id': "id_example",
    }
    try:
        # Update an assignment override
        api_response = api_instance.update_assignment_override(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->update_assignment_override: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'assignment_id': "assignment_id_example",
        'id': "id_example",
    }
    body = dict(
        assignment_override_due_at="1970-01-01T00:00:00.00Z",
        assignment_override_lock_at="1970-01-01T00:00:00.00Z",
        assignment_override_student_ids=[
            1
        ],
        assignment_override_title="assignment_override_title_example",
        assignment_override_unlock_at="1970-01-01T00:00:00.00Z",
    )
    try:
        # Update an assignment override
        api_response = api_instance.update_assignment_override(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AssignmentsApi->update_assignment_override: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**assignment_override[due_at]** | str, datetime,  | str,  | The day/time the overridden assignment is due. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. If absent, this override will not affect due date. May be present but null to indicate the override removes any previous due date. | [optional] value must conform to RFC-3339 date-time
**assignment_override[lock_at]** | str, datetime,  | str,  | The day/time the overridden assignment becomes locked. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. If absent, this override will not affect the lock date. May be present but null to indicate the override removes any previous lock date. | [optional] value must conform to RFC-3339 date-time
**[assignment_override[student_ids]](#assignment_override[student_ids])** | list, tuple,  | tuple,  | The IDs of the override&#x27;s target students. If present, the IDs must each identify a user with an active student enrollment in the course that is not already targetted by a different adhoc override. Ignored unless the override being updated is adhoc. | [optional] 
**assignment_override[title]** | str,  | str,  | The title of an adhoc assignment override. Ignored unless the override being updated is adhoc. | [optional] 
**assignment_override[unlock_at]** | str, datetime,  | str,  | The day/time the overridden assignment becomes unlocked. Accepts times in ISO 8601 format, e.g. 2014-10-21T18:48:00Z. If absent, this override will not affect the unlock date. May be present but null to indicate the override removes any previous unlock date. | [optional] value must conform to RFC-3339 date-time
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

# assignment_override[student_ids]

The IDs of the override's target students. If present, the IDs must each identify a user with an active student enrollment in the course that is not already targetted by a different adhoc override. Ignored unless the override being updated is adhoc.

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  | The IDs of the override&#x27;s target students. If present, the IDs must each identify a user with an active student enrollment in the course that is not already targetted by a different adhoc override. Ignored unless the override being updated is adhoc. | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
assignment_id | AssignmentIdSchema | | 
id | IdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#update_assignment_override.ApiResponseFor200) | No response was specified

#### update_assignment_override.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**AssignmentOverride**](../../models/AssignmentOverride.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

