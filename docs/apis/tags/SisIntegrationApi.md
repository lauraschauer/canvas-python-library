<a name="__pageTop"></a>
# openapi_client.apis.tags.sis_integration_api.SisIntegrationApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**disable_assignments_currently_enabled_for_grade_export_to_sis**](#disable_assignments_currently_enabled_for_grade_export_to_sis) | **put** /sis/courses/{course_id}/disable_post_to_sis | Disable assignments currently enabled for grade export to SIS
[**retrieve_assignments_enabled_for_grade_export_to_sis_accounts**](#retrieve_assignments_enabled_for_grade_export_to_sis_accounts) | **get** /sis/accounts/{account_id}/assignments | Retrieve assignments enabled for grade export to SIS
[**retrieve_assignments_enabled_for_grade_export_to_sis_courses**](#retrieve_assignments_enabled_for_grade_export_to_sis_courses) | **get** /sis/courses/{course_id}/assignments | Retrieve assignments enabled for grade export to SIS

# **disable_assignments_currently_enabled_for_grade_export_to_sis**
<a name="disable_assignments_currently_enabled_for_grade_export_to_sis"></a>
> disable_assignments_currently_enabled_for_grade_export_to_sis(course_id)

Disable assignments currently enabled for grade export to SIS

Disable all assignments flagged as \"post_to_sis\", with the option of making it specific to a grading period, in a course.  On success, the response will be 204 No Content with an empty body.  On failure, the response will be 400 Bad Request with a body of a specific message.  For disabling assignments in a specific grading period

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import sis_integration_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sis_integration_api.SisIntegrationApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': 1,
    }
    try:
        # Disable assignments currently enabled for grade export to SIS
        api_response = api_instance.disable_assignments_currently_enabled_for_grade_export_to_sis(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SisIntegrationApi->disable_assignments_currently_enabled_for_grade_export_to_sis: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': 1,
    }
    body = dict(
        grading_period_id=1,
    )
    try:
        # Disable assignments currently enabled for grade export to SIS
        api_response = api_instance.disable_assignments_currently_enabled_for_grade_export_to_sis(
            path_params=path_params,
            body=body,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SisIntegrationApi->disable_assignments_currently_enabled_for_grade_export_to_sis: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**grading_period_id** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the grading period. | [optional] value must be a 64 bit integer
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
decimal.Decimal, int,  | decimal.Decimal,  |  | value must be a 64 bit integer

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#disable_assignments_currently_enabled_for_grade_export_to_sis.ApiResponseFor200) | No response was specified

#### disable_assignments_currently_enabled_for_grade_export_to_sis.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **retrieve_assignments_enabled_for_grade_export_to_sis_accounts**
<a name="retrieve_assignments_enabled_for_grade_export_to_sis_accounts"></a>
> retrieve_assignments_enabled_for_grade_export_to_sis_accounts(account_id)

Retrieve assignments enabled for grade export to SIS

Retrieve a list of published assignments flagged as \"post_to_sis\". See the Assignments API for more details on assignments. Assignment group and section information are included for convenience.  Each section includes course information for the origin course and the cross-listed course, if applicable. The `origin_course` is the course to which the section belongs or the course from which the section was cross-listed. Generally, the `origin_course` should be preferred when performing integration work. The `xlist_course` is provided for consistency and is only present when the section has been cross-listed. See Sections API and Courses Api for me details.  The `override` is only provided if the Differentiated Assignments course feature is turned on and the assignment has an override for that section. When there is an override for the assignment the override object's keys/values can be merged with the top level assignment object to create a view of the assignment object specific to that section. See Assignments api for more information on assignment overrides.  restricts to courses that start before this date (if they have a start date) restricts to courses that end after this date (if they have an end date) information to include.    \"student_overrides\":: returns individual student override information

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import sis_integration_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sis_integration_api.SisIntegrationApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': 1,
    }
    query_params = {
    }
    try:
        # Retrieve assignments enabled for grade export to SIS
        api_response = api_instance.retrieve_assignments_enabled_for_grade_export_to_sis_accounts(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SisIntegrationApi->retrieve_assignments_enabled_for_grade_export_to_sis_accounts: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': 1,
    }
    query_params = {
        'course_id': 1,
        'starts_before': "1970-01-01T00:00:00.00Z",
        'ends_after': "1970-01-01T00:00:00.00Z",
        'include': "student_overrides",
    }
    try:
        # Retrieve assignments enabled for grade export to SIS
        api_response = api_instance.retrieve_assignments_enabled_for_grade_export_to_sis_accounts(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SisIntegrationApi->retrieve_assignments_enabled_for_grade_export_to_sis_accounts: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | optional
starts_before | StartsBeforeSchema | | optional
ends_after | EndsAfterSchema | | optional
include | IncludeSchema | | optional


# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
decimal.Decimal, int,  | decimal.Decimal,  |  | value must be a 64 bit integer

# StartsBeforeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str, datetime,  | str,  |  | value must conform to RFC-3339 date-time

# EndsAfterSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str, datetime,  | str,  |  | value must conform to RFC-3339 date-time

# IncludeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["student_overrides", ] 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
decimal.Decimal, int,  | decimal.Decimal,  |  | value must be a 64 bit integer

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#retrieve_assignments_enabled_for_grade_export_to_sis_accounts.ApiResponseFor200) | No response was specified

#### retrieve_assignments_enabled_for_grade_export_to_sis_accounts.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **retrieve_assignments_enabled_for_grade_export_to_sis_courses**
<a name="retrieve_assignments_enabled_for_grade_export_to_sis_courses"></a>
> retrieve_assignments_enabled_for_grade_export_to_sis_courses(course_id)

Retrieve assignments enabled for grade export to SIS

Retrieve a list of published assignments flagged as \"post_to_sis\". See the Assignments API for more details on assignments. Assignment group and section information are included for convenience.  Each section includes course information for the origin course and the cross-listed course, if applicable. The `origin_course` is the course to which the section belongs or the course from which the section was cross-listed. Generally, the `origin_course` should be preferred when performing integration work. The `xlist_course` is provided for consistency and is only present when the section has been cross-listed. See Sections API and Courses Api for me details.  The `override` is only provided if the Differentiated Assignments course feature is turned on and the assignment has an override for that section. When there is an override for the assignment the override object's keys/values can be merged with the top level assignment object to create a view of the assignment object specific to that section. See Assignments api for more information on assignment overrides.  restricts to courses that start before this date (if they have a start date) restricts to courses that end after this date (if they have an end date) information to include.    \"student_overrides\":: returns individual student override information

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import sis_integration_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sis_integration_api.SisIntegrationApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': 1,
    }
    query_params = {
    }
    try:
        # Retrieve assignments enabled for grade export to SIS
        api_response = api_instance.retrieve_assignments_enabled_for_grade_export_to_sis_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SisIntegrationApi->retrieve_assignments_enabled_for_grade_export_to_sis_courses: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': 1,
    }
    query_params = {
        'account_id': 1,
        'starts_before': "1970-01-01T00:00:00.00Z",
        'ends_after': "1970-01-01T00:00:00.00Z",
        'include': "student_overrides",
    }
    try:
        # Retrieve assignments enabled for grade export to SIS
        api_response = api_instance.retrieve_assignments_enabled_for_grade_export_to_sis_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling SisIntegrationApi->retrieve_assignments_enabled_for_grade_export_to_sis_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | optional
starts_before | StartsBeforeSchema | | optional
ends_after | EndsAfterSchema | | optional
include | IncludeSchema | | optional


# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
decimal.Decimal, int,  | decimal.Decimal,  |  | value must be a 64 bit integer

# StartsBeforeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str, datetime,  | str,  |  | value must conform to RFC-3339 date-time

# EndsAfterSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str, datetime,  | str,  |  | value must conform to RFC-3339 date-time

# IncludeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["student_overrides", ] 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
decimal.Decimal, int,  | decimal.Decimal,  |  | value must be a 64 bit integer

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#retrieve_assignments_enabled_for_grade_export_to_sis_courses.ApiResponseFor200) | No response was specified

#### retrieve_assignments_enabled_for_grade_export_to_sis_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

