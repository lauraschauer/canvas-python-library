<a name="__pageTop"></a>
# openapi_client.apis.tags.authentication_providers_api.AuthenticationProvidersApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_authentication_provider**](#add_authentication_provider) | **post** /v1/accounts/{account_id}/authentication_providers | Add authentication provider
[**delete_authentication_provider**](#delete_authentication_provider) | **delete** /v1/accounts/{account_id}/authentication_providers/{id} | Delete authentication provider
[**get_authentication_provider**](#get_authentication_provider) | **get** /v1/accounts/{account_id}/authentication_providers/{id} | Get authentication provider
[**list_authentication_providers**](#list_authentication_providers) | **get** /v1/accounts/{account_id}/authentication_providers | List authentication providers
[**show_account_auth_settings**](#show_account_auth_settings) | **get** /v1/accounts/{account_id}/sso_settings | show account auth settings
[**update_account_auth_settings**](#update_account_auth_settings) | **put** /v1/accounts/{account_id}/sso_settings | update account auth settings
[**update_authentication_provider**](#update_authentication_provider) | **put** /v1/accounts/{account_id}/authentication_providers/{id} | Update authentication provider

# **add_authentication_provider**
<a name="add_authentication_provider"></a>
> AuthenticationProvider add_authentication_provider(account_id)

Add authentication provider

Add external authentication provider(s) for the account. Services may be CAS, Facebook, GitHub, Google, LDAP, LinkedIn, Microsoft, OpenID Connect, SAML, or Twitter.  Each authentication provider is specified as a set of parameters as described below. A provider specification must include an 'auth_type' parameter with a value of 'canvas', 'cas', 'clever', 'facebook', 'github', 'google', 'ldap', 'linkedin', 'microsoft', 'openid_connect', 'saml', or 'twitter'. The other recognized parameters depend on this auth_type; unrecognized parameters are discarded. Provider specifications not specifying a valid auth_type are ignored.  You can set the 'position' for any configuration. The config in the 1st position is considered the default. You can set 'jit_provisioning' for any configuration besides Canvas.  For Canvas, the additional recognized parameter is:  - self_registration    'all', 'none', or 'observer' - who is allowed to register as a new user  For CAS, the additional recognized parameters are:  - auth_base    The CAS server's URL.  - log_in_url [Optional]    An alternate SSO URL for logging into CAS. You probably should not set   this.  For Clever, the additional recognized parameters are:  - client_id [Required]    The Clever application's Client ID. Not available if configured globally   for Canvas.  - client_secret [Required]    The Clever application's Client Secret. Not available if configured   globally for Canvas.  - district_id [Optional]    A district's Clever ID. Leave this blank to let Clever handle the details   with its District Picker. This is required for Clever Instant Login to   work in a multi-tenant environment.  - login_attribute [Optional]    The attribute to use to look up the user's login in Canvas. Either   'id' (the default), 'sis_id', 'email', 'student_number', or   'teacher_number'. Note that some fields may not be populated for   all users at Clever.  - federated_attributes [Optional]    See FederatedAttributesConfig. Valid provider attributes are 'id',   'sis_id', 'email', 'student_number', and 'teacher_number'.  For Facebook, the additional recognized parameters are:  - app_id [Required]    The Facebook App ID. Not available if configured globally for Canvas.  - app_secret [Required]    The Facebook App Secret. Not available if configured globally for Canvas.  - login_attribute [Optional]    The attribute to use to look up the user's login in Canvas. Either   'id' (the default), or 'email'  - federated_attributes [Optional]    See FederatedAttributesConfig. Valid provider attributes are 'email',   'first_name', 'id', 'last_name', 'locale', and 'name'.  For GitHub, the additional recognized parameters are:  - domain [Optional]    The domain of a GitHub Enterprise installation. I.e.   github.mycompany.com. If not set, it will default to the public   github.com.  - client_id [Required]    The GitHub application's Client ID. Not available if configured globally   for Canvas.  - client_secret [Required]    The GitHub application's Client Secret. Not available if configured   globally for Canvas.  - login_attribute [Optional]    The attribute to use to look up the user's login in Canvas. Either   'id' (the default), or 'login'  - federated_attributes [Optional]    See FederatedAttributesConfig. Valid provider attributes are 'email',   'id', 'login', and 'name'.  For Google, the additional recognized parameters are:  - client_id [Required]    The Google application's Client ID. Not available if configured globally   for Canvas.  - client_secret [Required]    The Google application's Client Secret. Not available if configured   globally for Canvas.  - hosted_domain [Optional]    A Google Apps domain to restrict logins to. See   https://developers.google.com/identity/protocols/OpenIDConnect?hl=en#hd-param  - login_attribute [Optional]    The attribute to use to look up the user's login in Canvas. Either   'sub' (the default), or 'email'  - federated_attributes [Optional]    See FederatedAttributesConfig. Valid provider attributes are 'email',   'family_name', 'given_name', 'locale', 'name', and 'sub'.  For LDAP, the additional recognized parameters are:  - auth_host    The LDAP server's URL.  - auth_port [Optional, Integer]    The LDAP server's TCP port. (default: 389)  - auth_over_tls [Optional]    Whether to use TLS. Can be 'simple_tls', or 'start_tls'. For backwards   compatibility, booleans are also accepted, with true meaning simple_tls.   If not provided, it will default to start_tls.  - auth_base [Optional]    A default treebase parameter for searches performed against the LDAP   server.  - auth_filter    LDAP search filter. Use !{{login}} as a placeholder for the username   supplied by the user. For example: \"(sAMAccountName=!{{login}})\".  - identifier_format [Optional]    The LDAP attribute to use to look up the Canvas login. Omit to use   the username supplied by the user.  - auth_username    Username  - auth_password    Password  For LinkedIn, the additional recognized parameters are:  - client_id [Required]    The LinkedIn application's Client ID. Not available if configured globally   for Canvas.  - client_secret [Required]    The LinkedIn application's Client Secret. Not available if configured   globally for Canvas.  - login_attribute [Optional]    The attribute to use to look up the user's login in Canvas. Either   'id' (the default), or 'emailAddress'  - federated_attributes [Optional]    See FederatedAttributesConfig. Valid provider attributes are 'emailAddress',   'firstName', 'id', 'formattedName', and 'lastName'.  For Microsoft, the additional recognized parameters are:  - application_id [Required]    The application's ID.  - application_secret [Required]    The application's Client Secret (Password)  - tenant [Optional]    See https://azure.microsoft.com/en-us/documentation/articles/active-directory-v2-protocols/   Valid values are 'common', 'organizations', 'consumers', or an Azure Active Directory Tenant   (as either a UUID or domain, such as contoso.onmicrosoft.com). Defaults to 'common'  - login_attribute [Optional]    See https://azure.microsoft.com/en-us/documentation/articles/active-directory-v2-tokens/#idtokens   Valid values are 'sub', 'email', 'oid', or 'preferred_username'. Note   that email may not always be populated in the user's profile at   Microsoft. Oid will not be populated for personal Microsoft accounts.   Defaults to 'sub'  - federated_attributes [Optional]    See FederatedAttributesConfig. Valid provider attributes are 'email',   'name', 'preferred_username', 'oid', and 'sub'.  For OpenID Connect, the additional recognized parameters are:  - client_id [Required]    The application's Client ID.  - client_secret [Required]    The application's Client Secret.  - authorize_url [Required]    The URL for getting starting the OAuth 2.0 web flow  - token_url [Required]    The URL for exchanging the OAuth 2.0 authorization code for an Access   Token and ID Token  - scope [Optional]    Space separated additional scopes to request for the token. Note that   you need not specify the 'openid' scope, or any scopes that can be   automatically inferred by the rules defined at   http://openid.net/specs/openid-connect-core-1_0.html#ScopeClaims  - end_session_endpoint [Optional]    URL to send the end user to after logging out of Canvas. See   https://openid.net/specs/openid-connect-session-1_0.html#RPLogout  - userinfo_endpoint [Optional]    URL to request additional claims from. If the initial ID Token received   from the provider cannot be used to satisfy the login_attribute and   all federated_attributes, this endpoint will be queried for additional   information.  - login_attribute [Optional]    The attribute of the ID Token to look up the user's login in Canvas.   Defaults to 'sub'.  - federated_attributes [Optional]    See FederatedAttributesConfig. Any value is allowed for the provider   attribute names, but standard claims are listed at   http://openid.net/specs/openid-connect-core-1_0.html#StandardClaims  For SAML, the additional recognized parameters are:  - metadata [Optional]    An XML document to parse as SAML metadata, and automatically populate idp_entity_id,   log_in_url, log_out_url, certificate_fingerprint, and identifier_format  - metadata_uri [Optional]    A URI to download the SAML metadata from, and automatically populate idp_entity_id,   log_in_url, log_out_url, certificate_fingerprint, and identifier_format. This URI   will also be saved, and the metadata periodically refreshed, automatically. If   the metadata contains multiple entities, also supply idp_entity_id to distinguish   which one you want (otherwise the only entity in the metadata will be inferred).   If you provide the URI 'urn:mace:incommon' or 'http://ukfederation.org.uk',   the InCommon or UK Access Management Federation metadata aggregate, respectively,   will be used instead, and additional validation checks will happen (including   validating that the metadata has been properly signed with the   appropriate key).  - idp_entity_id    The SAML IdP's entity ID  - log_in_url    The SAML service's SSO target URL  - log_out_url [Optional]    The SAML service's SLO target URL  - certificate_fingerprint    The SAML service's certificate fingerprint.  - identifier_format    The SAML service's identifier format. Must be one of:    - urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress   - urn:oasis:names:tc:SAML:2.0:nameid-format:entity   - urn:oasis:names:tc:SAML:2.0:nameid-format:kerberos   - urn:oasis:names:tc:SAML:2.0:nameid-format:persistent   - urn:oasis:names:tc:SAML:2.0:nameid-format:transient   - urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified   - urn:oasis:names:tc:SAML:1.1:nameid-format:WindowsDomainQualifiedName   - urn:oasis:names:tc:SAML:1.1:nameid-format:X509SubjectName  - requested_authn_context [Optional]    The SAML AuthnContext  - sig_alg [Optional]    If set, +AuthnRequest+, +LogoutRequest+, and +LogoutResponse+ messages   are signed with the corresponding algorithm. Supported algorithms are:    - {http://www.w3.org/2000/09/xmldsig#rsa-sha1}   - {http://www.w3.org/2001/04/xmldsig-more#rsa-sha256}    RSA-SHA1 and RSA-SHA256 are acceptable aliases.  - federated_attributes [Optional]    See FederatedAttributesConfig. Any value is allowed for the provider attribute names.  For Twitter, the additional recognized parameters are:  - consumer_key [Required]    The Twitter Consumer Key. Not available if configured globally for Canvas.  - consumer_secret [Required]    The Twitter Consumer Secret. Not available if configured globally for Canvas.  - login_attribute [Optional]    The attribute to use to look up the user's login in Canvas. Either   'user_id' (the default), or 'screen_name'  - parent_registration [Optional] - DEPRECATED 2017-11-03    Accepts a boolean value, true designates the authentication service   for use on parent registrations.  Only one service can be selected   at a time so if set to true all others will be set to false  - federated_attributes [Optional]    See FederatedAttributesConfig. Valid provider attributes are 'name',   'screen_name', 'time_zone', and 'user_id'.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import authentication_providers_api
from openapi_client.model.authentication_provider import AuthenticationProvider
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = authentication_providers_api.AuthenticationProvidersApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    try:
        # Add authentication provider
        api_response = api_instance.add_authentication_provider(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AuthenticationProvidersApi->add_authentication_provider: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#add_authentication_provider.ApiResponseFor200) | No response was specified

#### add_authentication_provider.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**AuthenticationProvider**](../../models/AuthenticationProvider.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **delete_authentication_provider**
<a name="delete_authentication_provider"></a>
> delete_authentication_provider(account_idid)

Delete authentication provider

Delete the config

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import authentication_providers_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = authentication_providers_api.AuthenticationProvidersApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'id': "id_example",
    }
    try:
        # Delete authentication provider
        api_response = api_instance.delete_authentication_provider(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling AuthenticationProvidersApi->delete_authentication_provider: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
id | IdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#delete_authentication_provider.ApiResponseFor200) | No response was specified

#### delete_authentication_provider.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **get_authentication_provider**
<a name="get_authentication_provider"></a>
> AuthenticationProvider get_authentication_provider(account_idid)

Get authentication provider

Get the specified authentication provider

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import authentication_providers_api
from openapi_client.model.authentication_provider import AuthenticationProvider
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = authentication_providers_api.AuthenticationProvidersApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'id': "id_example",
    }
    try:
        # Get authentication provider
        api_response = api_instance.get_authentication_provider(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AuthenticationProvidersApi->get_authentication_provider: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
id | IdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_authentication_provider.ApiResponseFor200) | No response was specified

#### get_authentication_provider.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**AuthenticationProvider**](../../models/AuthenticationProvider.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_authentication_providers**
<a name="list_authentication_providers"></a>
> [AuthenticationProvider] list_authentication_providers(account_id)

List authentication providers

Returns a paginated list of authentication providers

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import authentication_providers_api
from openapi_client.model.authentication_provider import AuthenticationProvider
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = authentication_providers_api.AuthenticationProvidersApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    try:
        # List authentication providers
        api_response = api_instance.list_authentication_providers(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AuthenticationProvidersApi->list_authentication_providers: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_authentication_providers.ApiResponseFor200) | No response was specified

#### list_authentication_providers.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**AuthenticationProvider**]({{complexTypePrefix}}AuthenticationProvider.md) | [**AuthenticationProvider**]({{complexTypePrefix}}AuthenticationProvider.md) | [**AuthenticationProvider**]({{complexTypePrefix}}AuthenticationProvider.md) |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **show_account_auth_settings**
<a name="show_account_auth_settings"></a>
> SSOSettings show_account_auth_settings(account_id)

show account auth settings

The way to get the current state of each account level setting that's relevant to Single Sign On configuration  You can list the current state of each setting with \"update_sso_settings\"

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import authentication_providers_api
from openapi_client.model.sso_settings import SSOSettings
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = authentication_providers_api.AuthenticationProvidersApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    try:
        # show account auth settings
        api_response = api_instance.show_account_auth_settings(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AuthenticationProvidersApi->show_account_auth_settings: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#show_account_auth_settings.ApiResponseFor200) | No response was specified

#### show_account_auth_settings.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**SSOSettings**](../../models/SSOSettings.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **update_account_auth_settings**
<a name="update_account_auth_settings"></a>
> SSOSettings update_account_auth_settings(account_id)

update account auth settings

For various cases of mixed SSO configurations, you may need to set some configuration at the account level to handle the particulars of your setup.  This endpoint accepts a PUT request to set several possible account settings. All setting are optional on each request, any that are not provided at all are simply retained as is.  Any that provide the key but a null-ish value (blank string, null, undefined) will be UN-set.  You can list the current state of each setting with \"show_sso_settings\"

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import authentication_providers_api
from openapi_client.model.sso_settings import SSOSettings
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = authentication_providers_api.AuthenticationProvidersApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    try:
        # update account auth settings
        api_response = api_instance.update_account_auth_settings(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AuthenticationProvidersApi->update_account_auth_settings: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#update_account_auth_settings.ApiResponseFor200) | No response was specified

#### update_account_auth_settings.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**SSOSettings**](../../models/SSOSettings.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **update_authentication_provider**
<a name="update_authentication_provider"></a>
> AuthenticationProvider update_authentication_provider(account_idid)

Update authentication provider

Update an authentication provider using the same options as the create endpoint. You can not update an existing provider to a new authentication type.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import authentication_providers_api
from openapi_client.model.authentication_provider import AuthenticationProvider
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = authentication_providers_api.AuthenticationProvidersApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'id': "id_example",
    }
    try:
        # Update authentication provider
        api_response = api_instance.update_authentication_provider(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling AuthenticationProvidersApi->update_authentication_provider: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
id | IdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#update_authentication_provider.ApiResponseFor200) | No response was specified

#### update_authentication_provider.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**AuthenticationProvider**](../../models/AuthenticationProvider.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

