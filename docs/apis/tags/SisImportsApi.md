<a name="__pageTop"></a>
# openapi_client.apis.tags.sis_imports_api.SisImportsApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**abort_all_pending_sis_imports**](#abort_all_pending_sis_imports) | **put** /v1/accounts/{account_id}/sis_imports/abort_all_pending | Abort all pending SIS imports
[**abort_sis_import**](#abort_sis_import) | **put** /v1/accounts/{account_id}/sis_imports/{id}/abort | Abort SIS import
[**get_sis_import_list**](#get_sis_import_list) | **get** /v1/accounts/{account_id}/sis_imports | Get SIS import list
[**get_sis_import_status**](#get_sis_import_status) | **get** /v1/accounts/{account_id}/sis_imports/{id} | Get SIS import status
[**import_sis_data**](#import_sis_data) | **post** /v1/accounts/{account_id}/sis_imports | Import SIS data
[**restore_workflow_states_of_sis_imported_items**](#restore_workflow_states_of_sis_imported_items) | **put** /v1/accounts/{account_id}/sis_imports/{id}/restore_states | Restore workflow_states of SIS imported items

# **abort_all_pending_sis_imports**
<a name="abort_all_pending_sis_imports"></a>
> bool abort_all_pending_sis_imports(account_id)

Abort all pending SIS imports

Abort already created but not processed or processing SIS imports.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import sis_imports_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sis_imports_api.SisImportsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    try:
        # Abort all pending SIS imports
        api_response = api_instance.abort_all_pending_sis_imports(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SisImportsApi->abort_all_pending_sis_imports: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#abort_all_pending_sis_imports.ApiResponseFor200) | No response was specified

#### abort_all_pending_sis_imports.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **abort_sis_import**
<a name="abort_sis_import"></a>
> SisImport abort_sis_import(account_idid)

Abort SIS import

Abort a SIS import that has not completed.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import sis_imports_api
from openapi_client.model.sis_import import SisImport
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sis_imports_api.SisImportsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'id': "id_example",
    }
    try:
        # Abort SIS import
        api_response = api_instance.abort_sis_import(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SisImportsApi->abort_sis_import: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
id | IdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#abort_sis_import.ApiResponseFor200) | No response was specified

#### abort_sis_import.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**SisImport**](../../models/SisImport.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **get_sis_import_list**
<a name="get_sis_import_list"></a>
> [SisImport] get_sis_import_list(account_id)

Get SIS import list

Returns the list of SIS imports for an account  Example:   curl https://<canvas>/api/v1/accounts/<account_id>/sis_imports \\     -H 'Authorization: Bearer <token>'

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import sis_imports_api
from openapi_client.model.sis_import import SisImport
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sis_imports_api.SisImportsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    query_params = {
    }
    try:
        # Get SIS import list
        api_response = api_instance.get_sis_import_list(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SisImportsApi->get_sis_import_list: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': "account_id_example",
    }
    query_params = {
        'created_since': "1970-01-01T00:00:00.00Z",
    }
    try:
        # Get SIS import list
        api_response = api_instance.get_sis_import_list(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SisImportsApi->get_sis_import_list: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
created_since | CreatedSinceSchema | | optional


# CreatedSinceSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str, datetime,  | str,  |  | value must conform to RFC-3339 date-time

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_sis_import_list.ApiResponseFor200) | No response was specified

#### get_sis_import_list.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**SisImport**]({{complexTypePrefix}}SisImport.md) | [**SisImport**]({{complexTypePrefix}}SisImport.md) | [**SisImport**]({{complexTypePrefix}}SisImport.md) |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **get_sis_import_status**
<a name="get_sis_import_status"></a>
> SisImport get_sis_import_status(account_idid)

Get SIS import status

Get the status of an already created SIS import.    Examples:     curl https://<canvas>/api/v1/accounts/<account_id>/sis_imports/<sis_import_id> \\         -H 'Authorization: Bearer <token>'

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import sis_imports_api
from openapi_client.model.sis_import import SisImport
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sis_imports_api.SisImportsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'id': "id_example",
    }
    try:
        # Get SIS import status
        api_response = api_instance.get_sis_import_status(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SisImportsApi->get_sis_import_status: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
id | IdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_sis_import_status.ApiResponseFor200) | No response was specified

#### get_sis_import_status.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**SisImport**](../../models/SisImport.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **import_sis_data**
<a name="import_sis_data"></a>
> SisImport import_sis_data(account_id)

Import SIS data

Import SIS data into Canvas. Must be on a root account with SIS imports enabled.  For more information on the format that's expected here, please see the \"SIS CSV\" section in the API docs.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import sis_imports_api
from openapi_client.model.sis_import import SisImport
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sis_imports_api.SisImportsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    try:
        # Import SIS data
        api_response = api_instance.import_sis_data(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SisImportsApi->import_sis_data: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': "account_id_example",
    }
    body = None
    try:
        # Import SIS data
        api_response = api_instance.import_sis_data(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SisImportsApi->import_sis_data: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**add_sis_stickiness** | bool,  | BoolClass,  | This option, if present, will process all changes as if they were UI changes. This means that \&quot;stickiness\&quot; will be added to changed fields. This option is only processed if &#x27;override_sis_stickiness&#x27; is also provided. | [optional] 
**attachment** | str,  | str,  | There are two ways to post SIS import data - either via a application/x-www-form-urlencoded form-field-style attachment, or via a non-multipart raw post request.  &#x27;attachment&#x27; is required for application/x-www-form-urlencoded style posts. Assumed to be SIS data from a file upload form field named &#x27;attachment&#x27;.  Examples:   curl -F attachment&#x3D;@&lt;filename&gt; -H \&quot;Authorization: Bearer &lt;token&gt;\&quot; \\       https://&lt;canvas&gt;/api/v1/accounts/&lt;account_id&gt;/sis_imports.json?import_type&#x3D;instructure_csv  If you decide to do a raw post, you can skip the &#x27;attachment&#x27; argument, but you will then be required to provide a suitable Content-Type header. You are encouraged to also provide the &#x27;extension&#x27; argument.  Examples:   curl -H &#x27;Content-Type: application/octet-stream&#x27; --data-binary @&lt;filename&gt;.zip \\       -H \&quot;Authorization: Bearer &lt;token&gt;\&quot; \\       https://&lt;canvas&gt;/api/v1/accounts/&lt;account_id&gt;/sis_imports.json?import_type&#x3D;instructure_csv&amp;extension&#x3D;zip    curl -H &#x27;Content-Type: application/zip&#x27; --data-binary @&lt;filename&gt;.zip \\       -H \&quot;Authorization: Bearer &lt;token&gt;\&quot; \\       https://&lt;canvas&gt;/api/v1/accounts/&lt;account_id&gt;/sis_imports.json?import_type&#x3D;instructure_csv    curl -H &#x27;Content-Type: text/csv&#x27; --data-binary @&lt;filename&gt;.csv \\       -H \&quot;Authorization: Bearer &lt;token&gt;\&quot; \\       https://&lt;canvas&gt;/api/v1/accounts/&lt;account_id&gt;/sis_imports.json?import_type&#x3D;instructure_csv    curl -H &#x27;Content-Type: text/csv&#x27; --data-binary @&lt;filename&gt;.csv \\       -H \&quot;Authorization: Bearer &lt;token&gt;\&quot; \\       https://&lt;canvas&gt;/api/v1/accounts/&lt;account_id&gt;/sis_imports.json?import_type&#x3D;instructure_csv&amp;batch_mode&#x3D;1&amp;batch_mode_term_id&#x3D;15 | [optional] 
**batch_mode** | bool,  | BoolClass,  | If set, this SIS import will be run in batch mode, deleting any data previously imported via SIS that is not present in this latest import. See the SIS CSV Format page for details. Batch mode cannot be used with diffing. | [optional] 
**batch_mode_term_id** | str,  | str,  | Limit deletions to only this term. Required if batch mode is enabled. | [optional] 
**change_threshold** | decimal.Decimal, int,  | decimal.Decimal,  | If set with batch_mode, the batch cleanup process will not run if the number of items deleted is higher than the percentage set. If set to 10 and a term has 200 enrollments, and batch would delete more than 20 of the enrollments the batch will abort before the enrollments are deleted. The change_threshold will be evaluated for course, sections, and enrollments independently. If set with diffing, diffing  will not be performed if the files are greater than the threshold as a percent. If set to 5 and the file is more than 5% smaller or more than 5% larger than the file that is being compared to, diffing will not be performed. If the files are less than 5%, diffing will be performed. See the SIS CSV Format documentation for more details. Required for multi_term_batch_mode. | [optional] value must be a 64 bit integer
**clear_sis_stickiness** | bool,  | BoolClass,  | This option, if present, will clear \&quot;stickiness\&quot; from all fields touched by this import. Requires that &#x27;override_sis_stickiness&#x27; is also provided. If &#x27;add_sis_stickiness&#x27; is also provided, &#x27;clear_sis_stickiness&#x27; will overrule the behavior of &#x27;add_sis_stickiness&#x27; | [optional] 
**diffing_data_set_identifier** | str,  | str,  | If set on a CSV import, Canvas will attempt to optimize the SIS import by comparing this set of CSVs to the previous set that has the same data set identifier, and only applying the difference between the two. See the SIS CSV Format documentation for more details. Diffing cannot be used with batch_mode | [optional] 
**diffing_drop_status** | str,  | str,  | If diffing_drop_status is passed, this SIS import will use this status for enrollments that are not included in the sis_batch. Defaults to &#x27;deleted&#x27; | [optional] must be one of ["deleted", "completed", "inactive", ] 
**diffing_remaster_data_set** | bool,  | BoolClass,  | If true, and diffing_data_set_identifier is sent, this SIS import will be part of the data set, but diffing will not be performed. See the SIS CSV Format documentation for details. | [optional] 
**extension** | str,  | str,  | Recommended for raw post request style imports. This field will be used to distinguish between zip, xml, csv, and other file format extensions that would usually be provided with the filename in the multipart post request scenario. If not provided, this value will be inferred from the Content-Type, falling back to zip-file format if all else fails. | [optional] 
**import_type** | str,  | str,  | Choose the data format for reading SIS data. With a standard Canvas install, this option can only be &#x27;instructure_csv&#x27;, and if unprovided, will be assumed to be so. Can be part of the query string. | [optional] 
**multi_term_batch_mode** | bool,  | BoolClass,  | Runs batch mode against all terms in terms file. Requires change_threshold. | [optional] 
**override_sis_stickiness** | bool,  | BoolClass,  | Many fields on records in Canvas can be marked \&quot;sticky,\&quot; which means that when something changes in the UI apart from the SIS, that field gets \&quot;stuck.\&quot; In this way, by default, SIS imports do not override UI changes. If this field is present, however, it will tell the SIS import to ignore \&quot;stickiness\&quot; and override all fields. | [optional] 
**skip_deletes** | bool,  | BoolClass,  | When set the import will skip any deletes. This does not account for objects that are deleted during the batch mode cleanup process. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#import_sis_data.ApiResponseFor200) | No response was specified

#### import_sis_data.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**SisImport**](../../models/SisImport.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **restore_workflow_states_of_sis_imported_items**
<a name="restore_workflow_states_of_sis_imported_items"></a>
> Progress restore_workflow_states_of_sis_imported_items(account_idid)

Restore workflow_states of SIS imported items

This will restore the the workflow_state for all the items that changed their workflow_state during the import being restored. This will restore states for items imported with the following importers: accounts.csv terms.csv courses.csv sections.csv group_categories.csv groups.csv users.csv admins.csv This also restores states for other items that changed during the import. An example would be if an enrollment was deleted from a sis import and the group_membership was also deleted as a result of the enrollment deletion, both items would be restored when the sis batch is restored.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import sis_imports_api
from openapi_client.model.progress import Progress
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = sis_imports_api.SisImportsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'id': "id_example",
    }
    try:
        # Restore workflow_states of SIS imported items
        api_response = api_instance.restore_workflow_states_of_sis_imported_items(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SisImportsApi->restore_workflow_states_of_sis_imported_items: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': "account_id_example",
        'id': "id_example",
    }
    body = dict(
        batch_mode=True,
        unconclude_only=True,
        undelete_only=True,
    )
    try:
        # Restore workflow_states of SIS imported items
        api_response = api_instance.restore_workflow_states_of_sis_imported_items(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling SisImportsApi->restore_workflow_states_of_sis_imported_items: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**batch_mode** | bool,  | BoolClass,  | If set, will only restore items that were deleted from batch_mode. | [optional] 
**unconclude_only** | bool,  | BoolClass,  | If set, will only restore enrollments that were concluded. This will ignore any items that were created or deleted. | [optional] 
**undelete_only** | bool,  | BoolClass,  | If set, will only restore items that were deleted. This will ignore any items that were created or modified. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
id | IdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#restore_workflow_states_of_sis_imported_items.ApiResponseFor200) | No response was specified

#### restore_workflow_states_of_sis_imported_items.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**Progress**](../../models/Progress.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

