<a name="__pageTop"></a>
# openapi_client.apis.tags.external_tools_api.ExternalToolsApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_external_tool_accounts**](#create_external_tool_accounts) | **post** /v1/accounts/{account_id}/external_tools | Create an external tool
[**create_external_tool_courses**](#create_external_tool_courses) | **post** /v1/courses/{course_id}/external_tools | Create an external tool
[**create_tool_from_toolconfiguration_accounts**](#create_tool_from_toolconfiguration_accounts) | **post** /v1/accounts/{account_id}/developer_keys/{developer_key_id}/create_tool | Create Tool from ToolConfiguration
[**create_tool_from_toolconfiguration_courses**](#create_tool_from_toolconfiguration_courses) | **post** /v1/courses/{course_id}/developer_keys/{developer_key_id}/create_tool | Create Tool from ToolConfiguration
[**delete_external_tool_accounts**](#delete_external_tool_accounts) | **delete** /v1/accounts/{account_id}/external_tools/{external_tool_id} | Delete an external tool
[**delete_external_tool_courses**](#delete_external_tool_courses) | **delete** /v1/courses/{course_id}/external_tools/{external_tool_id} | Delete an external tool
[**edit_external_tool_accounts**](#edit_external_tool_accounts) | **put** /v1/accounts/{account_id}/external_tools/{external_tool_id} | Edit an external tool
[**edit_external_tool_courses**](#edit_external_tool_courses) | **put** /v1/courses/{course_id}/external_tools/{external_tool_id} | Edit an external tool
[**get_sessionless_launch_url_for_external_tool_accounts**](#get_sessionless_launch_url_for_external_tool_accounts) | **get** /v1/accounts/{account_id}/external_tools/sessionless_launch | Get a sessionless launch url for an external tool.
[**get_sessionless_launch_url_for_external_tool_courses**](#get_sessionless_launch_url_for_external_tool_courses) | **get** /v1/courses/{course_id}/external_tools/sessionless_launch | Get a sessionless launch url for an external tool.
[**get_single_external_tool_accounts**](#get_single_external_tool_accounts) | **get** /v1/accounts/{account_id}/external_tools/{external_tool_id} | Get a single external tool
[**get_single_external_tool_courses**](#get_single_external_tool_courses) | **get** /v1/courses/{course_id}/external_tools/{external_tool_id} | Get a single external tool
[**list_external_tools_accounts**](#list_external_tools_accounts) | **get** /v1/accounts/{account_id}/external_tools | List external tools
[**list_external_tools_courses**](#list_external_tools_courses) | **get** /v1/courses/{course_id}/external_tools | List external tools
[**list_external_tools_groups**](#list_external_tools_groups) | **get** /v1/groups/{group_id}/external_tools | List external tools

# **create_external_tool_accounts**
<a name="create_external_tool_accounts"></a>
> create_external_tool_accounts(account_id)

Create an external tool

Create an external tool in the specified course/account. The created tool will be returned, see the \"show\" endpoint for an example.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    try:
        # Create an external tool
        api_response = api_instance.create_external_tool_accounts(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->create_external_tool_accounts: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': "account_id_example",
    }
    body = None
    try:
        # Create an external tool
        api_response = api_instance.create_external_tool_accounts(
            path_params=path_params,
            body=body,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->create_external_tool_accounts: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**consumer_key** | str,  | str,  | The consumer key for the external tool | 
**privacy_level** | str,  | str,  | What information to send to the external tool. | must be one of ["anonymous", "name_only", "public", ] 
**name** | str,  | str,  | The name of the tool | 
**shared_secret** | str,  | str,  | The shared secret with the external tool | 
**account_navigation[display_type]** | str,  | str,  | The layout type to use when launching the tool. Must be \&quot;full_width\&quot;, \&quot;full_width_in_context\&quot;, \&quot;borderless\&quot;, or \&quot;default\&quot; | [optional] 
**account_navigation[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**account_navigation[selection_height]** | str,  | str,  | The height of the dialog the tool is launched in | [optional] 
**account_navigation[selection_width]** | str,  | str,  | The width of the dialog the tool is launched in | [optional] 
**account_navigation[text]** | str,  | str,  | The text that will show on the left-tab in the account navigation | [optional] 
**account_navigation[url]** | str,  | str,  | The url of the external tool for account navigation | [optional] 
**config_type** | str,  | str,  | Configuration can be passed in as CC xml instead of using query parameters. If this value is \&quot;by_url\&quot; or \&quot;by_xml\&quot; then an xml configuration will be expected in either the \&quot;config_xml\&quot; or \&quot;config_url\&quot; parameter. Note that the name parameter overrides the tool name provided in the xml | [optional] 
**config_url** | str,  | str,  | URL where the server can retrieve an XML tool configuration, as specified in the CC xml specification. This is required if \&quot;config_type\&quot; is set to \&quot;by_url\&quot; | [optional] 
**config_xml** | str,  | str,  | XML tool configuration, as specified in the CC xml specification. This is required if \&quot;config_type\&quot; is set to \&quot;by_xml\&quot; | [optional] 
**course_home_sub_navigation[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**course_home_sub_navigation[icon_url]** | str,  | str,  | The url of the icon to show in the right-side course home navigation menu | [optional] 
**course_home_sub_navigation[text]** | str,  | str,  | The text that will show on the right-side course home navigation menu | [optional] 
**course_home_sub_navigation[url]** | str,  | str,  | The url of the external tool for right-side course home navigation menu | [optional] 
**course_navigation[default]** | bool,  | BoolClass,  | Whether the navigation option will show in the course by default or whether the teacher will have to explicitly enable it | [optional] 
**course_navigation[display_type]** | str,  | str,  | The layout type to use when launching the tool. Must be \&quot;full_width\&quot;, \&quot;full_width_in_context\&quot;, \&quot;borderless\&quot;, or \&quot;default\&quot; | [optional] 
**course_navigation[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**course_navigation[text]** | str,  | str,  | The text that will show on the left-tab in the course navigation | [optional] 
**course_navigation[visibility]** | str,  | str,  | Who will see the navigation tab. \&quot;admins\&quot; for course admins, \&quot;members\&quot; for students, null for everyone | [optional] must be one of ["admins", "members", ] 
**course_navigation[windowTarget]** | str,  | str,  | Determines how the navigation tab will be opened. \&quot;_blank\&quot; Launches the external tool in a new window or tab. \&quot;_self\&quot; (Default) Launches the external tool in an iframe inside of Canvas. | [optional] must be one of ["_blank", "_self", ] 
**custom_fields[field_name]** | str,  | str,  | Custom fields that will be sent to the tool consumer; can be used multiple times | [optional] 
**description** | str,  | str,  | A description of the tool | [optional] 
**domain** | str,  | str,  | The domain to match links against. Either \&quot;url\&quot; or \&quot;domain\&quot; should be set, not both. | [optional] 
**editor_button[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**editor_button[icon_url]** | str,  | str,  | The url of the icon to show in the WYSIWYG editor | [optional] 
**editor_button[message_type]** | str,  | str,  | Set this to ContentItemSelectionRequest to tell the tool to use content-item; otherwise, omit | [optional] 
**editor_button[selection_height]** | str,  | str,  | The height of the dialog the tool is launched in | [optional] 
**editor_button[selection_width]** | str,  | str,  | The width of the dialog the tool is launched in | [optional] 
**editor_button[url]** | str,  | str,  | The url of the external tool | [optional] 
**homework_submission[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**homework_submission[message_type]** | str,  | str,  | Set this to ContentItemSelectionRequest to tell the tool to use content-item; otherwise, omit | [optional] 
**homework_submission[text]** | str,  | str,  | The text that will show on the homework submission tab | [optional] 
**homework_submission[url]** | str,  | str,  | The url of the external tool | [optional] 
**icon_url** | str,  | str,  | The url of the icon to show for this tool | [optional] 
**link_selection[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**link_selection[message_type]** | str,  | str,  | Set this to ContentItemSelectionRequest to tell the tool to use content-item; otherwise, omit | [optional] 
**link_selection[text]** | str,  | str,  | The text that will show for the link selection text | [optional] 
**link_selection[url]** | str,  | str,  | The url of the external tool | [optional] 
**migration_selection[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**migration_selection[message_type]** | str,  | str,  | Set this to ContentItemSelectionRequest to tell the tool to use content-item; otherwise, omit | [optional] 
**migration_selection[url]** | str,  | str,  | The url of the external tool | [optional] 
**not_selectable** | bool,  | BoolClass,  | Default: false, if set to true the tool won&#x27;t show up in the external tool selection UI in modules and assignments | [optional] 
**oauth_compliant** | bool,  | BoolClass,  | Default: false, if set to true LTI query params will not be copied to the post body. | [optional] 
**resource_selection[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**resource_selection[icon_url]** | str,  | str,  | The url of the icon to show in the module external tool list | [optional] 
**resource_selection[selection_height]** | str,  | str,  | The height of the dialog the tool is launched in | [optional] 
**resource_selection[selection_width]** | str,  | str,  | The width of the dialog the tool is launched in | [optional] 
**resource_selection[url]** | str,  | str,  | The url of the external tool | [optional] 
**text** | str,  | str,  | The default text to show for this tool | [optional] 
**tool_configuration[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**tool_configuration[message_type]** | str,  | str,  | Set this to ContentItemSelectionRequest to tell the tool to use content-item; otherwise, omit | [optional] 
**tool_configuration[prefer_sis_email]** | bool,  | BoolClass,  | Set this to default the lis_person_contact_email_primary to prefer provisioned sis_email; otherwise, omit | [optional] 
**tool_configuration[url]** | str,  | str,  | The url of the external tool | [optional] 
**url** | str,  | str,  | The url to match links against. Either \&quot;url\&quot; or \&quot;domain\&quot; should be set, not both. | [optional] 
**user_navigation[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**user_navigation[text]** | str,  | str,  | The text that will show on the left-tab in the user navigation | [optional] 
**user_navigation[url]** | str,  | str,  | The url of the external tool for user navigation | [optional] 
**user_navigation[visibility]** | str,  | str,  | Who will see the navigation tab. \&quot;admins\&quot; for admins, \&quot;public\&quot; or \&quot;members\&quot; for everyone | [optional] must be one of ["admins", "members", "public", ] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#create_external_tool_accounts.ApiResponseFor200) | No response was specified

#### create_external_tool_accounts.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **create_external_tool_courses**
<a name="create_external_tool_courses"></a>
> create_external_tool_courses(course_id)

Create an external tool

Create an external tool in the specified course/account. The created tool will be returned, see the \"show\" endpoint for an example.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    try:
        # Create an external tool
        api_response = api_instance.create_external_tool_courses(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->create_external_tool_courses: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
    }
    body = None
    try:
        # Create an external tool
        api_response = api_instance.create_external_tool_courses(
            path_params=path_params,
            body=body,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->create_external_tool_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**consumer_key** | str,  | str,  | The consumer key for the external tool | 
**privacy_level** | str,  | str,  | What information to send to the external tool. | must be one of ["anonymous", "name_only", "public", ] 
**name** | str,  | str,  | The name of the tool | 
**shared_secret** | str,  | str,  | The shared secret with the external tool | 
**account_navigation[display_type]** | str,  | str,  | The layout type to use when launching the tool. Must be \&quot;full_width\&quot;, \&quot;full_width_in_context\&quot;, \&quot;borderless\&quot;, or \&quot;default\&quot; | [optional] 
**account_navigation[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**account_navigation[selection_height]** | str,  | str,  | The height of the dialog the tool is launched in | [optional] 
**account_navigation[selection_width]** | str,  | str,  | The width of the dialog the tool is launched in | [optional] 
**account_navigation[text]** | str,  | str,  | The text that will show on the left-tab in the account navigation | [optional] 
**account_navigation[url]** | str,  | str,  | The url of the external tool for account navigation | [optional] 
**config_type** | str,  | str,  | Configuration can be passed in as CC xml instead of using query parameters. If this value is \&quot;by_url\&quot; or \&quot;by_xml\&quot; then an xml configuration will be expected in either the \&quot;config_xml\&quot; or \&quot;config_url\&quot; parameter. Note that the name parameter overrides the tool name provided in the xml | [optional] 
**config_url** | str,  | str,  | URL where the server can retrieve an XML tool configuration, as specified in the CC xml specification. This is required if \&quot;config_type\&quot; is set to \&quot;by_url\&quot; | [optional] 
**config_xml** | str,  | str,  | XML tool configuration, as specified in the CC xml specification. This is required if \&quot;config_type\&quot; is set to \&quot;by_xml\&quot; | [optional] 
**course_home_sub_navigation[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**course_home_sub_navigation[icon_url]** | str,  | str,  | The url of the icon to show in the right-side course home navigation menu | [optional] 
**course_home_sub_navigation[text]** | str,  | str,  | The text that will show on the right-side course home navigation menu | [optional] 
**course_home_sub_navigation[url]** | str,  | str,  | The url of the external tool for right-side course home navigation menu | [optional] 
**course_navigation[default]** | bool,  | BoolClass,  | Whether the navigation option will show in the course by default or whether the teacher will have to explicitly enable it | [optional] 
**course_navigation[display_type]** | str,  | str,  | The layout type to use when launching the tool. Must be \&quot;full_width\&quot;, \&quot;full_width_in_context\&quot;, \&quot;borderless\&quot;, or \&quot;default\&quot; | [optional] 
**course_navigation[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**course_navigation[text]** | str,  | str,  | The text that will show on the left-tab in the course navigation | [optional] 
**course_navigation[visibility]** | str,  | str,  | Who will see the navigation tab. \&quot;admins\&quot; for course admins, \&quot;members\&quot; for students, null for everyone | [optional] must be one of ["admins", "members", ] 
**course_navigation[windowTarget]** | str,  | str,  | Determines how the navigation tab will be opened. \&quot;_blank\&quot; Launches the external tool in a new window or tab. \&quot;_self\&quot; (Default) Launches the external tool in an iframe inside of Canvas. | [optional] must be one of ["_blank", "_self", ] 
**custom_fields[field_name]** | str,  | str,  | Custom fields that will be sent to the tool consumer; can be used multiple times | [optional] 
**description** | str,  | str,  | A description of the tool | [optional] 
**domain** | str,  | str,  | The domain to match links against. Either \&quot;url\&quot; or \&quot;domain\&quot; should be set, not both. | [optional] 
**editor_button[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**editor_button[icon_url]** | str,  | str,  | The url of the icon to show in the WYSIWYG editor | [optional] 
**editor_button[message_type]** | str,  | str,  | Set this to ContentItemSelectionRequest to tell the tool to use content-item; otherwise, omit | [optional] 
**editor_button[selection_height]** | str,  | str,  | The height of the dialog the tool is launched in | [optional] 
**editor_button[selection_width]** | str,  | str,  | The width of the dialog the tool is launched in | [optional] 
**editor_button[url]** | str,  | str,  | The url of the external tool | [optional] 
**homework_submission[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**homework_submission[message_type]** | str,  | str,  | Set this to ContentItemSelectionRequest to tell the tool to use content-item; otherwise, omit | [optional] 
**homework_submission[text]** | str,  | str,  | The text that will show on the homework submission tab | [optional] 
**homework_submission[url]** | str,  | str,  | The url of the external tool | [optional] 
**icon_url** | str,  | str,  | The url of the icon to show for this tool | [optional] 
**link_selection[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**link_selection[message_type]** | str,  | str,  | Set this to ContentItemSelectionRequest to tell the tool to use content-item; otherwise, omit | [optional] 
**link_selection[text]** | str,  | str,  | The text that will show for the link selection text | [optional] 
**link_selection[url]** | str,  | str,  | The url of the external tool | [optional] 
**migration_selection[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**migration_selection[message_type]** | str,  | str,  | Set this to ContentItemSelectionRequest to tell the tool to use content-item; otherwise, omit | [optional] 
**migration_selection[url]** | str,  | str,  | The url of the external tool | [optional] 
**not_selectable** | bool,  | BoolClass,  | Default: false, if set to true the tool won&#x27;t show up in the external tool selection UI in modules and assignments | [optional] 
**oauth_compliant** | bool,  | BoolClass,  | Default: false, if set to true LTI query params will not be copied to the post body. | [optional] 
**resource_selection[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**resource_selection[icon_url]** | str,  | str,  | The url of the icon to show in the module external tool list | [optional] 
**resource_selection[selection_height]** | str,  | str,  | The height of the dialog the tool is launched in | [optional] 
**resource_selection[selection_width]** | str,  | str,  | The width of the dialog the tool is launched in | [optional] 
**resource_selection[url]** | str,  | str,  | The url of the external tool | [optional] 
**text** | str,  | str,  | The default text to show for this tool | [optional] 
**tool_configuration[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**tool_configuration[message_type]** | str,  | str,  | Set this to ContentItemSelectionRequest to tell the tool to use content-item; otherwise, omit | [optional] 
**tool_configuration[prefer_sis_email]** | bool,  | BoolClass,  | Set this to default the lis_person_contact_email_primary to prefer provisioned sis_email; otherwise, omit | [optional] 
**tool_configuration[url]** | str,  | str,  | The url of the external tool | [optional] 
**url** | str,  | str,  | The url to match links against. Either \&quot;url\&quot; or \&quot;domain\&quot; should be set, not both. | [optional] 
**user_navigation[enabled]** | bool,  | BoolClass,  | Set this to enable this feature | [optional] 
**user_navigation[text]** | str,  | str,  | The text that will show on the left-tab in the user navigation | [optional] 
**user_navigation[url]** | str,  | str,  | The url of the external tool for user navigation | [optional] 
**user_navigation[visibility]** | str,  | str,  | Who will see the navigation tab. \&quot;admins\&quot; for admins, \&quot;public\&quot; or \&quot;members\&quot; for everyone | [optional] must be one of ["admins", "members", "public", ] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#create_external_tool_courses.ApiResponseFor200) | No response was specified

#### create_external_tool_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **create_tool_from_toolconfiguration_accounts**
<a name="create_tool_from_toolconfiguration_accounts"></a>
> ContextExternalTool create_tool_from_toolconfiguration_accounts(account_iddeveloper_key_id)

Create Tool from ToolConfiguration

Creates context_external_tool from attached tool_configuration of the provided developer_key if not already present in context. DeveloperKey must have a ToolConfiguration to create tool or 404 will be raised. Will return an existing ContextExternalTool if one already exists.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from openapi_client.model.context_external_tool import ContextExternalTool
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'developer_key_id': "developer_key_id_example",
    }
    try:
        # Create Tool from ToolConfiguration
        api_response = api_instance.create_tool_from_toolconfiguration_accounts(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->create_tool_from_toolconfiguration_accounts: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': "account_id_example",
        'developer_key_id': "developer_key_id_example",
    }
    body = None
    try:
        # Create Tool from ToolConfiguration
        api_response = api_instance.create_tool_from_toolconfiguration_accounts(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->create_tool_from_toolconfiguration_accounts: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**course_id** | str,  | str,  | if course | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
developer_key_id | DeveloperKeyIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# DeveloperKeyIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#create_tool_from_toolconfiguration_accounts.ApiResponseFor200) | No response was specified

#### create_tool_from_toolconfiguration_accounts.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**ContextExternalTool**](../../models/ContextExternalTool.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **create_tool_from_toolconfiguration_courses**
<a name="create_tool_from_toolconfiguration_courses"></a>
> ContextExternalTool create_tool_from_toolconfiguration_courses(course_iddeveloper_key_id)

Create Tool from ToolConfiguration

Creates context_external_tool from attached tool_configuration of the provided developer_key if not already present in context. DeveloperKey must have a ToolConfiguration to create tool or 404 will be raised. Will return an existing ContextExternalTool if one already exists.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from openapi_client.model.context_external_tool import ContextExternalTool
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'developer_key_id': "developer_key_id_example",
    }
    try:
        # Create Tool from ToolConfiguration
        api_response = api_instance.create_tool_from_toolconfiguration_courses(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->create_tool_from_toolconfiguration_courses: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
        'developer_key_id': "developer_key_id_example",
    }
    body = None
    try:
        # Create Tool from ToolConfiguration
        api_response = api_instance.create_tool_from_toolconfiguration_courses(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->create_tool_from_toolconfiguration_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**account_id** | str,  | str,  | if account | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
developer_key_id | DeveloperKeyIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# DeveloperKeyIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#create_tool_from_toolconfiguration_courses.ApiResponseFor200) | No response was specified

#### create_tool_from_toolconfiguration_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**ContextExternalTool**](../../models/ContextExternalTool.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **delete_external_tool_accounts**
<a name="delete_external_tool_accounts"></a>
> delete_external_tool_accounts(account_idexternal_tool_id)

Delete an external tool

Remove the specified external tool

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'external_tool_id': "external_tool_id_example",
    }
    try:
        # Delete an external tool
        api_response = api_instance.delete_external_tool_accounts(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->delete_external_tool_accounts: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
external_tool_id | ExternalToolIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# ExternalToolIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#delete_external_tool_accounts.ApiResponseFor200) | No response was specified

#### delete_external_tool_accounts.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **delete_external_tool_courses**
<a name="delete_external_tool_courses"></a>
> delete_external_tool_courses(course_idexternal_tool_id)

Delete an external tool

Remove the specified external tool

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'external_tool_id': "external_tool_id_example",
    }
    try:
        # Delete an external tool
        api_response = api_instance.delete_external_tool_courses(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->delete_external_tool_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
external_tool_id | ExternalToolIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# ExternalToolIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#delete_external_tool_courses.ApiResponseFor200) | No response was specified

#### delete_external_tool_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **edit_external_tool_accounts**
<a name="edit_external_tool_accounts"></a>
> edit_external_tool_accounts(account_idexternal_tool_id)

Edit an external tool

Update the specified external tool. Uses same parameters as create

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'external_tool_id': "external_tool_id_example",
    }
    try:
        # Edit an external tool
        api_response = api_instance.edit_external_tool_accounts(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->edit_external_tool_accounts: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
external_tool_id | ExternalToolIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# ExternalToolIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#edit_external_tool_accounts.ApiResponseFor200) | No response was specified

#### edit_external_tool_accounts.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **edit_external_tool_courses**
<a name="edit_external_tool_courses"></a>
> edit_external_tool_courses(course_idexternal_tool_id)

Edit an external tool

Update the specified external tool. Uses same parameters as create

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'external_tool_id': "external_tool_id_example",
    }
    try:
        # Edit an external tool
        api_response = api_instance.edit_external_tool_courses(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->edit_external_tool_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
external_tool_id | ExternalToolIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# ExternalToolIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#edit_external_tool_courses.ApiResponseFor200) | No response was specified

#### edit_external_tool_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **get_sessionless_launch_url_for_external_tool_accounts**
<a name="get_sessionless_launch_url_for_external_tool_accounts"></a>
> get_sessionless_launch_url_for_external_tool_accounts(account_id)

Get a sessionless launch url for an external tool.

Returns a sessionless launch url for an external tool.  NOTE: Either the id or url must be provided unless launch_type is assessment or module_item.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    query_params = {
    }
    try:
        # Get a sessionless launch url for an external tool.
        api_response = api_instance.get_sessionless_launch_url_for_external_tool_accounts(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->get_sessionless_launch_url_for_external_tool_accounts: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': "account_id_example",
    }
    query_params = {
        'id': "id_example",
        'url': "url_example",
        'assignment_id': "assignment_id_example",
        'module_item_id': "module_item_id_example",
        'launch_type': "assessment",
    }
    try:
        # Get a sessionless launch url for an external tool.
        api_response = api_instance.get_sessionless_launch_url_for_external_tool_accounts(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->get_sessionless_launch_url_for_external_tool_accounts: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
id | IdSchema | | optional
url | UrlSchema | | optional
assignment_id | AssignmentIdSchema | | optional
module_item_id | ModuleItemIdSchema | | optional
launch_type | LaunchTypeSchema | | optional


# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UrlSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# ModuleItemIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# LaunchTypeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["assessment", "module_item", ] 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_sessionless_launch_url_for_external_tool_accounts.ApiResponseFor200) | No response was specified

#### get_sessionless_launch_url_for_external_tool_accounts.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **get_sessionless_launch_url_for_external_tool_courses**
<a name="get_sessionless_launch_url_for_external_tool_courses"></a>
> get_sessionless_launch_url_for_external_tool_courses(course_id)

Get a sessionless launch url for an external tool.

Returns a sessionless launch url for an external tool.  NOTE: Either the id or url must be provided unless launch_type is assessment or module_item.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
    }
    try:
        # Get a sessionless launch url for an external tool.
        api_response = api_instance.get_sessionless_launch_url_for_external_tool_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->get_sessionless_launch_url_for_external_tool_courses: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
        'id': "id_example",
        'url': "url_example",
        'assignment_id': "assignment_id_example",
        'module_item_id': "module_item_id_example",
        'launch_type': "assessment",
    }
    try:
        # Get a sessionless launch url for an external tool.
        api_response = api_instance.get_sessionless_launch_url_for_external_tool_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->get_sessionless_launch_url_for_external_tool_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
id | IdSchema | | optional
url | UrlSchema | | optional
assignment_id | AssignmentIdSchema | | optional
module_item_id | ModuleItemIdSchema | | optional
launch_type | LaunchTypeSchema | | optional


# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# UrlSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# ModuleItemIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# LaunchTypeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["assessment", "module_item", ] 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_sessionless_launch_url_for_external_tool_courses.ApiResponseFor200) | No response was specified

#### get_sessionless_launch_url_for_external_tool_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **get_single_external_tool_accounts**
<a name="get_single_external_tool_accounts"></a>
> get_single_external_tool_accounts(account_idexternal_tool_id)

Get a single external tool

Returns the specified external tool.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'external_tool_id': "external_tool_id_example",
    }
    try:
        # Get a single external tool
        api_response = api_instance.get_single_external_tool_accounts(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->get_single_external_tool_accounts: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
external_tool_id | ExternalToolIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# ExternalToolIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_single_external_tool_accounts.ApiResponseFor200) | No response was specified

#### get_single_external_tool_accounts.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **get_single_external_tool_courses**
<a name="get_single_external_tool_courses"></a>
> get_single_external_tool_courses(course_idexternal_tool_id)

Get a single external tool

Returns the specified external tool.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
        'external_tool_id': "external_tool_id_example",
    }
    try:
        # Get a single external tool
        api_response = api_instance.get_single_external_tool_courses(
            path_params=path_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->get_single_external_tool_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 
external_tool_id | ExternalToolIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# ExternalToolIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_single_external_tool_courses.ApiResponseFor200) | No response was specified

#### get_single_external_tool_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_external_tools_accounts**
<a name="list_external_tools_accounts"></a>
> list_external_tools_accounts(account_id)

List external tools

Returns the paginated list of external tools for the current context. See the get request docs for a single tool for a list of properties on an external tool.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    query_params = {
    }
    try:
        # List external tools
        api_response = api_instance.list_external_tools_accounts(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->list_external_tools_accounts: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': "account_id_example",
    }
    query_params = {
        'search_term': "search_term_example",
        'selectable': True,
        'include_parents': True,
    }
    try:
        # List external tools
        api_response = api_instance.list_external_tools_accounts(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->list_external_tools_accounts: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
search_term | SearchTermSchema | | optional
selectable | SelectableSchema | | optional
include_parents | IncludeParentsSchema | | optional


# SearchTermSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# SelectableSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# IncludeParentsSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_external_tools_accounts.ApiResponseFor200) | No response was specified

#### list_external_tools_accounts.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_external_tools_courses**
<a name="list_external_tools_courses"></a>
> list_external_tools_courses(course_id)

List external tools

Returns the paginated list of external tools for the current context. See the get request docs for a single tool for a list of properties on an external tool.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
    }
    try:
        # List external tools
        api_response = api_instance.list_external_tools_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->list_external_tools_courses: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
        'search_term': "search_term_example",
        'selectable': True,
        'include_parents': True,
    }
    try:
        # List external tools
        api_response = api_instance.list_external_tools_courses(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->list_external_tools_courses: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
search_term | SearchTermSchema | | optional
selectable | SelectableSchema | | optional
include_parents | IncludeParentsSchema | | optional


# SearchTermSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# SelectableSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# IncludeParentsSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_external_tools_courses.ApiResponseFor200) | No response was specified

#### list_external_tools_courses.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_external_tools_groups**
<a name="list_external_tools_groups"></a>
> list_external_tools_groups(group_id)

List external tools

Returns the paginated list of external tools for the current context. See the get request docs for a single tool for a list of properties on an external tool.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import external_tools_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = external_tools_api.ExternalToolsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'group_id': "group_id_example",
    }
    query_params = {
    }
    try:
        # List external tools
        api_response = api_instance.list_external_tools_groups(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->list_external_tools_groups: %s\n" % e)

    # example passing only optional values
    path_params = {
        'group_id': "group_id_example",
    }
    query_params = {
        'search_term': "search_term_example",
        'selectable': True,
        'include_parents': True,
    }
    try:
        # List external tools
        api_response = api_instance.list_external_tools_groups(
            path_params=path_params,
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling ExternalToolsApi->list_external_tools_groups: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
search_term | SearchTermSchema | | optional
selectable | SelectableSchema | | optional
include_parents | IncludeParentsSchema | | optional


# SearchTermSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# SelectableSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

# IncludeParentsSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
bool,  | BoolClass,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
group_id | GroupIdSchema | | 

# GroupIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_external_tools_groups.ApiResponseFor200) | No response was specified

#### list_external_tools_groups.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

