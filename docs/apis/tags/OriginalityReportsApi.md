<a name="__pageTop"></a>
# openapi_client.apis.tags.originality_reports_api.OriginalityReportsApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_originality_report**](#create_originality_report) | **post** /lti/assignments/{assignment_id}/submissions/{submission_id}/originality_report | Create an Originality Report
[**edit_originality_report_files**](#edit_originality_report_files) | **put** /lti/assignments/{assignment_id}/files/{file_id}/originality_report | Edit an Originality Report
[**edit_originality_report_submissions**](#edit_originality_report_submissions) | **put** /lti/assignments/{assignment_id}/submissions/{submission_id}/originality_report/{id} | Edit an Originality Report
[**show_originality_report_files**](#show_originality_report_files) | **get** /lti/assignments/{assignment_id}/files/{file_id}/originality_report | Show an Originality Report
[**show_originality_report_submissions**](#show_originality_report_submissions) | **get** /lti/assignments/{assignment_id}/submissions/{submission_id}/originality_report/{id} | Show an Originality Report

# **create_originality_report**
<a name="create_originality_report"></a>
> OriginalityReport create_originality_report(assignment_idsubmission_idany_type)

Create an Originality Report

Create a new OriginalityReport for the specified file

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import originality_reports_api
from openapi_client.model.originality_report import OriginalityReport
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = originality_reports_api.OriginalityReportsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'assignment_id': "assignment_id_example",
        'submission_id': "submission_id_example",
    }
    body = dict(
        originality_report_file_id=1,
        originality_report_originality_report_file_id=1,
        originality_report_originality_report_url="originality_report_originality_report_url_example",
        originality_report_originality_score=3.14,
        originality_report_tool_setting_resource_type_code="originality_report_tool_setting_resource_type_code_example",
        originality_report_tool_setting_resource_url="originality_report_tool_setting_resource_url_example",
        originality_report_workflow_state="originality_report_workflow_state_example",
    )
    try:
        # Create an Originality Report
        api_response = api_instance.create_originality_report(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OriginalityReportsApi->create_originality_report: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson] | required |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**originality_report[originality_score]** | decimal.Decimal, int, float,  | decimal.Decimal,  | A number between 0 and 100 representing the measure of the specified file&#x27;s originality. | value must be a 32 bit float
**originality_report[file_id]** | decimal.Decimal, int,  | decimal.Decimal,  | The id of the file being given an originality score. Required if creating a report associated with a file. | [optional] value must be a 64 bit integer
**originality_report[originality_report_file_id]** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the file within Canvas that contains the originality report for the submitted file provided in the request URL. | [optional] value must be a 64 bit integer
**originality_report[originality_report_url]** | str,  | str,  | The URL where the originality report for the specified file may be found. | [optional] 
**originality_report[tool_setting][resource_type_code]** | str,  | str,  | The resource type code of the resource handler Canvas should use for the LTI launch for viewing originality reports. If set Canvas will launch to the message with type &#x27;basic-lti-launch-request&#x27; in the specified resource handler rather than using the originality_report_url. | [optional] 
**originality_report[tool_setting][resource_url]** | str,  | str,  | The URL Canvas should launch to when showing an LTI originality report. Note that this value is inferred from the specified resource handler&#x27;s message \&quot;path\&quot; value (See &#x60;resource_type_code&#x60;) unless it is specified. If this parameter is used a &#x60;resource_type_code&#x60; must also be specified. | [optional] 
**originality_report[workflow_state]** | str,  | str,  | May be set to \&quot;pending\&quot;, \&quot;error\&quot;, or \&quot;scored\&quot;. If an originality score is provided a workflow state of \&quot;scored\&quot; will be inferred. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
assignment_id | AssignmentIdSchema | | 
submission_id | SubmissionIdSchema | | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# SubmissionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#create_originality_report.ApiResponseFor200) | No response was specified

#### create_originality_report.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OriginalityReport**](../../models/OriginalityReport.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **edit_originality_report_files**
<a name="edit_originality_report_files"></a>
> OriginalityReport edit_originality_report_files(assignment_idfile_id)

Edit an Originality Report

Modify an existing originality report. An alternative to this endpoint is to POST the same parameters listed below to the CREATE endpoint.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import originality_reports_api
from openapi_client.model.originality_report import OriginalityReport
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = originality_reports_api.OriginalityReportsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'assignment_id': "assignment_id_example",
        'file_id': "file_id_example",
    }
    try:
        # Edit an Originality Report
        api_response = api_instance.edit_originality_report_files(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OriginalityReportsApi->edit_originality_report_files: %s\n" % e)

    # example passing only optional values
    path_params = {
        'assignment_id': "assignment_id_example",
        'file_id': "file_id_example",
    }
    body = dict(
        originality_report_originality_report_file_id=1,
        originality_report_originality_report_url="originality_report_originality_report_url_example",
        originality_report_originality_score=3.14,
        originality_report_tool_setting_resource_type_code="originality_report_tool_setting_resource_type_code_example",
        originality_report_tool_setting_resource_url="originality_report_tool_setting_resource_url_example",
        originality_report_workflow_state="originality_report_workflow_state_example",
    )
    try:
        # Edit an Originality Report
        api_response = api_instance.edit_originality_report_files(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OriginalityReportsApi->edit_originality_report_files: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**originality_report[originality_report_file_id]** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the file within Canvas that contains the originality report for the submitted file provided in the request URL. | [optional] value must be a 64 bit integer
**originality_report[originality_report_url]** | str,  | str,  | The URL where the originality report for the specified file may be found. | [optional] 
**originality_report[originality_score]** | decimal.Decimal, int, float,  | decimal.Decimal,  | A number between 0 and 100 representing the measure of the specified file&#x27;s originality. | [optional] value must be a 32 bit float
**originality_report[tool_setting][resource_type_code]** | str,  | str,  | The resource type code of the resource handler Canvas should use for the LTI launch for viewing originality reports. If set Canvas will launch to the message with type &#x27;basic-lti-launch-request&#x27; in the specified resource handler rather than using the originality_report_url. | [optional] 
**originality_report[tool_setting][resource_url]** | str,  | str,  | The URL Canvas should launch to when showing an LTI originality report. Note that this value is inferred from the specified resource handler&#x27;s message \&quot;path\&quot; value (See &#x60;resource_type_code&#x60;) unless it is specified. If this parameter is used a &#x60;resource_type_code&#x60; must also be specified. | [optional] 
**originality_report[workflow_state]** | str,  | str,  | May be set to \&quot;pending\&quot;, \&quot;error\&quot;, or \&quot;scored\&quot;. If an originality score is provided a workflow state of \&quot;scored\&quot; will be inferred. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
assignment_id | AssignmentIdSchema | | 
file_id | FileIdSchema | | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# FileIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#edit_originality_report_files.ApiResponseFor200) | No response was specified

#### edit_originality_report_files.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OriginalityReport**](../../models/OriginalityReport.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **edit_originality_report_submissions**
<a name="edit_originality_report_submissions"></a>
> OriginalityReport edit_originality_report_submissions(assignment_idsubmission_idid)

Edit an Originality Report

Modify an existing originality report. An alternative to this endpoint is to POST the same parameters listed below to the CREATE endpoint.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import originality_reports_api
from openapi_client.model.originality_report import OriginalityReport
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = originality_reports_api.OriginalityReportsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'assignment_id': "assignment_id_example",
        'submission_id': "submission_id_example",
        'id': "id_example",
    }
    try:
        # Edit an Originality Report
        api_response = api_instance.edit_originality_report_submissions(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OriginalityReportsApi->edit_originality_report_submissions: %s\n" % e)

    # example passing only optional values
    path_params = {
        'assignment_id': "assignment_id_example",
        'submission_id': "submission_id_example",
        'id': "id_example",
    }
    body = dict(
        originality_report_originality_report_file_id=1,
        originality_report_originality_report_url="originality_report_originality_report_url_example",
        originality_report_originality_score=3.14,
        originality_report_tool_setting_resource_type_code="originality_report_tool_setting_resource_type_code_example",
        originality_report_tool_setting_resource_url="originality_report_tool_setting_resource_url_example",
        originality_report_workflow_state="originality_report_workflow_state_example",
    )
    try:
        # Edit an Originality Report
        api_response = api_instance.edit_originality_report_submissions(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OriginalityReportsApi->edit_originality_report_submissions: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**originality_report[originality_report_file_id]** | decimal.Decimal, int,  | decimal.Decimal,  | The ID of the file within Canvas that contains the originality report for the submitted file provided in the request URL. | [optional] value must be a 64 bit integer
**originality_report[originality_report_url]** | str,  | str,  | The URL where the originality report for the specified file may be found. | [optional] 
**originality_report[originality_score]** | decimal.Decimal, int, float,  | decimal.Decimal,  | A number between 0 and 100 representing the measure of the specified file&#x27;s originality. | [optional] value must be a 32 bit float
**originality_report[tool_setting][resource_type_code]** | str,  | str,  | The resource type code of the resource handler Canvas should use for the LTI launch for viewing originality reports. If set Canvas will launch to the message with type &#x27;basic-lti-launch-request&#x27; in the specified resource handler rather than using the originality_report_url. | [optional] 
**originality_report[tool_setting][resource_url]** | str,  | str,  | The URL Canvas should launch to when showing an LTI originality report. Note that this value is inferred from the specified resource handler&#x27;s message \&quot;path\&quot; value (See &#x60;resource_type_code&#x60;) unless it is specified. If this parameter is used a &#x60;resource_type_code&#x60; must also be specified. | [optional] 
**originality_report[workflow_state]** | str,  | str,  | May be set to \&quot;pending\&quot;, \&quot;error\&quot;, or \&quot;scored\&quot;. If an originality score is provided a workflow state of \&quot;scored\&quot; will be inferred. | [optional] 
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
assignment_id | AssignmentIdSchema | | 
submission_id | SubmissionIdSchema | | 
id | IdSchema | | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# SubmissionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#edit_originality_report_submissions.ApiResponseFor200) | No response was specified

#### edit_originality_report_submissions.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OriginalityReport**](../../models/OriginalityReport.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **show_originality_report_files**
<a name="show_originality_report_files"></a>
> OriginalityReport show_originality_report_files(assignment_idfile_id)

Show an Originality Report

Get a single originality report

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import originality_reports_api
from openapi_client.model.originality_report import OriginalityReport
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = originality_reports_api.OriginalityReportsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'assignment_id': "assignment_id_example",
        'file_id': "file_id_example",
    }
    try:
        # Show an Originality Report
        api_response = api_instance.show_originality_report_files(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OriginalityReportsApi->show_originality_report_files: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
assignment_id | AssignmentIdSchema | | 
file_id | FileIdSchema | | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# FileIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#show_originality_report_files.ApiResponseFor200) | No response was specified

#### show_originality_report_files.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OriginalityReport**](../../models/OriginalityReport.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **show_originality_report_submissions**
<a name="show_originality_report_submissions"></a>
> OriginalityReport show_originality_report_submissions(assignment_idsubmission_idid)

Show an Originality Report

Get a single originality report

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import originality_reports_api
from openapi_client.model.originality_report import OriginalityReport
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = originality_reports_api.OriginalityReportsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'assignment_id': "assignment_id_example",
        'submission_id': "submission_id_example",
        'id': "id_example",
    }
    try:
        # Show an Originality Report
        api_response = api_instance.show_originality_report_submissions(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling OriginalityReportsApi->show_originality_report_submissions: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
assignment_id | AssignmentIdSchema | | 
submission_id | SubmissionIdSchema | | 
id | IdSchema | | 

# AssignmentIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# SubmissionIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#show_originality_report_submissions.ApiResponseFor200) | No response was specified

#### show_originality_report_submissions.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**OriginalityReport**](../../models/OriginalityReport.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

