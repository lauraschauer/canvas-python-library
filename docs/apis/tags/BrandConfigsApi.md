<a name="__pageTop"></a>
# openapi_client.apis.tags.brand_configs_api.BrandConfigsApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_brand_config_variables_that_should_be_used_for_this_domain**](#get_brand_config_variables_that_should_be_used_for_this_domain) | **get** /v1/brand_variables | Get the brand config variables that should be used for this domain

# **get_brand_config_variables_that_should_be_used_for_this_domain**
<a name="get_brand_config_variables_that_should_be_used_for_this_domain"></a>
> get_brand_config_variables_that_should_be_used_for_this_domain()

Get the brand config variables that should be used for this domain

Will redirect to a static json file that has all of the brand variables used by this account. Even though this is a redirect, do not store the redirected url since if the account makes any changes it will redirect to a new url. Needs no authentication.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import brand_configs_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = brand_configs_api.BrandConfigsApi(api_client)

    # example, this endpoint has no required or optional parameters
    try:
        # Get the brand config variables that should be used for this domain
        api_response = api_instance.get_brand_config_variables_that_should_be_used_for_this_domain()
    except openapi_client.ApiException as e:
        print("Exception when calling BrandConfigsApi->get_brand_config_variables_that_should_be_used_for_this_domain: %s\n" % e)
```
### Parameters
This endpoint does not need any parameter.

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#get_brand_config_variables_that_should_be_used_for_this_domain.ApiResponseFor200) | No response was specified

#### get_brand_config_variables_that_should_be_used_for_this_domain.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

