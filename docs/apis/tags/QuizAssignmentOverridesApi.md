<a name="__pageTop"></a>
# openapi_client.apis.tags.quiz_assignment_overrides_api.QuizAssignmentOverridesApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**retrieve_assignment_overridden_dates_for_quizzes**](#retrieve_assignment_overridden_dates_for_quizzes) | **get** /v1/courses/{course_id}/quizzes/assignment_overrides | Retrieve assignment-overridden dates for quizzes

# **retrieve_assignment_overridden_dates_for_quizzes**
<a name="retrieve_assignment_overridden_dates_for_quizzes"></a>
> QuizAssignmentOverrideSetContainer retrieve_assignment_overridden_dates_for_quizzes(course_id)

Retrieve assignment-overridden dates for quizzes

Retrieve the actual due-at, unlock-at, and available-at dates for quizzes based on the assignment overrides active for the current API user.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import quiz_assignment_overrides_api
from openapi_client.model.quiz_assignment_override_set_container import QuizAssignmentOverrideSetContainer
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = quiz_assignment_overrides_api.QuizAssignmentOverridesApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
    }
    try:
        # Retrieve assignment-overridden dates for quizzes
        api_response = api_instance.retrieve_assignment_overridden_dates_for_quizzes(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling QuizAssignmentOverridesApi->retrieve_assignment_overridden_dates_for_quizzes: %s\n" % e)

    # example passing only optional values
    path_params = {
        'course_id': "course_id_example",
    }
    query_params = {
        'quiz_assignment_overrides[0][quiz_ids]': [
        1
    ],
    }
    try:
        # Retrieve assignment-overridden dates for quizzes
        api_response = api_instance.retrieve_assignment_overridden_dates_for_quizzes(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling QuizAssignmentOverridesApi->retrieve_assignment_overridden_dates_for_quizzes: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
quiz_assignment_overrides[0][quiz_ids] | QuizAssignmentOverrides0QuizIdsSchema | | optional


# QuizAssignmentOverrides0QuizIdsSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | decimal.Decimal, int,  | decimal.Decimal,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
course_id | CourseIdSchema | | 

# CourseIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#retrieve_assignment_overridden_dates_for_quizzes.ApiResponseFor200) | No response was specified

#### retrieve_assignment_overridden_dates_for_quizzes.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**QuizAssignmentOverrideSetContainer**](../../models/QuizAssignmentOverrideSetContainer.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

