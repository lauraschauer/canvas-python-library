<a name="__pageTop"></a>
# openapi_client.apis.tags.planner_api.PlannerApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**list_planner_items**](#list_planner_items) | **get** /v1/planner/items | List planner items

# **list_planner_items**
<a name="list_planner_items"></a>
> list_planner_items()

List planner items

Retrieve the paginated list of objects to be shown on the planner for the current user with the associated planner override to override an item's visibility if set.  [   {     \"context_type\": \"Course\",     \"course_id\": 1,     \"visible_in_planner\": true, // Whether or not it is displayed on the student planner     \"planner_override\": { ... planner override object ... }, // Associated PlannerOverride object if user has toggled visibility for the object on the planner     \"submissions\": false, // The statuses of the user's submissions for this object     \"plannable_id\": \"123\",     \"plannable_type\": \"discussion_topic\",     \"plannable\": { ... discussion topic object },     \"html_url\": \"/courses/1/discussion_topics/8\"   },   {     \"context_type\": \"Course\",     \"course_id\": 1,     \"visible_in_planner\": true,     \"planner_override\": {         \"id\": 3,         \"plannable_type\": \"Assignment\",         \"plannable_id\": 1,         \"user_id\": 2,         \"workflow_state\": \"active\",         \"marked_complete\": true, // A user-defined setting for marking items complete in the planner         \"dismissed\": false, // A user-defined setting for hiding items from the opportunities list         \"deleted_at\": null,         \"created_at\": \"2017-05-18T18:35:55Z\",         \"updated_at\": \"2017-05-18T18:35:55Z\"     },     \"submissions\": { // The status as it pertains to the current user       \"excused\": false,       \"graded\": false,       \"late\": false,       \"missing\": true,       \"needs_grading\": false,       \"with_feedback\": false     },     \"plannable_id\": \"456\",     \"plannable_type\": \"assignment\",     \"plannable\": { ... assignment object ...  },     \"html_url\": \"http://canvas.instructure.com/courses/1/assignments/1#submit\"   },   {     \"visible_in_planner\": true,     \"planner_override\": null,     \"submissions\": false, // false if no associated assignment exists for the plannable item     \"plannable_id\": \"789\",     \"plannable_type\": \"planner_note\",     \"plannable\": {       \"id\": 1,       \"todo_date\": \"2017-05-30T06:00:00Z\",       \"title\": \"hello\",       \"details\": \"world\",       \"user_id\": 2,       \"course_id\": null,       \"workflow_state\": \"active\",       \"created_at\": \"2017-05-30T16:29:04Z\",       \"updated_at\": \"2017-05-30T16:29:15Z\"     },     \"html_url\": \"http://canvas.instructure.com/api/v1/planner_notes.1\"   } ]

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import planner_api
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = planner_api.PlannerApi(api_client)

    # example passing only optional values
    query_params = {
        'start_date': "1970-01-01",
        'end_date': "1970-01-01",
        'context_codes': [
        "context_codes_example"
    ],
        'filter': "new_activity",
    }
    try:
        # List planner items
        api_response = api_instance.list_planner_items(
            query_params=query_params,
        )
    except openapi_client.ApiException as e:
        print("Exception when calling PlannerApi->list_planner_items: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
start_date | StartDateSchema | | optional
end_date | EndDateSchema | | optional
context_codes | ContextCodesSchema | | optional
filter | FilterSchema | | optional


# StartDateSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str, date,  | str,  |  | value must conform to RFC-3339 full-date YYYY-MM-DD

# EndDateSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str, date,  | str,  |  | value must conform to RFC-3339 full-date YYYY-MM-DD

# ContextCodesSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# FilterSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | must be one of ["new_activity", ] 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_planner_items.ApiResponseFor200) | No response was specified

#### list_planner_items.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[] |  |
headers | Unset | headers were not defined |

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

