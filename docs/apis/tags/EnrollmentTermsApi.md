<a name="__pageTop"></a>
# openapi_client.apis.tags.enrollment_terms_api.EnrollmentTermsApi

All URIs are relative to *https://canvas.hw.ac.uk/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_enrollment_term**](#create_enrollment_term) | **post** /v1/accounts/{account_id}/terms | Create enrollment term
[**delete_enrollment_term**](#delete_enrollment_term) | **delete** /v1/accounts/{account_id}/terms/{id} | Delete enrollment term
[**list_enrollment_terms**](#list_enrollment_terms) | **get** /v1/accounts/{account_id}/terms | List enrollment terms
[**update_enrollment_term**](#update_enrollment_term) | **put** /v1/accounts/{account_id}/terms/{id} | Update enrollment term

# **create_enrollment_term**
<a name="create_enrollment_term"></a>
> EnrollmentTerm create_enrollment_term(account_id)

Create enrollment term

Create a new enrollment term for the specified account.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import enrollment_terms_api
from openapi_client.model.enrollment_term import EnrollmentTerm
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = enrollment_terms_api.EnrollmentTermsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    try:
        # Create enrollment term
        api_response = api_instance.create_enrollment_term(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EnrollmentTermsApi->create_enrollment_term: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': "account_id_example",
    }
    body = None
    try:
        # Create enrollment term
        api_response = api_instance.create_enrollment_term(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EnrollmentTermsApi->create_enrollment_term: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationXWwwFormUrlencoded, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/x-www-form-urlencoded' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationXWwwFormUrlencoded

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict, str, date, datetime, uuid.UUID, int, float, decimal.Decimal, bool, None, list, tuple, bytes, io.FileIO, io.BufferedReader,  | frozendict.frozendict, str, decimal.Decimal, BoolClass, NoneClass, tuple, bytes, FileIO |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**enrollment_term[end_at]** | str, datetime,  | str,  | The day/time the term ends. Accepts times in ISO 8601 format, e.g. 2015-01-10T18:48:00Z. | [optional] value must conform to RFC-3339 date-time
**enrollment_term[name]** | str,  | str,  | The name of the term. | [optional] 
**enrollment_term[overrides][enrollment_type][end_at]** | str, datetime,  | str,  | The day/time the term ends, overridden for the given enrollment type. *enrollment_type* can be one of StudentEnrollment, TeacherEnrollment, TaEnrollment, or DesignerEnrollment | [optional] value must conform to RFC-3339 date-time
**enrollment_term[overrides][enrollment_type][start_at]** | str, datetime,  | str,  | The day/time the term starts, overridden for the given enrollment type. *enrollment_type* can be one of StudentEnrollment, TeacherEnrollment, TaEnrollment, or DesignerEnrollment | [optional] value must conform to RFC-3339 date-time
**enrollment_term[sis_term_id]** | str,  | str,  | The unique SIS identifier for the term. | [optional] 
**enrollment_term[start_at]** | str, datetime,  | str,  | The day/time the term starts. Accepts times in ISO 8601 format, e.g. 2015-01-10T18:48:00Z. | [optional] value must conform to RFC-3339 date-time
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#create_enrollment_term.ApiResponseFor200) | No response was specified

#### create_enrollment_term.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**EnrollmentTerm**](../../models/EnrollmentTerm.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **delete_enrollment_term**
<a name="delete_enrollment_term"></a>
> EnrollmentTerm delete_enrollment_term(account_idid)

Delete enrollment term

Delete the specified enrollment term.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import enrollment_terms_api
from openapi_client.model.enrollment_term import EnrollmentTerm
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = enrollment_terms_api.EnrollmentTermsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'id': "id_example",
    }
    try:
        # Delete enrollment term
        api_response = api_instance.delete_enrollment_term(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EnrollmentTermsApi->delete_enrollment_term: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
id | IdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#delete_enrollment_term.ApiResponseFor200) | No response was specified

#### delete_enrollment_term.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**EnrollmentTerm**](../../models/EnrollmentTerm.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **list_enrollment_terms**
<a name="list_enrollment_terms"></a>
> [EnrollmentTerm] list_enrollment_terms(account_id)

List enrollment terms

A paginated list of all of the terms in the account.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import enrollment_terms_api
from openapi_client.model.enrollment_term import EnrollmentTerm
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = enrollment_terms_api.EnrollmentTermsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
    }
    query_params = {
    }
    try:
        # List enrollment terms
        api_response = api_instance.list_enrollment_terms(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EnrollmentTermsApi->list_enrollment_terms: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': "account_id_example",
    }
    query_params = {
        'workflow_state': [
        "active"
    ],
        'include': [
        "overrides"
    ],
    }
    try:
        # List enrollment terms
        api_response = api_instance.list_enrollment_terms(
            path_params=path_params,
            query_params=query_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EnrollmentTermsApi->list_enrollment_terms: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
query_params | RequestQueryParams | |
path_params | RequestPathParams | |
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### query_params
#### RequestQueryParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
workflow_state | WorkflowStateSchema | | optional
include | IncludeSchema | | optional


# WorkflowStateSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

# IncludeSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
items | str,  | str,  |  | 

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#list_enrollment_terms.ApiResponseFor200) | No response was specified

#### list_enrollment_terms.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
list, tuple,  | tuple,  |  | 

### Tuple Items
Class Name | Input Type | Accessed Type | Description | Notes
------------- | ------------- | ------------- | ------------- | -------------
[**EnrollmentTerm**]({{complexTypePrefix}}EnrollmentTerm.md) | [**EnrollmentTerm**]({{complexTypePrefix}}EnrollmentTerm.md) | [**EnrollmentTerm**]({{complexTypePrefix}}EnrollmentTerm.md) |  | 

### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

# **update_enrollment_term**
<a name="update_enrollment_term"></a>
> EnrollmentTerm update_enrollment_term(account_idid)

Update enrollment term

Update an existing enrollment term for the specified account.

### Example

* Bearer Authentication (bearerAuth):
```python
import openapi_client
from openapi_client.apis.tags import enrollment_terms_api
from openapi_client.model.enrollment_term import EnrollmentTerm
from pprint import pprint
# Defining the host is optional and defaults to https://canvas.hw.ac.uk/api
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "https://canvas.hw.ac.uk/api"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure Bearer authorization: bearerAuth
configuration = openapi_client.Configuration(
    access_token = 'YOUR_BEARER_TOKEN'
)
# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = enrollment_terms_api.EnrollmentTermsApi(api_client)

    # example passing only required values which don't have defaults set
    path_params = {
        'account_id': "account_id_example",
        'id': "id_example",
    }
    try:
        # Update enrollment term
        api_response = api_instance.update_enrollment_term(
            path_params=path_params,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EnrollmentTermsApi->update_enrollment_term: %s\n" % e)

    # example passing only optional values
    path_params = {
        'account_id': "account_id_example",
        'id': "id_example",
    }
    body = dict(
        enrollment_term_end_at="1970-01-01T00:00:00.00Z",
        enrollment_term_name="enrollment_term_name_example",
        enrollment_term_overrides_enrollment_type_end_at="1970-01-01T00:00:00.00Z",
        enrollment_term_overrides_enrollment_type_start_at="1970-01-01T00:00:00.00Z",
        enrollment_term_sis_term_id="enrollment_term_sis_term_id_example",
        enrollment_term_start_at="1970-01-01T00:00:00.00Z",
    )
    try:
        # Update enrollment term
        api_response = api_instance.update_enrollment_term(
            path_params=path_params,
            body=body,
        )
        pprint(api_response)
    except openapi_client.ApiException as e:
        print("Exception when calling EnrollmentTermsApi->update_enrollment_term: %s\n" % e)
```
### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
body | typing.Union[SchemaForRequestBodyApplicationJson, Unset] | optional, default is unset |
path_params | RequestPathParams | |
content_type | str | optional, default is 'application/json' | Selects the schema and serialization of the request body
accept_content_types | typing.Tuple[str] | default is ('application/json', ) | Tells the server the content type(s) that are accepted by the client
stream | bool | default is False | if True then the response.content will be streamed and loaded from a file like object. When downloading a file, set this to True to force the code to deserialize the content to a FileSchema file
timeout | typing.Optional[typing.Union[int, typing.Tuple]] | default is None | the timeout used by the rest client
skip_deserialization | bool | default is False | when True, headers and body will be unset and an instance of api_client.ApiResponseWithoutDeserialization will be returned

### body

# SchemaForRequestBodyApplicationJson

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
dict, frozendict.frozendict,  | frozendict.frozendict,  |  | 

### Dictionary Keys
Key | Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | ------------- | -------------
**enrollment_term[end_at]** | str, datetime,  | str,  | The day/time the term ends. Accepts times in ISO 8601 format, e.g. 2015-01-10T18:48:00Z. | [optional] value must conform to RFC-3339 date-time
**enrollment_term[name]** | str,  | str,  | The name of the term. | [optional] 
**enrollment_term[overrides][enrollment_type][end_at]** | str, datetime,  | str,  | The day/time the term ends, overridden for the given enrollment type. *enrollment_type* can be one of StudentEnrollment, TeacherEnrollment, TaEnrollment, or DesignerEnrollment | [optional] value must conform to RFC-3339 date-time
**enrollment_term[overrides][enrollment_type][start_at]** | str, datetime,  | str,  | The day/time the term starts, overridden for the given enrollment type. *enrollment_type* can be one of StudentEnrollment, TeacherEnrollment, TaEnrollment, or DesignerEnrollment | [optional] value must conform to RFC-3339 date-time
**enrollment_term[sis_term_id]** | str,  | str,  | The unique SIS identifier for the term. | [optional] 
**enrollment_term[start_at]** | str, datetime,  | str,  | The day/time the term starts. Accepts times in ISO 8601 format, e.g. 2015-01-10T18:48:00Z. | [optional] value must conform to RFC-3339 date-time
**any_string_name** | dict, frozendict.frozendict, str, date, datetime, int, float, bool, decimal.Decimal, None, list, tuple, bytes, io.FileIO, io.BufferedReader | frozendict.frozendict, str, BoolClass, decimal.Decimal, NoneClass, tuple, bytes, FileIO | any string name can be used but the value must be the correct type | [optional]

### path_params
#### RequestPathParams

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
account_id | AccountIdSchema | | 
id | IdSchema | | 

# AccountIdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

# IdSchema

## Model Type Info
Input Type | Accessed Type | Description | Notes
------------ | ------------- | ------------- | -------------
str,  | str,  |  | 

### Return Types, Responses

Code | Class | Description
------------- | ------------- | -------------
n/a | api_client.ApiResponseWithoutDeserialization | When skip_deserialization is True this response is returned
200 | [ApiResponseFor200](#update_enrollment_term.ApiResponseFor200) | No response was specified

#### update_enrollment_term.ApiResponseFor200
Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
response | urllib3.HTTPResponse | Raw response |
body | typing.Union[SchemaFor200ResponseBodyApplicationJson, ] |  |
headers | Unset | headers were not defined |

# SchemaFor200ResponseBodyApplicationJson
Type | Description  | Notes
------------- | ------------- | -------------
[**EnrollmentTerm**](../../models/EnrollmentTerm.md) |  | 


### Authorization

[bearerAuth](../../../README.md#bearerAuth)

[[Back to top]](#__pageTop) [[Back to API list]](../../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../../README.md#documentation-for-models) [[Back to README]](../../../README.md)

